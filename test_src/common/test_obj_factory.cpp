/*
 * =====================================================================================
 *
 *       Filename:  test_obj_factory.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/20/2015 03:19:55 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include <gtest/gtest.h>
#include "common/obj_factory.h"
#include <iostream>

using namespace bsp::common;
using namespace std;

namespace{
	
class Obj{
public:
	Obj(int _i) : i(_i){}
public:
	int i;
};

class ObjInit{
public:
	ObjInit(int _i) : i(_i){}
	Obj* init(void* ptr) const{
		return new(ptr) Obj(i);	
	}
public:
	int i;
};

}

TEST(ObjFactoryTest, BasicTest){
	vector<Obj*> objs;
	
	ObjFactory<Obj, ObjInit> factory;
	for(int i = 0;i < 100;++i){
		ObjInit oi(i);
		Obj* obj = factory.newObject(oi);	
		objs.push_back(obj);
	}

	factory.allocFin();

	int i = 0;
	for(vector<Obj*>::iterator vi = objs.begin();vi != objs.end();++vi){
		ASSERT_EQ((*vi)->i, i) << "i = " << i << endl;
		++i;	
	}

	factory.destroy(objs);

	ASSERT_EQ(true, true);
}
