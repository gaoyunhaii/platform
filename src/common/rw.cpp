/*
 * =====================================================================================
 *
 *       Filename:  serialize.cpp
 *
 *    Description:    
 *
 *        Version:  1.0
 *        Created:  2013年08月12日 19时09分01秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
extern "C"{
	#include <unistd.h> 
	#include <endian.h>
}
#include <cstring>
#include <cassert>


#include "common/rw.h"
#include "common/util.h"
//#include <iostream>
//using namespace std;

bsp::common::rw::LineReader::LineReader(InputStream* _input, bool _coe, int _buf_size):
		input(_input), buf_size(_buf_size), read_pos(0), avail(0), close_on_exit(_coe){
	buffer = new char[buf_size];
}

bsp::common::rw::LineReader::~LineReader(){
	if(close_on_exit && input){
		input->close();
		delete input;
		input = 0;
	}

	delete[] buffer;
}

ssize_t bsp::common::rw::LineReader::readline(string& dst, ssize_t max_len){
	//1. copy current buffer to dst			
	//2. while not enough, continue read until max_len occured.
	//3. only max_len - chars will be readed, the last char will left for \0
	//4. if read returns -1, then -1 will also be returned and copyed data will be ignored
	//5. returned line doesn't contain "\n"
	ssize_t readed = 0;	//use seperate variable in case buffer is not null initially
	char* pc;	
	bool limited = !(max_len <= 0);

	dst.clear();

	while(true){
		//check buf	
		for(pc = buffer + read_pos;pc < buffer + avail;++pc){
			if(*pc == '\n' || (limited && readed >= max_len)){
				//return 		
				if(*pc == '\n'){
					++read_pos;
					++readed;
					//if last char in buffer is \r, then remove this char
					if(readed >= 1 && dst[dst.size() - 1] == '\r'){
						dst.erase(dst.size() - 1);
						--readed; 
					}
				}
				return readed;			
			}		
			dst.append(1, *pc);
			++readed;
			++read_pos;
		}

		//refill the buffer
		//std::cout << "before read " << std::endl;
		avail = input->read(buffer, buf_size);
		if(avail < 0){
			if(errno != EINTR){
				return -1;
			}			
			else{
				avail = 0;
			}
		}
		else if(avail == 0){
			//EOF, return readed
			return readed;
		}
		else{
			read_pos = 0;
		}
	}		
	//we'll never get here;
	return -1;
}


bsp::common::rw::ObjectWriter::ObjectWriter(OutputStream* _out, bool _coe, int buf_size):
		output(_out), close_on_exit(_coe), closed(false){
	buf = new char[buf_size];
	end = buf + buf_size;
	h = t = buf;
}

bsp::common::rw::ObjectWriter::~ObjectWriter(){
	_do_close();
}

void bsp::common::rw::ObjectWriter::flush() throw(SocketCloseException, IOException){
	_assert_not_closed();

	_write_out();
	output->flush();
}

void bsp::common::rw::ObjectWriter::close() throw(SocketCloseException, IOException){
	_do_close();
}

void bsp::common::rw::ObjectWriter::_do_close(){
	if(!closed){
		flush();
		if(close_on_exit){
			output->flush();
			output->close();
			delete output;
			output = 0;	
		}

		delete[] buf;
		buf = NULL;
		closed = true;
	}
}

size_t bsp::common::rw::ObjectWriter::writeInt(int i) throw(SocketCloseException, IOException){
	_assert_not_closed();

	uint32_t send = htobe32(*((uint32_t*)&i));
	_write(&send, sizeof(uint32_t));
	return sizeof(uint32_t);
}

size_t bsp::common::rw::ObjectWriter::writeLongLong(long long l) throw(SocketCloseException, IOException){
	_assert_not_closed();

	uint64_t send = htobe64(*((uint64_t*)&l));
	_write(&send, sizeof(uint64_t));
	return sizeof(uint64_t);
}

size_t bsp::common::rw::ObjectWriter::writeDouble(double d) throw(SocketCloseException, IOException){
	_assert_not_closed();

	uint64_t send = htobe64(*((uint64_t*)&d));
	_write(&send, sizeof(uint64_t));
	return sizeof(uint64_t);
}

size_t bsp::common::rw::ObjectWriter::writeString(const char* c, ssize_t size) throw(SocketCloseException, IOException){
	_assert_not_closed();

	if(size < 0){
		size = strlen(c);
	}

	size_t size_of_size = writeLongLong((long long)size);
	_write(c, size);
	size_of_size += size;
	return size_of_size;
}

size_t bsp::common::rw::ObjectWriter::writeOutputString(const char* c, ssize_t size) throw(SocketCloseException, IOException){
	_assert_not_closed();

	if(size < 0){
		size = strlen(c);
	}
	
	_write(c, size);
	return (size_t)(size);
}

size_t bsp::common::rw::ObjectWriter::writeRaw(const char* c, ssize_t size) throw(SocketCloseException, IOException){
	_assert_not_closed();

	_write(c, size);
	return (size_t)size;
}

void bsp::common::rw::ObjectWriter::_write(const void* tbuf, size_t size) throw(SocketCloseException, IOException){
	if (size <= 0){
		//c-style str
		return;
	}

	const char* t_start = (const char*)tbuf;			
	const char* t_end = t_start + size;
	size_t re_size = size;
	
	do{
		size_t remain = end - t;
		if(remain <= 0){
			_write_out();	
			remain = end - t;
		}

		//now remain > 0
		size_t to_write = remain;
		if(re_size < to_write){
			to_write = re_size;
		}

		//cout << "to_write: " << to_write << endl;
		
		memcpy(t, t_start, to_write);
		t_start += to_write;
		t += to_write;
		re_size -= to_write;

	} while(t_start < t_end);
}

void bsp::common::rw::ObjectWriter::_write_out() throw(SocketCloseException, IOException){
	if(h >= t){
		return;
	}

	char* start = h;	
	do{
		//cout << "write out : " << (start - buf) << ":" << (t - buf) << endl;
		//ssize_t writed = ::write(fd, start, t - start);
		ssize_t writed = output->write(start, t -start);
		if (writed < 0){
			throw IOException();	
		}
		start += writed;
	} while(start < t);

	//now buf is empty, reset h and t to the head of buffer;
	h = t = buf;
}


bsp::common::rw::ObjectReader::ObjectReader(InputStream* _input, bool _coe, int buf_size):
		input(_input), close_on_exit(_coe), closed(false){
	if(buf_size > 0){
		buf = new char[buf_size];
		end = buf + buf_size;
		h = t = buf;
		disabled_buf = false;
	}
	else{
		buf = NULL; //Disable buffer
		end = h = t = NULL; //reset every pointer, they won't be used
		disabled_buf = true;
	}

}

bsp::common::rw::ObjectReader::~ObjectReader(){
	_do_close();
}

void bsp::common::rw::ObjectReader::close(){
	_do_close();
}

void bsp::common::rw::ObjectReader::_do_close(){
	if(!closed){
		if(close_on_exit){
			input->close();
			delete input;
			input = 0;
		}

		//OK even if buf == NULL
		delete[] buf;
		buf = NULL;
		
		closed = true;
	}			
}

int bsp::common::rw::ObjectReader::readInt() throw(SocketCloseException, IOException){
	_assert_not_closed();
	
	uint32_t re;
	_read(&re, sizeof(uint32_t));
	re = be32toh(re);
	int ri = *((int*)&re);

	//printf("readed value = %d\n", ri);

	return ri;
}

long long bsp::common::rw::ObjectReader::readLongLong() throw(SocketCloseException, IOException){
	_assert_not_closed();

	uint64_t re;
	_read(&re, sizeof(uint64_t));
	re = be64toh(re);
	long long rl = *((long long*)&re);
	return rl;
}

double bsp::common::rw::ObjectReader::readDouble() throw(SocketCloseException, IOException){
	_assert_not_closed();

	uint64_t re;
	_read(&re, sizeof(uint64_t));
	re = be64toh(re);
	double rl = *((double*)&re);
	return rl;
}

void bsp::common::rw::ObjectReader::readString(string& re) throw(SocketCloseException, IOException){
	_assert_not_closed();

	re.clear();
	size_t size = (size_t)readLongLong();
	_read(&re, size, false);
}

/* *
 * if copy == ture , use memcpy
 * if copy == false & tbuf instance of string*, use += 
 * else: undefined
 * */
void bsp::common::rw::ObjectReader::_read(void* tbuf, size_t size, bool copy) throw(SocketCloseException, IOException){
	if(size <= 0){
		return;
	}

	//If buf is disabled, read directly
	if(disabled_buf){
		char tmp_buf[size + 1]; //+1 for safety
		void* buf = NULL;
		if(copy){
			buf = tbuf;
		}
		else{
			buf = tmp_buf;	
		}

		size_t readed = 0;
		while(readed < size){
			ssize_t re = input->read(buf + readed, size - readed);
			if(re == 0){
				throw SocketCloseException();
			}
			else if(re < 0){
				throw IOException();
			}

			readed += re;
		}

		if(!copy){
			((string*)tbuf)->append(tmp_buf, tmp_buf + size);
		}	

		return;
	}

	size_t re_size = size;
	char* tbuf_start = (char*)tbuf;

	do{
		size_t remain = t - h;
		if(remain <= 0){
			_read_in();	
			remain = t - h;	
		}
		size_t to_read = remain;
		if(re_size < to_read){
			to_read = re_size;
		}
		
		//TODO: ugly design 
		if(copy){
			memcpy(tbuf_start + size - re_size, h, to_read);
		}
		else{
			((string*)tbuf)->append(h, h + to_read);
		}

		re_size -= to_read;
		h += to_read;

	} while(re_size > 0);
			
}

void bsp::common::rw::ObjectReader::_read_in() throw(SocketCloseException, IOException){
	//If h == t, reset h and t to head
	if(h == t){
		h = t = buf;
	}

	size_t remain = end - t; //remain can't be equal to end 
	//cout << "remain " << remain << endl;
	//ssize_t readed = ::read(fd, t, remain);
	ssize_t readed = input->read(t, remain);
	//cout << "readed : " << readed << " : " << (t - buf) << ":" << endl;
	if(readed == 0){
		throw SocketCloseException();
	}
	else if(readed <= 0){
		perror("readed <= 0");
		throw IOException();
	}
	else{
		t += readed;
	}
}
