/*
 * =====================================================================================
 *
 *       Filename:  blocking.cpp
 *
 *    Description:  Implementation of blocking module
 *
 *        Version:  1.0
 *        Created:  2013年03月13日 23时18分16秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  BLOCKING_CPP_INC
#define  BLOCKING_CPP_INC

extern "C"{
	#include <pthread.h>
}

#include "common/blocking.h"
#include "common/atomic.h"

using bsp::atomic::MutexGetLock;


namespace bsp{namespace blocking{

	template <class T>
	struct Node{
		Node(const T& _t, Node<T>* _n):t(_t), next(_n){}
	public:
		T t;
		Node<T>* next;
	};

}}

template <class T>
bsp::blocking::SyncQueue<T>::SyncQueue()
	:head(NULL), tail(NULL), size(0){
	pthread_mutex_init(&lock, NULL);		
}


/* 
 * 使用对像内部的锁是不可能保证对像正确析构的，反而有可能造成死锁
 * 我们将保证析构一个对像时不会有其它线程调用其它类方法的责任交给调用者
 * */

template <class T>
bsp::blocking::SyncQueue<T>::~SyncQueue(){
	Node<T>* tmp;
	tmp = head;
	while(tmp != NULL){
		head = tmp->next;
		delete tmp;	
		tmp = head;
	}	
	
	head = tail = NULL;

	pthread_mutex_destroy(&lock);
}

template <class T>
void bsp::blocking::SyncQueue<T>::enqueue(T& t){
	MutexGetLock get_lock(&lock);
	
	Node<T>* new_t = new Node<T>(t, NULL);
	
	if(tail != NULL){
		tail->next = new_t;	
	}

	tail = new_t;

	if(head == NULL){
		head = new_t;
	}

	size += 1;	
}

template <class T>
T bsp::blocking::SyncQueue<T>::dequeue(){
	MutexGetLock get_lock(&lock);
	
	if(head == NULL){
		throw EmptyException();	
	}
	else{
		T t = head->t;
		Node<T>* old_head = head;
		head = head->next;
		
		/* If head is NULL, this list is already null */
		if(head == NULL){
			tail = NULL;
		}
		
		size -= 1;

		delete old_head;
			
		return t;
	}
}

template <class T>
int bsp::blocking::SyncQueue<T>::getSize(){
	return size;
}

template <class T>
bsp::blocking::BlockingQueue<T>::BlockingQueue()
	:head(NULL), tail(NULL), size(0){
	pthread_mutex_init(&this->lock, NULL);
	pthread_cond_init(&this->not_empty_cond, NULL);
}

template <class T>
bsp::blocking::BlockingQueue<T>::~BlockingQueue(){
	Node<T>* tmp;
	
	tmp = head;
	while(tmp != NULL){
		head = tmp->next;
		delete tmp;	
		tmp = head;
	}	
	
	head = tail = NULL;

	pthread_cond_destroy(&not_empty_cond);


	pthread_mutex_destroy(&lock);
}

template <class T>
void bsp::blocking::BlockingQueue<T>::enqueue(T& t){
	MutexGetLock get_lock(&lock);

	Node<T>* new_t = new Node<T>(t, NULL);
	
	if(tail != NULL){
		tail->next = new_t;	
	}

	tail = new_t;

	if(head == NULL){
		head = new_t;
	}

	size += 1;	
	pthread_cond_signal(&not_empty_cond);
}

template <class T>
T bsp::blocking::BlockingQueue<T>::dequeue(){
	MutexGetLock get_lock(&lock);
	
	while(head == NULL){
		pthread_cond_wait(&not_empty_cond, &lock);
	}

	T t(head->t);
	Node<T>* old_head = head;
	head = head->next;
	
	/* If head is NULL, this list is already null */
	if(head == NULL){
		tail = NULL;
	}
	
	size -= 1;

	delete old_head;
		
	return t;
}



template <class T>
bsp::blocking::EmptyWaitingBlockingQueue<T>::EmptyWaitingBlockingQueue()
	:head(NULL), tail(NULL), 
	lock(PTHREAD_MUTEX_INITIALIZER),
	not_empty_cond(PTHREAD_COND_INITIALIZER),
	empty_cond(PTHREAD_COND_INITIALIZER), size(0),
	poper_size(0x7fffffff), blocker_size(0){}

template <class T>
bsp::blocking::EmptyWaitingBlockingQueue<T>::~EmptyWaitingBlockingQueue(){
	Node<T>* tmp;
	
	tmp = head;
	while(tmp != NULL){
		head = tmp->next;
		delete tmp;	
		tmp = head;
	}	
	
	head = tail = NULL;

	pthread_cond_destroy(&not_empty_cond);

	pthread_mutex_destroy(&lock);
}


template <class T>
void bsp::blocking::EmptyWaitingBlockingQueue<T>::enqueue(T& t){
	MutexGetLock get_lock(&lock);

	Node<T>* new_t = new Node<T>(t, NULL);
	
	if(tail != NULL){
		tail->next = new_t;	
	}

	tail = new_t;

	if(head == NULL){
		head = new_t;
	}

	size += 1;	

	//printf("size = %d\n", size);

	pthread_cond_signal(&not_empty_cond);
}

template <class T>
T bsp::blocking::EmptyWaitingBlockingQueue<T>::dequeue(){
	MutexGetLock get_lock(&lock);	
	//printf("deque\n");
	while(head == NULL){
		++blocker_size;

		//printf("b = %d, p = %d\n", blocker_size, poper_size);
		if(blocker_size >= poper_size){
			//printf("wake up\n");
			pthread_cond_signal(&empty_cond);	
		}

		pthread_cond_wait(&not_empty_cond, &lock);
		--blocker_size;
	}

	T t(head->t);
	Node<T>* old_head = head;
	head = head->next;
	
	/* If head is NULL, this list is already null */
	if(head == NULL){
		tail = NULL;
	}
	
	size -= 1;
	delete old_head;

	//printf("size = %d\n", size);
	//If head is NULL, wake up all waiting threads
	return t;
}

template <class T>
void bsp::blocking::EmptyWaitingBlockingQueue<T>::waitForEmpty(int p_size){
	MutexGetLock get_lock(&lock);

	/**
	 * Only when the queue are empty, and all threads have blocked for new elements,
	 * All previous elements are all handled and we can leave now
	 * */
	if(!head && blocker_size >= p_size){
		return;
	}

	poper_size = p_size;

	pthread_cond_wait(&empty_cond, &lock);
}


template <class T>
bsp::blocking::MultiEmptyWaitingBlockingQueue<T>::MultiEmptyWaitingBlockingQueue(int size){
	
}

template <class T>
bsp::blocking::MultiEmptyWaitingBlockingQueue<T>::~MultiEmptyWaitingBlockingQueue(){

}

template <class T>
void bsp::blocking::MultiEmptyWaitingBlockingQueue<T>::enqueue(T& t, int id){

}

template <class T>
T bsp::blocking::MultiEmptyWaitingBlockingQueue<T>::dequeue(int id){

}

template <class T>
int bsp::blocking::MultiEmptyWaitingBlockingQueue<T>::getSize(){
	return 0;
}

template <class T>
void bsp::blocking::MultiEmptyWaitingBlockingQueue<T>::waitForEmpty(int p_size, int id){

}


#endif   /* ----- #ifndef BLOCKING_CPP_INC  ----- */
