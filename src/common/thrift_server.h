/*
 * =====================================================================================
 *
 *       Filename:  thrift_server.h
 *
 *    Description:  Enculpation of thrift server
 *
 *        Version:  1.0
 *        Created:  2013年07月18日 11时13分54秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef  THRIFT_SERVER_INC
#define  THRIFT_SERVER_INC

#include <thrift/server/TNonblockingServer.h>
#include <thrift/concurrency/ThreadManager.h>
#include <thrift/transport/TTransportUtils.h>
#include "common/dthread.h"

using apache::thrift::server::TNonblockingServer;
using apache::thrift::concurrency::ThreadManager;
using apache::thrift::transport::TTransport;
using apache::thrift::TException;
using bsp::multi_thread::DaemonThread;

namespace bsp{
	namespace common{
		template <class HANDLER, class PROCESSOR>
		class ThriftServer{
		public:
			ThriftServer(int _port, HANDLER* handler, int workerCount = 10);
			void start();
			void stop();
			/*
			 * @Deprecated
			 * */
			void* join();
			~ThriftServer();
		private:
			int port;
			int worker_count;
			boost::shared_ptr<ThreadManager> thread_manager;
			TNonblockingServer* server;
			DaemonThread* main_thread;

			//used to notify master thread that we have started
			pthread_mutex_t lock;
			pthread_cond_t started_cond;
			bool started;
		};


		template <class CLIENT>
		class ThriftClient{
		public:
			ThriftClient(const char* addr, int port);
			~ThriftClient();
			void open() throw(TException);
			void close() throw(TException);
			CLIENT* getClient(){return client;};
		private:
			boost::shared_ptr<TTransport> transport;
			CLIENT* client;
		};
	}
}

#endif   /* ----- #ifndef THRIFT_SERVER_INC  ----- */
