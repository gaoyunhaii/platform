/*
 * =====================================================================================
 *
 *       Filename:  atomic.h
 *
 *    Description:  Atomic classes
 *
 *        Version:  1.0
 *        Created:  2013年03月13日 16时32分38秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:
 *
 * =====================================================================================
 */

#ifndef  ATOMIC_INC
#define  ATOMIC_INC

extern "C"{
	#include <pthread.h>
}
#include <cassert>

#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <cstring>

namespace bsp{

	/**
	 * \brief bsp::atomic provides tools and implementations of ATOMIC operations
	 */
	namespace atomic{
		template <class T>
		inline void atomic_set_pointer(T** from, T* to){
			assert(sizeof(long) == sizeof(void*));
			__sync_fetch_and_add((long*)from, (long)to - (long)(*from));
		}

		template <class T>
		inline bool atomic_compare_and_swap(T* mem, const T old_value, const T new_value){
			return __sync_bool_compare_and_swap(mem, old_value, new_value);
		}

		template <class T>
		inline void atomic_incre(T* from, T value){
			__sync_fetch_and_add(from, value);
		}

		template <class T>
		inline T atomic_incre_re(T* from, T value){
			return __sync_fetch_and_add(from, value);
		}

		template <class T>
		inline T atomic_incre_re_after(T* from, T value){
			return __sync_add_and_fetch(from, value);
		}

		template <class T>
		inline void atomic_set(T* from, T to){
			T diff = to - *from;
			__sync_fetch_and_add(from, diff);
		}

		class MutexGetLock{
		public:
			MutexGetLock(pthread_mutex_t*);
			~MutexGetLock();
		private:
			pthread_mutex_t* lock;
		};

		class MutexTryLock{
		public:
			MutexTryLock(pthread_mutex_t* _lock);
			~MutexTryLock();
			bool isLocked(){
				return this->lock_re == 0;
			}
		private:
			int lock_re;
			pthread_mutex_t* lock;
		};

		class RWLockGetRead{
		public:
			RWLockGetRead(pthread_rwlock_t*);
			~RWLockGetRead();
		private:
			pthread_rwlock_t* lock;
		};

		class RWLockGetWrite{
		public:
			RWLockGetWrite(pthread_rwlock_t*);
			~RWLockGetWrite();
		private:
			pthread_rwlock_t* lock;

		};

		class CountDownWaiter{
		public:
			CountDownWaiter();
			void reset(int value);
			void countDown(bool wait_on_notify = true);
			void wait();
			int getValue(){return value;}
			void ask_notifier_to_exit();
		private:
			int value;
			bool in_progress;
			pthread_mutex_t lock;
			pthread_cond_t count_cond;
			pthread_cond_t start_cond;
		};


		/**
		 * Wait till a number is acquired
		 * */
		//class CountWaiter{
		//public:
		//	CountWaiter();
		//	void addValue(int);
		//	void isWillWait(int);
		//	void wait(int, CountDownWaiter* count_down);
		//	void clear();
		//	void restart();
		//	int getValue(){return value;}
		//private:
		//	int value;
		//	int aim_value;
		//	bool some_is_waiting;

		//	pthread_mutex_t lock;
		//	pthread_cond_t count_cond;
		//	pthread_cond_t start_cond;
		//};



		/**
		 * \brief A specialized class to coordinate  a single message controller and a single executor
		 * We have tried several general synchronizer to achieve this function,
		 * however, many concurrency problems emerged.
		 * Therefore, we prefer to a specially designed tool at this time
		 * */
		class MessageExecutionCoor{
		public:
			MessageExecutionCoor();
			void initMaxControlMess(int _mcm);
			void newTurn();

			void controlMessageReceived(int exited, int _data_mess);
			void dataMessageReceived();

			void waitForAllMessages();

			int getControlReceived(){
				return control_mess_received;
			}

			int getDataReceived(){
				return data_mess_received;
			}
		private:
			void _test_turn_end();
		private:
			int max_control_mess;
			int max_data_mess;
			int control_mess_received;
			int data_mess_received;

			int control_mess_to_decre;

			bool executor_is_waiting;

			pthread_mutex_t lock;
			pthread_cond_t recv_can_run;
			pthread_cond_t executor_can_run;
		};



		//starts here
		class ConcurrentBitmap{
		public:
			ConcurrentBitmap(size_t _s) : size(_s){
				size_t length = size / sizeof(uint8_t);
				size_t remain = size % sizeof(uint8_t);

				if(remain){
					++length;
				}

				buffer = (uint8_t*)malloc(sizeof(uint8_t) * length);
				memset(buffer, 0, sizeof(uint8_t) * length);
			}

			void setBit(size_t i){
				size_t index = i / sizeof(uint8_t);
				int offset = i % sizeof(uint8_t);

				uint8_t to_or = 1 << offset;
				__sync_fetch_and_or(buffer + index, to_or);
			}

			void clearBit(int i){
				size_t index = i / sizeof(uint8_t);
				int offset = i % sizeof(uint8_t);

				uint8_t to_and = ~(1 << offset);
				__sync_fetch_and_and(buffer + index, to_and);
			}

			int getBit(int i){
				size_t index = i / sizeof(uint8_t);
				int offset = i % sizeof(uint8_t);

				return (buffer[index] & (1 << offset)) >> offset;
			}

			void clearAll(){
				size_t length = size / sizeof(uint8_t);
				size_t remain = size % sizeof(uint8_t);

				if(remain){
					++length;
				}

				memset(buffer, 0, sizeof(uint8_t) * length);
			}
		private:
			size_t size;
			uint8_t* buffer;
		};

		class CountDownLatch{
		public:
			CountDownLatch():value(0), lock(PTHREAD_MUTEX_INITIALIZER), all_finished(PTHREAD_COND_INITIALIZER){}

			void setValue(int _v){
				MutexGetLock get_lock(&lock);
				value = _v;
			}

			void wait(){
				MutexGetLock get_lock(&lock);
				//printf("%x on wait find value = %d\n", &value, value);
				if(value == 0){
					return;
				}

				pthread_cond_wait(&all_finished, &lock);
			}

			void countDown(){
				MutexGetLock get_lock(&lock);
				--value;
				//printf("%x on count down value = %d\n", &value, value);

				if(value == 0){
					pthread_cond_broadcast(&all_finished);
				}
			}
		private:
			int value;
			pthread_mutex_t lock;
			pthread_cond_t all_finished;
		};


//        class ThreadCoordinator {
//        public:
//            ThreadCoordinator() :
//                    finished(true), lock(PTHREAD_MUTEX_INITIALIZER), cond_finished(PTHREAD_COND_INITIALIZER) { }
//
//            void reset() {
//                finished = false;
//            }
//
//            void startNewTurn() {
//                MutexGetLock get_lock(&lock);
//                finished = false;
//            }
//
//            void waitTillFinished() {
//                MutexGetLock get_lock(&lock);
//
//                if (finished) {
//                    return;
//                }
//
//                pthread_cond_wait(&cond_finished, &lock);
//            }
//
//            void onFinished() {
//                MutexGetLock get_lock(&lock);
//                pthread_cond_broadcast(&cond_finished);
//            }
//
//        private:
//            bool finished;
//            pthread_mutex_t lock;
//            pthread_cond_t cond_finished;
//        };

		class TurnedSynchronizer{
		public:
			TurnedSynchronizer() : stopped(false){
				turn_control.setValue(1);
				task_control.setValue(1);
			}

			void waitForTurnStart(){
				turn_control.wait();
				turn_control.setValue(1);
			}

			void waitForMyTask(){
				task_control.wait();
				task_control.setValue(1);
			}

			void notifyForMyTaskFinished(){
				task_control.countDown();
			}

			void notifyForTurnStarted(){
				turn_control.countDown();
			}

			bool isStopped(){
				return stopped;
			}

			void markStop(){
				atomic_set<bool>(&stopped, true);
			}
		private:
			CountDownLatch turn_control;
			CountDownLatch task_control;

			bool stopped;
		};
	}
}


#endif   /* ----- #ifndef ATOMIC_INC  ----- */
