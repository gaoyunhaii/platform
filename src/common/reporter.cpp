/*
 * =====================================================================================
 *
 *       Filename:  reporter.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  09/15/2013 03:57:37 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include <cstdlib>
#include "common/reporter.h"
#include "common/atomic.h"
using namespace bsp::common::task;
using namespace bsp::atomic;

namespace bsp{namespace common{namespace task{

	class RMBNode{
	public:
		RMBNode():prev(NULL), next(NULL){}
		friend class ReportMsgBuffer;
	private:
		RMBNode* prev;
		RMBNode* next;
		string msg;
	};

}}}


bsp::common::task::ReportMsgBuffer::ReportMsgBuffer():
		header(NULL), write_pointer(NULL), read_pointer(NULL){
	//create the first node			
	RMBNode* node = new RMBNode();
	header = node;
	write_pointer = node;
}

void bsp::common::task::ReportMsgBuffer::addLine(const char* msg){
	write_pointer->msg = msg;

	//add a new node
	RMBNode* node = new RMBNode();
	node->prev = write_pointer;
	write_pointer->next = node;

	//FIXME: this operation must be atomic
	//write_pointer = node;
	
	atomic_incre<int>(&size, 1);
	atomic_set_pointer<RMBNode>(&write_pointer, node);
}

void bsp::common::task::ReportMsgBuffer::startIter(){
	read_pointer = header;
	RMBNode* last_write = write_pointer; //assert following write_pointer is the same
	header = last_write;

	read_end = last_write->prev;
	//since write_pointer == node, this means node.prev has been setted
	last_write->prev = NULL;
	if(read_end){
		read_end->next = NULL;
	}
}

bool bsp::common::task::ReportMsgBuffer::nextLine(string& re){
	if(!read_end || !read_pointer){
		return false;	
	}

	re = read_pointer->msg;			
	RMBNode* next = read_pointer->next;
	delete read_pointer;
	read_pointer = next;

	//FIXME: Is this logic reasonable? 
	atomic_incre(&size, -1);

	return true;
}

void bsp::common::task::ReportMsgBuffer::endIter(){
	//nop	
}
