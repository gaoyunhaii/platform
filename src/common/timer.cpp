/*
 * =====================================================================================
 *
 *       Filename:  timer.cpp
 *
 *    Description:  A timer thread
 *
 *        Version:  1.0
 *        Created:  2013年07月23日 13时10分20秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
extern "C"{
	#include <time.h>
}
#include "common/timer.h"
#include <iostream>
using std::endl;

void bsp::common::TimerRunnable::run(){
	int re;
	struct timespec aim, remain;		

	while(true){//This thread will be stopped by pthread_cancel, thus there's no need to maintain its state
		aim.tv_sec = interval.tv_sec;
		aim.tv_nsec = interval.tv_nsec;

		while((re = nanosleep(&aim, &remain)) < 0){
			aim.tv_sec = remain.tv_sec;
			aim.tv_nsec = remain.tv_nsec;
		}

		//take action
		timer_action();
	}
}
