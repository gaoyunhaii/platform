/*
 * =====================================================================================
 *
 *       Filename:  thread_pool.h
 *
 *    Description:  An implementation of thread pools
 *
 *        Version:  1.0
 *        Created:  2013年03月10日 23时14分10秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  THREAD_POOL_INC
#define  THREAD_POOL_INC

#include "common/blocking.h"
#include "common/dthread.h"
#include <unordered_map>
extern "C"{
	#include <pthread.h>
}

using std::unordered_map;
using bsp::blocking::BlockingQueue;

namespace bsp{
	namespace multi_thread{

		//@thread safe;
		class ThreadPool{
		public:
			ThreadPool(int core_size, int max_size, BlockingQueue<Runnable*>* queue = 0);
			void submit(Runnable* r);
			void start();
			void shutdown();
			void shutdownImmidiately();
			void shutdownAbruptly();
			~ThreadPool();
			friend class WorkerExiter;
			friend class WorkerRunnable;
		private:
			enum POOL_STATE{
				INIT, RUNNING, SHUTDOWN, SHUTDOWN_IMMI, ABRUPT, STOPPED
			};

			int core_size;
			int max_size;
			
			BlockingQueue<Runnable*>* queue;
			int current_size;
			unordered_map<int, DaemonThread*>* workers;
			
			int next_wid;

			//state
			POOL_STATE state;
			pthread_mutex_t main_lock; //guard state
			pthread_cond_t term_cond;
			

			//exit
			void onWorkerExit(int wid);
			DaemonThread* addThread();
		};
	}
}
#endif   /* ----- #ifndef THREAD_POOL_INC  ----- */
