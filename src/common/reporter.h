/*
 * =====================================================================================
 *
 *       Filename:  reporter.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  09/15/2013 03:57:29 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  REPORTER_INC
#define  REPORTER_INC

#include <unordered_map>
#include <string>
#include <cstring>
using std::unordered_map;
using std::string;

namespace bsp{namespace common{namespace task{

	class RMBNode;

	/* 
	 * This is a specific queue used to manage messages.
	 * The queue performs normally only if there's one reader and one writer.
	 * As for message submitting, the reader is heartbeat thread and the writer is the 
	 * task manager thread.
	 *
	 * TODO: we don't make this class a template for simplificity and optimization.
	 **/
	class ReportMsgBuffer{
	public:
		ReportMsgBuffer();
		void addLine(const char* line);
		void startIter();	
		bool nextLine(string& re);
		void endIter();
		int getSize(){
			return size;
		}
	private:
		RMBNode* header;
		RMBNode* write_pointer;
		RMBNode* read_pointer;
		RMBNode* read_end;

		int size;
	private:
		void _add_node();
	};


}}}

#endif   /* ----- #ifndef REPORTER_INC  ----- */

