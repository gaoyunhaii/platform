/*
 * =====================================================================================
 *
 *       Filename:  timer.h
 *
 *    Description:  A timer thread
 *
 *        Version:  1.0
 *        Created:  2013年07月23日 13时10分09秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  TIMER_INC
#define  TIMER_INC

#include "common/dthread.h"
using bsp::multi_thread::Runnable;

namespace bsp{
	namespace common{
		class TimerRunnable : public bsp::multi_thread::Runnable{
		public:
			TimerRunnable(int _milli_interval){
				interval.tv_sec = _milli_interval / 1000;	
				interval.tv_nsec = (_milli_interval % 1000) * 1000000;
			}
			void run();
			virtual void timer_action() = 0;
		private:
			struct timespec interval;
		};
	}
}

#endif   /* ----- #ifndef TIMER_INC  ----- */
