/*
 * =====================================================================================
 *
 *       Filename:  thrift_server.cpp
 *
 *    Description:  implementation of non-template method in thrift_server.h
 *
 *        Version:  1.0
 *        Created:  2013年07月24日 19时57分34秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
extern "C"{
	#include <pthread.h>
}
#include "common/thrift_server.h"
#include "common/thrift_server.hpp"

void bsp::common::ThriftEventHandler::preServe(){
	MutexGetLock get_lock(lock);
	*started = true;
	pthread_cond_broadcast(cond);
}

