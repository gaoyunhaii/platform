/*
 * =====================================================================================
 *
 *       Filename:  util.cpp
 *
 *    Description:  A list of utility functions
 *
 *        Version:  1.0
 *        Created:  2013年03月09日 21时27分59秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
extern "C"{
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <unistd.h>
	#include <signal.h>
	#include <fcntl.h>
	#include <netdb.h>
	#include <sys/socket.h>
	#include <arpa/inet.h>
	#include <errno.h>
	#include <time.h>
	#include <cstdio>
	#include <execinfo.h>
	#include <dirent.h>
}

#include "common/util.h"
#include "common/sexception.h"
#include <vector>
#include <string>
#include <cstdlib>
#include <iostream>
#include <cstdlib>
#include <sstream>

using std::vector;
using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::ostringstream;

namespace{
	const char HEX_CHARS[] = {
		'0', '1', '2', '3', '4', '5', '6', '7', '8',
		'9', 'A', 'B', 'C', 'D', 'E', 'F'
	};
	bool g_rand_inited = false;
}

void bsp::common::str_strip(const char* str, char ch, const char** start, const char** end){
	const char* s, *e;
	for(s = str;*s && *s == ch;++s);
	if(!*s){
		*start = *end = 0;
		return;
	}
	for(e = s;*e;++e);
	for(e = e - 1; e > s && *e == ch; --e);
	*start = s;
	*end = e + 1;
}

void bsp::common::str_tokenize(const char* str, char ch, vector<string>& result){
	const char* s, *e;	
	s = str;

	result.clear();

	while(true){
		for(e = s;*e && *e != ch;++e);
		result.push_back(string(s, e));							
		if(!*e || !*(e + 1)){
			break;	
		}
		s = e + 1;
	}
}

void bsp::common::str_tokenize_multi(const char* str, char ch, vector<string>& result){
	const char* s, *e;	

	result.clear();

	for(s = str;*s && *s == ch;++s);
	while(true){
		for(e = s;*e && *e != ch;++e);

		if(e > s){
			result.push_back(string(s, e));							
		}

		if(!*e || !*(e + 1)){
			break;	
		}

		for(s = e + 1;*s && *s == ch;++s);
	}
}


void bsp::common::str_getline(const char* str, const char** end, const char** next){
	const char* s = str;
	if(!(*s)){
		*end = NULL;
		*next = NULL;
		return;
	}				

	for(;*s && *s != '\n';++s);
	// *s == '\n' or *s == 0
	if(s == str || (s == str + 1 && *str == '\r')){//empty line
		*end = NULL;
	}

	if(*(s - 1) == '\r'){
		*end = s - 1;
	}
	else{
		*end = s;
	}

	*next = s + 1;
} 

void bsp::common::daemonize(const char* pid_fn, const char* out_file) throw(OSException){
	pid_t pid;
	umask(0);	
	if((pid = fork()) < 0){
		throw OSException();
	}	
	else if(pid != 0){
		exit(0); //parent
	}

	print_pid(pid_fn);	
	setsid();
	
	reg_signal(SIGHUP, SIG_IGN);

	if(chdir("/") < 0){
		throw OSException();
	}

	//close(STDOUT_FILENO);
	//close(STDERR_FILENO);
	int fd = open(out_file, O_RDWR | O_CREAT | O_APPEND, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
	if(fd < 0){
		throw OSException();
	}
	dup2(fd, STDOUT_FILENO);
	dup2(fd, STDERR_FILENO);
}

void bsp::common::reg_signal(int signo, void (*handler)(int)) throw (OSException){
	struct sigaction sa;
	sa.sa_handler = handler;	
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	if(sigaction(signo, &sa, NULL) < 0){
		throw OSException();
	}
}

int bsp::common::get_ip_by_hostname(const char* hostname, string& ret_ip){
	int rc, err;	
	char* str_host = 0;
	struct hostent hbuf;
	struct hostent* result;
	int size = 1024;
	char dst[1024];
	
	str_host = (char*)malloc(size);
	while((rc = gethostbyname_r(hostname, &hbuf, str_host, size, &result, &err)) == ERANGE){
		size *= 2;
		str_host = (char*)realloc(str_host, size);	
	}

	if(result != NULL){
		inet_ntop(hbuf.h_addrtype, hbuf.h_addr_list[0], dst, 1024);				
		ret_ip = dst;
		free(str_host);		
		return 0;
	}
	else{
		free(str_host);
		return -1;
	}
	return -1;
}


void bsp::common::get_time_hex_str(string& result){
	time_t ct = time(NULL);
	struct tm ctm;
	char buf[20];
	localtime_r(&ct, &ctm);
	strftime(buf, 20, "%Y%m%d%H%M%S", &ctm);		
	result = buf;		
}

char bsp::common::rand_char(){
	if(!g_rand_inited){
		srand(time(NULL));
		g_rand_inited = true;
	}	
	
	int index = ((int)(rand() * 16.0 / RAND_MAX)) % 16; //[0, 16] % 16, however, p(index = 16) = 0;since it's only a point
	return HEX_CHARS[index];
}

namespace{
	const int MAX_BT_TRACE = 16;	
	
	void get_execute_path(string& result){
		char link[1024];
		char exe[1024];
		snprintf(link,sizeof link,"/proc/%d/exe",getpid());
		ssize_t re = readlink(link,exe,sizeof exe);
		if(re ==-1) {
			return;
		}

		result.clear();
		result.append(exe, exe + re);
	}
}

void bsp::common::trans_stack_trace(string& result, void* trace[], int trace_size){
	ostringstream oss;
	char** msg = NULL;
	char addr_buf[1024];

	msg = backtrace_symbols(trace, trace_size);

	string exe_path;
	get_execute_path(exe_path);

	result.clear();
	
	for(int i = 0;i < trace_size;++i){
		oss.str("");
		oss << "addr2line " << trace[i] << " -e " << exe_path;
		
		FILE* pf = popen(oss.str().c_str(), "r");	
		fgets(addr_buf, 1024, pf);
		pclose(pf);

		result = result + msg[i] + "\t" + addr_buf;
	}

}

void bsp::common::get_stack_trace(string& result, int start){
	ostringstream oss;
	void* trace[start + MAX_BT_TRACE];
	char** msg = NULL;

	char addr_buf[1024];

	int trace_size = backtrace(trace, start + MAX_BT_TRACE);
	msg = backtrace_symbols(trace, trace_size);
	
	string exe_path;
	get_execute_path(exe_path);


	result.clear();
	
	for(int i = start;i < trace_size;++i){
		oss.str("");
		oss << "addr2line " << trace[i] << " -e " << exe_path;
		
		FILE* pf = popen(oss.str().c_str(), "r");	
		fgets(addr_buf, 1024, pf);
		pclose(pf);

		result = result + msg[i] + "\t" + addr_buf;
	}
}

namespace{
	const char* SOCK_START = "socket:[";
}

void bsp::common::get_socket_inodes(unordered_set<unsigned long>& results){
	pid_t pid = getpid();

	ostringstream oss;
	oss << "/proc/" << pid << "/fd";

	//list dir			
	struct dirent* file;

	string filename = oss.str() + '/';
	string::iterator dir_end = filename.end();

	char buf[1024];
	
	DIR* dir = opendir(oss.str().c_str());
	if(dir){
		while((file = readdir(dir)) != NULL){
			if(file->d_name[0] == '.'){
				continue;
			}

			filename.replace(dir_end, filename.end(), file->d_name);	
			readlink(filename.c_str(), buf, 1024);		

			if(strncmp(buf, SOCK_START, sizeof(SOCK_START)) == 0){
				char* end_strtol;
				unsigned long value = strtol(buf + sizeof(SOCK_START), &end_strtol, 10);		
				results.insert(value);
			}

		}

		closedir(dir);	
	}
}

namespace{
	const int INODE_POS = 9;
	const int ADDR_POS = 1;
	const int STATE_POS = 3;
	const char* LISTEN_STATE = "0A";
	const char* TCP_FILES[]  = {
		"/proc/net/tcp",
		"/proc/net/tcp6",
		NULL	
	};
}

int bsp::common::get_newly_opened_socket(unordered_set<unsigned long>& pre_inodes){
	unordered_set<unsigned long> new_inodes;
	get_socket_inodes(new_inodes);

	unordered_set<unsigned long>::iterator si = pre_inodes.begin();
	for(;si != pre_inodes.end();++si){
		//cout << *si << endl;
		unordered_set<unsigned long>::iterator in_new_iter = new_inodes.find(*si);
		new_inodes.erase(in_new_iter);
	}

	//cout << "===" << endl;
	//si = new_inodes.begin();
	//for(;si != new_inodes.end();++si){
	//	cout << *si << endl;
	//}

	const char** filename = TCP_FILES;
	while(filename != NULL){
		ifstream tcp_status(*filename);	
		string line;
		vector<string> parts;
		char* strtol_end;
		//skip the header line
		getline(tcp_status, line);
		while(getline(tcp_status, line)){
			parts.clear();
			str_tokenize_multi(line.c_str(), ' ', parts);
		
			unsigned long inode = strtol(parts[INODE_POS].c_str(), &strtol_end, 10);
			unordered_set<unsigned long>::iterator iter = new_inodes.find(inode);
			if(iter != new_inodes.end() && strncmp(parts[STATE_POS].c_str(), LISTEN_STATE, sizeof(LISTEN_STATE)) == 0){
				//get port;
				const char* start = parts[ADDR_POS].c_str();
				for(;*start && *start != ':';++start);
				int port = strtol(start + 1, &strtol_end, 16);
				return port;
			}
		}

		tcp_status.close();

		++filename;
	}

	return -1;
}

