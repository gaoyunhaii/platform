/*
 * =====================================================================================
 *
 *       Filename:  obj_factory.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/20/2015 02:28:20 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  COMMON_OBJ_FACTORY_INC
#define  COMMON_OBJ_FACTORY_INC

#include "common/cache.h"
#include <vector>

using std::vector;
using bsp::common::NeverFreeCache;

namespace bsp{namespace common{

template <class T, class INIT_T>
class ObjFactory{
public:
	ObjFactory() : cache(sizeof(T)){}

	T* newObject(const INIT_T& initor){
		void* ptr = cache.allocAnObj();
		return initor.init(ptr);
	}

	void allocFin(){
		cache.allocFin();
	}

	void destroy(const vector<T*>& vertices){
		typename vector<T*>::const_iterator vi = vertices.begin();
		for(;vi != vertices.end();++vi){
			(*vi)->~T();
		}
	}
private:
	NeverFreeCache cache;
};


template <class T>
class DefaultInit{
public:
	T* init(void* ptr) const{
		return new(ptr) T();
	}
};

}}

#endif   /* ----- #ifndef COMMON_OBJ_FACTORY_INC  ----- */
