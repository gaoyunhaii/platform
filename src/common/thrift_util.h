/*
 * =====================================================================================
 *
 *       Filename:  thrift_util.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  05/15/2014 03:38:25 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  COMMON_THRIFT_UTIL_INC
#define  COMMON_THRIFT_UTIL_INC

#include "master/gen-cpp/Master.h"
#include "slave/gen-cpp/Slave.h"
#include "common/configuration.h"

using namespace bsp::common;

namespace bsp{namespace common{namespace thrift{


/**
 * Copy properties between jobs and tasks
 *
 * */
template <class FROM, class TO>
void copy_job_parameters(const FROM& from, TO& to){
	to.job_dir = from.job_dir;
	to.lib_name = from.lib_name;
	to.task_class = from.task_class;
	to.partition_class = from.partition_class; 
	to.nr_partitions = from.nr_partitions;
	to.parser_class = from.parser_class;
/* input */
	to.input_class = from.input_class;
	to.input_path = from.input_path;
/* output */
	to.output_class = from.output_class;
	to.output_dir = from.output_dir;
/* template parameters */
	to.vertex_value_class = from.vertex_value_class;
	to.edge_value_class = from.edge_value_class;
	to.message_value_class = from.message_value_class;
	to.message_seri_class = from.message_seri_class;
/* user header file*/
	to.user_header_file = from.user_header_file;
}

template <class T>
void copy_job_parameters_to_conf(Configuration& conf, const T& from){
	conf.setValue("job_dir", from.job_dir.c_str());
	conf.setValue("lib_name", from.lib_name.c_str());
	conf.setValue("task_class", from.task_class.c_str());
	conf.setValue("partition_class", from.partition_class.c_str()); 
	conf.setIntValue("nr_partitions", from.nr_partitions);
	conf.setValue("parser_class", from.parser_class.c_str());
/* input */
	conf.setValue("input_class", from.input_class.c_str());
	conf.setValue("input_path", from.input_path.c_str());
/* output */
	conf.setValue("output_class", from.output_class.c_str());
	conf.setValue("output_dir", from.output_dir.c_str());
/* template parameters */
	conf.setValue("vertex_value_class", from.vertex_value_class.c_str());
	conf.setValue("edge_value_class", from.edge_value_class.c_str());
	conf.setValue("message_value_class", from.message_value_class.c_str());
	conf.setValue("message_seri_class", from.message_seri_class.c_str());
/* user header file*/
	conf.setValue("user_header_file", from.user_header_file.c_str());
}

template <class T>
void copy_conf_to_job_parameters(T& to, const Configuration& conf){
	conf.getValue(to.job_dir, "job_dir");
	conf.getValue(to.lib_name, "lib_name");
	conf.getValue(to.task_class, "task_class");
	conf.getValue(to.partition_class, "partition_class"); 
	to.nr_partitions = conf.getIntValue("nr_partitions", 0);
	conf.getValue(to.parser_class, "parser_class");
/* input */
	conf.getValue(to.input_class, "input_class");
	conf.getValue(to.input_path, "input_path");
/* output */
	conf.getValue(to.output_class, "output_class");
	conf.getValue(to.output_dir, "output_dir");
/* template parameters */
	conf.getValue(to.vertex_value_class, "vertex_value_class");
	conf.getValue(to.edge_value_class, "edge_value_class");
	conf.getValue(to.message_value_class, "message_value_class");
	conf.getValue(to.message_seri_class, "message_seri_class");
/* user header file*/
	conf.getValue(to.user_header_file, "user_header_file");
}


}}}


#endif   /* ----- #ifndef COMMON_THRIFT_UTIL_INC  ----- */
