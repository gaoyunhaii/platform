/*
 * =====================================================================================
 *
 *       Filename:  file_util.h
 *
 *    Description:  utils handing files and filenames
 *
 *        Version:  1.0
 *        Created:  2013年07月16日 13时48分35秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef  FILE_UTIL_INC
#define  FILE_UTIL_INC

#include <string>
#include <exception>
#include "common/sexception.h"
using std::string;
using std::exception;

namespace bsp{
	namespace common{
		//@Deprecated
		string file_join_path(const char* base, ...);
		void file_join_path(string& re, const char* base, ...);
		void file_name(string& re, const char* path);
		void file_lib_name(string& re, const char* fn);
		void check_dir(const char* dir) throw(FileSystemBadException);
		void mk_bsp_dir(const char* dir) throw(FileSystemBadException);
	}
}

#endif   /* ----- #ifndef FILE_UTIL_INC  ----- */
