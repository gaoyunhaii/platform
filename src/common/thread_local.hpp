/*
 * =====================================================================================
 *
 *       Filename:  thread_local.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  05/05/2014 10:17:51 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef  THREAD_LOCAL_HPP_INC
#define  THREAD_LOCAL_HPP_INC

#include "common/thread_local.h"
#include <cstdio>

template <class T>
void bsp::multi_thread::delete_destroy(void* obj){
	printf("tls destroy called \n");
	fflush(stdout);
	delete (T*)obj;
}

template <class T>
bsp::multi_thread::ThreadLocal<T>::ThreadLocal(TLDestroyFunc func){
	pthread_key_create(&key, func);
}

template <class T>
bsp::multi_thread::ThreadLocal<T>::~ThreadLocal(){
	pthread_key_delete(key);
}

template <class T>
T* bsp::multi_thread::ThreadLocal<T>::get(){
	T* data = (T*)pthread_getspecific(key);
	return data;
}

template <class T>
void bsp::multi_thread::ThreadLocal<T>::set(T* d){
	pthread_setspecific(key, d);
}

#endif   /* ----- #ifndef THREAD_LOCAL_HPP_INC  ----- */
