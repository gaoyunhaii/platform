/*
 * =====================================================================================
 *
 *       Filename:  atomic.cpp
 *
 *    Description:  implementation of atomic utils
 *
 *        Version:  1.0
 *        Created:  2013年07月15日 23时01分29秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include "common/atomic.h"
#include <cstdio>
#include <cassert>
extern "C"{
	#include <pthread.h>
}

bsp::atomic::MutexGetLock::MutexGetLock(pthread_mutex_t* _lock):
	lock(_lock){
	pthread_mutex_lock(lock);	
}

bsp::atomic::MutexGetLock::~MutexGetLock(){
	pthread_mutex_unlock(lock);
}

bsp::atomic::MutexTryLock::MutexTryLock(pthread_mutex_t* _lock):
	lock(_lock){
	lock_re = pthread_mutex_trylock(lock);
}

bsp::atomic::MutexTryLock::~MutexTryLock(){
	if(lock_re == 0){
		pthread_mutex_unlock(lock);
	}
}

bsp::atomic::RWLockGetRead::RWLockGetRead(pthread_rwlock_t* _lock):
		lock(_lock){
	pthread_rwlock_rdlock(lock);	
}

bsp::atomic::RWLockGetRead::~RWLockGetRead(){
	pthread_rwlock_unlock(lock);
}

bsp::atomic::RWLockGetWrite::RWLockGetWrite(pthread_rwlock_t* _lock):
		lock(_lock){
	pthread_rwlock_wrlock(lock);	
}

bsp::atomic::RWLockGetWrite::~RWLockGetWrite(){
	pthread_rwlock_unlock(lock);
}

bsp::atomic::CountDownWaiter::CountDownWaiter():
		in_progress(false),
		lock(PTHREAD_MUTEX_INITIALIZER),
		count_cond(PTHREAD_COND_INITIALIZER),
		start_cond(PTHREAD_COND_INITIALIZER)
{
}

void bsp::atomic::CountDownWaiter::reset(int v){
	MutexGetLock get_lock(&lock);
	
	value = v;	
	in_progress = (value > 0);	
	pthread_cond_broadcast(&start_cond);
}

void bsp::atomic::CountDownWaiter::countDown(bool wait_on_notify){
	MutexGetLock get_lock(&lock);

	if(!in_progress){
		pthread_cond_wait(&start_cond, &lock);
	}

	value -= 1;
	if(value == 0){
		in_progress = false;	
		pthread_cond_broadcast(&count_cond);

		if(wait_on_notify){
			pthread_cond_wait(&start_cond, &lock);
		}
	}
}

void bsp::atomic::CountDownWaiter::wait(){
	MutexGetLock get_lock(&lock);	

	if(in_progress){
		pthread_cond_wait(&count_cond, &lock);
	}
}

void bsp::atomic::CountDownWaiter::ask_notifier_to_exit(){
	MutexGetLock get_lock(&lock);
	pthread_cond_broadcast(&start_cond);
}


//bsp::atomic::CountWaiter::CountWaiter()
//		:value(0), aim_value(0), some_is_waiting(false),
//		lock(PTHREAD_MUTEX_INITIALIZER),
//		count_cond(PTHREAD_COND_INITIALIZER),
//		start_cond(PTHREAD_COND_INITIALIZER){
//}
//
//void bsp::atomic::CountWaiter::addValue(int v){
//	MutexGetLock get_lock(&lock);
//	value += v;	
//
//	if(some_is_waiting){
//		//printf("value = %d, aim_value = %d\n", value, aim_value);
//		if(value >= aim_value){
//			some_is_waiting = false;
//			pthread_cond_broadcast(&count_cond);						
//			pthread_cond_wait(&start_cond, &lock);
//		}						
//	}
//}
//
///**
// * User should make sure there will not be any condition
// */
//bool bsp::atomic::CountWaiter::isWillWait(int v){
//	return value < v;
//}
//
//void bsp::atomic::CountWaiter::wait(int v, CountDownWaiter* count_down){
//	MutexGetLock get_lock(&lock);
//
//	if(value >= v){
//		//printf("statisfied: value = %d, v = %d\n", value, v);
//		return; //cond satisfied		
//	}
//
//	some_is_waiting = true;
//	aim_value = v;
//
//	if(count_down){
//		count_down->ask_notifier_to_exit();	
//	}
//
//	pthread_cond_wait(&count_cond, &lock);
//}
//
//void bsp::atomic::CountWaiter::clear(){
//	MutexGetLock get_lock(&lock);
//
//	value = 0;
//	aim_value = 0;
//	some_is_waiting = false;
//}
//
//void bsp::atomic::CountWaiter::restart(){
//	MutexGetLock get_lock(&lock);
//	pthread_cond_broadcast(&start_cond);	
//}


bsp::atomic::MessageExecutionCoor::MessageExecutionCoor()
		:max_control_mess(0), max_data_mess(0),	
		control_mess_received(0), data_mess_received(0), control_mess_to_decre(0),
		executor_is_waiting(false),
		lock(PTHREAD_MUTEX_INITIALIZER), recv_can_run(PTHREAD_COND_INITIALIZER),
		executor_can_run(PTHREAD_COND_INITIALIZER)
{

}

void bsp::atomic::MessageExecutionCoor::initMaxControlMess(int _mcm){
	max_control_mess = _mcm;
}

void bsp::atomic::MessageExecutionCoor::newTurn(){
	MutexGetLock get_lock(&lock);

	max_control_mess -= control_mess_to_decre;
	assert(max_control_mess >= 0);
	control_mess_to_decre = 0;
	max_data_mess = 0;
	control_mess_received = 0;
	data_mess_received = 0;
	executor_is_waiting = false;

	pthread_cond_signal(&recv_can_run);
}

void bsp::atomic::MessageExecutionCoor::controlMessageReceived(int exited, int _data_mess){
	MutexGetLock get_lock(&lock);

	control_mess_received += 1;
	control_mess_to_decre += exited;
	max_data_mess += _data_mess;	

	_test_turn_end();
}

void bsp::atomic::MessageExecutionCoor::dataMessageReceived(){
	MutexGetLock get_lock(&lock);
	data_mess_received += 1;

	_test_turn_end();
}

void bsp::atomic::MessageExecutionCoor::_test_turn_end(){
	if(control_mess_received >= max_control_mess && data_mess_received >= max_data_mess){
		//turn end		

		if(executor_is_waiting){
			pthread_cond_broadcast(&executor_can_run);
		}

		pthread_cond_wait(&recv_can_run, &lock);
	}
}

void bsp::atomic::MessageExecutionCoor::waitForAllMessages(){
	MutexGetLock get_lock(&lock);

	if(control_mess_received >= max_control_mess && data_mess_received >= max_data_mess){
		return;	
	}

	executor_is_waiting = true;
	pthread_cond_wait(&executor_can_run, &lock);
	executor_is_waiting = false;
}

////=====================IncreCountDownWaiter===============
//void bsp::atomic::IncreCountDownWaiter::IncreCountDownWaiter():
//		value(0), in_progress(false), 
//		lock(PTHREAD_MUTEX_INITIALIZER),
//		count_cond(PTHREAD_RWLOCK_INITIALIZER){}
//
//void bsp::atomic::IncreCountDownWaiter::incre(long long nv){
//}
