/*
 * =====================================================================================
 *
 *       Filename:  sexception.h
 *
 *    Description:  All private exceptions
 *
 *        Version:  1.0
 *        Created:  2013年07月22日 09时10分44秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef  SEXCEPTION_INC
#define  SEXCEPTION_INC

extern "C"{
	#include <errno.h>
}
#include <cstring>
#include <exception>
#include <string>
#include <iostream>

using std::exception;
using std::string;
using std::cout;
using std::ostream;

namespace bsp{
	namespace common{
		const int MAX_ERROR_CHRAS = 1024;
		class BaseException : public exception{
		public:
			BaseException(){
				_gen_stack_trace();
			}
			BaseException(const char* what):
					content(what){
				_gen_stack_trace();	
			}
			virtual const char* what() const throw(){
				return content.c_str();
			}
			virtual void printStackTrace(ostream& os = cout){
				os << stack_trace;
			}
			virtual ~BaseException() throw(){};
		protected:
			void setContent(const char* new_what){
				content = new_what;
			}
		private:
			string stack_trace;
			string content;
		private:
			void _gen_stack_trace();
		};

		class FileSystemBadException : public BaseException{
		public:
			FileSystemBadException(const char* what):
				BaseException(what){};
		};	

		class OSException : public BaseException{
		public:
			OSException(){
				char buf[MAX_ERROR_CHRAS];	
				char* re = NULL;
#if (_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && ! _GNU_SOURCE
				strerror_r(errno, buf, MAX_ERROR_CHRAS);
				re = buf;
#else
				re = strerror_r(errno, buf, MAX_ERROR_CHRAS);
#endif
				setContent(re);
			};
			OSException(const char* what):BaseException(what){};
		};

		class IOException : public OSException{
		public:
			IOException():OSException(){}
			IOException(const char* what):
				OSException(what){}
		};

		class SocketCloseException : public BaseException{
		public:
			SocketCloseException(){};
		};

		/* 
		 * This exception indicates failure occured in parseing configuration 
		 * files like *.xml, *.properties.
		 * */
		class ParseException : public BaseException{
		public:
			ParseException():BaseException("Fail to parse"){};	
			ParseException(const char* what):
					BaseException(what){}
		};

		/* 
		 * This exception indicates required properties are not found in
		 * Configuration object.
		 * */
		class NotFoundException : public BaseException{
		public:
			NotFoundException():BaseException("Fail to found"){};	
			NotFoundException(const char* what):BaseException(what){};
		};

		/* 
		 * This exception indicates compiling fails. The executors based on cpp codes have 
		 * to compile generated source files to acquire task's executable file. 
		 * */
		class CompileException : public BaseException{
		public:
			CompileException() : BaseException("Fail to compile"){};
			CompileException(const char* what):BaseException(what){}
		};

		/* 
		 * This exception indicates startup of task's executable fails. 
		 * <em>note</em>: The slave will not wait for the finish event of the task any more. 
		 * Monitoring on tasks is now done by job masters. Therefore, the only chance to 
		 * throw an ExecuteException is when starting the task processes.
		 * */
		class ExecuteException : public BaseException{
		public:
			ExecuteException() : BaseException("Fail to execute"){};
			ExecuteException(const char* what):BaseException(what){}
		};

	}
}

#endif   /* ----- #ifndef SEXCEPTION_INC  ----- */


