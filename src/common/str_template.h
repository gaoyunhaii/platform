/*
 * =====================================================================================
 *
 *       Filename:  template.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年08月06日 17时07分34秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  STR_TEMPLATE_INC
#define  STR_TEMPLATE_INC

#include <unordered_map>
#include <iostream>
#include <string>
using std::ostream;
using std::unordered_map;
using std::string;

namespace bsp{ namespace common { 

class StrTemplate{
public:
	StrTemplate(const char* _temp):
		temp(_temp){};
	void instance(ostream& result, const unordered_map<string, string>& params);
	void instance(const char* filename, const unordered_map<string, string>& params);
private:
	string temp;
private:
	bool _getParam(string& value, const string& key, const unordered_map<string, string>& params);
};

}}

#endif   /* ----- #ifndef STR_TEMPLATE_INC  ----- */
