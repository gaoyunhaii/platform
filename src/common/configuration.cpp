/*
 * =====================================================================================
 *
 *       Filename:  configuration.cpp
 *
 *    Description:  Xml parser and configuration
 *
 *        Version:  1.0
 *        Created:  2013年07月16日 09时36分05秒
 *       Revision:  none
 *       Compiler:  gcc
 * *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include "common/configuration.h"

#include <cstring>
#include <string>
#include <cstdlib>
#include "common/util.h"

using std::string;
using bsp::common::str_tokenize;

namespace bsp{
	namespace common{
		const char* const ROOT = "configuration";				
		const char* const WRAPPER = "property";
		const char* const KEY = "name";
		const char* const VALUE = "value";

		const char* const ENCODING = "UTF-8";
		
		const char* DEFAULE_VALUE[][2] = {
			{"dir.master", "/home/owner-pc/mount/E/lab/graph/bsp_test/master"},
			{"dir.slave", "/home/owner-pc/mount/E/lab/graph/bsp_test/slave"},
			{"dir.logs", "/home/owner-pc/mount/E/lab/graph/bsp_test/logs"},	
			{"dir.home", "/home/owner-pc/mount/E/lab/graph/bsp"},

			{"addr.master", "127.0.0.1"},
			{"hdfs.addr", "localhost"},
			{"hdfs.port", "9000"},

			{"dir.slave.compile.include", "include"},
			{"dir.slave.compile.lib", "lib"},
			{"dir.slave.compile.config", "sbin/bsp-config.sh"},
			{"dir.other_includes", ""},

			{"dir.hdfs.submit", "/tmp/bsp/submit"}, 

			{"port.nynn", "30001"},
			{"port.master.thrift", "54250"},
			{"port.slave.thrift", "54251"},
			{"port.master.ns", "54252"},
			{"port.master.coordinator", "54253"},
			{"port.master.query", "54255"},
			{"port.slave.message.out", "54254"},
			
			{"ipc.slave.message.in", "ipc/proxy"},
			{"ipc.slave.message.control", "ipc/control"},

			{"jobs.slave.slots", "10"},

			{"interval.slave.heartbeat", "5000"},

			{"threads.master.jobs", "4"},

			{"amount.master.nofity", "5"},
			{"amount.master.max_accumu", "5"},

			{NULL, NULL}
		};
		const char LIST_SPLIT = ',';
	}
}

void bsp::common::XMLParser::parse(const char* filename, unordered_map<string, string>& map) throw(ParseException, IOException){
	xmlDocPtr doc;
	xmlNodePtr cur;

	doc = xmlParseFile(filename);
	if (doc == NULL ) {
		throw ParseException(filename);
	}

	cur = xmlDocGetRootElement(doc);
	if (cur == NULL) {
		xmlFreeDoc(doc);
		throw ParseException(filename);
	}
	
	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if(cur->type == XML_ELEMENT_NODE){
			if(!xmlStrcmp(cur->name, (const xmlChar*)WRAPPER)){
				parseProperty(doc, cur, map);
			}
		}
		cur = cur->next;
	}
	xmlFreeDoc(doc);
}

void bsp::common::XMLParser::parseProperty(xmlDocPtr doc, xmlNodePtr node, unordered_map<string, string>& map) throw(ParseException){
	xmlChar *key = NULL, *value = NULL;
	xmlNodePtr cur = node->xmlChildrenNode;
	while (cur != NULL) {
		if ((!xmlStrcmp(cur->name, (const xmlChar *)KEY))) {
			key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
		}
		else if((!xmlStrcmp(cur->name, (const xmlChar*)VALUE))){
			value = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
		}
		cur = cur->next;
	}
	if(key && value){
		map[(const char*)key] = (const char*)value;
	}
	if(key){
		xmlFree(key);
	}
	if(value){
		xmlFree(value);
	}
}


void bsp::common::XMLWriter::write(const char* filename, unordered_map<string, string>& map) throw(ParseException, IOException){
	int re;
	xmlDocPtr doc;
	xmlTextWriterPtr writer = xmlNewTextWriterDoc(&doc, 0);
	if(!writer){
		throw ParseException();
	}

	re = xmlTextWriterStartDocument(writer, NULL, "UTF-8", NULL);
	if(re < 0){
		throw ParseException();
	}

	re = xmlTextWriterStartElement(writer, (xmlChar*)ROOT);
	if(re < 0){
		throw ParseException();	
	}

	re = xmlTextWriterWriteFormatString(writer, "%s", "\n");
	if(re < 0){
		throw ParseException();
	}

	unordered_map<string, string>::iterator mi = map.begin();
	for(;mi != map.end();++mi){
		writeProperty(writer, mi->first, mi->second);
	}

	re = xmlTextWriterEndElement(writer);
	if(re < 0){
		throw ParseException();
	}

	xmlFreeTextWriter(writer);
	re = xmlSaveFileEnc(filename, doc, ENCODING);
	if(re < 0){
		throw IOException();
	}
	xmlFreeDoc(doc);
}

void bsp::common::XMLWriter::writeProperty(xmlTextWriterPtr& writer, const string& key, const string& value) throw(ParseException){
	int re = xmlTextWriterStartElement(writer, (xmlChar*)WRAPPER);
	if(re < 0){
		throw ParseException();
	}
	
	re = xmlTextWriterStartElement(writer, (xmlChar*)KEY);
	if(re < 0){
		throw ParseException();
	}

	re = xmlTextWriterWriteFormatString(writer, "%s", key.c_str());	
	if(re < 0){
		throw ParseException();
	}

	re = xmlTextWriterEndElement(writer);
	if(re < 0){
		throw ParseException();
	}

	re = xmlTextWriterStartElement(writer, (xmlChar*)VALUE);
	if(re < 0){
		throw ParseException();
	}

	re = xmlTextWriterWriteFormatString(writer, "%s", value.c_str());	
	if(re < 0){
		throw ParseException();
	}

	re = xmlTextWriterEndElement(writer);
	if(re < 0){
		throw ParseException();
	}

	re = xmlTextWriterEndElement(writer);
	if(re < 0){
		throw ParseException();
	}

	re = xmlTextWriterWriteFormatString(writer, "%s", "\n");
	if(re < 0){
		throw ParseException();
	}
}

bsp::common::Configuration::Configuration(bool use_default){
	//default values;						
	if(use_default){
		const char* (*defs)[2] = bsp::common::DEFAULE_VALUE;	
		while((*defs)[0] && (*defs)[1]){
			values[(*defs)[0]] = (*defs)[1];		
			++defs;
		}
	}
}


bsp::common::Configuration::~Configuration(){
}

void bsp::common::Configuration::getValue(string& ret, const char* key, const char* def) const throw(NotFoundException){
	unordered_map<string, string>::const_iterator mi = values.find(key);			
	if(mi != values.end()){
		ret = mi->second;
	}
	else{ 
		if(def != NULL){
			ret = def;
		}
		else{
			string what = "Can't find value for ";
			what += key;
			throw NotFoundException(what.c_str());
		}
	}
}

int bsp::common::Configuration::getIntValue(const char* key, const int def) const{
	unordered_map<string, string>::const_iterator mi = values.find(key);			
	if(mi != values.end()){
		return atoi(mi->second.c_str());
	}
	return def;
}

double bsp::common::Configuration::getDoubleValue(const char* key, const double def) const{
	unordered_map<string, string>::const_iterator mi = values.find(key);			
	if(mi != values.end()){
		return atof(mi->second.c_str());
	}
	return def;
}

void bsp::common::Configuration::getListValue(const char* key, vector<string>& def_rets) const{
	unordered_map<string, string>::const_iterator mi = values.find(key);			
	if(mi != values.end()){
		//clear def_rets	
		def_rets.clear();
		//tokenizer mi->second and append to def_rets
		str_tokenize(mi->second.c_str(), LIST_SPLIT, def_rets);	
	}
}

void bsp::common::Configuration::setValue(const char* key, const char* value){
	values[key] = value;
}

void bsp::common::Configuration::setIntValue(const char* key, int value){
	oss.str("");
	oss << value;
	values[key] = oss.str();	
}

void bsp::common::Configuration::setDoubleValue(const char* key, double value){
	oss.str("");
	oss << value;
	values[key] = oss.str();
}

void bsp::common::Configuration::setListValue(const char* key, const vector<string>& value){
			
}

void bsp::common::Configuration::readFile(const char* file) throw(IOException, ParseException){
	parser.parse(file, values);
}

void bsp::common::Configuration::writeFile(const char* file) throw(IOException, ParseException){
	writer.write(file, values);
}

void bsp::common::Configuration::copyValues(Configuration& other_conf, const char* key_mappings[][2]){
	string value;
	const char* (*defs)[2] = key_mappings;
	while((*defs)[0] && (*defs)[1]){
		other_conf.getValue(value, (*defs)[0]);
		setValue((*defs)[1], value.c_str());
		++defs;
	}
}
