/*
 * =====================================================================================
 *
 *       Filename:  util.h
 *
 *    Description:  A set of util macros, constants and utility functions
 *
 *        Version:  1.0
 *        Created:  2013年03月09日 21时24分00秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  UTIL_INC
#define  UTIL_INC

extern "C"{
	#include <arpa/inet.h>
	#include <sys/socket.h>
	#include <sys/types.h>
	#include <sys/prctl.h>
	#include <unistd.h>
	#include <fcntl.h>
	#include <signal.h>
}
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include "sexception.h"
using std::vector;
using std::string;
using std::unordered_map;
using std::unordered_set;
using std::ofstream;
using std::ifstream;
using std::ostringstream;
using std::endl;

/**
 * Integer types redefinition
 * We assume :
 * 		int : 4B
 * 		long : ?B
 * 		int4 : = 4B
 * 		llong : = 8B
 * 
 * We'll use ordinary int as long as it's length is not critical
 * But we'll keep using llong instead of long long
 *
 * */

typedef __int32_t int4;
typedef __int64_t llong;


/**
 * Following is a list of macros used to define getter and setter for class 
 */

#define ACCESSOR(type, name) \
	type get_##name(){ \
		return this->name;\
	}

#define MUTATOR(type, name) \
	type set_##name(type name){ \
		this->name = name; \
	}

#define MUTATOR_POINTER(type, name) \
	type set_##name(type* p##name){ \
		this->name = *p##name; \
	}

#define ACCESSOR_MUTATOR(type, name) \
	ACCESSOR(type, name) \
	MUTATOR(type, name) \
	MUTATOR_POINTER(type, name)


#define PROPERTY_ACCESSOR(Type, Name)\
const Type& get_##Name() const{\
    return Name;\
}\
Type& mutable_##Name(){\
    return Name;\
}

#define PROPERTY_READER(Type, Name) \
const Type& get_##Name() const{\
    return Name;\
}

#define PROPERTY_POINTER_READER(Type, Name) \
Type& get_##Name(){\
    return *Name;\
}


namespace bsp{
	namespace common{
		namespace constants{
			const int MAX_HOSTNAME = 1024;
		}

		void str_strip(const char* str, char ch, const char** start, const char** end);
		/* 
		 * 起始位置的分隔符将导致空字符串，但如果分隔符在最后，则将会被忽略
		 *
		 * */
		void str_tokenize(const char* str, char ch, vector<string>& result);


		void str_tokenize_multi(const char* str, char ch, vector<string>& result);


		void str_getline(const char* str, const char** end, const char** next);

		/* 
		 * 根据《unix环境高级编程第13章实现》 
		 * */
		void daemonize(const char* pid_fn, const char* out_file) throw(OSException);
		void reg_signal(int signo, void (*handler)(int)) throw(OSException);

		/* 
		 *
		 * @return: 0 for found and -1 for not found
		 * */
		int get_ip_by_hostname(const char* hostname, string& ret_ip);

		inline long get_time_millis(){
			struct timespec t;	
			clock_gettime(CLOCK_REALTIME, &t);
			return t.tv_sec * 1000 + t.tv_nsec / 1000000;
		}

		inline long long get_time_us(){
			struct timespec t;	
			clock_gettime(CLOCK_REALTIME, &t);
			return t.tv_sec * 1000000 + t.tv_nsec / 1000;
		}

		void get_time_hex_str(string& result);

		char rand_char();

		inline void str_addr(string& result, struct in_addr* paddr){
			char buf[1024];
			inet_ntop(AF_INET, paddr, buf, 1024);
			result = buf;
		}

		inline void de_str_addr(struct in_addr* paddr, const char* saddr){
			inet_pton(AF_INET, saddr, paddr);
		}

		inline void set_nonblocking(int sockfd){
			int flags = fcntl(sockfd, F_GETFL, 0);  
			fcntl(sockfd, F_SETFL, flags|O_NONBLOCK);  
		}

		template <class KEY, class T>
		inline T find_in_map(const unordered_map<KEY, T>& map, const KEY& key, const T nullValue){
			typename unordered_map<KEY, T>::const_iterator mi = map.find(key);
			if(mi != map.end()){
				return mi->second;
			}
			return nullValue;
		}

		template <class K, class V>
		inline void add_to_map(unordered_map<K, V>& map, const K& key, const V newValue, const V defaultValue){
			typename unordered_map<K, V>::iterator iter = map.find(key);		
			if(iter == map.end()){
				map[key] = defaultValue + newValue;
			}
			else{
				map[key] += newValue;
			}
		}

		template <class ITER>
		inline void print_list(ostream& out, ITER start, ITER end){
			out << "[";
			for(ITER i = start;i != end;++i){
				out << *i << ",";
			}
			out << "]" << endl;
		}

		template <class ITER>
		inline void print_map(ostream& out, ITER start, ITER end){
			out << "[";
			for(ITER i = start;i != end;++i){
				out << i->first << ":"	<< i->second << ",";
			}

			out << "]" << endl;
		}

		void trans_stack_trace(string& result, void* trace[], int trace_size);
		void get_stack_trace(string& result, int start = 0);

		inline void print_pid(const char* fn){
			ofstream os(fn);
			pid_t pid = getpid();	
			os << pid;
			os.close();
		}

        inline void print_free_mem(){
            pid_t pid = getpid();
            ostringstream oss;
            oss << "/proc/" << pid << "/status";
            ifstream in(oss.str().c_str());
            string line;
            while(getline(in, line)){
                if(!strncmp(line.c_str(), "VmRSS", 5)){
                    cout << line << endl;
                }   
            }   

            in.close();
        }   

		inline void set_pr_name(const char* first, int index){
			ostringstream oss;
			oss << first << index;		
			prctl(PR_SET_NAME, (unsigned long)oss.str().c_str(), 0, 0, 0);
		}
		//For fetching ports used
		void get_socket_inodes(unordered_set<unsigned long>& results);
		int get_newly_opened_socket(unordered_set<unsigned long>& pre_inodes);
	}
}


#endif   /* ----- #ifndef UTIL_INC  ----- */
