/*
 * =====================================================================================
 *
 *       Filename:  sexception.cpp
 *
 *    Description:  same with .h
 *
 *        Version:  1.0
 *        Created:  2013年07月22日 09时10分54秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include "common/sexception.h"
#include "common/util.h"
using namespace bsp::common;

void bsp::common::BaseException::_gen_stack_trace(){
	get_stack_trace(stack_trace, 3);	
}
