/*
 * =====================================================================================
 *
 *       Filename:  csocket.h
 *
 *    Description:  A encapsulation of unix socket to facilitate future usage
 *
 *        Version:  1.0
 *        Created:  2013年06月23日 23时57分06秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  CSOCKET_INC
#define  CSOCKET_INC

extern "C"{
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <sys/un.h>
	#include <netinet/in.h>
	#include <strings.h>
}

#include <iostream>
#include <string>
#include <memory>
#include "common/sexception.h"
#include "common/dthread.h"

using std::string;
using namespace bsp::common;

namespace bsp{ namespace common{
	class Socket{
	public:	
		virtual ~Socket(){}
		virtual const struct sockaddr* getAddr() const = 0;
		virtual socklen_t getAddrLen() const = 0;
		virtual int getFd() const = 0;
	};

	class ServerSocket{
	public:	
		virtual ~ServerSocket(){}
		virtual std::shared_ptr<Socket> accept() throw (IOException)= 0;		
		virtual int getFd() const = 0;
		virtual const struct sockaddr* getAddr() const = 0;
	};



	class RemoteServerSocket;
	class LocalServerSocket;

	class RemoteConSocket : public Socket{
	public:
		RemoteConSocket():fd(-1), addrlen(sizeof(addr)){};
		~RemoteConSocket(){
			close(fd);
		}

		virtual int getFd() const{return fd;};
		virtual const struct sockaddr* getAddr() const{return (struct sockaddr*)&addr;};
		virtual socklen_t getAddrLen() const{return addrlen;};

		friend class RemoteServerSocket;
	private:
		int fd;
		struct sockaddr_in addr;
		socklen_t addrlen;
	};

	class LocalConSocket : public Socket{
	public:	
		LocalConSocket():fd(-1), addrlen(sizeof(addr)){};
		~LocalConSocket(){
			close(fd);
		}

		virtual int getFd() const{return fd;};
		virtual const struct sockaddr* getAddr() const{return (struct sockaddr*)&addr;};
		virtual socklen_t getAddrLen() const{return addrlen;};

		friend class LocalServerSocket;
	private:
		int fd;
		struct sockaddr_un addr;
		socklen_t addrlen;
	};

	class RemoteServerSocket : public ServerSocket{
	public:
		RemoteServerSocket(const char*, int) throw (IOException);
		~RemoteServerSocket();
		std::shared_ptr<Socket> accept() throw (IOException);

		int getFd() const{return listenfd;};
		const struct sockaddr* getAddr() const{return (struct sockaddr*)&sock_addr;}
	private:
		int listenfd;
		struct sockaddr_in sock_addr;
	};	

	class LocalServerSocket : public ServerSocket {
	public:	
		LocalServerSocket(const char*) throw (IOException);
		~LocalServerSocket();
		std::shared_ptr<Socket> accept() throw (IOException);

		int getFd() const{return listenfd;}
		const struct sockaddr* getAddr() const{return (struct sockaddr*)&sock_addr;}
	private:
		int listenfd;
		struct sockaddr_un sock_addr;
	};

	class RemoteSocket : public Socket{
	public:
		RemoteSocket(const char* addr, int port) throw (IOException);
		~RemoteSocket();


		const struct sockaddr* getAddr() const{
			return (struct sockaddr*)&serv_addr;
		}

		socklen_t getAddrLen() const{
			return addrlen;
		}

		int getFd() const{
			return fd;
		}
	private:
		int fd;
		struct sockaddr_in serv_addr;
		socklen_t addrlen;
	};

	class LocalSocket : public Socket{
	public:
		LocalSocket(const char* addr) throw (IOException);
		~LocalSocket();

		const struct sockaddr* getAddr() const{
			return (struct sockaddr*)&serv_addr;
		}

		socklen_t getAddrLen() const{
			return addrlen;
		}

		int getFd() const{
			return fd;
		}
	private:
		int fd;
		struct sockaddr_un serv_addr;
		socklen_t addrlen;
	};
}}

#endif   /* ----- #ifndef CSOCKET_INC  ----- */

