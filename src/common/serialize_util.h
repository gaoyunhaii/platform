/*
 * =====================================================================================
 *
 *       Filename:  serialize_util.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  06/18/2014 07:16:42 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  COMMON_SERIALIZE_UTIL_INC
#define  COMMON_SERIALIZE_UTIL_INC

#include <endian.h>
#include <cassert>
#include <cstring>
#include <cstdint>
#include <string>
using std::string;

namespace bsp{namespace common{namespace serialize{

class BoundedBuffer{
public:
	BoundedBuffer(void* _b):buf(_b), current_p((char*)buf){}
	void* getBuf() const{return buf;}
	char* const getCurrentP() const{return current_p;}
	size_t offset() const{return current_p - (char*)buf;}

	void advance(size_t size){current_p += size;}
	void drawback(size_t size){current_p -= size;}
	void seek(size_t size){current_p = (char*)buf + size;}

	void writeRaw(const void* start, size_t size){
		memcpy(current_p, start, size);
		advance(size);
	}

	void readRaw(void* dest, size_t size){
		memcpy(dest, current_p, size);
		advance(size);	
	}

	char& operator[](size_t index){
		return ((char*)buf)[index];
	}

	const char& operator[](size_t index) const{
		return ((char*)buf)[index];
	}
private:
	void* buf;
	char* current_p;
};

inline BoundedBuffer& operator<<(BoundedBuffer&  buf, uint8_t v){
	char* const p = buf.getCurrentP();
	*((uint8_t* const)p) = v;				
	buf.advance(sizeof(uint8_t));
	return buf;
}

inline BoundedBuffer& operator<<(BoundedBuffer&  buf, uint16_t v){
	char* const p = buf.getCurrentP();
	*((uint16_t* const)p) = htobe16(v);
	buf.advance(sizeof(uint16_t));
	return buf;
}

inline BoundedBuffer& operator<<(BoundedBuffer&  buf, uint32_t v){
	char* const p = buf.getCurrentP();
	*((uint32_t* const)p) = htobe32(v);				
	buf.advance(sizeof(uint32_t));
	return buf;
}

inline BoundedBuffer& operator<<(BoundedBuffer&  buf, uint64_t v){
	char* const p = buf.getCurrentP();
	*((uint64_t* const)p) = htobe64(v);
	buf.advance(sizeof(uint64_t));
	return buf;
}

inline BoundedBuffer& operator<<(BoundedBuffer& buf, int v){
	return operator<<(buf, *(reinterpret_cast<uint32_t*>(&v)));
}

inline BoundedBuffer& operator<<(BoundedBuffer& buf, long long int v){
	return operator<<(buf, *(reinterpret_cast<uint64_t*>(&v)));
}

inline BoundedBuffer& operator<<(BoundedBuffer& buf, double v){
	return operator<<(buf, *(reinterpret_cast<uint64_t*>(&v)));
}

inline BoundedBuffer& operator<<(BoundedBuffer& buf, float v){
    return operator<<(buf, *(reinterpret_cast<uint32_t*>(&v)));
}

inline BoundedBuffer& operator<<(BoundedBuffer& buf, const string& v){
	operator<<(buf, v.size());
	buf.writeRaw(v.c_str(), v.size());
	return buf;
}

inline BoundedBuffer& operator>>(BoundedBuffer& buf, uint8_t& rv){
	char* const p = buf.getCurrentP();
	rv = *((uint8_t*)p);
	buf.advance(sizeof(uint8_t));
	return buf;
}

inline BoundedBuffer& operator>>(BoundedBuffer& buf, uint16_t& rv){
	char* const p = buf.getCurrentP();
	rv = be16toh(*((uint16_t*)p));
	buf.advance(sizeof(uint16_t));
	return buf;
}

inline BoundedBuffer& operator>>(BoundedBuffer& buf, uint32_t& rv){
	char* const p = buf.getCurrentP();
	rv = be32toh(*((uint32_t*)p));
	buf.advance(sizeof(uint32_t));
	return buf;
}

inline BoundedBuffer& operator>>(BoundedBuffer& buf, uint64_t& rv){
	char* const p = buf.getCurrentP();
	rv = be64toh(*((uint64_t*)p));
	buf.advance(sizeof(uint64_t));
	return buf;
}

inline BoundedBuffer& operator>>(BoundedBuffer& buf, int& rv){
	return operator>>(buf, *(reinterpret_cast<uint32_t*>(&rv)));
}

inline BoundedBuffer& operator>>(BoundedBuffer& buf, long long int& rv){
	return operator>>(buf, *(reinterpret_cast<uint64_t*>(&rv)));
}

inline BoundedBuffer& operator>>(BoundedBuffer& buf, double& rv){
	return operator>>(buf, *(reinterpret_cast<uint64_t*>(&rv)));
}

inline BoundedBuffer& operator>>(BoundedBuffer& buf, float& rv){
    return operator>>(buf, *(reinterpret_cast<uint32_t*>(&rv)));
}

inline BoundedBuffer& operator>>(BoundedBuffer& buf, string& str){
	str.clear();

	size_t size = 0;	
	operator>>(buf, size);	
	
	str.append(buf.getCurrentP(), size);	
	buf.advance(size);
	return buf;
}


}}}

#endif   /* ----- #ifndef RUNTIME2_SERIALIZE_UTIL_INC  ----- */
