/*
 * =====================================================================================
 *
 *       Filename:  thread_group.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  06/22/2014 11:12:09 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  COMMON_THREAD_GROUP_HPP_INC
#define  COMMON_THREAD_GROUP_HPP_INC

#include "common/thread_group.h"

template <class IR>
bsp::multi_thread::ThreadGroup<IR>::ThreadGroup(int size){
	for(int i = 0;i < size;++i){
		IR* runnable = new IR(i);		
		DaemonThread* thread = new DaemonThread(runnable);

		runnables.push_back(runnable);
		threads.push_back(thread);
	}
}

template <class IR>
bsp::multi_thread::ThreadGroup<IR>::~ThreadGroup(){
	std::vector<DaemonThread*>::iterator tvi = threads.begin();
	for(;tvi != threads.end();++tvi){
		DaemonThread* thread = *tvi;
		delete thread;
	}

	typename std::vector<IR*>::iterator rvi = runnables.begin();
	for(;rvi != runnables.end();++rvi){
		IR* runnable = *rvi;
		delete runnable;
	}
}

template <class IR>
void bsp::multi_thread::ThreadGroup<IR>::start(){
	std::vector<DaemonThread*>::iterator tvi = threads.begin();
	for(;tvi != threads.end();++tvi){
		DaemonThread* thread = *tvi;			
		thread->start();
	}

}

template <class IR>
void bsp::multi_thread::ThreadGroup<IR>::markStop(){
	typename std::vector<IR*>::iterator rvi = runnables.begin();
	for(;rvi != runnables.end();++rvi){
		IR* runnable = *rvi;
		runnable->markStop();
	}
}


template <class IR>
void bsp::multi_thread::ThreadGroup<IR>::joinAll(){
	std::vector<DaemonThread*>::iterator tvi = threads.begin();
	for(;tvi != threads.end();++tvi){
		DaemonThread* thread = *tvi;			
		thread->join();
	}
}


#endif   /* ----- #ifndef COMMON_THREAD_GROUP_INC  ----- */
