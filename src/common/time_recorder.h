/*
 * =====================================================================================
 *
 *       Filename:  time_record.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  04/20/2014 12:09:54 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef  TIME_RECORDER_INC
#define  TIME_RECORDER_INC

#include <unordered_map>
#include <string>
#include "common/util.h"
#include "common/atomic.h"
#include <ostream>
#include <pthread.h>
#include <cstdio>

using std::string;
using std::unordered_map;
using namespace bsp::atomic;

namespace bsp{namespace common{


class TimeRecorderDumper{
};

ostream& operator<<(ostream& out, TimeRecorderDumper dumper);

class TimeRecord{
public:
	friend ostream& operator<<(ostream& out, TimeRecorderDumper dumper);

	TimeRecord():value(0), call_time(0), start(0), end(0){}

	void timerStart(){
		start = get_time_millis();
		//if(id == 0){
		//	id = pthread_self();
		//}
		//else{
		//	if(id != pthread_self()){
		//		printf("id = %ld, self = %ld\n", id, pthread_self());
		//		assert(id == pthread_self());
		//	}
		//}
	}
	void timerEnd(){
		if(start > 0){
			end = get_time_millis();
			long int add_value = end - start;
			atomic_incre<long int>(&value, add_value);
			start = -1;
			atomic_incre<long int>(&call_time, 1);
		}
	}
private:
	long int value;
	long int call_time;
	long int start;
	long int end;

	//pthread_t id;
};

class TimeRecorder{
public:
	typedef unordered_map<string, TimeRecord>::iterator MI;

	static TimeRecord& getRecord(const char* name){
		MutexGetLock get_lock(&records_lock);		
		return records[name];
	}
	static unordered_map<string, TimeRecord> records;
	static pthread_mutex_t records_lock;
};

class TimeRecorderContext{
public:
	//Depreved: very slow since lots of lock confliction
	//TimeRecorderContext(const char* _n):record(TimeRecorder::getRecord(_n)){
	//	record.timerStart();
	//}

	TimeRecorderContext(TimeRecord& _r):record(_r){
		record.timerStart();
	}

	~TimeRecorderContext(){
		record.timerEnd();
	}
private:
	TimeRecord& record;
};


}}

#endif   /* ----- #ifndef TIME_RECORDER_INC  ----- */
