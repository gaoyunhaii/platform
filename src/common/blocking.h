/*
 * =====================================================================================
 *
 *       Filename:  blocking.h
 *
 *    Description:  Implementation of blocking containers. Most containers is implemented 
 *    				Based on orignal STL containers.
 *
 *        Version:  1.0
 *        Created:  2013年03月13日 20时45分32秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  BLOCKING_INC
#define  BLOCKING_INC

extern "C"{
	#include <pthread.h>
}

#include <exception>
#include <iostream>
#include <vector>
using std::vector;

namespace bsp{
	namespace blocking{
		class EmptyException : public std::exception {
		public:
			const char* what(){
				return "This container is empty";
			}		
			~EmptyException() throw(){
				//std::cout << "unwind exception " << this << std::endl;	
			}
		};

		template <class T>
		class QueueInf{
			virtual void enqueue(T& t) = 0;
			virtual T dequeue() = 0;
		};

		/* 
		 * This class is thread safe
		 * T must support replication opeartions
		 * @todo memory allocator
		 * @todo higher parallel efficiency
		 * @todo seperate take lock and put lock
		 * */

		template <class T>
		struct Node;

		template <class T>
		class SyncQueue : public QueueInf<T>{
		public:
			SyncQueue();
			~SyncQueue();
			void enqueue(T& t);
			T dequeue();
			int getSize();
		private:
			Node<T>* head;
			Node<T>* tail;	
			pthread_mutex_t lock;
			int size;
		};

		template <class T>
		class BlockingQueue : public QueueInf<T>{
		public:
			BlockingQueue();
			~BlockingQueue();
			void enqueue(T& t);
			T dequeue();
			int getSize(){return size;};	
		private:
			Node<T>* head;
			Node<T>* tail;
			pthread_mutex_t lock;
			pthread_cond_t not_empty_cond;
			int size;
			/* 
			 * Without full_cond
			 * This is because there is no maximum 
			 * length
			 * */
		};



		template <class T>
		class EmptyWaitingBlockingQueue : public QueueInf<T>{
		public:
			EmptyWaitingBlockingQueue();
			~EmptyWaitingBlockingQueue();
			void enqueue(T& t);
			T dequeue();
			int getSize(){return size;};	
			void waitForEmpty(int p_size);
		private:

			Node<T>* head;
			Node<T>* tail;

			pthread_mutex_t lock;
			/**
			 * Wait for the queue to be *not* empty
			 */
			pthread_cond_t not_empty_cond;
			/**
			 * Wait for the queue to be empty
			 * */
			pthread_cond_t empty_cond;

			int size;


			/**
			 * When threads waiting for elements exceeds this size, 
			 * the empty event will be fired
			 * */
			int poper_size;

			/**
			 * Number of threads waiting for elements
			 * */
			int blocker_size;
		};

		template <class T>
		class MultiEmptyWaitingBlockingQueue : public QueueInf<T>{
		public:
			MultiEmptyWaitingBlockingQueue(int queue_size);
			~MultiEmptyWaitingBlockingQueue();
			void enqueue(T& t, int id);
			T dequeue(int id);
			int getSize();
			void waitForEmpty(int p_size, int id);
		private:
			vector<EmptyWaitingBlockingQueue<T>*> queues;
		};
	}
}

#endif   /* ----- #ifndef BLOCKING_INC  ----- */
