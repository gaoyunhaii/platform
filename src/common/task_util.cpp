/*
 * =====================================================================================
 *
 *       Filename:  task_util.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  05/16/2014 10:00:04 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include "task_util.h"
#include <iostream>
#include <sstream>
using namespace bsp::common::task;
using std::cerr;
using std::endl;
using std::ostringstream;


void bsp::common::task::exit_with_msg(const char* msg, int exit_no){
	cerr << msg << endl;
	exit(exit_no);
}

void bsp::common::task::exit_with_exception(BaseException& ex, int exit_no){
	ostringstream oss;
	oss << "Task failed because " << ex.what() << endl;
	oss << "stacktrace: " << endl;
	ex.printStackTrace(oss);
	oss << endl;

	exit_with_msg(oss.str().c_str(), exit_no);
}
