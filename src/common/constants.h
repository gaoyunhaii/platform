/*
 * =====================================================================================
 *
 *       Filename:  constants.h
 *
 *    Description: Constants shares in multiple objects 
 *
 *        Version:  1.0
 *        Created:  2013年08月02日 12时12分16秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  CONSTANTS_INC
#define  CONSTANTS_INC

namespace bsp{
	namespace common{
		namespace constants{
			//HDFS job_dir patterns
			const char* const HDFS_DIR_LIB = "lib";
			const char* const HDFS_DIR_INIT = "init";	
			const char* const HDFS_DIR_JOB[] = {
				HDFS_DIR_LIB,
				HDFS_DIR_INIT,
				NULL
			};

			const char* const HDFS_LIB_NAME = "libbspjob.so";
			const char* const HDFS_HEADER_NAME = "user_header.h";


			//struct ControlMessageHeader{
			//	int8_t type;	//0: register, 1: exit 
			//	char task_id[40];
			//};

			const char* const CONF_FILE_COMMON = "common.xml";
			const char* const CONF_FILE_MASTER = "master.xml";
			const char* const CONF_FILE_SLAVE = "slave.xml";

			const char* const DIR_CONF = "conf";

			enum TaskQueryMsgType{
				NORMAL_MSG, END_MSG		
			};
		}
	}
}

#endif   /* ----- #ifndef CONSTANTS_INC  ----- */


