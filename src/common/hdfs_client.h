/*
 * =====================================================================================
 *
 *       Filename:  hdfs_client.h
 *
 *    Description: 	An encapsulation of libhdfs.so 
 *
 *        Version:  1.0
 *        Created:  2013年08月01日 16时50分16秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef  HDFS_CLIENT_INC
#define  HDFS_CLIENT_INC

#include <hdfs.h>
#include "common/sexception.h"
#include "common/rw.h"
using bsp::common::IOException;
using bsp::common::rw::InputStream;
using bsp::common::rw::OutputStream;

namespace bsp{
	namespace common{
		//@ThreadSafe
		class HDFSClient{
		public:
			HDFSClient(const char* _raddr, int _rport):
				remote_addr(_raddr == NULL ? "" : _raddr), remote_port(_rport){}
			void connect() throw(IOException);
			void close() throw(IOException);

			void localToRemote(const char* lpath, const char* rpath) throw(IOException);
			void remoteToLocal(const char* rpath, const char* lpath) throw(IOException);
			void initJobDir(const char* path) throw(IOException);

			hdfsFile openRemoteFile(const char* path, int flags) throw(IOException);
			ssize_t readRemoteFile(hdfsFile file, void* buffer, size_t length) throw(IOException);
			ssize_t writeRemoteFile(hdfsFile file, const void* buffer, size_t length) throw(IOException);
			void flushRemoteFile(hdfsFile file) throw(IOException);
			void closeRemoteFile(hdfsFile file) throw(IOException);
			//TODO add flush to outputstream ? 
		private:
			string remote_addr;
			int remote_port;
			//we need to copy file from local to remote and vice versa	
			hdfsFS remote;
			hdfsFS local;
		};

		class HDFSFileInputStream : public InputStream{
		public:
			HDFSFileInputStream(HDFSClient& _client, const char* filename) throw(IOException):
					client(_client){
				file = client.openRemoteFile(filename, O_RDONLY);
			}

			ssize_t read(void* buffer, size_t max_size) throw(IOException){
				return client.readRemoteFile(file, buffer, max_size);
			}

			void close() throw(IOException){
				client.closeRemoteFile(file);	
			}
		private:
			HDFSClient& client;
			hdfsFile file;
		};

		class HDFSFileOutputStream : public OutputStream{
		public:
			HDFSFileOutputStream(HDFSClient& _client, const char* filename) throw(IOException):
					client(_client){
				file = client.openRemoteFile(filename, O_WRONLY);
			}

			ssize_t write(const void* buffer, size_t max_size) throw(IOException){
				return client.writeRemoteFile(file, buffer, max_size);
			}

			void flush() throw(IOException){
				client.flushRemoteFile(file);
			}

			void close() throw(IOException){
				client.closeRemoteFile(file);
			}
		private:
			HDFSClient& client;
			hdfsFile file;	
		};
	}
}


#endif   /* ----- #ifndef HDFS_CLIENT_INC  ----- */
