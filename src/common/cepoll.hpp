/*
 * =====================================================================================
 *
 *       Filename:  cepoll.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年08月13日 20时15分00秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  CEPOLL_HPP_INC
#define  CEPOLL_HPP_INC

#include <memory>
#include "common/logger.h"
#include "common/util.h"
#include <iostream>
#include "common/cepoll.h"
#include "common/atomic.h"

using std::endl;
using namespace bsp::atomic;
using namespace bsp::common;

namespace bsp{ namespace common{
	template <class T, class T_MANAGER>	
	class EPollServerRunnable : public bsp::multi_thread::Runnable{
	public:
		EPollServerRunnable(EPollServer<T, T_MANAGER>* _server):server(_server){}
		void run();
	private:
		EPollServer<T, T_MANAGER>* server;
	};

	template <class T, class T_MANAGER>
	void EPollServerRunnable<T, T_MANAGER>::run(){
		set_pr_name("epoll_server_", 0);

		while(!server->stopped){
			server->_deal_add_and_remove();

			int nfds = epoll_wait(server->ep_fd, server->events, MAX_SIZE, 1000);
			//printf("epolled = %d------------------------------\n", nfds);
			//cout << "epoll end " << nfds << endl;
			if(nfds < 0){
				LOG4CPLUS_ERROR(Logger::getRoot(), "Failed epoll on " << server->ep_fd << "," << nfds << "," << EINTR << "," << EBADF);
				throw IOException();
			}
			else if(nfds == 0){
				continue;
			}
			else if(nfds > 0){
				struct epoll_event* const events = server->events;

				int re = 0;
				for(int i = 0;i < nfds;++i){
					void* ptr = events[i].data.ptr;

					//printf("ptr = %zx, size = %zd\n", (void*)ptr, server->serv_sock_set.size());
					unordered_set<ServerSocket*>::iterator si = server->serv_sock_set.find((ServerSocket*)ptr);

					if(si != server->serv_sock_set.end()){
						ServerSocket* serv_sock = (ServerSocket*)ptr;

						//new clients
						std::shared_ptr<Socket> conn = serv_sock->accept();
						if(conn->getFd() < 0){
							LOG4CPLUS_ERROR(Logger::getRoot(), "Accept fail");
							return;
						}

						T* t = server->manager->createClient(conn, serv_sock);
						struct epoll_event ep;
						ep.data.ptr = t;
						if(server->wait_write){
							ep.events = EPOLLIN | EPOLLOUT;
						}
						else{
							ep.events = EPOLLIN;
						}

						re = epoll_ctl(server->ep_fd, EPOLL_CTL_ADD, conn->getFd(), &ep);
						if(re < 0){
							LOG4CPLUS_ERROR(Logger::getRoot(), "Failed to add to epoll");
						}
					}
					else{
						T* t = (T*)ptr;
						try{
							if(events[i].events & EPOLLIN){
								server->manager->onRead(t);
							}
							if(events[i].events & EPOLLOUT){
								server->manager->onWrite(t);
							}
							if(events[i].events & EPOLLERR){
								//Maybe error or hang up
								server->manager->onError(t);
							}
							if(events[i].events & EPOLLHUP){
								server->manager->onHup(t);	
							}
						} 
						catch (SocketCloseException& e){
							struct epoll_event del_dummy;
							LOG4CPLUS_INFO(Logger::getRoot(), "CEPoll: Socket closed");
							server->removeClient(t->getFd());
							//int fd = t->getFd();
							//re = epoll_ctl(server->ep_fd, EPOLL_CTL_DEL, fd, &del_dummy); 
							//if(re < 0){
							//	LOG4CPLUS_ERROR(Logger::getRoot(), "delete from epoll fail ");
							//}
						}

						catch(BaseException& e){
							struct epoll_event del_dummy;
							LOG4CPLUS_ERROR(Logger::getRoot(), "epoll error: " << e.what());
							e.printStackTrace();
							server->removeClient(t->getFd());
							//int fd = t->getFd();
							//re = epoll_ctl(server->ep_fd, EPOLL_CTL_DEL, fd, &del_dummy); 
							//if(re < 0){
							//	LOG4CPLUS_ERROR(Logger::getRoot(), "delete from epoll fail ");
							//}
						}
					}
				}	
			}
		}	
	}
}}

template <class T, class T_MANAGER>
bsp::common::EPollServer<T, T_MANAGER>::EPollServer(T_MANAGER* _manager, bool _ww) throw(OSException):
				manager(_manager), 
				ep_fd(0), stopped(false), 
				modify_lock(PTHREAD_MUTEX_INITIALIZER), 
				main_thread(NULL), wait_write(_ww){
	ep_fd = epoll_create(MAX_SIZE);	
	if(ep_fd < 0){
		throw OSException();
	}

	EPollServerRunnable<T, T_MANAGER>* runnable = new EPollServerRunnable<T, T_MANAGER>(this);
	main_thread = new DaemonThread(runnable, false, true);
}

template <class T, class T_MANAGER>
bsp::common::EPollServer<T, T_MANAGER>::~EPollServer(){
	//User should assert thread has stopped there
	delete main_thread;
	main_thread = 0;

	close(ep_fd);
}

template <class T, class T_MANAGER>
void bsp::common::EPollServer<T, T_MANAGER>::start(){
	main_thread->start();
}

template <class T, class T_MANAGER>
void bsp::common::EPollServer<T, T_MANAGER>::stop(){
	stopped = true;

	//wait for it to exit...
	main_thread->join();
}

template <class T, class T_MANAGER>
void bsp::common::EPollServer<T, T_MANAGER>::addServerSocket(ServerSocket* serv_sock){
	MutexGetLock get_lock(&modify_lock);
	serv_socks_to_add.push_back(serv_sock);	
}

template <class T, class T_MANAGER>
void bsp::common::EPollServer<T, T_MANAGER>::removeServerSocket(ServerSocket* serv_sock){
	MutexGetLock get_lock(&modify_lock);
	serv_socks_to_remove.push_back(serv_sock);
}

template <class T, class T_MANAGER>
void bsp::common::EPollServer<T, T_MANAGER>::removeClient(int fd){
	MutexGetLock get_lock(&modify_lock);
	fds_to_remove.push_back(fd);	
}


template <class T, class T_MANAGER>
void bsp::common::EPollServer<T, T_MANAGER>::_deal_add_and_remove(){
	MutexGetLock get_lock(&modify_lock);

	vector<ServerSocket*>::iterator svi = serv_socks_to_add.begin();
	for(;svi != serv_socks_to_add.end();++svi){
		ServerSocket* ss = *svi;

		struct epoll_event ev;
		ev.data.ptr = ss;
		ev.events = EPOLLIN;

		//printf("ss = %zx\n", (void*)ss);

		int re = epoll_ctl(ep_fd, EPOLL_CTL_ADD, ss->getFd(), &ev);
		if(re < 0){
			throw OSException();
		}

		//add this fd to map
		serv_sock_set.insert(ss);
	}
	serv_socks_to_add.clear();

	struct epoll_event del_dummy;
	vector<ServerSocket*>::iterator srvi = serv_socks_to_remove.begin();
	for(;srvi != serv_socks_to_remove.end();++srvi){
		ServerSocket* ss = *srvi;
		int del_fd = ss->getFd();
		int re = epoll_ctl(ep_fd, EPOLL_CTL_DEL, del_fd, &del_dummy);
		if(re < 0){
			throw OSException();
		}

		//remove this fd from map
		serv_sock_set.erase(ss);
	}
	serv_socks_to_remove.clear();


	vector<int>::iterator vi = fds_to_remove.begin();
	for(;vi != fds_to_remove.end();++vi){
		//printf("removed %d\n", *vi);
		int re = epoll_ctl(ep_fd, EPOLL_CTL_DEL, *vi, &del_dummy);
		if(re < 0){
			throw OSException();
		}
	}
	fds_to_remove.clear();
}

#endif   /* ----- #ifndef CEPOLL_INC  ----- */
