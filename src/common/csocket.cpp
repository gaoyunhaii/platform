/*
 * =====================================================================================
 *
 *       Filename:  csocket.cpp
 *
 *    Description:  A encapsulation of unix socket to facilitate future usage
 *
 *        Version:  1.0
 *        Created:  2013年06月23日 22时35分49秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

extern "C"{
	#include <unistd.h>
	#include <arpa/inet.h>
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <strings.h>
	#include <sys/socket.h>
}

#include "common/csocket.h"
#include "common/util.h"
#include <iostream>
#include <sstream>
#include <cstring>
#include <string>

using std::endl;
using std::string;
using std::ostringstream;
using bsp::common::str_addr;
using namespace bsp::multi_thread;


bsp::common::RemoteServerSocket::RemoteServerSocket(const char* addr, int port) throw(IOException){
	int re = 0;

	listenfd = socket(AF_INET, SOCK_STREAM, 0);	
	if(listenfd < 0){
		throw IOException();
	}

	//reuseaddr
	int rep = 1;
	re = setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &rep, sizeof(rep));
	if(re < 0){
		throw IOException();
	}
	//rcvbuf
	//int rcvbuf = 131072;
	//re = setsockopt(listenfd, SOL_SOCKET, SO_RCVBUF, &rcvbuf, sizeof(rcvbuf));
	//if(re < 0){
	//	throw IOException("Failed to set rcvbuf");
	//}

	bzero(&sock_addr, sizeof(sock_addr));
	sock_addr.sin_family = AF_INET;
	inet_pton(AF_INET, addr, &sock_addr.sin_addr);
	sock_addr.sin_port = htons(port);

	re = ::bind(listenfd, (struct sockaddr*)&sock_addr, sizeof(sock_addr));
	if(re < 0){
		throw IOException();
	}

	//If port are picked by random, we'll need to re-get the socket name
	if(port == 0){
		socklen_t addrlen = sizeof(sock_addr);		
		getsockname(listenfd, (struct sockaddr*)&sock_addr, &addrlen);
	}

	//FIXME: get maximum listenQ
	re = listen(listenfd, 200);
	if(re < 0){
		throw IOException();
	}
}

bsp::common::RemoteServerSocket::~RemoteServerSocket(){
	close(listenfd);
}

std::shared_ptr<Socket> bsp::common::RemoteServerSocket::accept() throw (IOException){
	string saddr;
	std::shared_ptr<Socket> ptr(new RemoteConSocket());
	RemoteConSocket* raw_ptr = (RemoteConSocket*)ptr.get();

	//Initially conn->addrlen MUST BE EQUAL TO sizeof(conn->addr)
	raw_ptr->addrlen = sizeof(raw_ptr->addr);

	raw_ptr->fd = ::accept(this->listenfd, (struct sockaddr*)&raw_ptr->addr, &raw_ptr->addrlen);
	if(raw_ptr->fd < 0){
		throw IOException();
	}

	str_addr(saddr, &raw_ptr->addr.sin_addr);
	//std::cout << " assigning " << conn->fd << " to " << saddr << ":" << ntohs(conn->addr.sin_port) << std::endl;
	
	return ptr;		
}

bsp::common::LocalServerSocket::LocalServerSocket(const char* addr) throw(IOException){
	int re = 0;

	listenfd = socket(AF_LOCAL, SOCK_STREAM, 0);	
	if(listenfd < 0){
		throw IOException();
	}

	unlink(addr);

	//reuseaddr
	//int rep = 1;
	//re = setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &rep, sizeof(rep));
	//if(re < 0){
	//	throw IOException();
	//}
	//rcvbuf
	//int rcvbuf = 131072;
	//re = setsockopt(listenfd, SOL_SOCKET, SO_RCVBUF, &rcvbuf, sizeof(rcvbuf));
	//if(re < 0){
	//	throw IOException("Failed to set rcvbuf");
	//}

	bzero(&sock_addr, sizeof(sock_addr));
	sock_addr.sun_family = AF_LOCAL;
	strncpy(sock_addr.sun_path, addr, strlen(addr));

	re = ::bind(listenfd, (struct sockaddr*)&sock_addr, sizeof(sock_addr));
	if(re < 0){
		throw IOException();
	}

	//FIXME: get maximum listenQ
	re = listen(listenfd, 200);
	if(re < 0){
		throw IOException();
	}
}

bsp::common::LocalServerSocket::~LocalServerSocket(){
	close(listenfd);
}

std::shared_ptr<Socket> bsp::common::LocalServerSocket::accept() throw (IOException){
	string saddr;
	std::shared_ptr<Socket> ptr(new LocalConSocket());
	LocalConSocket* raw_ptr = (LocalConSocket*)ptr.get();

	//Initially conn->addrlen MUST BE EQUAL TO sizeof(conn->addr)
	raw_ptr->addrlen = sizeof(raw_ptr->addr);

	raw_ptr->fd = ::accept(this->listenfd, (struct sockaddr*)&raw_ptr->addr, &raw_ptr->addrlen);
	if(raw_ptr->fd < 0){
		throw IOException();
	}

	return ptr;		
}


bsp::common::RemoteSocket::RemoteSocket(const char* saddr, int port) throw(IOException){
	int re = 0;

	fd = socket(AF_INET, SOCK_STREAM, 0);	
	if(fd < 0){
		throw IOException();
	}

	bzero(&serv_addr, sizeof(serv_addr));	
	serv_addr.sin_family = AF_INET;
	inet_pton(AF_INET, saddr, &serv_addr.sin_addr);
	serv_addr.sin_port = htons(port);
	
	re = connect(fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
	if(re < 0){
		throw IOException();
	}
}

bsp::common::RemoteSocket::~RemoteSocket(){
	close(fd);
}

bsp::common::LocalSocket::LocalSocket(const char* saddr) throw(IOException){
	int re = 0;

	fd = socket(AF_LOCAL, SOCK_STREAM, 0);	
	if(fd < 0){
		throw IOException();
	}

	bzero(&serv_addr, sizeof(serv_addr));	
	serv_addr.sun_family = AF_LOCAL;
	strncpy(serv_addr.sun_path, saddr, sizeof(serv_addr));
	
	re = connect(fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
	if(re < 0){
		throw IOException();
	}
}

bsp::common::LocalSocket::~LocalSocket(){
	close(fd);
}
