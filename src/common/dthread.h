/*
 * =====================================================================================
 *
 *       Filename:  dthread.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  05/01/2014 07:46:32 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef  DTHREAD_INC
#define  DTHREAD_INC

extern "C"{
	#include <pthread.h>
}

namespace bsp{namespace multi_thread{

class Runnable{
public:	
	virtual void run() = 0;
};

class Exiter{
public:
	virtual void onexit() = 0;
};


//@not thread safe
class DaemonThread{
public:
	DaemonThread(Runnable*, bool _detach = false, bool _delete_runnable = false, Exiter* exiter = 0);
	~DaemonThread(){};
	void start();
	void stop();
	/* 
	 * @Deprecated
	 * */
	void* join();
	pthread_t getPid(){return this->pid;};
	Runnable* getTask(){return this->task;};
private:
	pthread_t pid;
	Runnable* task;
	bool delete_runnable;
	bool detach;
	Exiter* exiter;
	bool running;

public:
	static int getThreadCount();
	static void joinAllThread();
	static void addOne();
	static void finOne();
private:
	static int thread_count;	
	static pthread_mutex_t thread_count_lock;
	static pthread_cond_t all_thread_end_cond;
};

}}

#endif   /* ----- #ifndef DTHREAD_INC  ----- */
