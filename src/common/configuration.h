/*
 * =====================================================================================
 *
 *       Filename:  configuration.h
 *
 *    Description:  Xml parser and configuration
 *
 *        Version:  1.0
 *        Created:  2013年07月16日 09时36分24秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef  CONFIGURATION_INC
#define  CONFIGURATION_INC

#include "common/sexception.h"
#include <vector>
#include <unordered_map>
#include <sstream>
#include <string>
extern "C"{
	#include <libxml/xmlmemory.h>
	#include <libxml/parser.h>
	#include <libxml/xmlwriter.h>
}
using std::string;
using std::vector;
using std::unordered_map;
using std::ostringstream;
using namespace bsp::common;

namespace bsp{
	namespace common{
		class XMLParser{
		public:
			void parse(const char* filename, unordered_map<string, string>& map) throw(ParseException, IOException);
		private:
			void parseProperty(xmlDocPtr doc, xmlNodePtr node, unordered_map<string, string>& map) throw(ParseException); 
		};

		class XMLWriter{
		public:
			void write(const char* filename, unordered_map<string, string>& map) throw(ParseException, IOException);	
		private:
			void writeProperty(xmlTextWriterPtr& writer, const string& key, const string& value) throw(ParseException);
		};

		class Configuration{
		public:
			/*
			 * Parse common.xml master.xml and slave.xml
			 * */
			Configuration(bool use_default = true);
			~Configuration();

			void getValue(string& value, const char* key, const char* def = NULL) const throw(NotFoundException);
			int getIntValue(const char* key, const int def) const;
			double getDoubleValue(const char* key, const double def) const;
			void getListValue(const char* key, vector<string>& def_rets) const;

			void setValue(const char* key, const char* value);
			void setIntValue(const char* key, int value);
			void setDoubleValue(const char* key, double value);
			void setListValue(const char* key, const vector<string>& value);

			void readFile(const char* file) throw(ParseException, IOException);
			void writeFile(const char* file) throw(ParseException, IOException);

			inline void copyValue(const Configuration& other_conf, const char* other_key, const char* new_key){
				string value;
				other_conf.getValue(value, other_key);
				setValue(new_key, value.c_str());
			}
			
			/* 
			 *
			 * Copying values from another Configuration object.
			 * key_mappings is a array of 2d const char* array indicating each copying items ended 
			 * with {NULL, NULL}.
			 * */
			void copyValues(Configuration& other_conf, const char* key_mappings[][2]);
		private:
			unordered_map<string, string> values;
			ostringstream oss;
			XMLParser parser;
			XMLWriter writer;
		};
	}
}



#endif   /* ----- #ifndef CONFIGURATION_INC  ----- */
