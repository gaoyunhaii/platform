/*
 * =====================================================================================
 *
 *       Filename:  cepoll.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年08月13日 19时34分14秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef  CEPOLL_INC
#define  CEPOLL_INC

extern "C"{
	#include <sys/epoll.h>
	#include <errno.h>
}

#include "common/csocket.h"
#include "common/dthread.h"
#include "common/sexception.h"
#include <vector>
#include <unordered_set>
using namespace bsp::multi_thread;
using std::vector;
using std::unordered_set;
using bsp::common::OSException;

namespace bsp{ namespace common{
	const int MAX_SIZE = 100;

	template <class T, class T_MANAGER>
	class EPollServerRunnable;

	template <class T, class T_MANAGER>
	class EPollServer {
	public:
		EPollServer(T_MANAGER* manger, bool _ww = false) throw(OSException);
		~EPollServer();

		void start();
		void stop();

		void addServerSocket(ServerSocket* serv_sock);
		void removeServerSocket(ServerSocket* serv_sock);
		void removeClient(int fd);

		friend class EPollServerRunnable<T, T_MANAGER>;
	private:
		T_MANAGER* manager;

		int ep_fd;
		bool stopped;
		struct epoll_event events[MAX_SIZE];

		vector<ServerSocket*> serv_socks_to_add;
		vector<ServerSocket*> serv_socks_to_remove;
		vector<int> fds_to_remove;
		pthread_mutex_t modify_lock;

		DaemonThread* main_thread;	
		bool wait_write;

		unordered_set<ServerSocket*> serv_sock_set;
	private:
		void _deal_add_and_remove();
	};
}}


#endif   /* ----- #ifndef CEPOLL_INC  ----- */
