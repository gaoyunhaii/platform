/*
 * =====================================================================================
 *
 *       Filename:  logger.cpp
 *
 *    Description:  utilities to init root logs 
 *
 *        Version:  1.0
 *        Created:  2013年07月20日 10时34分05秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include "common/logger.h"

#include <string>
#include <log4cplus/consoleappender.h>
#include <log4cplus/fileappender.h>
#include "common/file_util.h"

using std::string;
using namespace bsp::common;
using std::cin;

namespace{
	const LogLevel DEFAULT_LOG_LEVEL = DEBUG_LOG_LEVEL;
}

void bsp::common::init_logger(const char* name, const char* log_dir){
	string logger_file;	
	file_join_path(logger_file, log_dir, name, NULL);

	SharedAppenderPtr appender(new DailyRollingFileAppender(logger_file.c_str(), DAILY, true, 10));
	appender->setName(name);

	std::string pattern = "[%D{%Y-%m-%d %H:%M:%S}][%t][%l] %-5p - %m%n";
	std::auto_ptr<Layout> layout(new PatternLayout(pattern));
	appender->setLayout(layout);

	Logger root = Logger::getRoot();
	root.addAppender(appender);
	root.setLogLevel(DEFAULT_LOG_LEVEL);
}


void bsp::common::init_logger_console(const char* name){
	SharedAppenderPtr appender(new ConsoleAppender());
	appender->setName(name);

	std::string pattern = "[%D{%Y-%m-%d %H:%M:%S}][%t][%l] %-5p - %m%n";
	std::auto_ptr<Layout> layout(new PatternLayout(pattern));
	appender->setLayout(layout);

	Logger root = Logger::getRoot();
	root.addAppender(appender);
	root.setLogLevel(DEFAULT_LOG_LEVEL);
}

void bsp::common::close_logger(){
	Logger::shutdown();
}
