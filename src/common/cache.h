/*
 * =====================================================================================
 *
 *       Filename:  cache.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12/27/2013 10:10:30 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  CACHE_INC
#define  CACHE_INC

extern "C"{
	#include <pthread.h>
}

#include <cstdlib>
#include <vector>
#include <cstring>
#include <unordered_set>
using std::vector;
using std::unordered_set;

namespace bsp{namespace common{

class NeverFreeCache{
public:
	NeverFreeCache(size_t _os, size_t rec_seg_size = 104857600 /*  100M  */);
	~NeverFreeCache();

	void* allocAnObj();
	void allocFin();	

	void* locate(size_t i);
	void printSegs(const char* name);
private:
	size_t obj_size; 
	size_t seg_size;
	size_t seg_obj_amount;

	char* next_obj;
	size_t cur_seg_obj_left;

	vector<char*> seg_starts;

	pthread_mutex_t lock;

private:
	
	//For output
	size_t last_seg_size_shrinked;
};


class DiffSizeNeverFreeCache{
public:
	DiffSizeNeverFreeCache(size_t rec_seg_size = 104857600 /*  100M */);
	~DiffSizeNeverFreeCache();

	void* allocAnObj(size_t obj_size);
	void allocFin();
	void printSegs(const char* name);
private:
	size_t seg_size;

	size_t cur_seg_size;
	char* next_obj;
	size_t cur_seg_size_left;
	
	vector<char*> seg_starts;

	pthread_mutex_t lock;

private:
	void _new_seg(size_t obj_size);
	void _shrink_last_seg();

	//For output
	size_t last_seg_size_shrinked;
};


struct StringObj{
	size_t length;	//0 not included
	char content[0];
};

class SimpleStringHash : public std::hash<const char*>{
public:
	size_t operator()(const char* str) const{
		register size_t hash;  
		register const unsigned char *p;  
	  
		for(hash = 0, p = (const unsigned char*)str; *p ; p++){
			hash = 31 * hash + *p;  
		}

		int shift = sizeof(size_t) * 8 - 1;
		size_t mask = ~(1 << shift); // Why can't copy expression of shift here...?
		return (hash & mask);  
	}	
};

class SimpleStringPred : public std::hash<const char*>{
public:
	bool operator()(const char* s1, const char* s2) const{
		if(strcmp(s1, s2) == 0){
			return true;
		}
		return false;
	}
};

class SimpleStringLess : public std::hash<const char*>{
public:
	bool operator()(const char* s1, const char* s2) const{
		if(strcmp(s1, s2) < 0){
			return true;
		}
		return false;
	}
};

class StringPool{
public:
	StringPool(size_t rec_seg_size = 104857600/*  100M */);	
	~StringPool();

	StringObj* allocAnString(const char* input, ssize_t size = -1);
	void allocFin();
	void printSegs(const char* name);
private:
	DiffSizeNeverFreeCache cache;

	unordered_set<const char*, SimpleStringHash, SimpleStringPred> index;
	pthread_rwlock_t index_lock;


	//For output
	size_t last_seg_size_shrinked;
private:
	StringObj* _locate_obj(const char* start){
		return (StringObj*)(start - sizeof(size_t));
	}
};

}}

#endif   /* ----- #ifndef CACHE_INC  ----- */

