/*
 * =====================================================================================
 *
 *       Filename:  thrift_server.hpp
 *
 *    Description:  Implementation of thrift servers
 *
 *        Version:  1.0
 *        Created:  2013年07月18日 14时05分59秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef  THRIFT_SERVER_HPP_INC
#define  THRIFT_SERVER_HPP_INC

#include "common/thrift_server.h"
#include <thrift/concurrency/ThreadManager.h>
#include <thrift/concurrency/PosixThreadFactory.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/server/TServer.h>
#include <thrift/server/TThreadPoolServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TTransportUtils.h>
#include "common/logger.h"
#include "common/atomic.h"

using apache::thrift::concurrency::PosixThreadFactory;
using apache::thrift::protocol::TProtocolFactory;
using apache::thrift::TProcessor;
using apache::thrift::protocol::TBinaryProtocolFactory;
using apache::thrift::server::TServerEventHandler;
using apache::thrift::transport::TSocket;
using apache::thrift::transport::TFramedTransport;
using apache::thrift::protocol::TProtocol;
using apache::thrift::protocol::TBinaryProtocol;

using bsp::multi_thread::DaemonThread;
using bsp::multi_thread::Runnable;
using bsp::atomic::MutexGetLock;


#include <iostream>
using std::cout;
using std::endl;

namespace bsp{
	namespace common{
		class ThriftEventHandler : public TServerEventHandler{
		public:
			ThriftEventHandler(pthread_mutex_t* _lock, pthread_cond_t* _cond, bool* _started)
					:lock(_lock), cond(_cond), started(_started){};
			virtual void preServe();
		private:
			pthread_mutex_t* lock;
			pthread_cond_t* cond;
			bool* started;
		};

		class ThriftServerRunnable : public bsp::multi_thread::Runnable{
		public:
			ThriftServerRunnable(boost::shared_ptr<ThreadManager> _thread_manager, TNonblockingServer* _server):
					thread_manager(_thread_manager), server(_server){};
			void run(){
				thread_manager->start();
				server->serve();
			}
		private:
			boost::shared_ptr<ThreadManager> thread_manager;
			TNonblockingServer* server;	
		};
	}
}

template <class HANDLER, class PROCESSOR>
bsp::common::ThriftServer<HANDLER, PROCESSOR>::ThriftServer(int _port, HANDLER* _handler, int _worker_counter):
		port(_port), worker_count(_worker_counter), started(false){
	boost::shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());
	boost::shared_ptr<HANDLER> handler(_handler);
	boost::shared_ptr<TProcessor> processor(new PROCESSOR(handler));

	//boost::shared_ptr<TServerTransport> serverTransport(new TServerSocket(port));
	//boost::shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());

	thread_manager = ThreadManager::newSimpleThreadManager(worker_count);
	boost::shared_ptr<PosixThreadFactory> threadFactory(new PosixThreadFactory());

	thread_manager->threadFactory(threadFactory);
	//server = new TNonblockingServer(processor, serverTransport, transportFactory,protocolFactory,thread_manager);
	server = new TNonblockingServer(processor, protocolFactory, port, thread_manager);

	pthread_mutex_init(&lock, NULL);
	pthread_cond_init(&started_cond, NULL);
	boost::shared_ptr<TServerEventHandler> event_handler(new ThriftEventHandler(&lock, &started_cond, &started));
	server->setServerEventHandler(event_handler);

	ThriftServerRunnable* runnable = new ThriftServerRunnable(thread_manager, server);
	main_thread = new DaemonThread(runnable, false, true, 0);
}

template <class HANDLER, class PROCESSOR>
bsp::common::ThriftServer<HANDLER, PROCESSOR>::~ThriftServer(){
	if(server){
		delete server;
		server = 0;
	}

	if(main_thread){
		delete main_thread;
		main_thread = 0;
	}
}

template <class HANDLER, class PROCESSOR>
void bsp::common::ThriftServer<HANDLER, PROCESSOR>::start(){
	main_thread->start();
	MutexGetLock get_lock(&lock);
	if(!started){
		LOG4CPLUS_INFO(Logger::getRoot(), "wait for thrift server to start..." << endl);
		pthread_cond_wait(&started_cond, &lock);
	}
}


template <class HANDLER, class PROCESSOR>
void bsp::common::ThriftServer<HANDLER, PROCESSOR>::stop(){
	server->stop();
	thread_manager->stop();
}

template <class HANDLER, class PROCESSOR>
void* bsp::common::ThriftServer<HANDLER, PROCESSOR>::join(){
	void* re = main_thread->join();
	return re;
}

template <class CLIENT>
bsp::common::ThriftClient<CLIENT>::ThriftClient(const char* addr, int port){	
	boost::shared_ptr<TTransport> socket(new TSocket(addr, port));
	transport = boost::shared_ptr<TTransport>(new TFramedTransport(socket));
	boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));
	client = new CLIENT(protocol);
}

template <class CLIENT>
bsp::common::ThriftClient<CLIENT>::~ThriftClient(){
	delete client;
}

template <class CLIENT>
void bsp::common::ThriftClient<CLIENT>::open() throw(TException){
	transport->open();
}

template <class CLIENT>
void bsp::common::ThriftClient<CLIENT>::close() throw(TException){
	transport->close();
}

#endif   /* ----- #ifndef THRIFT_SERVER_HPP_INC  ----- */
