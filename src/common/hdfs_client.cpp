/*
 * =====================================================================================
 *
 *       Filename:  hdfs_client.cpp
 *
 *    Description:  An encapsulation of libhdfs.so 
 *
 *        Version:  1.0
 *        Created:  2013年08月01日 16时50分29秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include "common/logger.h"
#include <iostream>
#include "common/hdfs_client.h"
#include "common/sexception.h"
#include "common/constants.h"
#include "common/file_util.h"
//using std::cout;
using std::endl;
using bsp::common::file_join_path;
using bsp::common::IOException;
using namespace bsp::common::constants;

void bsp::common::HDFSClient::connect() throw(IOException){
	if(remote_addr.size() == 0){
		remote = hdfsConnect(NULL , 0);
	}
	else{
		remote = hdfsConnect(remote_addr.c_str() ,remote_port);
	}

	local = hdfsConnect(NULL, 0);

	if(!remote || !local){
		throw IOException();
	}
}

void bsp::common::HDFSClient::close() throw(IOException){
	int re_remote = 0, re_local = 0;
	if(remote){
		re_remote = hdfsDisconnect(remote);
		remote = NULL;
	}
	if(local){
		re_local = hdfsDisconnect(local);
		local = NULL;
	}
	if(re_remote < 0 || re_local < 0){
		throw IOException();
	}
}

void bsp::common::HDFSClient::localToRemote(const char* lpath, const char* rpath) throw(IOException){
	LOG4CPLUS_INFO(Logger::getRoot(), "copy " << lpath << " from local to remote " << rpath);
	int re = hdfsCopy(local, lpath, remote, rpath);
	if(re < 0){
		throw IOException();
	}
}

void bsp::common::HDFSClient::remoteToLocal(const char* rpath, const char* lpath) throw (IOException){
	LOG4CPLUS_INFO(Logger::getRoot(), "copy " << rpath << " from remote to local " << lpath);
	int re = hdfsCopy(remote, rpath, local, lpath);
	LOG4CPLUS_INFO(Logger::getRoot(), "End copying " << rpath << " from remote to local " << lpath);
	if(re < 0){
		throw IOException();
	}
}

void bsp::common::HDFSClient::initJobDir(const char* path) throw(IOException){
	string abs_path;
	const char* cur;
	int i = 0;
	int re;

	re = hdfsCreateDirectory(remote, path);
	if(re < 0){
		throw IOException();
	}
	
	while((cur = HDFS_DIR_JOB[i])){
		file_join_path(abs_path, path, cur, NULL);
		re = hdfsCreateDirectory(remote, abs_path.c_str());
		if(re < 0){
			throw IOException();
		}
		++i;
	}
}

hdfsFile bsp::common::HDFSClient::openRemoteFile(const char* path, int flags) throw(IOException){
	hdfsFile re = hdfsOpenFile(remote, path, flags, 0, 0, 0);
	if(!re){
		throw IOException();
	}

	return re;
}

ssize_t bsp::common::HDFSClient::readRemoteFile(hdfsFile file, void* buffer, size_t length) throw(IOException){
	tSize re = hdfsRead(remote, file, buffer, (tSize)length);
	if(re < 0){
		throw IOException();
	}

	return (ssize_t)re;
}


ssize_t bsp::common::HDFSClient::writeRemoteFile(hdfsFile file, const void* buffer, size_t length) throw(IOException){
	//cout << (const char*)buffer << "," << length << endl;
	tSize re = hdfsWrite(remote, file, buffer, (tSize)length);
	if(re < 0){
		throw IOException();
	}

	return (ssize_t)re;
}

void bsp::common::HDFSClient::flushRemoteFile(hdfsFile file) throw(IOException){
	int re = hdfsFlush(remote, file);
	if(re < 0){
		throw IOException();
	}
}

void bsp::common::HDFSClient::closeRemoteFile(hdfsFile file) throw(IOException){
	int re = hdfsCloseFile(remote, file);
	if(re < 0){
		throw IOException();
	}
}
