/*
 * =====================================================================================
 *
 *       Filename:  thread_pool.cpp
 *
 *    Description:  Thread pool implementation
 *
 *        Version:  1.0
 *        Created:  2013年03月21日 12时03分51秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include "common/thread_pool.h"
#include "common/blocking.h"
#include "common/blocking.hpp"
#include "common/atomic.h"

#include <iostream>
#include <exception>
#include <cstdio>

using std::cout;
using std::endl;

extern "C"{
	#include <pthread.h>	
	#include <errno.h>
}

using std::exception;
using bsp::blocking::BlockingQueue;
using namespace bsp::multi_thread;

namespace bsp{
	namespace multi_thread{
		class WorkerRunnable : public Runnable{
		public:
			WorkerRunnable(ThreadPool* _pool, int _wid, BlockingQueue<Runnable*>* _queue):
				pool(_pool), wid(_wid), queue(_queue), run_lock(PTHREAD_MUTEX_INITIALIZER){};
			void run();
			void setThread(DaemonThread* _thread){thread = _thread;};
			void cancelIfIdel();
			void cancelNow();
			~WorkerRunnable(){};
		private:
			ThreadPool* pool;
			int wid;
			BlockingQueue<Runnable*>* queue;
			DaemonThread* thread;
			pthread_mutex_t run_lock; //If thread is running, lock run_lock, then pool can tell it with try_lock
			Runnable* getTask();
		};

		class WorkerExiter : public Exiter{
		public:
			WorkerExiter(ThreadPool* _pool, int _wid):
				pool(_pool), wid(_wid){};
			~WorkerExiter(){};
			void onexit();
		private:
			ThreadPool* pool;
			int wid;
		};

		class ExitRunnable : public Runnable{
		public:
			void run(){};
			static Runnable* EXIT_RUNNABLE_POINTER;
		private:
			static ExitRunnable singleton;
		};
	}
}

void bsp::multi_thread::WorkerRunnable::run(){
	while(true){
		//wait for a new task
		Runnable* task = getTask();
		if(task == ExitRunnable::EXIT_RUNNABLE_POINTER){
			pthread_exit((void*)0);
		}
		pthread_mutex_lock(&run_lock);
		pthread_testcancel();
		task->run();
		pthread_mutex_unlock(&run_lock);
	}		
}

void bsp::multi_thread::WorkerRunnable::cancelIfIdel(){
	int re = pthread_mutex_trylock(&run_lock);
	if(re != EBUSY){
		pthread_cancel(thread->getPid());	
		pthread_mutex_unlock(&run_lock);
	}
}

void bsp::multi_thread::WorkerRunnable::cancelNow(){
	pthread_cancel(thread->getPid());
}

Runnable* bsp::multi_thread::WorkerRunnable::getTask(){
	Runnable* task;			
	if(pool->state == bsp::multi_thread::ThreadPool::SHUTDOWN_IMMI || pool->state == bsp::multi_thread::ThreadPool::ABRUPT){
		return ExitRunnable::EXIT_RUNNABLE_POINTER;
	}
	task = queue->dequeue();
	return task;
}

void bsp::multi_thread::WorkerExiter::onexit(){
	pool->onWorkerExit(wid);	
}

ExitRunnable bsp::multi_thread::ExitRunnable::singleton = ExitRunnable();
Runnable* bsp::multi_thread::ExitRunnable::EXIT_RUNNABLE_POINTER = &ExitRunnable::singleton;

//pthread_mutexattr_t test_attr;

bsp::multi_thread::ThreadPool::ThreadPool(int _core_size, int _max_size, BlockingQueue<Runnable*>* _queue)
	:core_size(_core_size), max_size(_max_size), queue(_queue), current_size(0),
		next_wid(1),state(INIT){
	if(core_size <= 0){
		core_size = 1;
	}

	if(max_size < core_size){
		max_size = core_size;
	}	

	workers = new unordered_map<int, DaemonThread*>();
	
	//pthread_mutexattr_init(&test_attr);
	//pthread_mutexattr_setpshared(&test_attr, PTHREAD_MUTEX_ERRORCHECK);
	//pthread_mutex_init(&main_lock, &test_attr);

	pthread_mutex_init(&main_lock, NULL);
	pthread_cond_init(&term_cond, NULL);

	if(!queue){
		queue = new BlockingQueue<Runnable*>();
	}
	
	state = INIT;
}

/*
 * This method has nothing to do with the states of pool itself, thus lock is unnecessary
 * */
void bsp::multi_thread::ThreadPool::submit(Runnable* task){
	MutexGetLock get_lock(&main_lock);
	if(state == RUNNING){
		queue->enqueue(task);
		if(queue->getSize() >= current_size && current_size < max_size){
			DaemonThread* worker = addThread();
			worker->start();
		}
	}
}

void bsp::multi_thread::ThreadPool::start(){
	while(current_size < core_size){
		DaemonThread* worker = addThread();	
		worker->start();
		//printf("%s %d] thread_pool %p start worker\n", __FILE__, __LINE__, this);		
	}

	state = RUNNING;			

}

void bsp::multi_thread::ThreadPool::shutdown(){
	MutexGetLock get_lock(&main_lock);
	//printf("%s %d] on exit shutdown %p\n", __FILE__, __LINE__, this);		

	if(state == SHUTDOWN || state == SHUTDOWN_IMMI || state == ABRUPT){
		pthread_cond_wait(&term_cond, &main_lock);
		return;
	}

	state = SHUTDOWN;
	for(int i = 0;i < current_size;++i){
		queue->enqueue(ExitRunnable::EXIT_RUNNABLE_POINTER);
	}
	//wait for exit;
	pthread_cond_wait(&term_cond, &main_lock);
}

void bsp::multi_thread::ThreadPool::shutdownImmidiately(){
	MutexGetLock get_lock(&main_lock);
	if(state == SHUTDOWN_IMMI || state == ABRUPT){
		pthread_cond_wait(&term_cond, &main_lock);
		return;
	}

	state = SHUTDOWN_IMMI;
	//now notify each worker they can exit now
	//since no one have state_lock, they can't have worker_lock either
	unordered_map<int, DaemonThread*>::iterator mi = workers->begin();
	for(;mi != workers->end();++mi){
		WorkerRunnable* r = dynamic_cast<WorkerRunnable*>(mi->second->getTask());	
		r->cancelIfIdel();
	}
	pthread_cond_wait(&term_cond, &main_lock);
}

void bsp::multi_thread::ThreadPool::shutdownAbruptly(){
	MutexGetLock get_lock(&main_lock);
	if(state == ABRUPT){
		pthread_cond_wait(&term_cond, &main_lock);
		return;
	}

	state = ABRUPT;
	//now notify each worker they can exit now
	//since no one have state_lock, they can't have worker_lock either
	unordered_map<int, DaemonThread*>::iterator mi = workers->begin();
	for(;mi != workers->end();++mi){
		WorkerRunnable* r = dynamic_cast<WorkerRunnable*>(mi->second->getTask());	
		r->cancelNow();
	}
	pthread_cond_wait(&term_cond, &main_lock);
}

void bsp::multi_thread::ThreadPool::onWorkerExit(int wid){
	MutexGetLock get_lock(&main_lock);

	//printf("%s %d] on exit %d\n", __FILE__, __LINE__, wid);		

	unordered_map<int, DaemonThread*>::iterator mi = workers->find(wid);
	if(mi == workers->end()){
		return;
	}
	
	delete mi->second->getTask();
	delete mi->second;

	workers->erase(mi);
	--current_size;
	if(state == RUNNING){
		while(current_size < core_size){ //in fact, if is sufficient
			DaemonThread* worker = addThread();
			worker->start();
		}
	}
	else{// we are shutting down...
		if(current_size == 0){//the last worker clears room
			state = STOPPED;
			pthread_cond_broadcast(&term_cond);
		}
	}
}

DaemonThread* bsp::multi_thread::ThreadPool::addThread(){
	//Lock must be acquired when I'am called		
	DaemonThread* worker;
	WorkerRunnable* worker_task;
	Exiter* exiter;
	
	worker_task = new WorkerRunnable(this, next_wid, queue);
	exiter = new WorkerExiter(this, next_wid);	
	worker = new DaemonThread(worker_task, true, false, exiter);
	worker_task->setThread(worker);
	(*workers)[next_wid] = worker;
	++next_wid;
	++current_size;
	return worker;
}

bsp::multi_thread::ThreadPool::~ThreadPool(){
	delete queue; 
	delete workers;
	pthread_mutex_destroy(&main_lock);
	pthread_cond_destroy(&term_cond);
}
