/*
 * =====================================================================================
 *
 *       Filename:  task_util.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  05/16/2014 09:58:47 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  COMMON_TASK_UTIL_INC
#define  COMMON_TASK_UTIL_INC

#include "sexception.h"
#include "util.h"
using namespace bsp::common;

namespace bsp{namespace common{namespace task{

	inline void gen_task_id(string& result, const string& job_id, int order){
		ostringstream oss;	
		oss << order;
		result.clear();
		result += job_id;
		result += '_';
		result += oss.str();
	}

	inline void de_task_id(string& job_id, int& order, const string& task_id){
		vector<string> re;
		str_tokenize(task_id.c_str(), '_', re);
		job_id = re[0];
		order = atoi(re[1].c_str());
	} 

	void exit_with_msg(const char* msg, int exit_no);
	void exit_with_exception(BaseException& ex, int exit_no);

}}}

#endif   /* ----- #ifndef COMMON_TASK_UTIL_INC  ----- */
