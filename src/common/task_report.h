/*
 * =====================================================================================
 *
 *       Filename:  task_report.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  09/16/2013 04:51:23 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  TASK_REPORT_INC
#define  TASK_REPORT_INC

#include <string>
using std::string;

namespace bsp{namespace common{namespace task{

	const char TASK_REPORT_START = '#';
	const int TASK_REPORT_START_NR = 3;

	const char TR_MASK_EXCEPTION_START = 0x80;
	const char TR_MASK_EXCEPTION_END = 0x40;
	const char TR_MASK_NORMAL = 0x20;
	const char TR_MASK_CONTENT_LINE = 0x10;

	class TaskReport{
	public:
		TaskReport():nr_step(0), nr_vertexes(0), nr_edges(0), flag(0){}
		void to_msg(string& re);
		void from_msg(const char* msg);

		void setNormal(){flag |= TR_MASK_NORMAL;}
		void setExStart(){flag |= TR_MASK_EXCEPTION_START;}
		void setExEnd(){flag |= TR_MASK_EXCEPTION_END;}

		bool isNormal(){return flag & TR_MASK_NORMAL;}
		bool isExStart(){return flag & TR_MASK_EXCEPTION_START;}
		bool isExEnd(){return flag & TR_MASK_EXCEPTION_END;}
		bool isContentLine(){return flag & TR_MASK_CONTENT_LINE;}
	public:
		int nr_step;
		int nr_vertexes;
		int nr_edges;
		string line_content;
	private:
		char flag;
		void setContentLine(){flag |= TR_MASK_CONTENT_LINE;}
	};

}}}

#endif   /* ----- #ifndef TASK_REPORT_INC  ----- */
