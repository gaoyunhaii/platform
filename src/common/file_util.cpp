/*
 * =====================================================================================
 *
 *       Filename:  file_util.cpp
 *
 *    Description:  utils handling files and filenames
 *
 *        Version:  1.0
 *        Created:  2013年07月16日 13时48分47秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include "common/util.h"
#include <string>
#include <cstdarg>
#include <vector>
#include <sstream>
#include "common/logger.h"
#include "common/sexception.h"
#include "common/file_util.h"
#include "common/util.h"
using bsp::common::str_strip;
using std::string;
using std::ostringstream;
using std::endl;

extern "C"{
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <errno.h>
}

#define HAS_MODE(mode, bit) ((mode & bit) == bit)

namespace{
	using bsp::common::FileSystemBadException;

	const mode_t DEFAULT_MODE = S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH;
	void _check_dir(const char* dir, struct stat& state) throw (FileSystemBadException){
		ostringstream exc;	
		//TODO need it no not?
		//uid_t cuser = geteuid();	
		//if(state.st_uid != cuser){
		//	exc << "Directory " << dir << " belongs to " << state.st_uid << " instead of " << cuser << endl;
		//	throw FileSystemBadException(exc.str().c_str());
		//}
		
		if(!HAS_MODE(state.st_mode, S_IRWXU)){
			exc << "Directory " << dir << "is in bad mode instead of 644" << endl;
			throw FileSystemBadException(exc.str().c_str());
		}

	}
}

string bsp::common::file_join_path(const char* base, ...){
	const char* str, *s, *e;
	string result("/");
	va_list ap;
	
	str_strip(base, '/', &s, &e);
	result.append(s, e);

	va_start(ap, base);
	while((str = va_arg(ap, const char*)) != 0){
		str_strip(str, '/', &s, &e);
		if(s && e){
			result.append(1, '/');
			result.append(s, e);
		}
	}
	va_end(ap);

	return result;
}

void bsp::common::file_join_path(string& result, const char* base, ...){
	const char* str, *s, *e;
	result = "/";
	va_list ap;
	
	str_strip(base, '/', &s, &e);
	result.append(s, e);

	va_start(ap, base);
	while((str = va_arg(ap, const char*)) != 0){
		str_strip(str, '/', &s, &e);
		if(s && e){ //If str = "///////..."
			result.append(1, '/');
			result.append(s, e);
		}
	}
	va_end(ap);
}

void bsp::common::file_name(string& re, const char* path){
	re.clear();
	if(!path){
		return;
	}

	const char* p = path;
	for(;*p;++p);
	//now p points to 0 
	for(p = p - 1;p >= path && *p != '/';--p);
	if(p >= path){
		if(*p == '/' && *(p + 1)){
			re = (p + 1);
		}
	}
	else if(p < path){
		re = path;	
	}
}

void bsp::common::file_lib_name(string& re, const char* fn){
	re.clear();

	const char* s = fn + 3;
	const char* e = s;
	for(;*e && *e != '.';++e);
	re.append(s, e);
}

void bsp::common::check_dir(const char* dir) throw (FileSystemBadException){
	int re = 0;
	struct stat state;
	
	re = lstat(dir, &state);
	if(re < 0){
		LOG4CPLUS_WARN(Logger::getRoot(), "Directory " << dir << " doesn't exists, recreate it");
		mkdir(dir, DEFAULT_MODE);
		LOG4CPLUS_WARN(Logger::getRoot(), "Directory " << dir << " created");
	}
	else{
		_check_dir(dir, state);
	}
}

void bsp::common::mk_bsp_dir(const char* dir) throw (FileSystemBadException){
	int re = 0;
	ostringstream exc;
	struct stat state;
	re = lstat(dir, &state);
	if(re < 0){
		re = mkdir(dir, DEFAULT_MODE);
		if(re < 0){
			exc << "Can't create " << dir << endl;
			throw FileSystemBadException(exc.str().c_str());
		}
	}
	else{
		_check_dir(dir, state);	
	}	
}


