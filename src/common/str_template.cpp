/*
 * =====================================================================================
 *
 *       Filename:  str_template.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年08月06日 17时08分29秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include "common/str_template.h"
#include "common/util.h"

using std::istringstream;
using std::getline;
using std::ofstream;
using namespace std;

namespace{
	const char SPLIT_CHAR = '$';
}

//FIXME: escape SPLIT_CHAR
void bsp::common::StrTemplate::instance(ostream& result, const unordered_map<string, string>& params){
	const char* p, *pp;	
	const char* e, *pe;
	const char* lout = temp.c_str();
	const char* end;
	pp = temp.c_str();
	p = pp + 1;
	while(true){
		for(;*p && *pp && !(*p == SPLIT_CHAR && *pp == SPLIT_CHAR);++p, ++pp);
		if(*p && *pp){
			//output [lout, pp);	
			if(pp > lout){
				result.write(lout, pp - lout);
				lout = pp;
			}

			for(pe = p + 1, e = p + 2;*e && *pe && !(*e == SPLIT_CHAR && *pe == SPLIT_CHAR);++e, ++pe);
			if(*e && *pe && pe > p + 1){
				//replace		
				string key;
				string value;
				key.append(p + 1, pe);
				_getParam(value, key, params);			
				result << value;
				//skip $$...$$
				lout = e + 1;
			}
			else{
				//output [lout, e + 1)
				result.write(lout, e + 1 - lout);
				if(*e == 0){
					break;
				}
				lout = e + 1;
			}
			pp = e + 1;
			p = e + 2;
		}
		else{
			//output [lout, p);	
			if(*pp == 0){
				end = pp;
			}
			else{
				end = p;
			}
			if(end > lout){
				result.write(lout, end - lout);
				lout = end;
			}
			break;
		}
	}
	result.flush();
}



void bsp::common::StrTemplate::instance(const char* filename, const unordered_map<string, string>& params){
	ofstream ofs(filename);		
	instance(ofs, params);
	ofs.close();
}

bool bsp::common::StrTemplate::_getParam(string& value, const string& key, const unordered_map<string, string>& params){
	//cout << "searching " << key << endl;
	unordered_map<string, string>::const_iterator mi = params.find(key);
	if(mi != params.end()){
		value = mi->second;
		return true;	
	}
	return false;
}

