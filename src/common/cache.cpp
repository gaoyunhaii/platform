/*
 * =====================================================================================
 *
 *       Filename:  cache.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12/27/2013 10:10:38 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include "common/cache.h"
#include <cstdlib>
#include <cassert>
#include <cstdio>
#include "common/atomic.h"

using namespace bsp::common;
using namespace bsp::atomic;

#include <iostream>
using namespace std;

bsp::common::NeverFreeCache::NeverFreeCache(size_t _os, size_t rec_seg_size):
		obj_size(_os), next_obj(NULL), cur_seg_obj_left(0), lock(PTHREAD_MUTEX_INITIALIZER),
		last_seg_size_shrinked(-1){
	//1. determine seg_size to a multiplies of _os						
	size_t remain = rec_seg_size % obj_size;	
	seg_size = rec_seg_size + (obj_size - remain); //floor
	seg_obj_amount = seg_size / obj_size; //at least 1 by floor
}

void* bsp::common::NeverFreeCache::allocAnObj(){
	MutexGetLock get_lock(&lock);

	if(cur_seg_obj_left == 0){
		next_obj = (char*)malloc(seg_size);
		seg_starts.push_back(next_obj);	
		cur_seg_obj_left = seg_obj_amount;
	}

	char* result = next_obj;	

	next_obj += obj_size;
	cur_seg_obj_left -= 1;
	
	return result;
}

void bsp::common::NeverFreeCache::allocFin(){
	if(!cur_seg_obj_left){
		return;
	}

	char* last_seg_start = seg_starts.back();
	//cout << "1.last seg start = " << (void*)last_seg_start << endl;

	size_t last_new_size = seg_size - cur_seg_obj_left * obj_size;
	//cout << "2.last new size = " << last_new_size << endl;

	last_seg_start = (char*)realloc(last_seg_start, last_new_size);

	//cout << "last seg start = " << (void*)last_seg_start << endl;
	seg_starts.back() = last_seg_start;	

	this->last_seg_size_shrinked = last_new_size;
}

void* bsp::common::NeverFreeCache::locate(size_t i){
	size_t seg_i = i / seg_obj_amount;		
	size_t seg_offset = i % seg_obj_amount;

	return seg_starts[seg_i] + seg_offset * obj_size;
}

bsp::common::NeverFreeCache::~NeverFreeCache(){
	vector<char*>::iterator vi = seg_starts.begin();	
	for(;vi != seg_starts.end();++vi){
		free(*vi);	
	}
}

bsp::common::DiffSizeNeverFreeCache::DiffSizeNeverFreeCache(size_t rec_seg_size):
		seg_size(rec_seg_size), cur_seg_size(0), next_obj(NULL), cur_seg_size_left(0),
		lock(PTHREAD_MUTEX_INITIALIZER), last_seg_size_shrinked(-1){
	//nop
}

void* bsp::common::DiffSizeNeverFreeCache::allocAnObj(size_t obj_size){
	MutexGetLock get_lock(&lock);

	if(cur_seg_size_left == 0){
		_new_seg(obj_size);
	}
	else if(cur_seg_size_left < obj_size){
		_shrink_last_seg();
		_new_seg(obj_size);
	}
		
	void* result = next_obj;		
	next_obj += obj_size;
	cur_seg_size_left -= obj_size;
	return result;
}

void bsp::common::DiffSizeNeverFreeCache::allocFin(){
	if(cur_seg_size_left > 0){
		_shrink_last_seg();
	}
}

bsp::common::DiffSizeNeverFreeCache::~DiffSizeNeverFreeCache(){
	vector<char*>::iterator vi = seg_starts.begin();
	for(;vi != seg_starts.end();++vi){
		free(*vi);
	}
}

void bsp::common::DiffSizeNeverFreeCache::_new_seg(size_t obj_size){
	//cout << "new seg : " << seg_starts.size() << endl;
	cur_seg_size = seg_size;
	if(cur_seg_size < obj_size){
		cur_seg_size = obj_size;
	}

	next_obj = (char*)malloc(cur_seg_size);
	cur_seg_size_left = cur_seg_size;

	seg_starts.push_back(next_obj);
}

void bsp::common::DiffSizeNeverFreeCache::_shrink_last_seg(){
	//printf("cache: %zd, %zd, %zd\n", seg_starts.size(), cur_seg_size, cur_seg_size_left);
	assert(seg_starts.size() > 0);
	assert(cur_seg_size_left >= 0);
	char* new_cur_seg_start = (char*)realloc(seg_starts.back(), cur_seg_size - cur_seg_size_left);	
	seg_starts.back() = new_cur_seg_start;

	this->last_seg_size_shrinked = cur_seg_size - cur_seg_size_left;
}

bsp::common::StringPool::StringPool(size_t rec_seg_size)
		:cache(rec_seg_size), index_lock(PTHREAD_RWLOCK_INITIALIZER){}
bsp::common::StringPool::~StringPool(){}

StringObj* bsp::common::StringPool::allocAnString(const char* input, ssize_t size){
	if(size < 0){
		size = strlen(input);
	}		

	{
		RWLockGetRead get_read(&index_lock);
		unordered_set<const char*, SimpleStringHash, SimpleStringPred>::iterator si = index.find(input);
		if(si != index.end()){
			//found
			return _locate_obj(*si);
		}
	}

	{
		RWLockGetWrite get_write(&index_lock);
		unordered_set<const char*, SimpleStringHash, SimpleStringPred>::iterator si = index.find(input);
		if(si != index.end()){
			//found
			return _locate_obj(*si);
		}

		StringObj* obj = (StringObj*)cache.allocAnObj(sizeof(StringObj) + size + 1); //tailing 0	
		obj->length = size;	
		strncpy(obj->content, input, size + 1);
		index.insert(obj->content);

		return obj;
	}
}

void bsp::common::StringPool::allocFin(){
	cache.allocFin();
}


void bsp::common::NeverFreeCache::printSegs(const char* name){
	cout << "never free cache " << name << ":" << seg_size << " * " << seg_starts.size() << "," << 
			this->last_seg_size_shrinked << endl;
}

void bsp::common::DiffSizeNeverFreeCache::printSegs(const char* name){
	cout << "diffsize never free cache " << name << ":" << seg_size << " * " << seg_starts.size() << "," << 
			this->last_seg_size_shrinked << endl;
}

void bsp::common::StringPool::printSegs(const char* name){
	this->cache.printSegs(name);
}
