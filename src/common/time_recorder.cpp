/*
 * =====================================================================================
 *
 *       Filename:  time_record.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  04/20/2014 12:10:00 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */


#include "common/time_recorder.h"
#include <cassert>
using namespace bsp::common;

unordered_map<string, TimeRecord> bsp::common::TimeRecorder::records = unordered_map<string, TimeRecord>();
pthread_mutex_t bsp::common::TimeRecorder::records_lock = PTHREAD_MUTEX_INITIALIZER;

ostream& bsp::common::operator<<(ostream& out, TimeRecorderDumper dumper){
	MutexGetLock get_lock(&TimeRecorder::records_lock);
	TimeRecorder::MI mi = (TimeRecorder::records).begin();
	for(;mi != (TimeRecorder::records).end();++mi){
		out << "====" << mi->first;
		out << ":" << "\t"; 
		out << (mi->second).value; 
		out << "\t" << (mi->second).call_time << endl;
	}

	return out;
}

