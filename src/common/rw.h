/*
 * =====================================================================================
 *
 *       Filename:  serialize.h
 *
 *    Description: 		  
 *
 *        Version:  1.0
 *        Created:  2013年08月12日 19时08分42秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  RW_INC
#define  RW_INC

extern "C"{
	#include <unistd.h>
}
#include "common/sexception.h"
#include <string>
using std::string;
using namespace bsp::common;

namespace bsp{ namespace common { namespace rw{
	
	class InputStream{
	public:
		virtual void close() throw (IOException) = 0;
		virtual ssize_t read(void* b, size_t max_size)= 0;
	};

	class OutputStream{
	public:
		virtual void flush() throw (IOException) = 0;
		virtual void close() throw (IOException) = 0;	
		virtual ssize_t write(const void* b, size_t size) = 0;
	};

	class FileInputStream : public InputStream{
	public:
		FileInputStream(int _fd):fd(_fd){}
		void close() throw(IOException){
			//FIXME: If something is not created by me, I can't close it
			/*  int re = ::close(fd);
			if(re < 0){
				throw IOException();
			}*/
		}
		ssize_t read(void* b, size_t max_size){
			ssize_t re = ::read(fd, b, max_size);
			return re;
		};
	private:
		int fd;
	};

	class FileOutputStream : public OutputStream{
	public:
		FileOutputStream(int _fd):fd(_fd){}
		void flush() throw (IOException){
			fsync(fd);
		}
		void close() throw(IOException){
			/*  int re = ::close(fd);
			if(re < 0){
				throw IOException();
			}*/
		}
		ssize_t write(const void* b, size_t max_size){
			ssize_t re = ::write(fd, b, max_size);
			return re;
		}
	private:
		int fd;	
	};


	class LineReader{
	public:
		LineReader(InputStream* _input, bool _coe = false, int _buf_size = 2048);
		~LineReader();
		ssize_t readline(string&, ssize_t max_len = -1);
	private:
		InputStream* input;
		char* buffer;
		int buf_size;
		size_t read_pos;
		ssize_t avail;
		bool close_on_exit;
	};

	/* 
	 * Encode basic types and write them into fd;
	 * */
	class ObjectWriter{
	public:
		ObjectWriter(OutputStream* out, bool _coe = false, int _buf_size = 2048);
		~ObjectWriter();
		void flush() throw(SocketCloseException, IOException) ;
		void close() throw(SocketCloseException, IOException) ;
		size_t writeInt(int) throw(SocketCloseException, IOException);	
		size_t writeLongLong(long long) throw(SocketCloseException, IOException);
		size_t writeDouble(double) throw(SocketCloseException, IOException);
		size_t writeString(const char*, ssize_t size = -1) throw(SocketCloseException, IOException);
		size_t writeOutputString(const char*, ssize_t size = -1) throw(SocketCloseException, IOException);
		size_t writeRaw(const char*, ssize_t size) throw(SocketCloseException, IOException);
	private:
		OutputStream* output;
		char* buf;
		char* end;
		/* 
		 * Pointer to first char to write to socket
		 * */
		char* h;
		/* 
		 * Pointer to next char to fill, namely [h, t) is the data
		 * */
		char* t;
		bool close_on_exit;

		bool closed;
	private:
		void _write(const void* tbuf, size_t size) throw(SocketCloseException, IOException);
		void _write_out() throw(SocketCloseException, IOException);

		void _do_close();
		inline void _assert_not_closed(){
			if(closed){
				throw IOException("write on closed stream");
			}
		}
	};

	class ObjectReader{
	public:
		ObjectReader(InputStream* _input, bool _coe = false, int _buf_size = 2048);
		~ObjectReader();
		void close();
		int readInt() throw(SocketCloseException, IOException);
		long long readLongLong() throw(SocketCloseException, IOException);
		double readDouble() throw(SocketCloseException, IOException);
		void readString(string& re) throw(SocketCloseException, IOException);
	private:
		InputStream* input;
		char* buf;
		char* end;
		char* h;
		char* t;	
		bool close_on_exit;

		bool closed;
		bool disabled_buf;
	private:
		void _read(void* buf, size_t size, bool copy = true) throw(SocketCloseException, IOException);
		void _read_in() throw(SocketCloseException, IOException);

		void _do_close();
		inline void _assert_not_closed(){
			if(closed){
				throw IOException("read on closed stream");
			}
		}
	};

}}}

#endif   /* ----- #ifndef SERIALIZE_INC  ----- */
