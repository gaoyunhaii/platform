/*
 * =====================================================================================
 *
 *       Filename:  dthread.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  05/01/2014 07:46:37 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include "common/dthread.h"

#include "common/atomic.h"
#include <iostream>
#include <exception>
#include <cstdio>
#include "common/logger.h"
using namespace bsp::multi_thread;
using namespace bsp::atomic;

using std::cout;
using std::endl;
using std::exception;

namespace bsp{namespace multi_thread{

	struct PtArg{
		Runnable* r;
		Exiter* exiter;
		bool delete_runnable;
		bool detach;
	};

	class ThreadCounter{
	public:
		ThreadCounter(){
			//FIXED: Now we add one before start the new thread.
			//DaemonThread::addOne();	
		};
		~ThreadCounter(){
			DaemonThread::finOne();
		}
	};

	void pthread_cleanup(void* arg){
		PtArg* pt_arg = static_cast<PtArg*>(arg);
		Exiter* exiter = pt_arg->exiter;
		if(exiter){
			exiter->onexit();
			delete exiter;
		}
		if(pt_arg->delete_runnable){
			delete pt_arg->r;
		}
		delete pt_arg;
		//printf("%s %d] %ld %s", __FILE__, __LINE__, pthread_self(), "thread cleanuped\n");		
	}

	void* pthread_wrapper(void* arg){
		PtArg* pt_arg = static_cast<PtArg*>(arg);
		pthread_cleanup_push(pthread_cleanup, pt_arg); //all cleanups happens in pthread_clean
		ThreadCounter counter; //RAII style counter
		if(pt_arg->detach){
			pthread_detach(pthread_self());
		}
		pt_arg->r->run();
		pthread_exit((void*)0); //Make sure pthread_cleanup be called
		pthread_cleanup_pop(0);
		return (void*)0;
	}
}}


bsp::multi_thread::DaemonThread::DaemonThread(Runnable* t, bool _detach, bool _delete_runnable, Exiter* _exiter):
		task(t),delete_runnable(_delete_runnable), detach(_detach), exiter(_exiter), running(false){
}

void bsp::multi_thread::DaemonThread::start(){
	if(running){
		return;
	}

	PtArg* arg = new PtArg();
	arg->r = task;
	arg->exiter = exiter;
	arg->detach = detach;
	arg->delete_runnable = delete_runnable;

	DaemonThread::addOne(); //add 1 before start a new thread
	int re = pthread_create(&pid, 0, pthread_wrapper, arg);
	if(re < 0){
		//printf("Can't create pthread\n");
		DaemonThread::finOne(); //FIXME : Is this right? re < 0 assure no thread starts ?
		throw exception();
	}
	
	running = true;
}

void bsp::multi_thread::DaemonThread::stop(){	
	pthread_cancel(pid);
}

void* bsp::multi_thread::DaemonThread::join(){
	void* re = 0;
	if(!detach && running){
		//LOG4CPLUS_INFO(Logger::getRoot(), pthread_self() << " thread " << pid << " : join");
		pthread_join(pid, &re);
		//LOG4CPLUS_INFO(Logger::getRoot(), "thread " << pid << " exited");
		//printf("%s %d] %ld exited with %ld\n", __FILE__, __LINE__, pthread_self(), re);		
		//fflush(stdout);
		running = false;
	}
	return re;
}

/**For main thread to wait all DaemonThread objects  */
int bsp::multi_thread::DaemonThread::thread_count = 0;
pthread_mutex_t bsp::multi_thread::DaemonThread::thread_count_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t bsp::multi_thread::DaemonThread::all_thread_end_cond = PTHREAD_COND_INITIALIZER;
int bsp::multi_thread::DaemonThread::getThreadCount(){
	return thread_count;
}
void bsp::multi_thread::DaemonThread::joinAllThread(){
	MutexGetLock get_lock(&thread_count_lock);
	if(thread_count > 0){
		//printf("%s %d] %s", __FILE__, __LINE__, "going to wait\n");		
		pthread_cond_wait(&all_thread_end_cond, &thread_count_lock);
	}
}
void bsp::multi_thread::DaemonThread::addOne(){
	MutexGetLock get_lock(&thread_count_lock);
	++thread_count;
}

void bsp::multi_thread::DaemonThread::finOne(){
	MutexGetLock get_lock(&thread_count_lock);
	--thread_count;
	if(thread_count == 0){
		//printf("%s %d] %s", __FILE__, __LINE__, "All is end\n");		
		pthread_cond_broadcast(&all_thread_end_cond);
	}
}

