/*
 * =====================================================================================
 *
 *       Filename:  thread_local.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  05/05/2014 10:17:46 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  THREAD_LOCAL_INC
#define  THREAD_LOCAL_INC

#include <pthread.h>

namespace bsp{namespace multi_thread{

typedef void (*TLDestroyFunc)(void*);

template <class T>
void delete_destroy(void*);

template <class T>
class ThreadLocal{
public:
	ThreadLocal(TLDestroyFunc func = delete_destroy<T>);
	~ThreadLocal();

	T* get();
	void set(T* t);
private:
	pthread_key_t key;
};

}}

#endif   /* ----- #ifndef THREAD_LOCAL_INC  ----- */
