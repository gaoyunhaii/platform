/*
 * =====================================================================================
 *
 *       Filename:  logger.h
 *
 *    Description:  glogs
 *
 *        Version:  1.0
 *        Created:  2013年07月20日 10时33分50秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
#include <log4cplus/configurator.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/helpers/stringhelper.h>

using namespace log4cplus;
using namespace log4cplus::helpers;

namespace bsp{
	namespace common{
		void init_logger(const char* name, const char* log_dir);
		void init_logger_console(const char* name);

		void close_logger();
	}
}
