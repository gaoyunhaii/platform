/*
 * =====================================================================================
 *
 *       Filename:  error_report.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  09/15/2013 04:26:56 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include "common/task_report.h"
#include <cstdio>
using namespace bsp::common::task;

void bsp::common::task::TaskReport::to_msg(string& re){
	re.clear();
	re.append(TASK_REPORT_START_NR, TASK_REPORT_START);

	char buf[1024];
	snprintf(buf, 1024, "%c %d %d %d\n", flag, nr_step, nr_vertexes, nr_edges);

	re += buf;
}

void bsp::common::task::TaskReport::from_msg(const char* msg){
	bool is_content_line = false;	
	const char* start = msg;

	for(int i = 0;i < TASK_REPORT_START_NR;++i){
		if(*start != TASK_REPORT_START){
			is_content_line = true;	
			break;
		}

		++start;
	}

	start = msg + TASK_REPORT_START_NR;

	if(is_content_line){
		setContentLine();	
		line_content = msg;
	} else{
		sscanf(start, "%c %d %d %d\n", &flag, &nr_step, &nr_vertexes, &nr_edges);
	}
}
