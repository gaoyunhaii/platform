/*
 * =====================================================================================
 *
 *       Filename:  thread_group.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  06/22/2014 11:10:54 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  COMMON_THREAD_GROUP_INC
#define  COMMON_THREAD_GROUP_INC

#include "common/dthread.h"
#include <vector>

namespace bsp{namespace multi_thread{

class IndexedRunnable : public bsp::multi_thread::Runnable{
public:
	IndexedRunnable(int _i):index(_i), stopped(false){}

	int getIndex() const{
		return index;
	}

	void markStop(){
		stopped = true;
	}

	bool isStopped() const{
		return stopped;
	}

	virtual void run() = 0;
private:
	int index;
	bool stopped;
};


template <class IR>
class ThreadGroup{
public:
	ThreadGroup(int _size);
	~ThreadGroup();

	void start();
	void markStop();
	void joinAll();

	size_t getSize(){
		return runnables.size();
	}

	IR* getRunnable(int index){
		return runnables[index];
	}
private:
	std::vector<IR*> runnables;
	std::vector<DaemonThread*> threads;
};

}}

#endif   /* ----- #ifndef COMMON_THREAD_GROUP_INC  ----- */
