/*
 * =====================================================================================
 *
 *       Filename:  sssp.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年11月05日 17时00分31秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include "system/bsp.hpp"
#include <mpi.h>
#include <limits>
#include <gperftools/profiler.h>
#include <gperftools/heap-profiler.h>

using namespace bsp::runtime;
using namespace bsp::runtime::synchronize;
using bsp::runtime::syn_priority::PriorityDriver;

float MAX = 0.001;
int max_turn = 0;
long long changed = 0;

const float DELTA = 0.0001;

class PageRankVertexValue {
public:
    PageRankVertexValue() :
            old_value(0), new_value(0), nei_value(0), c_c(-1), cc_turn(-1) { }

public:
    float old_value;
    float new_value;
    float nei_value;

    float c_c;
    int cc_turn;
    bool has_unsent;
};

class PageRankMessageValue {
public:
    size_t getSize() const {
        return sizeof(double);
    }

    void serialize(BoundedBuffer &bb, size_t size) const {
        bb << value;
    }

    void deserialize(BoundedBuffer &bb, size_t size) {
        bb >> value;
    }

public:
    double value;
};


class PageRankNotifyValue {
public:
    PageRankNotifyValue() : value(0) { }

    float value;
};

template<class TypeHinter>
class PageRankProgram : public bsp::runtime::syn_priority::DefaultPriorityProgram<TypeHinter>{
public:
    typedef typename TypeHinter::MessageValue MV;
    typedef typename TypeHinter::BufferValue BV;
    typedef typename TypeHinter::Provider PROVIDER;
    typedef MessageBuffer<TypeHinter> MESSAGE_BUFFER;
    typedef Sender<TypeHinter> SENDER;
    typedef typename TypeHinter::Executor EXECUTOR;

    void send_out_messages(Vertex<TypeHinter> *next, PROVIDER &provider, MESSAGE_BUFFER &buffer, SENDER &sender,
                           int channel_index) {
        PageRankVertexValue &vv = next->getMutableValue();

        //find my neighbors
        unordered_set<int> &workers = provider.getOutWorkers(next->getInnerId());

        VertexInfo *v_info = provider.getVertexInfoByInnerId(next->getInnerId());
        int nr_neighbor = v_info->getNeighborCount();
        if(nr_neighbor == 0){
            return;
        }

        double value = (vv.new_value - vv.old_value) / nr_neighbor;
        _TRACEE("----send: %ld send %f, %f, %f, %f\n", next->getId(), value, vv.new_value, vv.old_value, vv.nei_value);

        unordered_set<int>::iterator si = workers.begin();
        for (; si != workers.end(); ++si) {
            int to = *si;
            PageRankMessageValue mess;
            mess.value = value;
            NormalMessage<PageRankMessageValue> *normalMessage = new NormalMessage<PageRankMessageValue>(next->getId(),
                                                                                                         mess);
            sender.asyncSendMessage(normalMessage, to, channel_index);
        }

        vector<int> &inner_index = v_info->inner_edges;
        if (inner_index.size() > 0) {
            PageRankMessageValue mess;
            mess.value = value;
            buffer.onNewLocalMessage(*next, mess, channel_index);

//            int to = sender.getRank();
//            PageRankMessageValue mess;
//            mess.value = value;
//            NormalMessage<PageRankMessageValue> *normalMessage = new NormalMessage<PageRankMessageValue>(next->getId(),
//                                                                                                         mess);
//            sender.asyncSendMessage(normalMessage, to, channel_index);
        }
    }

    virtual bool isNotify() {
        return true;
    }

    virtual bool isHeadFirst(){
        return true;
    }

    virtual void onNewRemoteNotifyMessage(NotifyMessage<TypeHinter>& notify_message, const VertexId& vid, const MV& mv, InEdge<TypeHinter>* in_edge){
        notify_message.nv.value += mv.value;
        //notify_message.nv.change_value += c;
    }
    virtual void onLocalNotifyMessage(NotifyMessage<TypeHinter>& notify_message, const MV& mv, const Vertex<TypeHinter>* vertex, InnerEdge<TypeHinter>* inner_edge){
        notify_message.nv.value += mv.value;
        //notify_message.nv. += c;
    }

    virtual void onNewBufferMessage(const VertexId &vid, const MV &message, BV &value) {
        value += (BV) message.value;
    }

    //distinguis full update and partial update
    virtual void partialUpdateVertex(Vertex<TypeHinter>* next, EXECUTOR& executor, int channel_index){
        PROVIDER &provider = executor.getProvider();
        MESSAGE_BUFFER &buffer = executor.getMessageBuffer();
        SENDER &sender = executor.getSender();
        NewNotifier<TypeHinter>& notifier = executor.getNewNotifier();

        PageRankVertexValue &vv = next->getMutableValue();

        if (executor.getTurn() == 0) {//first turn;
            //send_out_messages(next, provider, buffer, sender, channel_index);
            return;
        }

        if(vv.cc_turn == executor.getTurn() - 1){
            vv.nei_value += vv.c_c;
            if(vv.new_value != vv.old_value){
                vv.new_value += vv.c_c;
            }
            else{
                vv.new_value = vv.old_value * 0.2 + vv.nei_value;
            }
        }

//        else{
//            NotifierIterator<TypeHinter> *ni = notifier.getNotifierIterator(next->getInnerId(), executor.getTurn() - 1);
//            NotifyMessage<TypeHinter> *remote_message = ni->remoteGetNext();
//            for (; remote_message != NULL; remote_message = ni->remoteGetNext()) {
//                vv.new_value += 0.8 * remote_message->nv.value;
//            }
//
//            NotifyMessage<TypeHinter> *local_message = ni->localGetNext();
//            for (; local_message != NULL; local_message = ni->localGetNext()) {
//                vv.new_value += 0.8 * local_message->nv.value;
//            }
//            delete ni;
//        }

        //count the change
        //add changed
        float c = vv.new_value - vv.old_value >= 0 ? vv.new_value - vv.old_value : vv.old_value - vv.new_value;
        //float c = vv.c_c >= 0 ? vv.c_c : -vv.c_c;
        atomic_incre<long long>(&changed, (long long)(c * 1.0 / MAX * 1000));

        //next->setHalt(true);
    }

    virtual void broadcastMessages(Vertex<TypeHinter>* next, EXECUTOR& executor, int channel_index){
        _TRACEE("%s do send for %ld\n", RED, next->getId());
        PROVIDER &provider = executor.getProvider();
        MESSAGE_BUFFER &buffer = executor.getMessageBuffer();
        SENDER &sender = executor.getSender();

        PageRankVertexValue &vv = next->getMutableValue();
        if(vv.new_value != vv.old_value){
            send_out_messages(next, provider, buffer, sender, channel_index);
            vv.old_value = vv.new_value;
        }
    }

    //mapping vertex to the bucket
    virtual BucketId getBucketId(Vertex<TypeHinter>* next, EXECUTOR& executor){
        //instead, we have to see how much the neighbors change in this iterationto decide if i am suitable to be changed
        NewNotifier<TypeHinter>& notifier = executor.getNewNotifier();
        int turn = executor.getTurn();
        PageRankVertexValue &vv = next->getMutableValue();

        float cc = 0;
        _TRACEE("%ld cc turn = %d, turn = %d\n", next->getId(), vv.cc_turn, turn);
        if(vv.cc_turn == turn){
            cc = vv.c_c;
        }
        else{ //or else we instead do the computation
            _TRACEE("----aggre for %ld\n", next->getId());
            NotifierIterator<TypeHinter> *ni = notifier.getNotifierIterator(next->getInnerId(), executor.getTurn(), true);
            NotifyMessage<TypeHinter> *remote_message = ni->remoteGetNext();
            for (; remote_message != NULL; remote_message = ni->remoteGetNext()) {
                cc += 0.8 * remote_message->nv.value;
                remote_message->nv.value = 0;
            }

            NotifyMessage<TypeHinter> *local_message = ni->localGetNext();
            for (; local_message != NULL; local_message = ni->localGetNext()) {
                cc += 0.8 * local_message->nv.value;
                local_message->nv.value = 0;
            }
            delete ni;

            vv.c_c = cc;
            vv.cc_turn = executor.getTurn();
        }
        //then see how much change we have
        float diff = vv.c_c + vv.new_value - vv.old_value;
        if(diff < 0){
            diff = -diff;
        }
        _TRACEE("----%ld has cc %f, %f - %f, %f, %f\n", next->getId(), cc, vv.new_value, vv.old_value, vv.nei_value, diff);

        if(diff > DELTA){
            return 0;
        }
        else{
            return END_BUCK;
        }
    }

    virtual bool beforeTurn(EXECUTOR& executor){
        changed = 0;
    }

    virtual bool afterTurn(EXECUTOR& executor, MPI_Comm& comm){
        if(executor.getTurn() <= 1){
            return 1;
        }

        //then we summarize the changed
        long long allChanged = 0;
        MPI_Allreduce(&changed, &allChanged, 1, MPI_LONG_LONG, MPI_SUM, comm);
        _TRACEE("changed = %f, all_changed = %f\n", (float)changed, (float)allChanged);
        if(allChanged > (long long)(1000)){
            return 1;
        }

        return 0;
    }
};


class PageRankTypeHinter : public bsp::runtime::syn_priority::DefaultPriorityTypeHinter<PageRankTypeHinter> {
public:
    typedef PageRankVertexValue VertexValue;
    typedef float EdgeValue;
    typedef PageRankMessageValue MessageValue;
    typedef PageRankNotifyValue NotifyValue;
    typedef PageRankProgram<PageRankTypeHinter> Program;
};


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:
 * =====================================================================================
 */
int main(int argc, char *argv[]) {
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    if (provided < MPI_THREAD_MULTIPLE) {
        _TRACEE("MPI version is bad\n");
        exit(1);
    }

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

//    oss.str("");
//    oss << "/tmp/platform/mem_" << rank;
//    HeapProfilerStart(oss.str().c_str());

    PriorityDriver<PageRankTypeHinter> driver(argv[1]);
    //output is argv[2], skip
    max_turn = atoi(argv[3]);

    //init
    AdjLineParser<PageRankTypeHinter> adjLineParser;
    FileInput<PageRankTypeHinter> file_input(adjLineParser);

    long input_start = get_time_millis();
    driver.input_data(file_input);
    long input_end = get_time_millis();

    _TRACEE("input time = %ld\n", (input_end - input_start));

    //now run some programs
    PageRankProgram<PageRankTypeHinter> program;

    Provider<PageRankTypeHinter> &provider = driver.getProvider();
    PriorityManager<PageRankTypeHinter> priority_manager(provider, program);
    VertexList<PageRankTypeHinter> &partial = priority_manager.getPartialList();
    //provider.dump();

    //now we init their initialize value
    int total = provider.totalVertices();
    _TRACEE("total vertices = %d\n", total);

    vector<Vertex<PageRankTypeHinter> *> &vertices = provider.getVertices();
    vector<Vertex<PageRankTypeHinter> *>::iterator vi = vertices.begin();
    for (; vi != vertices.end(); ++vi) {
        PageRankVertexValue &vv = (*vi)->getMutableValue();
        vv.old_value = 0;
        vv.new_value = 1.0 / total;

        //_TRACEE("%d, old = %f, new = %f\n", (*vi)->getId(), vv.old_value, vv.new_value);
        if(vv.new_value > DELTA){
            priority_manager.addToInitBucket(*vi);
        }
    }

    long run_start = get_time_millis();

    ostringstream oss;
    oss << "/tmp/platform/profile_" << rank;
    ProfilerStart(oss.str().c_str());

    driver.run_program(program, priority_manager);

    ProfilerStop();

    long run_end = get_time_millis();

    _TRACEE("run time = %ld\n",  (run_end - run_start));

    //get our values
    vi = vertices.begin();
    for (; vi != vertices.end(); ++vi) {
        Vertex<PageRankTypeHinter> *vertex = *vi;
        _TRACEE("%ld %f\n", vertex->getId(), vertex->getMutableValue().new_value);
    }

//    HeapProfilerStop();

    MPI_Finalize();
    return 0;
}                /* ----------  end of function main  ---------- */
