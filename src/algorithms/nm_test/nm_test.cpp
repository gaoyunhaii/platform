/*
 * =====================================================================================
 *
 *       Filename:  nm_client.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/01/2015 06:53:11 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include <cstdlib>
#include <mpi.h>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <iostream>

#include <vector>
#include <time.h>
#include <cmath>
#include <unistd.h>

using namespace std;

inline long long get_time_us(){
	struct timespec t;	
	clock_gettime(CLOCK_REALTIME, &t);
	return t.tv_sec * 1000000 + t.tv_nsec / 1000;
}


struct StatResult{
	double avg;
	double std;
};


StatResult do_stat(const vector<double>& input){
	StatResult sr;

	double sum = 0;
	vector<double>::const_iterator vi = input.begin();
	for(;vi != input.end();++vi){
		sum += *vi;
	}

	sr.avg = sum / input.size();

	sum = 0;
	for(vi = input.begin();vi != input.end();++vi){
		sum += (*vi - sr.avg) * (*vi - sr.avg);
	}

	sr.std = sqrt(sum / (input.size() - 1));

	return sr;
}


void do_server(int each_size, int count){// my rank = 0
	char* buf = new char[each_size + 1]();

	MPI_Status status;
	
	for(int i = 0;i < count;++i){
		MPI_Recv(buf, each_size, MPI_CHAR, 1, 0, MPI_COMM_WORLD, &status);

		//send it back
		MPI_Send(buf, each_size, MPI_CHAR, 1, 0, MPI_COMM_WORLD);
	}

	delete buf;
}


void do_client(int each_size, int count){ // my rank = 1
	char* buf = new char[each_size + 1]();
	char* recv_buf = new char[each_size + 1]();
	
	for(int i = 0;i < each_size;++i){
		buf[i] = (char)i;
	}

	vector<long> start_tss;
	vector<long> end_tss;
	vector<long> end_recv_tss;

	MPI_Status status;
	
	for(int i = 0;i < count;++i){
		long start = get_time_us();
		MPI_Send(buf, each_size, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
		long end = get_time_us();

		MPI_Recv(buf, each_size, MPI_CHAR, 0, 0, MPI_COMM_WORLD, &status);
		long end_recv = get_time_us();


		start_tss.push_back(start);
		end_tss.push_back(end);
		end_recv_tss.push_back(end_recv);
	}

	delete buf;
	delete recv_buf;
	
	ofstream of("/tmp/nm_mpi_test");

	vector<double> delay;
	for(int i = 0;i < count;++i){
		delay.push_back((double)end_tss[i] - (double)start_tss[i]);
		//cout << delay[i] << endl;
	}

	StatResult sr = do_stat(delay);
	of << sr.avg << "\t" << sr.std << "\t";

	delay.clear();	
	for(int i = 0;i < count;++i){
		delay.push_back((double)end_recv_tss[i] - (double)start_tss[i]);
	}
	sr = do_stat(delay);
	of << sr.avg << "\t" << sr.std << "\t" << endl;
	
	of.close();	
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] ) {
	MPI_Init(&argc, &argv);	

	int each_size = atoi(argv[1]);			
	int count = atoi(argv[2]);

	int myrank = 0;
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

	if(myrank == 0){// I am the server
		do_server(each_size, count);			
	}	
	else if(myrank == 1){//I am the client
		do_client(each_size, count);	
	}

	sleep(100);	

	MPI_Finalize();
	return 0;
}				/* ----------  end of function main  ---------- */
