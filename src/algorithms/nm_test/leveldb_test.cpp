//
// Created by owner-pc on 10/19/16.
//

#include <cassert>
#include <leveldb/db.h>
#include <iostream>

int main(){
    leveldb::DB* db;
    leveldb::Options options;
    options.create_if_missing = true;

    leveldb::Status status = leveldb::DB::Open(options, "/tmp/testdb", &db);
    assert(status.ok());

    std::string value;
    std::string key_1("key_1");
    leveldb::Status s = db->Get(leveldb::ReadOptions(), key_1, &value);
    if(s.ok()){
        std::cout << "value = " << value << std::endl;
    }
    else{
        value = "hehe";
        db->Put(leveldb::WriteOptions(), key_1, value);
    }

    delete db;
    return 0;
}
