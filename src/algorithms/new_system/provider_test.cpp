/*
 * =====================================================================================
 *
 *       Filename:  test.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/10/2015 04:04:31 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include "system/bsp.hpp"
#include <mpi.h>
#include "common/util.h"
#include "common/time_recorder.h"
#include <gperftools/profiler.h>
#include <gperftools/heap-profiler.h>

using namespace bsp::runtime;
using namespace bsp::runtime::synchronize;
using namespace bsp::common;

class VertexValue {
public:
    float v;
};

class EdgeValue {
public:
    EdgeValue(double _v) : v(_v) { }

public:
    float v;
};

template<class TypeHinter>
class Program {
public:
    void dump() { }
};

class TypeHinter : public bsp::runtime::synchronize::DefaultTypeHinter<TypeHinter> {
public:
    typedef ::VertexValue VertexValue;
    typedef ::EdgeValue EdgeValue;
    typedef ::Program<TypeHinter> Program;
};


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    TimeRecorder tr;

    Driver <TypeHinter> driver(argv[1]);
    AdjLineParser <TypeHinter> adjLineParser;
    FileInput <TypeHinter> file_input(adjLineParser);

    tr.getRecord("input").timerStart();
    driver.input_data(file_input);
    tr.getRecord("input").timerEnd();

    Provider <TypeHinter> &provider = driver.getProvider();
    //provider.dump();

    //now we tried the speed of querying the provider
    ValueContainer <TypeHinter> *vc = new ValueContainer<TypeHinter>(provider);

    //now we insert all
    tr.getRecord("first_input").timerStart();
    unordered_map<VertexId, vector<int> > &in_edge_map = provider.getInEdgeMap();
    unordered_map<VertexId, vector<int> >::iterator mi = in_edge_map.begin();
    for (; mi != in_edge_map.end(); ++mi) {
        float *bv = vc->allocAnObj();
        *bv = (float) mi->first;
        vc->insertValue(mi->first, bv);
    }

    {
        vector<Vertex<TypeHinter> *> &vertices = provider.getVertices();
        vector<Vertex<TypeHinter> *>::iterator tvi = vertices.begin();
        for (; tvi != vertices.end(); ++tvi) {
            float *bv = vc->allocAnObj();
            *bv = (float) ((*tvi)->getId());
            vc->insertValue((*tvi)->getId(), bv);
        }
    }
    tr.getRecord("first_input").timerEnd();
    bsp::common::print_free_mem();
    _TRACEE("first value container .size = %ld\n", vc->getValuesById().size());

    tr.getRecord("clear").timerStart();
    vc->reset();
    _TRACEE("after reset, mem = ");
    bsp::common::print_free_mem();
    tr.getRecord("clear").timerEnd();

    //now we reinsert all
    tr.getRecord("second_input").timerStart();
    mi = in_edge_map.begin();
    for (; mi != in_edge_map.end(); ++mi) {
        float *bv = vc->allocAnObj();
        *bv = (float) mi->first;
        vc->insertValue(mi->first, bv);
    }

    {
        vector<Vertex<TypeHinter> *> &vertices = provider.getVertices();
        vector<Vertex<TypeHinter> *>::iterator tvi = vertices.begin();
        for (; tvi != vertices.end(); ++tvi) {
            float *bv = vc->allocAnObj();
            *bv = (float) ((*tvi)->getId());
            vc->insertValue((*tvi)->getId(), bv);
        }
    }
    tr.getRecord("second_input").timerEnd();
    bsp::common::print_free_mem();
    _TRACEE("second value container .size = %ld\n", vc->getValuesById().size());


    ostringstream oss;
    oss << "/tmp/platform/profile_" << rank;
    ProfilerStart(oss.str().c_str());

    oss.str("");
    oss << "/tmp/platform/mem_" << rank;
    HeapProfilerStart(oss.str().c_str());

    tr.getRecord("compute").timerStart();
    int handled = 0;
    int handled_add = 0;

    vector<Vertex<TypeHinter> *> &vertices = provider.getVertices();
    vector<Vertex<TypeHinter> *>::iterator tvi = vertices.begin();
    for (; tvi != vertices.end(); ++tvi) {
        ++handled;

        //here we try to access all its edges
        vector<InnerEdge<TypeHinter> *> &inner_edges = provider.getInnerEdges();
        vector<InEdge<TypeHinter> *> &in_edges = provider.getInEdges();

        VertexInfo *v_info = provider.getVertexInfoByInnerId((*tvi)->getInnerId());
        VertexValue vv = (*tvi)->getMutableValue();

        vector<int>::iterator vi = v_info->inner_edges.begin();
        for (; vi != v_info->inner_edges.end(); ++vi) {
            InnerEdge<TypeHinter> *edge = inner_edges[*vi];
            const VertexId &vid = edge->getSrc()->getId();
            float *old_value = vc->findValue(vid);
            if (old_value != NULL) {
                vv.v += *old_value;
                ++handled_add;
            }


//            if(old_value == NULL){
//                _TRACEE("error: inner edge %ld is null\n", vid);
//                continue;
//            }
//            vv.new_value += *old_value;
        }

        vi = v_info->in_edges.begin();
        for (; vi != v_info->in_edges.end(); ++vi) {
            InEdge<TypeHinter> *edge = in_edges[*vi];
            const VertexId &vid = edge->getSrc();
            float *old_value = vc->findValue(vid);
            if (old_value != NULL) {
                vv.v += *old_value;
                ++handled_add;
            }


//            float *old_value = buffer.findRemoteOldValue(vid);
//            if(old_value == NULL){
//                _TRACEE("error: in edge %ld is null\n", vid);
//                continue;
//            }
//
//            vv.new_value += *old_value;
        }
    }

    tr.getRecord("compute").timerEnd();
    ProfilerStop();
    HeapProfilerStop();
    _TRACEE("handled = %d,handled_add = %d\n", handled, handled_add);

    cout << TimeRecorderDumper() << endl;

    //now run some programs

    MPI_Finalize();
    return 0;
}                /* ----------  end of function main  ---------- */
