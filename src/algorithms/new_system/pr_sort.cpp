/*
 * =====================================================================================
 *
 *       Filename:  pr_sort.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/10/2015 04:04:31 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include "system/bsp.hpp"
#include <mpi.h>
using namespace bsp::runtime;
using namespace bsp::runtime::synchronize;

class PRSortVertexValue{
public:
    PRSortVertexValue() :
            old_value(0){}
public:
    double old_value;
    double new_value;
};

class PRSortMessageValue{
public:
    size_t getSize() const{
        return sizeof(double);
    }

    void serialize(BoundedBuffer& bb, size_t size) const{
        bb << value;
    }

    void deserialize(BoundedBuffer &bb, size_t size) {
        bb >> value;
    }
public:
    double value;
};

template <class TypeHinter>
class PRSortProgram : public bsp::runtime::synchronize::DefaultProgram<TypeHinter> {
public:
    typedef typename TypeHinter::MessageValue MV;
    typedef typename TypeHinter::BufferValue BV;
    typedef typename TypeHinter::Provider PROVIDER;
    typedef MessageBuffer<TypeHinter> MESSAGE_BUFFER;
    typedef Sender<TypeHinter> SENDER;

    virtual bool isNotify() {
        return true;
    }

    void send_out_messages(Vertex<TypeHinter> *next, PROVIDER &provider, MESSAGE_BUFFER &buffer, SENDER &sender,
                           AggregatorManager &am){
        PRSortVertexValue& vv = next->getMutableValue();

        VertexInfo* v_info = provider.getVertexInfoByInnerId(next->getInnerId());
        int nr_neighbor = v_info->getNeighborCount();

        double value = vv.new_value / nr_neighbor;

        //find my neighbors
        _TRACEE("vertex id = %ld\n", next->getId());
        unordered_set<int> &workers = provider.getOutWorkers(next->getInnerId());
        _TRACEE("workers = %zd\n", workers.size());

        unordered_set<int>::iterator si = workers.begin();
        for(;si != workers.end();++si){
            int to = *si;
            PRSortMessageValue mess;
            mess.value = value;
            sender.sendNormalMessage(next->getId(), mess, to);
        }

        vector<int>& inner_index = provider.getInnerEdgeIndicesFromSrc(next->getId());
        if(inner_index.size() > 0){
            int to = sender.getRank();
            PRSortMessageValue mess;
            mess.value = value;
            sender.sendNormalMessage(next->getId(), mess, to);
        }
    }

    virtual void updateVertex(Vertex<TypeHinter> *next, PROVIDER &provider, MESSAGE_BUFFER &buffer, SENDER &sender,
                              AggregatorManager &am, Context& context) {
        PRSortVertexValue& vv = next->getMutableValue();
        _TRACEE("PRSort: computing %ld\n", next->getId());

        //then we read all the vertcies
        if(context.turn == 0){//first turn;
            vv.new_value = 1.0 / provider.totalVertices();
            send_out_messages(next, provider, buffer, sender, am);
            return;
        }

        if(context.turn >= 6){
            next->setHalt(true);
            return;
        }

        _TRACEE("PR Sort: not turn 0 %ld\n", next->getId());
        VertexInfo* v_info = provider.getVertexInfoByInnerId(next->getInnerId());

        //compute my new value
        vv.old_value = vv.new_value;
        vv.new_value = 0;

        vector<InnerEdge<TypeHinter>*>& inner_edges = provider.getInnerEdges();
        vector<InEdge<TypeHinter>*>& in_edges = provider.getInEdges();

        vector<int>::iterator vi = v_info->inner_edges.begin();
        for(;vi != v_info->inner_edges.end();++vi){
            InnerEdge<TypeHinter>* edge = inner_edges[*vi];
            const VertexId& vid = edge->getSrc()->getId();
            float* old_value = buffer.findOldValue(vid);
//            if(old_value == NULL){
//                _TRACEE("error: inner edge %ld is null\n", vid);
//                continue;
//            }

            vv.new_value += *old_value;
        }

        vi = v_info->in_edges.begin();
        for(;vi != v_info->in_edges.end();++vi){
            InEdge<TypeHinter>* edge = in_edges[*vi];
            const VertexId & vid = edge->getSrc();
            float* old_value = buffer.findOldValue(vid);
//            if(old_value == NULL){
//                _TRACEE("error: in edge %ld is null\n", vid);
//                continue;
//            }

            vv.new_value += *old_value;
        }

        vv.new_value = 0.2 * vv.old_value + 0.8 * vv.new_value;
        //now broad my new value
        send_out_messages(next, provider, buffer, sender, am);
    }

    virtual void onNewBufferMessage(const VertexId &vid, const MV &message, BV &value) {
//        _TRACEE("on new buffer message, id = %ld\n", vid);
        value = (BV)message.value;
    }
};

class PRSortTypeHinter : public bsp::runtime::synchronize::DefaultTypeHinter<PRSortTypeHinter>{
public:
    typedef PRSortVertexValue VertexValue;
    typedef float EdgeValue;
    typedef PRSortMessageValue MessageValue;
    typedef PRSortProgram<PRSortTypeHinter> Program;
};


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main(int argc, char *argv[]) {
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    if (provided < MPI_THREAD_MULTIPLE) {
        _TRACEE("MPI version is bad\n");
        exit(1);
    }
    //MPI_Init(&argc, &argv);

    Driver<PRSortTypeHinter> driver(argv[1]);

    AdjLineParser<PRSortTypeHinter> adjLineParser;
    FileInput<PRSortTypeHinter> file_input(adjLineParser);

    driver.input_data(file_input);

    Provider<PRSortTypeHinter> &provider = driver.getProvider();
    //provider.dump();

    //now run some programs
    PRSortProgram<PRSortTypeHinter> program;
    driver.run_program(program);

    //get our values
    vector<Vertex<PRSortTypeHinter>* >& vertices = provider.getVertices();
    vector<Vertex<PRSortTypeHinter>* >::iterator vi = vertices.begin();
    for(;vi != vertices.end();++vi){
        Vertex<PRSortTypeHinter>* vertex = *vi;
        _TRACEE("%ld %f\n", vertex->getId(), vertex->getMutableValue().new_value);
    }

    MPI_Finalize();
    return 0;
}                /* ----------  end of function main  ---------- */
