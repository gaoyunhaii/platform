/*
 * =====================================================================================
 *
 *       Filename:  sssp.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年11月05日 17时00分31秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include "system/bsp.hpp"
#include <mpi.h>
#include <limits>
#include <gperftools/profiler.h>
#include <gperftools/heap-profiler.h>

using namespace bsp::runtime;
using namespace bsp::runtime::synchronize;

const float NOT_REACH = std::numeric_limits<float>::max();

class SSSPVertexValue {
public:
    SSSPVertexValue() :
            old_value(NOT_REACH), new_value(NOT_REACH) { }

public:
    float old_value;
    float new_value;
};

class SSSPMessageValue {
public:
    size_t getSize() const{
        return sizeof(float);
    }

    void serialize(BoundedBuffer& bb, size_t size) const{
        bb << value;
    }

    void deserialize(BoundedBuffer &bb, size_t size) {
        bb >> value;
    }

public:
    float value;
};

class SSSPNotifyValue{
public:
    SSSPNotifyValue(): value(NOT_REACH){}

    float value;
    VertexId from;
};

template <class TypeHinter>
class SSSPProgram : public bsp::runtime::synchronize::DefaultProgram<TypeHinter> {
public:
    typedef typename TypeHinter::MessageValue MV;
    typedef typename TypeHinter::BufferValue BV;
    typedef typename TypeHinter::Provider PROVIDER;
    typedef MessageBuffer<TypeHinter> MESSAGE_BUFFER;
    typedef Sender<TypeHinter> SENDER;
    typedef typename TypeHinter::Executor EXECUTOR;

    virtual bool isNotify() {
        return true;
    }

    void send_out_messages(Vertex<TypeHinter> *next, PROVIDER &provider, MESSAGE_BUFFER &buffer, SENDER &sender,
                           int channel_index){
        SSSPVertexValue& vv = next->getMutableValue();
        VertexInfo *v_info = provider.getVertexInfoByInnerId(next->getInnerId());
        //_TRACEE("send out for vid %ld, value = %f\n", next->getId(), vv.new_value);

        //find my neighbors
//        _TRACEE("vertex id = %ld\n", next->getId());
        unordered_set<int> &workers = provider.getOutWorkers(next->getInnerId());
//        _TRACEE("workers = %zd\n", workers.size());

        unordered_set<int>::iterator si = workers.begin();
        for(;si != workers.end();++si){
            int to = *si;
            SSSPMessageValue mess;
            mess.value = vv.new_value;
            NormalMessage<SSSPMessageValue>* normalMessage = new NormalMessage<SSSPMessageValue>(next->getId(), mess);
            sender.asyncSendMessage(normalMessage, to, channel_index);
        }

        vector<int>& inner_index = v_info->inner_edges;
        if(inner_index.size() > 0){
            SSSPMessageValue mess;
            mess.value = vv.new_value;
            buffer.onNewLocalMessage(*next, mess, channel_index);

//            int to = sender.getRank();
//            SSSPMessageValue mess;
//            mess.value = vv.new_value;
//            NormalMessage<SSSPMessageValue>* normalMessage = new NormalMessage<SSSPMessageValue>(next->getId(), mess);
//            sender.asyncSendMessage(normalMessage, to, channel_index);
        }
    }

    virtual void updateVertex(Vertex<TypeHinter> *next, EXECUTOR& executor, int channel_index) {
        PROVIDER &provider = executor.getProvider();
        MESSAGE_BUFFER& buffer = executor.getMessageBuffer();
        SENDER& sender = executor.getSender();
        NewNotifier<TypeHinter>& notifier = executor.getNewNotifier();

        SSSPVertexValue& vv = next->getMutableValue();

        //_TRACEE("SSSP: computing %ld\n", next->getId());

        if(executor.getTurn() == 0){//first turn;
            if(vv.new_value != NOT_REACH){
                send_out_messages(next, provider, buffer, sender, channel_index);
                next->setHalt(true);
                return;
            }
            else{
                next->setHalt(true);
                return;
            }
        }

        //_TRACEE("SSSP: not turn 0 %ld\n", next->getId());
        //first find out who has change on the last turn

        NotifierIterator<TypeHinter>* ni = notifier.getNotifierIterator(next->getInnerId(), executor.getTurn() - 1);
        NotifyMessage<TypeHinter>* remote_message = ni->remoteGetNext();
        for(;remote_message != NULL;remote_message = ni->remoteGetNext()){
            if(remote_message->nv.value < vv.new_value){
                vv.new_value = remote_message->nv.value;
            }
//            float& old_value = buffer.findRemoteOldValue(in_edge->getSrc());
//            //_TRACEE("SSSP %ld: remote = %ld, value = %f\n", next->getId(), vid, *old_value);
//            float new_dist = old_value + in_edge->getValue();
//
//            if(new_dist < vv.new_value){
//                vv.new_value = new_dist;
//            }
        }

        NotifyMessage<TypeHinter>* local_message = ni->localGetNext();
        for(;local_message != NULL;local_message = ni->localGetNext()){
            if(local_message->nv.value < vv.new_value){
                vv.new_value = local_message->nv.value;
            }

//            float& old_value = buffer.findLocalOldValue(*(inner_edge->getSrc()));
//            //_TRACEE("SSSP %ld: local = %ld, value = %f\n", next->getId(), vid,  *old_value);
//
//            float new_dist = old_value + inner_edge->getValue();
//            if(new_dist < vv.new_value){
//                vv.new_value = new_dist;
//            }
        }
        delete ni;

        if(vv.new_value < vv.old_value){
            vv.old_value = vv.new_value;
            //send out
//            _TRACEE("will send messages %ld\n", next->getId());
            send_out_messages(next, provider, buffer, sender, channel_index);
        }

        next->setHalt(true);
    }

    virtual void onNewBufferMessage(const VertexId &vid, const MV &message, BV &value) {
        //_TRACEE("on new buffer message, id = %ld, value=%f\n", vid, message.value);
        value = (BV)message.value;
    }

    virtual void onNewRemoteNotifyMessage(NotifyMessage<TypeHinter>& notify_message, const VertexId& vid, const MV& mv, InEdge<TypeHinter>* in_edge){
        float new_value = mv.value + in_edge->getValue();
        if(new_value < notify_message.nv.value){
            notify_message.nv.value = new_value;
            notify_message.nv.from = vid;
        }
    }

    virtual void onLocalNotifyMessage(NotifyMessage<TypeHinter>& notify_message, const MV& mv, const Vertex<TypeHinter>* vertex, InnerEdge<TypeHinter>* inner_edge){
        float new_value = mv.value + inner_edge->getValue();
        if(new_value < notify_message.nv.value){
            notify_message.nv.value = new_value;
            notify_message.nv.from = vertex->getId();
        }
    }

};

class SSSPTypeHinter : public bsp::runtime::synchronize::DefaultTypeHinter<SSSPTypeHinter> {
public:
    typedef SSSPVertexValue VertexValue;
    typedef float EdgeValue;
    typedef SSSPMessageValue MessageValue;
    typedef SSSPNotifyValue NotifyValue;
    typedef SSSPProgram<SSSPTypeHinter> Program;
};


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:
 * =====================================================================================
 */
int main(int argc, char *argv[]) {
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    if (provided < MPI_THREAD_MULTIPLE) {
        _TRACEE("MPI version is bad\n");
        exit(1);
    }
    //MPI_Init(&argc, &argv);

    Driver<SSSPTypeHinter> driver(argv[1]);

    AdjLineParser<SSSPTypeHinter> adjLineParser;
    FileInput<SSSPTypeHinter> file_input(adjLineParser);

    long input_start = get_time_millis();
    driver.input_data(file_input);
    long input_end = get_time_millis();

    _TRACEE("input time = %ld\n", (input_end - input_start));

    Provider<SSSPTypeHinter> &provider = driver.getProvider();
    //provider.dump();

    //now we init the first vertex
    VertexId start = 1;

    Configuration& conf = driver.getConf();
    start = conf.getIntValue("task.sssp.start", 0);
    _TRACEE("start = %d\n", start);

    Vertex<SSSPTypeHinter> *init = provider.getVertexById(start);
    if (init != NULL) {
        SSSPVertexValue &value = init->getMutableValue();
        value.old_value = 0;
        value.new_value = 0;
        _TRACEE("init the value of the start vertex to 0\n");
    }

    //now run some programs
    SSSPProgram<SSSPTypeHinter> program;

    long run_start = get_time_millis();

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    ostringstream oss;
    oss << "/tmp/platform/profile_" << rank;
    ProfilerStart(oss.str().c_str());

    driver.run_program(program);

    ProfilerStop();

    long run_end = get_time_millis();

    _TRACEE("run time = %ld\n", (run_end - run_start));


    //get our values
    vector<Vertex<SSSPTypeHinter>* >& vertices = provider.getVertices();
    vector<Vertex<SSSPTypeHinter>* >::iterator vi = vertices.begin();
    for(;vi != vertices.end();++vi){
        Vertex<SSSPTypeHinter>* vertex = *vi;
       _TRACEE("%ld %f\n", vertex->getId(), vertex->getMutableValue().new_value);
    }

    MPI_Finalize();
    return 0;
}                /* ----------  end of function main  ---------- */
