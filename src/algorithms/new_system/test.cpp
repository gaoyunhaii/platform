/*
 * =====================================================================================
 *
 *       Filename:  test.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/10/2015 04:04:31 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include "system/bsp.hpp"
#include <mpi.h>
using namespace bsp::runtime;
using namespace bsp::runtime::synchronize;

class VertexValue {
public:
    float v;
};

class EdgeValue {
public:
    EdgeValue(double _v):v(_v){}
public:
    float v;
};

template <class TypeHinter>
class Program{
public:
    void dump(){}
};

class TypeHinter : public bsp::runtime::synchronize::DefaultTypeHinter<TypeHinter>{
public:
    typedef ::VertexValue VertexValue;
    typedef ::EdgeValue EdgeValue;
    typedef ::Program<TypeHinter> Program;
};


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    Driver<TypeHinter> driver(argv[1]);
    AdjLineParser<TypeHinter> adjLineParser;
    FileInput<TypeHinter> file_input(adjLineParser);

    driver.input_data(file_input);

    Provider<TypeHinter>& provider = driver.getProvider();
    provider.dump();

    //now run some programs

    MPI_Finalize();
    return 0;
}                /* ----------  end of function main  ---------- */
