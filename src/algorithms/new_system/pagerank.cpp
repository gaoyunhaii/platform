/*
 * =====================================================================================
 *
 *       Filename:  sssp.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年11月05日 17时00分31秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include "system/bsp.hpp"
#include <mpi.h>
#include <limits>
#include "common/time_recorder.h"
#include <gperftools/profiler.h>
#include <gperftools/heap-profiler.h>

using namespace bsp::runtime;
using namespace bsp::runtime::synchronize;
using namespace bsp::common;


int max_turn = 0;


class PageRankVertexValue {
public:
    PageRankVertexValue() :
            old_value(0), new_value(0) { }

public:
    float old_value;
    float new_value;
};

class PageRankMessageValue {
public:
    size_t getSize() const {
        return sizeof(float);
    }

    void serialize(BoundedBuffer &bb, size_t size) const {
        bb << value;
    }

    void deserialize(BoundedBuffer &bb, size_t size) {
        bb >> value;
    }

public:
    float value;
};

template<class TypeHinter>
class PageRankProgram : public bsp::runtime::synchronize::DefaultProgram<TypeHinter> {
public:
    typedef typename TypeHinter::MessageValue MV;
    typedef typename TypeHinter::BufferValue BV;
    typedef typename TypeHinter::Provider PROVIDER;
    typedef MessageBuffer<TypeHinter> MESSAGE_BUFFER;
    typedef Sender<TypeHinter> SENDER;
    typedef typename TypeHinter::Executor EXECUTOR;

    virtual bool isNotify() {
        return false;
    }

    void send_out_messages(Vertex<TypeHinter> *next, PROVIDER &provider, MESSAGE_BUFFER &buffer, SENDER &sender,
                           int channel_index) {
        PageRankVertexValue &vv = next->getMutableValue();

        //find my neighbors
        unordered_set<int> &workers = provider.getOutWorkers(next->getInnerId());

        VertexInfo *v_info = provider.getVertexInfoByInnerId(next->getInnerId());
        int nr_neighbor = v_info->getNeighborCount();

        double value = vv.new_value / nr_neighbor;

        unordered_set<int>::iterator si = workers.begin();
        for (; si != workers.end(); ++si) {
            int to = *si;
            PageRankMessageValue mess;
            mess.value = value;
            NormalMessage<PageRankMessageValue> *normalMessage = new NormalMessage<PageRankMessageValue>(next->getId(),
                                                                                                         mess);
            sender.asyncSendMessage(normalMessage, to, channel_index);
        }

        vector<int> &inner_index = v_info->inner_edges;
        if (inner_index.size() > 0) {
            PageRankMessageValue mess;
            mess.value = value;
            buffer.onNewLocalMessage(*next, mess, channel_index);

//            int to = sender.getRank();
//            PageRankMessageValue mess;
//            mess.value = value;
//            NormalMessage<PageRankMessageValue> *normalMessage = new NormalMessage<PageRankMessageValue>(next->getId(),
//                                                                                                         mess);
//            sender.asyncSendMessage(normalMessage, to, channel_index);
        }
    }

    virtual void updateVertex(Vertex<TypeHinter> *next, EXECUTOR &executor, int channel_index) {
        PROVIDER &provider = executor.getProvider();
        MESSAGE_BUFFER &buffer = executor.getMessageBuffer();
        SENDER &sender = executor.getSender();
        NewNotifier<TypeHinter>& notifier = executor.getNewNotifier();

        PageRankVertexValue &vv = next->getMutableValue();

        //_TRACEE("PageRank: computing %ld\n", next->getId());

        if (executor.getTurn() >= max_turn) {
            next->setHalt(true);
            return;
        }

        //_TRACEE("PR Sort: not turn 0 %ld\n", next->getId());
        VertexInfo *v_info = provider.getVertexInfoByInnerId(next->getInnerId());

        //compute my new value
        vector<InnerEdge<TypeHinter> *> &inner_edges = provider.getInnerEdges();
        vector<InEdge<TypeHinter> *> &in_edges = provider.getInEdges();

        if (executor.getTurn() > 0) {
            vv.old_value = vv.new_value;
            vv.new_value = 0;

            vector<int>::iterator vi = v_info->inner_edges.begin();
            for (; vi != v_info->inner_edges.end(); ++vi) {
                InnerEdge<TypeHinter> *edge = inner_edges[*vi];
                float old_value = buffer.findLocalOldValue(*(edge->getSrc()));
                vv.new_value += old_value;
            }

            vi = v_info->in_edges.begin();
            for (; vi != v_info->in_edges.end(); ++vi) {
                InEdge<TypeHinter> *edge = in_edges[*vi];
                float old_value = buffer.findRemoteOldValue(edge->getSrc());
                vv.new_value += old_value;
            }

            vv.new_value = 0.2 * vv.old_value + 0.8 * vv.new_value;
        }

        //_TRACEE("%d.value = %f\n", next->getId(), vv.new_value);
        //now broad my new value
        send_out_messages(next, provider, buffer, sender, channel_index);
    }

    virtual void onNewBufferMessage(const VertexId &vid, const MV &message, BV &value) {
        value = (BV) message.value;
    }
};

class PageRankTypeHinter : public bsp::runtime::synchronize::DefaultTypeHinter<PageRankTypeHinter> {
public:
    typedef PageRankVertexValue VertexValue;
    typedef float EdgeValue;
    typedef PageRankMessageValue MessageValue;
    typedef PageRankProgram<PageRankTypeHinter> Program;
};


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:
 * =====================================================================================
 */
int main(int argc, char *argv[]) {
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    if (provided < MPI_THREAD_MULTIPLE) {
        _TRACEE("MPI version is bad\n");
        exit(1);
    }


    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

//    oss.str("");
//    oss << "/tmp/platform/mem_" << rank;
//    HeapProfilerStart(oss.str().c_str());

    TimeRecorder tr;

    Driver<PageRankTypeHinter> driver(argv[1]);
    //output is argv[2], skip
    max_turn = atoi(argv[3]);

    //init
    AdjLineParser<PageRankTypeHinter> adjLineParser;
    FileInput<PageRankTypeHinter> file_input(adjLineParser);

    long input_start = get_time_millis();
    driver.input_data(file_input);
    long input_end = get_time_millis();

    _TRACEE("input time = %ld\n", (input_end - input_start));

    Provider<PageRankTypeHinter> &provider = driver.getProvider();
    //provider.dump();

    //now we init their initialize value
    int total = provider.totalVertices();
    _TRACEE("total vertices = %d\n", total);

    vector<Vertex<PageRankTypeHinter> *> &vertices = provider.getVertices();
    vector<Vertex<PageRankTypeHinter> *>::iterator vi = vertices.begin();
    for (; vi != vertices.end(); ++vi) {
        PageRankVertexValue &vv = (*vi)->getMutableValue();
        vv.old_value = 1.0 / total;
        vv.new_value = 1.0 / total;

        //_TRACEE("%d, old = %f, new = %f\n", (*vi)->getId(), vv.old_value, vv.new_value);
    }

    //now run some programs
    PageRankProgram<PageRankTypeHinter> program;

    long run_start = get_time_millis();

    ostringstream oss;
    oss << "/tmp/platform/profile_" << rank;
    ProfilerStart(oss.str().c_str());

    driver.run_program(program);

    ProfilerStop();

    long run_end = get_time_millis();

    _TRACEE("run time = %ld, %ld, %ld\n",  (run_end - run_start), run_start, run_end);

    //get our values
    vi = vertices.begin();
    for (; vi != vertices.end(); ++vi) {
        Vertex<PageRankTypeHinter> *vertex = *vi;
        //_TRACEE("%ld %f\n", vertex->getId(), vertex->getMutableValue().new_value);
    }

//    HeapProfilerStop();

    cout << TimeRecorderDumper() << endl;

    MPI_Finalize();
    return 0;
}                /* ----------  end of function main  ---------- */
