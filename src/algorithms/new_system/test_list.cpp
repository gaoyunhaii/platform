/*
 * =====================================================================================
 *
 *       Filename:  test_list.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年03月08日 13时39分26秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include <vector>
#include <cstdio>
#include "common/dthread.h"
#include "common/atomic.h"
#include <unistd.h>
using namespace std;
using namespace bsp::atomic;
using namespace bsp::multi_thread;

struct ListNode{
    ListNode(int _v) : val(_v){}

    ListNode* next;
    int val;
};

void append(ListNode*& head, ListNode*& tail, ListNode* cur){
    if(head == tail && tail == NULL){
        head = tail = cur;
    }

    tail->next = cur;
    cur->next = NULL;
    tail = cur;
}

ListNode* head = NULL;
ListNode* tail = NULL;

ListNode* current = NULL;
int i = 0;


class ListNodeRunnable : public Runnable{
public:
    void run(){
        int count = 0;
        while(true){
            ListNode* cur = current;
            if(cur){
                if(!atomic_compare_and_swap(&current, cur, cur->next)){
                    ++count;
                    continue;
                }


                //usleep(100);
            }
            else{
                break;
            }
        }

//        while(true){
//            int current = atomic_incre_re(&i, 1);
//            if(current >= 5000000 / 8){
//                break;
//            }
//
//        }

        printf("%d\n", count);
    }
};

int main(){
    for(int i = 0;i < 5000000 / 8;++i){
        ListNode* ln = new ListNode(i);
        append(head, tail, ln);
    }


    //now we create threads to iteartor it
    current = head;
    i = 0;

    vector<DaemonThread*> threads;
    for(int i = 0;i < 8;++i){
        ListNodeRunnable* runnable = new ListNodeRunnable();
        DaemonThread* dt = new DaemonThread(runnable, false, true);
        threads.push_back(dt);
    }

    typedef vector<DaemonThread*>::iterator VI;
    for(VI vi = threads.begin();vi != threads.end();++vi){
        (*vi)->start();
    }

    DaemonThread::joinAllThread();
}