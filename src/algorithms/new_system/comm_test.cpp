/*
 * =====================================================================================
 *
 *       Filename:  test.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/10/2015 04:04:31 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include "system/bsp.hpp"
#include <mpi.h>
#include "common/util.h"
#include "common/time_recorder.h"
#include <gperftools/profiler.h>
#include <gperftools/heap-profiler.h>
using namespace bsp::runtime;
using namespace bsp::runtime::synchronize;
using namespace bsp::common;

class VertexValue {
public:
    float v;
};

class EdgeValue {
public:
    EdgeValue(double _v):v(_v){}
public:
    float v;
};

class PageRankMessageValue {
public:
    size_t getSize() const {
        return sizeof(double);
    }

    void serialize(BoundedBuffer &bb, size_t size) const {
        bb << value;
    }

    void deserialize(BoundedBuffer &bb, size_t size) {
        bb >> value;
    }

public:
    double value;
};

template <class TypeHinter>
class TheProgram{
public:
    typedef typename TypeHinter::MessageValue MV;
    typedef typename TypeHinter::BufferValue BV;
    typedef typename TypeHinter::Provider PROVIDER;
    typedef MessageBuffer<TypeHinter> MESSAGE_BUFFER;
    typedef Sender<TypeHinter> SENDER;
    typedef typename TypeHinter::Executor EXECUTOR;

    virtual bool isNotify() {}
    void dump(){}
    virtual void updateVertex(Vertex<TypeHinter> *next, EXECUTOR &executor, int channel_index) {}
    virtual void onNewBufferMessage(const VertexId &vid, const MV &message, BV &value) {}
};

class TypeHinter : public bsp::runtime::synchronize::DefaultTypeHinter<TypeHinter>{
public:
    typedef ::VertexValue VertexValue;
    typedef ::EdgeValue EdgeValue;
    typedef PageRankMessageValue MessageValue;
    typedef TheProgram<TypeHinter> Program;
};

MRCoordinator* coorinator = NULL;
int totalSize = 0;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

template <class TypeHinter>
class MyHandler : public MessageHandler<TypeHinter>{
public:
    typedef typename TypeHinter::BufferValue BV;
    typedef typename TypeHinter::MessageValue MV;
    typedef typename TypeHinter::Program PROGRAM;

    MyHandler(int worker_size, int comm_size, PROGRAM& p, MessageBuffer<TypeHinter>& _buffer, AggregatorManager& am)
            : MessageHandler<TypeHinter>(worker_size, 1, comm_size, p, _buffer, am), finished(false){

    }

    void onNewMessage(int from, void* message, size_t size) {
        //_TRACEE("a new message from %d with size = %ld\n", from, size);

        totalSize += size;

        int type;
        size_t raw_size;

        BoundedBuffer bb(message);
        bb >> type;
        bb >> raw_size;

        MutexGetLock get_lock(&lock);

        bool to_exit;
        if(type == NORMAL_MESSAGE){
            to_exit = coorinator->onDataMessage(from);
        }
        else if(type == CONTROL_MESSAGE){
            ControlMessage cm;
            cm.deserialize(bb, raw_size);

            to_exit = coorinator->onStatMessage(from, cm.nr_messages);
        }

        //coorinator->print_count();
        if(to_exit){
            finished = true;
            pthread_cond_signal(&cond);
        }
    }

    void waitTillEnded(){
        MutexGetLock get_lock(&lock);
        if(finished){
            return;
        }

        pthread_cond_wait(&cond, &lock);
    }
private:
    bool finished;
};

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main(int argc, char *argv[]) {
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    if (provided < MPI_THREAD_MULTIPLE) {
        _TRACEE("MPI version is bad\n");
        exit(1);
    }

    //start the dummy objects
    Driver<TypeHinter> driver(argv[1]);
    Provider<TypeHinter>& provider = driver.getProvider();
    TheProgram<TypeHinter> pro;

    int total = atoi(argv[3]);
    _TRACEE("total = %d\n", total);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int comm_size;
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);

    MPI_Comm comms[comm_size];
    for(int i = 0;i < comm_size;++i){
        comms[i] = 0;
        MPI_Comm_dup(MPI_COMM_WORLD, comms + i);
    }

    for(int i = 0;i < comm_size;++i){
        _TRACEE("comms [%d] = %d\n", i, (int)comms[i]);
    }

    _TRACEE("communicator copied\n");

    coorinator = new MRCoordinator(comm_size);

//    ostringstream oss;
//    oss << "/tmp/platform/profile_" << rank;
//    ProfilerStart(oss.str().c_str());
//
//    oss.str("");
//    oss << "/tmp/platform/mem_" << rank;
//    HeapProfilerStart(oss.str().c_str());

    AggregatorManager aggregator_manager(comm_size);

    int receiver_worker_size = 3;
    NewNotifier<TypeHinter> notifier(receiver_worker_size, provider, pro);
    MessageBuffer<TypeHinter> buffer(receiver_worker_size, rank, provider, pro, notifier);

    TimeRecorder tr;

    MPI_Comm comm = MPI_COMM_WORLD;
    MyHandler<TypeHinter> handler(receiver_worker_size, comm_size, pro, buffer, aggregator_manager);
    Receiver<TypeHinter> receiver(comms[rank], handler);

    receiver.start();

    int sender_worker_size = 3;
    Sender<TypeHinter> sender(comms, sender_worker_size);
    sender.start();
    sender.turnInitialize();

    tr.getRecord("send").timerStart();

    //now we start several thread to send messages
    int sendTo = -1;
    unordered_map<int, int> counts;

    for(int i = 0;i < total;++i){
        PageRankMessageValue mv;
        mv.value = 100;
        NormalMessage<PageRankMessageValue>* nm = new NormalMessage<PageRankMessageValue>(i, mv);

        sendTo = (sendTo + 1) % comm_size;
        if(sendTo == rank){
            sendTo = (sendTo + 1) % comm_size;
        }

        sender.asyncSendMessage(nm, sendTo, i % sender_worker_size);
        //sender.sendRawMessage(nm, sendTo);
        //_TRACEE("send %d messages\n", i);

        counts[sendTo] += 1;

        if(i % 1000 == 0){
            _TRACEE("send %d messages\n", i);
        }
    }

    _TRACEE("data message finished\n");
    sender.turnFinalize();

    for(int i = 0;i < comm_size;++i){
        _TRACEE("send %d messages to %d\n", counts[i], i);
        ControlMessage* cm = new ControlMessage(counts[i]);
        sender.sendRawMessage(cm, i);
    }

    handler.waitTillEnded();

    tr.getRecord("send").timerEnd();

    LocalControlMessage lcm(0);
    sender.sendRawMessage(&lcm, rank);
    receiver.stop();
    sender.stop();

    _TRACEE("total size = %d\n", totalSize);
    cout << TimeRecorderDumper() << endl;

    //now run some programs

    MPI_Finalize();
    return 0;
}                /* ----------  end of function main  ---------- */
