/*
 * =====================================================================================
 *
 *       Filename:  spsc_queue.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年11月24日 14时05分38秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include <boost/lockfree/spsc_queue.hpp>
#include <boost/lockfree/queue.hpp>
#include "common/dthread.cpp"
#include "common/util.h"
#include "common/util.cpp"
#include "common/sexception.cpp"
#include "common/atomic.cpp"
//#include <gperftools/profiler.h>
#include <exception>

using boost::lockfree::spsc_queue;
using boost::lockfree::queue;
using bsp::multi_thread::DaemonThread;
using bsp::common::get_time_us;

spsc_queue<int> hehe(1000);
const int TEST = 1000000000;
const int EACH = 100000;

class PopRunnable : public bsp::multi_thread::Runnable{
public:
    void run(){
        long start, end;
        for(int i = 0;i < TEST;++i){
            int value;
            while(!hehe.pop(value));

            if(value != i){
                throw std::exception();
            }

            if(value % EACH == 0){
                start = get_time_us();
            }

            if(value % EACH == EACH - 1){
                end = get_time_us();
                printf("time = %ld, avg = %lf\n", (end - start), (double)EACH * 1000000.0 / (end - start));
            }
        }
    }
};


int main(){
    PopRunnable* pr = new PopRunnable();
    DaemonThread* dt = new DaemonThread(pr, false, true);

//    ProfilerStart("spsc.prof");

    dt->start();
    for(int i = 0;i < TEST;++i){
        while(!hehe.push(i));
    }

    dt->join();

//    ProfilerStop();
}