/*
 * =====================================================================================
 *
 *       Filename:  jaccobi.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  06/03/2015 08:59:37 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */


#include <mpi.h>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <iostream>

using namespace std;

const int EACH_M = 25;
const int N = 100;
const int STEPS = 100;



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main ( int argc, char *argv[] ) {
	MPI_Init(&argc, &argv);	

	int size = 0;
	int myrank = 0;

	MPI_Comm_size(MPI_COMM_WORLD, &size);		
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

	MPI_Status status;

	//do the input						
	//store by column
	double** data = new double*[EACH_M + 2];
	for(int i = 0;i < EACH_M + 2;++i){
		data[i] = new double[N]();
		data[i][0] = 8;
		data[i][N - 1] = 8;
	}

	if(myrank == 0){
		for(int i = 0;i < N;++i){ data[1][i] = 8;
		}
	}

	if(myrank == size - 1){
		for(int i = 0;i < N;++i){
			data[EACH_M][i] = 8;
		}
	}


	//compute
	for(int i = 0;i < STEPS;++i){
		if(myrank > 0){
			MPI_Send(data[1], N, MPI_DOUBLE, myrank - 1, 0, MPI_COMM_WORLD);
		}

		if(myrank < size - 1){
			MPI_Recv(data[EACH_M + 1], N, MPI_DOUBLE, myrank + 1, 0, MPI_COMM_WORLD, &status);
		}			

		if(myrank < size - 1){
			MPI_Send(data[EACH_M], N, MPI_DOUBLE, myrank + 1, 0, MPI_COMM_WORLD);
		}

		if(myrank > 0){
			MPI_Recv(data[0], N, MPI_DOUBLE, myrank - 1, 0, MPI_COMM_WORLD, &status);	
		}

		int start = 1;
		int end = EACH_M;

		if(myrank == 0){
			start = 2;
		}

		if(myrank == size - 1){
			end = EACH_M - 1;	
		}

		for(int i = start;i <= end;++i){
			for(int j = 1;j < N - 1;++j){
				data[i][j] = (data[i - 1][j] + data[i][j - 1] + data[i + 1][j] + data[i][j + 1]) / 4.0;
			}
		}
		
	}

	ostringstream oss;
	oss << "/tmp/output/output_" << myrank;

	cout << "writing to " << oss.str() << endl;

	ofstream of(oss.str().c_str());
	
	for(int i = 0;i < N;++i){
		for(int j = 1;j < EACH_M + 1;++j){
			of << std::setprecision(2) << data[j][i] << "\t";
		}
		of << endl;
	}

	of.close();


	MPI_Finalize();
	return 0;
} /* ----------  end of function main  ---------- */
