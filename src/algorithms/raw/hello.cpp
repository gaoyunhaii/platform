/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  02/11/2014 01:32:09 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include "mpi.h"
#include <stdio.h>
#include <math.h>
#include <unistd.h>


int main(int argc, char* argv[]){

	int myid, np;
	int namelen;

	char processor_name[MPI_MAX_PROCESSOR_NAME];

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	MPI_Comm_size(MPI_COMM_WORLD, &np);

	MPI_Get_processor_name(processor_name, &namelen);

	fprintf(stderr, "hello world, process %d of %d on %s, success = %d\n", myid, np, processor_name, MPI_SUCCESS);

	MPI_Finalize();

	return 0;
}
