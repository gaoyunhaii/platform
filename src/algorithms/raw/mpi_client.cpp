/*
 * =====================================================================================
 *
 *       Filename:  mpi_client.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  11/22/2015 05:41:25 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:
 *
 * =====================================================================================
 */
#include <mpi.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include "time.h"
//#include "gperftools/profiler.h"

//#include "common/util.h"
//#include "common/util.cpp"
//#include "common/sexception.h"
//#include "common/sexception.cpp"
#define _TRACEE(f, ...) printf("%s:%d] ", __FILE__, __LINE__);printf(f, ##__VA_ARGS__);fflush(stdout)

inline long long get_time_us(){
    struct timespec t;
    clock_gettime(CLOCK_REALTIME, &t);
    return t.tv_sec * 1000000 + t.tv_nsec / 1000;
}

int main(int argc, char** argv){
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);

    if (provided < MPI_THREAD_MULTIPLE) {
        _TRACEE("MPI version is bad\n");
        exit(1);
    }

    FILE* file = fopen("/tmp/hehe", "w");

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    //then we can do the test
    if(rank == 0){
        //ProfilerStart("rank0.prof");

        for(int t = 0;t < 20;++t){
            long start = get_time_us();
            for(int i = 0;i < 50000;++i){
                MPI_Status status;
                //MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

                //int from = status.MPI_SOURCE;
                //int length = 0;
                //MPI_Get_count(&status, MPI_BYTE, &length);
                //if(length <= 0){
                //    _TRACEE("bad messages received, length = %d\n", length);
                //}

                void* raw_buffer = malloc(102400);
                //MPI_Recv(raw_buffer, length, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                MPI_Recv(raw_buffer, 102400, MPI_CHAR, 1, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                free(raw_buffer);
                //_TRACEE("received\n");
            }
            //do nothing
            long end = get_time_us();

            _TRACEE("50000 used %ld us\n", (end - start));
        }

        //ProfilerStop();
    }
    else{
        //ProfilerStart("rank1.prof");

        for(int i = 0;i < 20;++i){
            void* raw_buffer = malloc(102400);
            long start = get_time_us();

            for(int i = 0;i < 50000;++i){
                MPI_Send(raw_buffer, 102400, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
                fprintf(file, "sent\n");
            }

            long end = get_time_us();

            long to_sleep = (1e6) - (end - start);
            fprintf(file, "before to sleep, sleep = %ld\n", to_sleep);
            //usleep(to_sleep);
            fprintf(file, "50000 sent\n");
            fflush(file);
        }

        //ProfilerStop();
    }

    MPI_Finalize();
    return 0;
}