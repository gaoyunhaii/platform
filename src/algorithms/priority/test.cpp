/*
 * =====================================================================================
 *
 *       Filename:  test.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/10/2015 04:04:31 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include "system/bsp_priority.hpp"
#include <mpi.h>
#include <limits>
using namespace bsp::runtime;
using namespace bsp::runtime::syn_priority;

const float NOT_REACH = std::numeric_limits<float>::max();

class SSSPVertexValue {
public:
    SSSPVertexValue() :
            old_value(NOT_REACH), new_value(NOT_REACH) { }

public:
    float old_value;
    float new_value;
};

class SSSPMessageValue {
public:
    size_t getSize() const{
        return sizeof(double);
    }

    void serialize(BoundedBuffer& bb, size_t size) const{
        bb << value;
    }

    void deserialize(BoundedBuffer &bb, size_t size) {
        bb >> value;
    }

public:
    double value;
};


template <class TypeHinter>
class Program{
public:
    void dump(){}
};

class SSSPTypeHinter : public bsp::runtime::syn_priority::DefaultTypeHinter<SSSPTypeHinter>{
public:
    typedef SSSPVertexValue VertexValue;
    typedef float EdgeValue;
    typedef SSSPMessageValue MessageValue;
};


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    Driver<SSSPTypeHinter> driver(argv[1]);
    AdjLineParser<SSSPTypeHinter> adjLineParser;
    FileInput<SSSPTypeHinter> file_input(adjLineParser);

    driver.input_data(file_input);

    Provider<SSSPTypeHinter>& provider = driver.getProvider();
    provider.dump();
    //now run some programs
    //here we directly write an algorithm to implement the priority sssp
    //now we init the first vertex

    VertexId start = 1;
    Vertex<SSSPTypeHinter> *init = provider.getVertexById(start);
    if (init != NULL) {
        SSSPVertexValue &value = init->getMutableValue();
        value.old_value = 0;
        value.new_value = 0;
        _TRACEE("init the value of the start vertex to 0\n");
    }

    float delta = 100;




    MPI_Finalize();
    return 0;
}                /* ----------  end of function main  ---------- */
