/*
 * =====================================================================================
 *
 *       Filename:  sssp.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年11月05日 17时00分31秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include "system/bsp_priority.hpp"
#include <mpi.h>
#include <limits>
#include <cmath>

using namespace bsp::runtime;
using namespace bsp::runtime::syn_priority;

const float NOT_REACH = std::numeric_limits<float>::max();
const float DELTA = 100;

struct GlobalData{
public:
    GlobalData():
        current_bucket(0){}

    int current_bucket;
};


GlobalData& getGlobalData(){
    static GlobalData gd;
    return gd;
}


class SSSPVertexValue {
public:
    SSSPVertexValue() :
            old_value(NOT_REACH), new_value(NOT_REACH) { }

public:
    float old_value;
    float new_value;
};

class SSSPMessageValue {
public:
    size_t getSize() const{
        return sizeof(double);
    }

    void serialize(BoundedBuffer& bb, size_t size) const{
        bb << value;
    }

    void deserialize(BoundedBuffer &bb, size_t size) {
        bb >> value;
    }

public:
    double value;
};

template <class TypeHinter>
class SSSPProgram : public bsp::runtime::syn_priority::DefaultProgram<TypeHinter> {
public:
    typedef typename TypeHinter::MessageValue MV;
    typedef typename TypeHinter::BufferValue BV;
    typedef typename TypeHinter::Provider PROVIDER;
    typedef MessageBuffer<TypeHinter> MESSAGE_BUFFER;
    typedef Sender<TypeHinter> SENDER;
    typedef typename TypeHinter::Executor EXECUTOR;

    virtual bool isNotify() {
        return true;
    }

    void send_out_messages(Vertex<TypeHinter> *next, PROVIDER &provider, MESSAGE_BUFFER &buffer, SENDER &sender,
                           int channel_index){
        SSSPVertexValue& vv = next->getMutableValue();
        //_TRACEE("send out for vid %ld, value = %f\n", next->getId(), vv.new_value);

        //find my neighbors
//        _TRACEE("vertex id = %ld\n", next->getId());
        unordered_set<int> &workers = provider.getOutWorkers(next->getInnerId());
//        _TRACEE("workers = %zd\n", workers.size());

        unordered_set<int>::iterator si = workers.begin();
        for(;si != workers.end();++si){
            int to = *si;
            SSSPMessageValue mess;
            mess.value = vv.new_value;
            NormalMessage<SSSPMessageValue>* normalMessage = new NormalMessage<SSSPMessageValue>(next->getId(), mess);
            sender.asyncSendMessage(normalMessage, to, channel_index);
        }

        vector<int>& inner_index = provider.getInnerEdgeIndicesFromSrc(next->getId());
        if(inner_index.size() > 0){
            SSSPMessageValue mess;
            mess.value = vv.new_value;
            buffer.onNewLocalMessage(next->getId(), mess, channel_index);

//            int to = sender.getRank();
//            SSSPMessageValue mess;
//            mess.value = vv.new_value;
//            NormalMessage<SSSPMessageValue>* normalMessage = new NormalMessage<SSSPMessageValue>(next->getId(), mess);
//            sender.asyncSendMessage(normalMessage, to, channel_index);
        }
    }

    virtual void updateVertex(Vertex<TypeHinter> *next, EXECUTOR& executor, int channel_index) {
        PROVIDER &provider = executor.getProvider();
        MESSAGE_BUFFER& buffer = executor.getMessageBuffer();
        SENDER& sender = executor.getSender();
        NewNotifier<TypeHinter>& notifier = executor.getNewNotifier();

        SSSPVertexValue& vv = next->getMutableValue();
        GlobalData& gd = getGlobalData();
        float min = gd.current_bucket * DELTA;
        float max = (gd.current_bucket + 1) * DELTA;
        float next_max = (gd.current_bucket + 2) * DELTA;

        if(vv.new_value < min){
            _TRACEE("I am already converge, should not be updated again");
            next->setHalt(true);
            return;
        }

//        if(vv.new_value >= max){ //I will not updated now. I will only be updated when current_bucket changes
//            //TODO: update this when current buckets changes, so I cannot be halt
//            return;
//        }

          _TRACEE("SSSP: computing %ld, cb = %d, min = %f, max = %f\n", next->getId(), gd.current_bucket, min, max);
//        if(executor.getTurn() == 0){//first turn;
//            if(vv.new_value != NOT_REACH){
//                send_out_messages(next, provider, buffer, sender, channel_index);
//                next->setHalt(true);
//                return;
//            }
//            else{
//                next->setHalt(true);
//                return;
//            }
//        }

        //_TRACEE("SSSP: not turn 0 %ld\n", next->getId());
        //first find out who has change on the last turn
        NotifierIterator<TypeHinter>* ni = notifier.getNotifierIterator(next->getId());

        InEdge<TypeHinter>* in_edge = ni->remoteGetNext();
        for(;in_edge != NULL;in_edge = ni->remoteGetNext()){
            const VertexId& vid = in_edge->getSrc();

            float* old_value = buffer.findRemoteOldValue(vid);

            if(old_value == NULL){
                _TRACEE("bugs, old value not found %ld\n", vid);
                return;
            }
            //_TRACEE("SSSP %ld: remote = %ld, value = %f\n", next->getId(), vid, *old_value);

            float new_dist = *old_value + in_edge->getValue();
            if(new_dist < vv.new_value){
                vv.new_value = new_dist;
            }
        }

        InnerEdge<TypeHinter>* inner_edge = ni->localGetNext();
        for(;inner_edge != NULL;inner_edge = ni->localGetNext()){
            const VertexId& vid = inner_edge->getSrc()->getId();

            float* old_value = buffer.findLocalOldValue(vid);
            if(old_value == NULL){
                _TRACEE("bugs, old value not found%ld\n", vid);
                return;
            }
            //_TRACEE("SSSP %ld: local = %ld, value = %f\n", next->getId(), vid,  *old_value);

            float new_dist = *old_value + inner_edge->getValue();
            if(new_dist < vv.new_value){
                vv.new_value = new_dist;
            }
        }
        delete ni;

        bool updated = false;
        if(vv.new_value < vv.old_value){//update the old value first
            vv.old_value = vv.new_value;
            updated = true;
        }
        else{
            if(executor.getTurn() == 0 && vv.new_value != NOT_REACH){
                updated = true;
            }
        }

        //check if whether it is ok to do this
        if(vv.new_value >= min && vv.new_value < max){
            _TRACEE("%ld in the current bucket\n", next->getId());
            //we are in the bucket now. we need to notify others
            if(updated){
                _TRACEE("%ld case 1\n", next->getId());
                send_out_messages(next, provider, buffer, sender, channel_index);
            }

            next->setHalt(true);
        }
        else{
            //we are in the larger buckets
            _TRACEE("%ld case 2\n", next->getId());
            next->setHalt(true);
        }

        //next->setHalt(true);
    }

    virtual void onNewBufferMessage(const VertexId &vid, const MV &message, BV &value) {
        //_TRACEE("on new buffer message, id = %ld, value=%f\n", vid, message.value);
        value = (BV)message.value;
    }
};

class SSSPTypeHinter : public bsp::runtime::syn_priority::DefaultTypeHinter<SSSPTypeHinter> {
public:
    typedef SSSPVertexValue VertexValue;
    typedef float EdgeValue;
    typedef SSSPMessageValue MessageValue;
    typedef SSSPProgram<SSSPTypeHinter> Program;
};


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:
 * =====================================================================================
 */
int main(int argc, char *argv[]) {
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    if (provided < MPI_THREAD_MULTIPLE) {
        _TRACEE("MPI version is bad\n");
        exit(1);
    }
    //MPI_Init(&argc, &argv);

    Driver<SSSPTypeHinter> driver(argv[1]);

    AdjLineParser<SSSPTypeHinter> adjLineParser;
    FileInput<SSSPTypeHinter> file_input(adjLineParser);

    long input_start = get_time_millis();
    driver.input_data(file_input);
    long input_end = get_time_millis();

    _TRACEE("input time = %ld\n", (input_end - input_start));

    Provider<SSSPTypeHinter> &provider = driver.getProvider();
    //provider.dump();

    //now we init the first vertex
    VertexId start = 1;
    Vertex<SSSPTypeHinter> *init = provider.getVertexById(start);
    if (init != NULL) {
        SSSPVertexValue &value = init->getMutableValue();
        value.old_value = 0;
        value.new_value = 0;
        _TRACEE("init the value of the start vertex to 0\n");
    }

    GlobalData& gd = getGlobalData();

    //now run some programs
    while(true){
        _TRACEE("start again...");
        SSSPProgram<SSSPTypeHinter> program;
        long run_start = get_time_millis();
        driver.run_program(program);
        long run_end = get_time_millis();

        _TRACEE("run time = %ld\n", (run_end - run_start));

        //here we count how many are left in the next bucket
        float min = gd.current_bucket * DELTA;
        float max = (gd.current_bucket + 1) * DELTA;

        _TRACEE("min = %f, max = %f\n", min, max);

        float nextMin = NOT_REACH;

        vector<Vertex<SSSPTypeHinter> *> &vertices = provider.getVertices();
        vector<Vertex<SSSPTypeHinter> *>::iterator vi = vertices.begin();
        for (; vi != vertices.end(); ++vi) {
            SSSPVertexValue &vv = (*vi)->getMutableValue();

            if(vv.new_value < max){
                continue;
            }

            if(vv.new_value < nextMin){
                nextMin = vv.new_value;
            }
        }

        //synchronize with other process to get a common min value
        float common_min = 0;
        MPI_Allreduce(&nextMin, &common_min, 1, MPI_FLOAT, MPI_MIN, MPI_COMM_WORLD);
        _TRACEE("next min is %f, common min is %f\n", nextMin, common_min);

        if(common_min == NOT_REACH){
            break;
        }

        //then we calcuate where to go by the common_min
        int next_buck = (int)floor(common_min / DELTA);
        gd.current_bucket = next_buck;
        min = gd.current_bucket * DELTA;
        max = (gd.current_bucket + 1) * DELTA;
        _TRACEE("next buck is %d, min = %f, max = %f\n", gd.current_bucket, min, max);

        vector<Vertex<SSSPTypeHinter> *>::iterator svi = vertices.begin();
        for (; svi != vertices.end(); ++svi) {
            SSSPVertexValue &vv = (*svi)->getMutableValue();
            if(vv.new_value >= min && vv.new_value < max){
                (*svi)->setHalt(false);
                vv.old_value = NOT_REACH;
            }
        }
    }

    //get our values
    vector<Vertex<SSSPTypeHinter>* >& vertices = provider.getVertices();
    vector<Vertex<SSSPTypeHinter>* >::iterator vi = vertices.begin();
    for(;vi != vertices.end();++vi){
        Vertex<SSSPTypeHinter>* vertex = *vi;
       _TRACEE("%ld %f\n", vertex->getId(), vertex->getMutableValue().new_value);
    }

    MPI_Finalize();
    return 0;
}                /* ----------  end of function main  ---------- */