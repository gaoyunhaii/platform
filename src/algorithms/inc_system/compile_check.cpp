//
// Created by owner-pc on 10/22/16.
//

#include "common/thrift_server.h"
#include "common/thrift_server.hpp"
#include "common/util.h"
#include <inc_system/inc_system.h>

//handlers
#include "inc_system/handlers/new_transaction_handler.h"
#include "inc_system/handlers/graph_storage_handler.h"

//algorithms
#include "inc_system/algorithm/snapshot_algorithm.h"

#include <mpi.h>
#include <limits>

using namespace bsp::inc_system;
using namespace bsp::inc_system::handlers;

int main(int argc, char* argv[]){
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    if (provided < MPI_THREAD_MULTIPLE) {
        _TRACEE("MPI version is bad\n");
        exit(1);
    }
    MPI_Comm world = MPI_COMM_WORLD;
    int my_rank = get_rank(world);

    //start the daemon, then we start to try to input a graph and call the storage layer
    Driver driver(argv[1]);

    string input_path;
    driver.getConf().getValue(input_path, "task.path.input");
    _TRACEE("input path = %s\n", input_path.c_str());

    string snapshot_path;
    driver.getConf().getValue(snapshot_path, "task.path.snapshot");
    _TRACEE("snapshot path = %s\n", snapshot_path.c_str());

    IncExecutor inc_executor(MPI_COMM_WORLD);
    inc_executor.start_bg_services();

    AlgorithmManager manager;
    manager.mutable_algorithms().push_back(new SnapshotAlgorithm(driver.getHdfsClient(),
                                                             snapshot_path.c_str(), inc_executor));

    TransactionIniter initer(world, input_path.c_str(), driver.getHdfsClient(),
                             inc_executor.get_sender(), inc_executor.get_part_manager());

    //register the
    MessageHandler& handler = inc_executor.get_handler();
    handler.setHandlerFunc(GRAPH_MODIFICATION, GraphModificationHandler(inc_executor, manager));
    if(my_rank == 0){//when asked me for new id
        handler.setHandlerFunc(NEW_TRANSACTION, NewTransactionHandler(inc_executor));
    }
    else if(my_rank == 1){//when new id hold
        handler.setHandlerFunc(NEW_TRANS_ID, NewTransIdHandler(inc_executor, initer));
    }
    _TRACEE("handler registered\n");

    if(my_rank == 1){ //who is responsible for starting the graph input
        initer.start();
    }

    //wait till all ends;
//    if(my_rank == 1){
//        initer.stop();
//    }

    DaemonThread::joinAllThread();
    MPI_Finalize();
    return 0;
};