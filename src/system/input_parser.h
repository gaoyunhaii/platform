/*
 * =====================================================================================
 *
 *       Filename:  input_parser.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  09/19/2015 09:59:54 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_INPUT_PARSER_INC
#define  SYSTEM_INPUT_PARSER_INC

#include "system/provider.h"
#include "system/provider.hpp"
#include "system/runtime.h"
#include <string>
#include <cstdlib>
#include <cassert>

using std::string;
using bsp::runtime::Provider;
using bsp::runtime::VertexId;

namespace {
    const char FIRST_SPLIT = ':';
    const char SECOND_SPLIT = ',';
}

namespace bsp {
    namespace runtime {

        template<class TypeHinter>
        class AdjLineParser {
        public:
            typedef typename TypeHinter::VertexValue VV;
            typedef typename TypeHinter::EdgeValue EV;
            typedef typename TypeHinter::Partitioner PART;

            void initialize(Provider<TypeHinter> &provider) { }

            bool parseLine(Line& result, const string &line, Provider<TypeHinter> &provider) {
                const char *start = line.c_str();
                const char *end = line.c_str();
                for (; *end && *end != FIRST_SPLIT; ++end);
                assert(*end);

                VV vv;

                VertexId id = strtol(start, NULL, 10);
                //provider.addVertex(id, vv);
                result.source = id;

                while (true) {
                    start = end + 1;
                    end = start;
                    for (; *end && *end != SECOND_SPLIT; ++end);

                    if (!*end) { //specially for those has no edges
                        break;
                    }

                    //id first
                    VertexId to_id = strtol(start, NULL, 10);
                    //provider.addVertex(to_id, vv);

                    //then the value
                    start = end + 1;
                    for (end = start; *end && *end != FIRST_SPLIT && *end != SECOND_SPLIT; ++end);
                    double value = strtod(start, NULL);

                    //provider.addEdge(id, to_id, EV(value));
                    EdgePair ep;
                    ep.id = to_id;
                    ep.value = value;
                    result.edges.push_back(ep);

                    for (; *end && *end != FIRST_SPLIT; ++end);

                    if (!*end || (*end == FIRST_SPLIT && !*(end + 1))) {
                        break;
                    }
                }

                return true;
            }

            void organize(Provider<TypeHinter> &provider) { }
        };


    }
}


#endif   /* ----- #ifndef SYSTEM_INPUT_PARSER_INC  ----- */
