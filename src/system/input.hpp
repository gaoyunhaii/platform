/*
 * =====================================================================================
 *
 *       Filename:  input.hpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  07/11/2015 03:14:03 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_INPUT_HPP_INC
#define  SYSTEM_INPUT_HPP_INC

#include <iostream>
#include "system/runtime_constants.h"
#include "common/rw.h"
#include "system/input.h"
#include "system/runtime.h"
#include "common/util.h"

#include <string>
#include "common/atomic.h"

using std::string;
using std::cout;
using std::endl;
using bsp::common::rw::LineReader;
using bsp::common::print_free_mem;
using bsp::atomic::atomic_incre;

template <class TypeHinter>
void bsp::runtime::FileInputWorker<TypeHinter>::run(){
    Line* line = NULL;
    while(true){
        while(parent->lines.pop(line)){
            //_TRACEE("%d get %p %s\n", 1, line,  line->c_str());
            //parse it
            typename TypeHinter::VertexValue vv;
            Provider<TypeHinter>& _provider = *(parent->provider);
            _provider.addVertex(line->source, vv);

            for(list<EdgePair>::iterator epi = line->edges.begin();epi != line->edges.end();++epi){
                _provider.addVertex(epi->id, vv);
                _provider.addEdge(line->source, epi->id, epi->value);
            }
            atomic_incre(&parent->total_lines, -1);
            delete line;
            line = NULL;
        }

        if(parent->all_work_pushed && parent->total_lines == 0){
            break;
        }
    }
}


template <class TypeHinter>
void bsp::runtime::FileInput<TypeHinter>::input(const Configuration &config, HDFSClient &hdfs_client,
                                                Provider<TypeHinter> &_provider) {
    string input_path;
    config.getValue(input_path, "task.path.input");
    _TRACEE("input path = %s\n", input_path.c_str());
    InputStream *input = new HDFSFileInputStream(hdfs_client, input_path.c_str());

    //now do the input
    LineReader reader(input, false, 64 * 1024 * 1024);
    ssize_t re;
    bool cont;
    this->provider = &_provider;

    //we first start the worker
    for(int i = 0;i < threads.getSize();++i){
        threads.getRunnable(i)->setParent(this);
    }
    threads.start();

    parser.initialize(_provider);
    int line_readed = 0;
    string line;
    while ((re = reader.readline(line, -1)) > 0) {
        //_TRACEE("master readed %p %s\n", line, line->c_str());
        //push this to the queue
        Line* result = new Line;
        parser.parseLine(*result, line, _provider);

        while(!lines.push(result));
        atomic_incre(&total_lines, 1);

//        cont = parser.parseLine(line, provider);

        ++line_readed;
        if(line_readed % 10000 == 9999){
            _TRACEE("%d lines readed, free mem is ", line_readed);
            print_free_mem();
        }
//        if (!cont) {
//            break;
//        }
    }
    all_work_pushed = true;

    //wait till all threads finish their work
    _TRACEE("wait for all now %d\n" , total_lines);
    threads.joinAll();
    _TRACEE("all finished %d\n" , total_lines);

    //here we collect their data and push them to provider
    parser.organize(_provider);
    _TRACEE("Input finished, free mem is ");
    print_free_mem();
}

#endif
