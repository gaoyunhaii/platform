/*
 * =====================================================================================
 *
 *       Filename:  input.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/11/2015 03:13:58 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef  SYSTEM_INPUT_INC
#define  SYSTEM_INPUT_INC

#include "common/rw.h"
#include "common/configuration.h"
#include "common/hdfs_client.h"
#include "system/provider.h"
#include "common/dthread.h"
#include "common/thread_group.h"
#include "common/thread_group.hpp"
#include <boost/lockfree/queue.hpp>
#include "system/runtime.h"
#include "common/atomic.h"

using bsp::common::HDFSFileInputStream;
using bsp::common::Configuration;
using bsp::common::rw::InputStream;
using bsp::runtime::Provider;
using boost::lockfree::queue;
using bsp::multi_thread::ThreadGroup;
using bsp::atomic::CountDownLatch;

namespace bsp {
    namespace runtime {

        template <class TypeHinter>
        class FileInput;

        template<class TypeHinter>
        class FileInputWorker : public bsp::multi_thread::Runnable {
        public:
            FileInputWorker(int index) : parent(NULL){}
            void run();

            void setParent(FileInput<TypeHinter>* _p){
                parent = _p;
            }

        private:
            FileInput<TypeHinter>* parent;
        };


        template<class TypeHinter>
        class FileInput {
        public:
            friend class FileInputWorker<TypeHinter>;

            typedef typename TypeHinter::VertexValue VV;
            typedef typename TypeHinter::EdgeValue EV;
            typedef typename TypeHinter::Partitioner PART;
            typedef typename TypeHinter::InputParser PARSER;

            FileInput(PARSER &_p) : parser(_p), lines(1000000), threads(1), total_lines(0), all_work_pushed(false) {

            };

            void input(const Configuration &config, HDFSClient &hdfs_client, Provider<TypeHinter> &provider);

        private:
            PARSER &parser;
            queue<Line*> lines;

            ThreadGroup<FileInputWorker<TypeHinter> > threads;

            int total_lines;
            bool all_work_pushed;

            Provider<TypeHinter>* provider;
        };

    }
}

#endif