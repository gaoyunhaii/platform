/*
 * =====================================================================================
 *
 *       Filename:  message_buffer.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  10/02/2015 04:54:13 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYNCHRONIZE_MESSAGE_BUFFER_INC
#define  SYSTEM_SYNCHRONIZE_MESSAGE_BUFFER_INC

#include <pthread.h>
#include <unordered_map>
#include "common/obj_factory.h"
#include "system/runtime.h"
#include "system/runtime_bg.h"
#include "common/atomic.h"
#include <vector>
#include "common/dthread.h"
#include "system/runtime_constants.h"
#include <boost/lockfree/spsc_queue.hpp>

using bsp::common::ObjFactory;
using bsp::common::DefaultInit;
using std::unordered_map;
using bsp::runtime::VertexId;
using bsp::atomic::ConcurrentBitmap;
using bsp::atomic::TurnedSynchronizer;
using std::vector;
using bsp::multi_thread::Runnable;
using bsp::multi_thread::DaemonThread;
using boost::lockfree::spsc_queue;

namespace bsp{
    namespace runtime{
        namespace synchronize{
            template <class TypeHinter>
            class MessageBuffer;

            template <class TypeHinter>
            class Notifier;

            class NotifierContext{
            public:
                NotifierContext(): src(-1), is_local(false){}
                NotifierContext(const VertexId& _s, bool _il):
                        src(_s), is_local(_il){}

                VertexId src;
                bool is_local;
            };

            template <class TypeHinter>
            class ValueContainer{
            public:
                typedef typename TypeHinter::BufferValue BV;
                typedef typename TypeHinter::Provider PROVIDER;

                friend class MessageBuffer<TypeHinter>;
                friend class Notifier<TypeHinter>;

                ValueContainer(PROVIDER& _p):
                            provider(_p),
                            notifier(100000),
                            max_notifier(0),
                            turn_stopped(false),
                            message_map(provider.totalLocalVertices()){
//                    value_factory = new ObjFactory<BV, DefaultInit<BV> >();
                }

                ~ValueContainer(){
//                    value_factory->destroy(values);
//                    delete value_factory;
                }

                void reset(){
//                    value_factory->destroy(values);
//                    delete value_factory;

//                    values.clear();
//                    value_factory = new ObjFactory<BV, DefaultInit<BV> >();

                    typename unordered_map<VertexId, BV*>::iterator mi = values_by_id.begin();
                    for(;mi != values_by_id.end();++mi){
                        delete mi->second;
                    }
                    values_by_id.clear();

                    //notifier.clear();
                    NotifierContext dummy;
                    while(notifier.pop(dummy));

                    max_notifier = 0;
                    turn_stopped = false;
                    in_messages.clear();
                    inner_messages.clear();

                    message_map.clearAll();
                }

                BV* findValue(const VertexId& vid){
                    typename unordered_map<VertexId, BV*>::iterator mi = values_by_id.find(vid);
                    if(mi == values_by_id.end()){
                        return NULL;
                    }

                    return mi->second;
                }

                void insertValue(const VertexId& vid, BV* value){
                    values_by_id[vid] = value;
                }

                const vector<InEdge<TypeHinter>* >* getMyRemoteNotifier(const VertexId& src) const{
                    typename unordered_map<VertexId, vector<InEdge<TypeHinter>* > >::const_iterator mi = in_messages.find(src);
                    if(mi == in_messages.end()){
                        return NULL;
                    }

                    return &mi->second;
                }

                const vector<InnerEdge<TypeHinter>* >* getMyLocalNotifier(const VertexId& src) const{
                    typename unordered_map<VertexId, vector<InnerEdge<TypeHinter>* > >::const_iterator mi = inner_messages.find(src);
                    if(mi == inner_messages.end()){
                        return NULL;
                    }

                    return &mi->second;
                }
            private:
                PROVIDER& provider;

                unordered_map<VertexId, BV*> values_by_id;
//                vector<BV*> values;
//                ObjFactory<BV, DefaultInit<BV> >* value_factory;

                spsc_queue<NotifierContext> notifier;

                //for notifier
                int max_notifier;
                bool turn_stopped;

                ConcurrentBitmap message_map;

                //notifier in detail if needed
                unordered_map<VertexId, vector<InEdge<TypeHinter>* > > in_messages;
                unordered_map<VertexId, vector<InnerEdge<TypeHinter>* > > inner_messages;
            };

            template <class TypeHinter>
            class Notifier : public Runnable{
            public:
                typedef typename TypeHinter::Provider PROVIDER;

                Notifier(TurnedSynchronizer& _ts, PROVIDER& _p, MessageBuffer<TypeHinter>& _buffer):
                            stopped(false), synchronizer(_ts),
                            provider(_p), buffer(_buffer), next_to_notify(0){

                }

                void setStop(){
                    this->stopped = true;
                }

                void run();
            private:
                bool stopped;
                TurnedSynchronizer& synchronizer;

                PROVIDER &provider;
                MessageBuffer<TypeHinter> &buffer;

                //reuslt here
                int next_to_notify;
            };


            template <class TypeHinter>
            class MessageBuffer{
            public:
                typedef typename TypeHinter::MessageValue MV;
                typedef typename TypeHinter::BufferValue BV;
                typedef typename TypeHinter::Program PROGRAM;
                typedef typename TypeHinter::Provider PROVIDER;

                friend class Notifier<TypeHinter>;

                MessageBuffer(PROVIDER& _p, bool _need_notify):
                            provider(_p), need_notify(_need_notify),
                            notifier_synchronizer(NULL),
                            notifier(NULL), notifier_thread(NULL)
                {
                    old_container = new ValueContainer<TypeHinter>(provider);
                    new_container = new ValueContainer<TypeHinter>(provider);

                    if(need_notify){
                        notifier_synchronizer = new TurnedSynchronizer();
                        notifier = new Notifier<TypeHinter>(*notifier_synchronizer, provider, *this);
                        notifier_thread = new DaemonThread(notifier, false, false);
                    }
                }

                ~MessageBuffer(){
                    delete old_container;
                    delete new_container;
                }

                void start(){
                    if(need_notify){
                        //_TRACEE("start notifier thread\n");
                        notifier_thread->start();
                    }
                }

                void stop(){

                }

                void turnInitialize(PROGRAM& program);
                void turnFinalize(PROGRAM& program);

                void onNewMessage(const VertexId& src, const MV& mv, PROGRAM& program, bool is_local);

                bool hasMessageReceived(int inner_id){
                    return old_container->message_map.getBit(inner_id);
                }

                unordered_map<VertexId, vector<InEdge<TypeHinter>* > >& getInMessages(){
                    return old_container->in_messages;
                }

                unordered_map<VertexId, vector<InnerEdge<TypeHinter>* > >& getInnerMessages(){
                    return old_container->inner_messages;
                }

                const vector<InEdge<TypeHinter>* >* getMyRemoteNotifier(const VertexId& src) const{
                    return old_container->getMyRemoteNotifier(src);
                }

                const vector<InnerEdge<TypeHinter>* >* getMyLocalNotifier(const VertexId& src) const{
                    return old_container->getMyLocalNotifier(src);
                }

                BV* findOldValue(const VertexId& vid) const{
                    return old_container->findValue(vid);
                }
            private:
                PROVIDER& provider;
                bool need_notify;

                TurnedSynchronizer* notifier_synchronizer;
                Notifier<TypeHinter>* notifier;
                DaemonThread* notifier_thread;

                ValueContainer<TypeHinter>* old_container;
                ValueContainer<TypeHinter>* new_container;
            };
        }
    }
}


#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_MESSAGE_BUFFER_INC  ----- */
