/*
 * =====================================================================================
 *
 *       Filename:  bsp.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/10/2015 04:07:56 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_BSP_HPP_INC
#define  SYSTEM_BSP_HPP_INC

#include "system/runtime.h"
#include "system/runtime_bg.h"
#include "system/mpi_util.h"

#include "system/syn_priority/notifier.h"
#include "system/syn_priority/notifier.hpp"
#include "system/syn_priority/message_handler.h"
#include "system/syn_priority/message_handler.hpp"
#include "system/syn_priority/message_buffer.h"
#include "system/syn_priority/message_buffer.hpp"
#include "system/syn_priority/receiver.h"
#include "system/syn_priority/receiver.hpp"
#include "system/syn_priority/notifier.h"
#include "system/syn_priority/notifier.hpp"
#include "system/syn_priority/syn_executor.h"
#include "system/syn_priority/syn_executor.hpp"
#include "system/syn_priority/main.h"
#include "system/syn_priority/main.hpp"


#endif   /* ----- #ifndef SYSTEM_BSP_HPP_INC  ----- */


