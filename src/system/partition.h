/*
 * =====================================================================================
 *
 *       Filename:  partition.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/21/2015 10:47:36 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef  SYSTEM_PARTITION_INC
#define  SYSTEM_PARTITION_INC

#include "common/configuration.h"
#include "common/rw.h"
#include "system/runtime.h"
#include "common/hdfs_client.h"
#include <unordered_map>
#include <unordered_set>

using std::unordered_set;
using std::unordered_map;
using bsp::common::Configuration;
using bsp::runtime::VertexId ;
using bsp::common::HDFSClient;
using bsp::common::rw::LineReader;
using bsp::common::rw::InputStream;

namespace bsp{ namespace runtime{

class PrePartitioner{
public:
	PrePartitioner(const Configuration& _conf, HDFSClient& _client):
                config(_conf), hdfs_client(_client){};
	void init(){
        string input_path;
        config.getValue(input_path, "task.path.part");
        _TRACEE("partition path = %s\n", input_path.c_str());

        InputStream* input = new HDFSFileInputStream(hdfs_client, input_path.c_str());

        //now do the input
        LineReader reader(input, false, 20480);
        string line;
        char* end = NULL;

        int re;
        while((re = reader.readline(line, -1)) > 0) {
            end = NULL;
            VertexId vid = strtoll(line.c_str(), &end, 10);

            int order = strtol(end, NULL, 10);

            vid_parts[vid] = order;
        }
    }

	int getPart(const VertexId& vid){
		unordered_map<VertexId, int>::iterator mi = vid_parts.find(vid);
		if(mi != vid_parts.end()){
			return mi->second;
		}

		return -1;
	}
private:
	unordered_map<VertexId, int> vid_parts;
	const Configuration& config;
	HDFSClient& hdfs_client;
};

}}

#endif   /* ----- #ifndef SYSTEM_PARTITION_INC  ----- */

