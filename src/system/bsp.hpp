/*
 * =====================================================================================
 *
 *       Filename:  bsp.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/10/2015 04:07:56 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_BSP_HPP_INC
#define  SYSTEM_BSP_HPP_INC

#include "system/runtime.h"
#include "system/runtime_bg.h"
#include "system/mpi_util.h"

#include "system/synchronize/notifier.h"
#include "system/synchronize/notifier.hpp"
#include "system/synchronize/message_handler.h"
#include "system/synchronize/message_handler.hpp"
#include "system/synchronize/message_buffer.h"
#include "system/synchronize/message_buffer.hpp"
#include "system/synchronize/receiver.h"
#include "system/synchronize/receiver.hpp"
#include "system/synchronize/notifier.h"
#include "system/synchronize/notifier.hpp"
#include "system/synchronize/syn_executor.h"
#include "system/synchronize/syn_executor.hpp"
#include "system/synchronize/main.h"
#include "system/synchronize/main.hpp"


//priority
#include "system/new_syn_priority/main.h"
#include "system/new_syn_priority/main.hpp"

#endif   /* ----- #ifndef SYSTEM_BSP_HPP_INC  ----- */


