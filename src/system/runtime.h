/*
 * =====================================================================================
 *
 *       Filename:  runtime.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/10/2015 03:46:06 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_RUNTIME_H_INC
#define  SYSTEM_RUNTIME_H_INC

#include <vector>
#include <list>
#include <cstdint>
#include <cstdlib>
#include <cstdio>
using std::vector;
using std::list;

namespace bsp{ namespace runtime{

typedef uint64_t VertexId;

enum VertexType{
	INNER, BOUNDARY
};

//enum PriorityState{
//    COMPUTED, COMPUTING, TO_BE_COMPUTE
//};

template <class TypeHinter>
class Vertex{
public:
	typedef typename TypeHinter::VertexValue VV;
	typedef typename TypeHinter::BufferValue BV;
    typedef typename TypeHinter::NotifyValue NV;

	Vertex(const VertexId& vid, int _iid, const VV& _vv):
			id(vid), innerId(_iid), type(BOUNDARY), halt(false), value(_vv),
            //priority_state(TO_BE_COMPUTE),
            prev(NULL), next(NULL),
			fast_prev(NULL), fast_next(NULL){}

	const VertexId& getId() const{
		return id;
	}

	const int getInnerId() const{
		return innerId;
	}

	const VertexType getType() const{
		return type;
	}

	const bool isHalt() const{
		return halt;
	}

	void setHalt(bool h){
		halt = h;
	}

	const VV& getValue() const{
		return value;	
	}

	VV& getMutableValue(){
		return value;
	}

	const BV& getBufferValue() const{
		return buffer_value;
	}

	BV& getMutableBufferValue(){
		return buffer_value;
	}

//
//    NV* getOldNotifyForTurn(int turn){
//        if(time_one == turn - 1){
//            return &nv_one;
//        }
//
//        if(time_two == turn - 1){
//            return &nv_two;
//        }
//
//        return NULL;
//    }
//
//    NV* getToUpdateNotifyForTurn(int turn){
//        if(time_one == turn){
//            return &nv_one;
//        }
//
//        if(time_two == turn){
//            return &nv_two;
//        }
//
//        if(time_one < time_two){
//            time_one = turn;
//            return &nv_one;
//        }
//        else{
//            time_two = turn;
//            return &nv_two;
//        }
//    }

private:
	VertexId id;
	int innerId;
	VertexType type;
	bool halt;

	VV value;
	BV buffer_value;

public:
    //PriorityState priority_state;
    Vertex<TypeHinter>* prev;
    Vertex<TypeHinter>* next;

	Vertex<TypeHinter>* fast_prev;
	Vertex<TypeHinter>* fast_next;

//    //notifiication
//    NV nv_one;
//    NV nv_two;
//    int time_one;
//    int time_two;
};

template <class TypeHinter>
class VertexList{
public:
	VertexList():
			start(NULL), end(NULL), fast_end(NULL), count(0){
	}

	void push_back(Vertex<TypeHinter>* vertex){
		if(!start){
			vertex->prev = vertex->next = NULL;
			start = end = vertex;
		}
		else{
			end->next = vertex;
			vertex->prev = end;
			vertex->next = NULL;
			end = vertex;
		}

		++this->count;
		if(count < 50){
			fast_end = start;
		}
		else{
			//we move fast end to the next
			fast_end->fast_next = end;
			end->fast_prev = fast_end;
			fast_end = fast_end->next;
		}
	}

    void replace_another(VertexList<TypeHinter>& other){
        start = other.start;
        end = other.end;
		fast_end = other.fast_end;
		count = other.count;

        other.fast_end = other.start = other.end = NULL;
		other.count = 0;
    }

    void push_back_another(VertexList<TypeHinter>& other){
        if(start == end && start == NULL){
            replace_another(other);
        }

        if(other.start == NULL && other.end == NULL){
            return;
        }

        end->next = other.start;
        other.start->prev = end;
        end = other.end;

		if(other.fast_end){
			fast_end = other.fast_end;
		}
		count += other.count;

        other.fast_end = other.start = other.end = NULL;
		other.count = 0;
    }

    void remove_vertex(Vertex<TypeHinter>* which){
		//first we adjust the fast_prev and fast_next

		if(which->fast_prev){
			//since it is removed, we will have to adjust it to null
			which->fast_prev->fast_next = NULL;
			which->fast_prev = NULL;
		}

		//the comes the ordinary removing
        if(which->prev){
            which->prev->next = which->next;
        }

        if(which->next){
            which->next->prev = which->prev;
        }

        if(which == end){
            end = which->prev;
        }

        if(which == fast_end){
            fast_end = which->prev;
        }

        if(which == start){
            start = which->next;
        }

        which->prev = NULL;
        which->next = NULL;
		which->fast_next = NULL;
		which->fast_prev = NULL;

        --this->count;
    }

    Vertex<TypeHinter>* getStart(){
        return start;
    }

    Vertex<TypeHinter>* getEnd(){
        return end;
    }

	void print(){
		Vertex<TypeHinter>* cur = getStart();
        while(cur){
            printf("%ld", cur->getId());
            if(cur->fast_next){
                printf("(%ld)", cur->fast_next->getId());
            }
            printf(" ");

            cur = cur->next;
        }
        printf("\n");
	}

	int size(){
//		Vertex<TypeHinter>* cur = start;
//		int count = 0;
//		for(;cur;cur = cur->next){
//			++count;
//		}
		return count;
	}

private:
	Vertex<TypeHinter>* start;
	Vertex<TypeHinter>* end;

	Vertex<TypeHinter>* fast_end;
	int count;
};



template <class TypeHinter>
class BorderVertex{
public:
	typedef typename TypeHinter::BufferValue BV;
public:
	BorderVertex(const VertexId& _id, int _iid) :
				id(_id), innerId(_iid){
	}

	const VertexId& getId() const{
		return id;
	}

	const int getInnerId() const{
		return innerId;
	}

    const int getPart() const{
        return part;
    }

    void setPart(int _np){
        part = _np;
    }

private:
	VertexId id;
	int innerId;
    int part;
};


template <class TypeHinter>
class InnerEdge{
public:
	typedef typename TypeHinter::VertexValue VV;
	typedef typename TypeHinter::EdgeValue EV;

	InnerEdge(Vertex<TypeHinter>* _s, Vertex<TypeHinter>* _d, const EV& _v):
				src(_s), dest(_d), value(_v){}

	Vertex<TypeHinter>* getSrc(){
		return src;
	}

	Vertex<TypeHinter>* getDest(){
		return dest;	
	}

	const EV& getValue() const{
		return value;
	}	

	EV& getMutableValue(){
		return value;
	}

private:
	Vertex<TypeHinter>* src;
	Vertex<TypeHinter>* dest;
	EV value;
};


template <class TypeHinter>
class OuterEdge{
public:
	typedef typename TypeHinter::EdgeValue EV;

	OuterEdge(const VertexId& _d, const EV& _v):
			dest(_d), value(_v){}

	VertexId& getDest(){
		return dest;
	}

	const EV& getValue() const{
		return value;
	}	

	EV& getMutableValue(){
		return value;
	}

private:
	VertexId dest;
	EV value;
};

template <class TypeHinter>
class InEdge{
public:
	typedef typename TypeHinter::EdgeValue EV;

	InEdge(BorderVertex<TypeHinter>& _src, Vertex<TypeHinter>& _dest, const EV& _v):
			src(_src), dest(_dest), value(_v){
    }

	BorderVertex<TypeHinter>& getSrc(){
		return src;
	}

	Vertex<TypeHinter>& getDest(){
		return dest;
	};

	const EV& getValue() const{
		return value;
	}	

	EV& getMutableValue(){
		return value;
	}

private:
	BorderVertex<TypeHinter>& src;
	Vertex<TypeHinter>& dest;
	EV value;
};

struct EdgePair{
	VertexId id;
	double value;
};

struct Line{
	VertexId source;
	list<EdgePair> edges;
};

}}

#endif   /* ----- #ifndef SYSTEM_RUNTIME_H_INC  ----- */

