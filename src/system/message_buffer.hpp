/*
 * =====================================================================================
 *
 *       Filename:  message_buffer.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  10/02/2015 04:54:22 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef  SYSTEM_SYNCHRONIZE_MESSAGE_BUFFER_HPP_INC
#define  SYSTEM_SYNCHRONIZE_MESSAGE_BUFFER_HPP_INC

#include <system/synchronize/message_buffer.h>
#include "common/atomic.h"
#include "system/runtime_constants.h"
#include <unistd.h>
using bsp::atomic::atomic_incre;

template <class TypeHinter>
void bsp::runtime::synchronize::MessageBuffer<TypeHinter>::onNewMessage(const VertexId &src, const MV &mv,
                                                                    PROGRAM &program, bool is_local) {
    //we update the value in the new_container
    BV* base_value = new_container->findValue(src);
    if(base_value == NULL){
        base_value = program.initBufferMessage();
        new_container->insertValue(src, base_value);

        BV* old_base_value = old_container->findValue(src);
        if(old_base_value != NULL){
            *base_value = *old_base_value;
        }
    }

    //_TRACEE("buffer base_value is %lf\n", *base_value);
    program.onNewBufferMessage(src, mv, *base_value);

    //now update the message_map
    if(!need_notify){
        if(!is_local){
            vector<InEdge<TypeHinter>*> in_edges = provider.getInEdges();
            vector<int>* edge_indices = provider.getInEdgeIndicesFromSrc(src);
            if(edge_indices != NULL){
                vector<int>::iterator vi = edge_indices->begin();
                for(;vi != edge_indices->end();++vi){
                    InEdge<TypeHinter>* in_edge = in_edges[*vi];
                    int inner_id = in_edge->getDest().getInnerId();

                    new_container->message_map.setBit(inner_id);
                }
            }
        }
        else{
            vector<InnerEdge<TypeHinter>*>& inner_edges = provider.getInnerEdges();
            vector<int>& inner_index = provider.getInnerEdgeIndicesFromSrc(src);

            vector<int>::iterator vi = inner_index.begin();
            for(;vi != inner_index.end();++vi){
                InnerEdge<TypeHinter>* inner_edge = inner_edges[*vi];
                int inner_id = inner_edge->getDest()->getInnerId();

                new_container->message_map.setBit(inner_id);
            }
        }
    }


    //add this vertices in
    NotifierContext nc(src, is_local);
    while(!new_container->notifier.push(nc));

    //new_container->notifier.push_back(NotifierContext(src, is_local));
    //_TRACEE("mB: push_back \n");
    if(need_notify){
        //if we need to remember who has signaled
        atomic_incre<int>(&new_container->max_notifier, 1);
    }
}

template <class TypeHinter>
void bsp::runtime::synchronize::Notifier<TypeHinter>::run(){
    while(!stopped){
        synchronizer.waitForTurnStart();
        _TRACEE("notifier turn initialzed \n");

        //now I will start to do my task
        //I'll wait till more works to do
        //get the value container for this turn

        ValueContainer<TypeHinter>& container = *buffer.new_container;
        //now we can go deal with unfinished notifier

        int& max_notifier = container.max_notifier;
        bool& turn_stopped = container.turn_stopped;
        unordered_map<VertexId, vector<InEdge<TypeHinter>* > >& in_messages = buffer.new_container->in_messages;
        unordered_map<VertexId, vector<InnerEdge<TypeHinter>* > >& inner_messages = buffer.new_container->inner_messages;
//        _TRACEE("in_messages = %p, inner_messages = %p\n", &in_messages, &inner_messages);

        spsc_queue<NotifierContext>& notifier = container.notifier;

        while(max_notifier == 0 && !turn_stopped){
            usleep(50);
        }

        while(true){
            while(next_to_notify >= max_notifier && !turn_stopped){ //no messages here
                //_TRACEE("notifier max_notifier = %d, next_to_notify = %d, turn_stopped = %d \n", max_notifier, next_to_notify, turn_stopped);
                usleep(50);
            }

            if(turn_stopped && next_to_notify >= max_notifier){
                break;
            }

            while(next_to_notify < max_notifier){
                //_TRACEE("notifier max_notifier = %d, next_to_notify = %d, turn_stopped = %d \n", max_notifier, next_to_notify, turn_stopped);
                if(next_to_notify % 100000 == 0){
                    _TRACEE("%s notifier: next_to_notifier = %d, max_notifier = %d \n %s", GREEN, next_to_notify, max_notifier, NONE);
                }

                NotifierContext nc;
                while(!notifier.pop(nc));

                VertexId next = nc.src;
                bool is_local = nc.is_local;

                if(!is_local){
                    //handle next
                    vector<InEdge<TypeHinter>*>& in_edges = provider.getInEdges();
                    vector<int>* in_index = provider.getInEdgeIndicesFromSrc(next);
//                   _TRACEE("notifier for %ld, is_local = %d, in_index = %zd\n", next, is_local, in_index->size());

                    vector<int>::iterator vi = in_index->begin();
                    for(;vi != in_index->end();++vi){
                        InEdge<TypeHinter>* edge = in_edges[*vi];

                        const VertexId &dest_id = edge->getDest().getId();
                        int inner_id = edge->getDest().getInnerId();

                        in_messages[dest_id].push_back(edge);
                        buffer.new_container->message_map.setBit(inner_id);
                    }
                }
                else{
                    //handle local message
                    vector<InnerEdge<TypeHinter>*>& inner_edges = provider.getInnerEdges();
                    vector<int>& inner_index = provider.getInnerEdgeIndicesFromSrc(next);
//                    _TRACEE("notifier for %ld, is_local = %d, inner_index = %zd\n", next, is_local, inner_index.size());

                    vector<int>::iterator vi = inner_index.begin();
                    for(;vi != inner_index.end();++vi){
                        InnerEdge<TypeHinter>* inner_edge = inner_edges[*vi];

                        const VertexId& dest_id = inner_edge->getDest()->getId();
                        int inner_id = inner_edge->getDest()->getInnerId();

                        inner_messages[dest_id].push_back(inner_edge);
                        buffer.new_container->message_map.setBit(inner_id);
                    }
                }

                atomic_incre(&next_to_notify, 1);
            }

            _TRACEE("%s notifier: next_to_notifier = %d, max_notifier = %d \n %s", GREEN, next_to_notify, max_notifier, NONE);
        }


        _TRACEE("notifier turn stopped \n");
        next_to_notify = 0;
        synchronizer.notifyForMyTaskFinished();
    }
}


template <class TypeHinter>
void bsp::runtime::synchronize::MessageBuffer<TypeHinter>::turnInitialize(PROGRAM &program) {
    //nop
    if(need_notify){
        notifier_synchronizer->notifyForTurnStarted();
    }
}

template <class TypeHinter>
void bsp::runtime::synchronize::MessageBuffer<TypeHinter>::turnFinalize(PROGRAM &program) {
    if(need_notify){
        ValueContainer<TypeHinter>& container = *new_container;
        container.turn_stopped = true;
        _TRACEE("ask notifier to stop\n");
        notifier_synchronizer->waitForMyTask();
    }

    //_TRACEE("old = %p, new = %p\n", old_container, new_container);
    std::swap(old_container, new_container);
    //_TRACEE("old = %p, new = %p\n", old_container, new_container);

    //clear the new container now
    new_container->reset();

    //print old_container
    //_TRACEE("in_messages = %p, inner_messages = %p\n", &old_container->in_messages, &old_container->inner_messages);
    //_TRACEE("old container: %p %zd %zd\n", (void*)old_container, old_container->in_messages.size(), old_container->inner_messages.size());
    unordered_map<VertexId, vector<InEdge<TypeHinter>* > >& in_messages = old_container->in_messages;
    unordered_map<VertexId, vector<InnerEdge<TypeHinter>* > >& inner_messages = old_container->inner_messages;
//    typename unordered_map<VertexId, vector<InEdge<TypeHinter>* > >::iterator mi = in_messages.begin();
//    for(;mi != in_messages.end();++mi){
//        _TRACEE("remote %ld %zd\n", mi->first, mi->second.size());
//    }
//
//    typename unordered_map<VertexId, vector<InnerEdge<TypeHinter>* > >::iterator imi = inner_messages.begin();
//    for(;imi != inner_messages.end();++imi){
//        _TRACEE("local %ld %zd\n", imi->first, imi->second.size());
//    }

}


#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_MESSAGE_BUFFER_HPP_INC  ----- */
