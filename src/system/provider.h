/*
 * =====================================================================================
 *
 *       Filename:  provider.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/15/2015 03:03:14 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_PROVIDER_H_INC
#define  SYSTEM_PROVIDER_H_INC

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include "common/configuration.h"
#include "common/obj_factory.h"
#include "system/runtime.h"
#include "system/runtime_bg.h"
using std::vector;
using std::unordered_map;
using bsp::common::ObjFactory;
using bsp::common::DefaultInit;
using bsp::common::Configuration;

namespace bsp{ namespace runtime{

class VertexInfo{
public:
	int getNeighborCount() const{
		return inner_edges.size() + outer_edges.size();
	}
public:
	vector<int> inner_edges;
	vector<int> dest_inner_edges;
	vector<int> outer_edges;
	vector<int> in_edges;
	unordered_set<int> outer_subgraphs;
};


class BorderVertexInfo{
public:
	vector<int> in_edges;
};


template<class TypeHinter>
class Provider{
public:
	typedef typename TypeHinter::VertexValue VV;
	typedef typename TypeHinter::EdgeValue EV;
	typedef typename TypeHinter::Partitioner PART;

	Provider(const Configuration& configuration, PART& part);

	//for input
	void addVertex(const VertexId& id, const VV& vv);										
	void addEdge(const VertexId& src, const VertexId& dest, const EV& vv);

	vector<Vertex<TypeHinter>* >& getVertices(){
		return vertices;
	}

	int totalLocalVertices() const{
		return this->local_total_vertices;
	}

	int totalLocalEdge() const{
		return this->local_total_edges;
	}

	int totalVertices() const{
		return this->total_vertices;
	}

	int totalEdges() const{
		return this->total_edges;
	}

	void setTotalVertices(int t){
		this->total_vertices = t;
	}

	void setTotalEdges(int t){
		this->total_edges = t;
	}

    int totalBorderVertices() const{
        return this->border_vertices.size();
    }

//	unordered_map<VertexId, vector<int> >& getInEdgeMap(){
//		return in_edges_by_src;
//	}

	vector<InEdge<TypeHinter>*>& getInEdges(){
		return in_edges;
	}

	vector<InnerEdge<TypeHinter>*>& getInnerEdges(){
		return inner_edges;
	}

	vector<OuterEdge<TypeHinter>*>& getOuterEdges(){
		return outer_edges;
	}

//	vector<int>* getInEdgeIndicesFromSrc(const VertexId& vid){
//		InEdgeMap::iterator mi = in_edges_by_src.find(vid);
//		if(mi == in_edges_by_src.end()){
//			return NULL;
//		}
//
//		return &mi->second;
//	}

	vector<int>& getInnerEdgeIndicesFromSrc(const VertexId& vid){
		Vertex<TypeHinter>* vertex = getVertexById(vid);
		int inner_id = vertex->getInnerId();

		VertexInfo* info = vertex_infos[inner_id];
		return info->inner_edges;
	}

	unordered_set<int>& getOutWorkers(int inner_id){
		VertexInfo* info = vertex_infos[inner_id];
		return info->outer_subgraphs;
	}

	void dump();

	Vertex<TypeHinter>* getVertexByInnerId(int inner_id){
		return vertices[inner_id];
	}

	Vertex<TypeHinter>* getVertexById(const VertexId& id){
		MappingMap::iterator mi = id_inner_map.find(id);
		if(mi == id_inner_map.end()){
			return NULL;
		}

		int inner_id = mi->second;
		return getVertexByInnerId(inner_id);
	}

    BorderVertex<TypeHinter>* getBorderVertexByInnerId(int inner_id){
        return border_vertices[inner_id];
    }

    BorderVertex<TypeHinter>* getBorderVertexById(const VertexId& id){
        MappingMap::iterator mi = border_id_inner_map.find(id);
        if(mi == border_id_inner_map.end()){
            return NULL;
        }

        int inner_id = mi->second;
        return getBorderVertexByInnerId(inner_id);
    }

	VertexInfo* getVertexInfoByInnerId(int inner_id){
		return vertex_infos[inner_id];
	}

    BorderVertexInfo* getBorderVertexInfoByInnerId(int inner_id){
        return border_vertex_infos[inner_id];
    }

	PART& getPart(){
		return part;
	}

private: //all the data for the graph
	vector<Vertex<TypeHinter>*> vertices;
	ObjFactory<Vertex<TypeHinter>, VertexInit<TypeHinter> > vertex_factory;

	vector<VertexInfo*> vertex_infos;
	ObjFactory<VertexInfo, DefaultInit<VertexInfo> > vertex_info_factory;

    vector<BorderVertex<TypeHinter>*> border_vertices;
    ObjFactory<BorderVertex<TypeHinter>, BorderVertexInit<TypeHinter> > border_vertex_factory;

    vector<BorderVertexInfo*> border_vertex_infos;
    ObjFactory<BorderVertexInfo, DefaultInit<BorderVertexInfo> > border_vertex_info_factory;

	vector<InnerEdge<TypeHinter>*> inner_edges;
	ObjFactory<InnerEdge<TypeHinter>, InnerEdgeInit<TypeHinter> > inner_edge_factory;

	vector<OuterEdge<TypeHinter>*> outer_edges;
	ObjFactory<OuterEdge<TypeHinter>, OuterEdgeInit<TypeHinter> > outer_edge_factory;

	vector<InEdge<TypeHinter>*> in_edges;
	ObjFactory<InEdge<TypeHinter>, InEdgeInit<TypeHinter> > in_edge_factory;

//	typedef unordered_map<VertexId, vector<int> > InEdgeMap;
//	InEdgeMap in_edges_by_src;

	typedef unordered_map<VertexId, int> MappingMap;
	MappingMap id_inner_map;
    MappingMap border_id_inner_map;

	int local_total_vertices;
	int local_total_edges;
	int total_vertices;
	int total_edges;

public: //add all vertices in a list
	VertexList<TypeHinter> vertex_list;
private: //other required objects;
	const Configuration& configuration;
	PART& part;
	int order;

public://other statistics
	int cut_edge;
};


}}

#endif   /* ----- #ifndef SYSTEM_PROVIDER_H_INC  ----- */


