/*
 * =====================================================================================
 *
 *       Filename:  provider.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/15/2015 03:03:19 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_PROVIDER_HPP_INC
#define  SYSTEM_PROVIDER_HPP_INC

#include "system/provider.h"
#include "common/configuration.h"
#include "system/runtime_constants.h"
using bsp::common::Configuration;
using namespace bsp::runtime;

template<class TypeHinter>
bsp::runtime::Provider<TypeHinter>::Provider(const Configuration& _conf, PART& part) :
        configuration(_conf), part(part), local_total_vertices(0), local_total_edges(0), total_vertices(0), total_edges(0){
	order = configuration.getIntValue(CONF_KEY_ORDER, 0);
}

template<class TypeHinter>
void bsp::runtime::Provider<TypeHinter>::addVertex(const VertexId& id, const VV& vv){
	int vp = part.getPart(id);
	if(vp != order){
		return;
	}

	MappingMap::iterator mi = id_inner_map.find(id);
	if(mi == id_inner_map.end()){
		this->local_total_vertices += 1;


		int inner_id = id_inner_map.size();
	
		//create the new vertex
		VertexInit<TypeHinter> vi(id, inner_id, vv);
		Vertex<TypeHinter>* vertex = vertex_factory.newObject(vi);
		vertices.push_back(vertex);
		id_inner_map[id] = inner_id;

		DefaultInit<VertexInfo> di;
		VertexInfo* vertex_info = vertex_info_factory.newObject(di);
		vertex_infos.push_back(vertex_info);

		//then we add the vertices to the linked list
		vertex_list.push_back(vertex);
	}
}

template<class TypeHinter>
void bsp::runtime::Provider<TypeHinter>::addEdge(const VertexId& src, const VertexId& dest, const EV& ev){
	int src_part = part.getPart(src);				
	int dest_part = part.getPart(dest);

	if(src_part == order){ //This vertex belongs to me
		this->local_total_edges += 1;

		MappingMap::iterator src_mi = id_inner_map.find(src);
		if(src_mi == id_inner_map.end()){
			throw BaseException("Edges added before source vertex");
		}
		int inner_id = src_mi->second;
		Vertex<TypeHinter>* src_vertex = vertices[inner_id];
		VertexInfo* info = vertex_infos[inner_id];

		MappingMap::iterator dest_mi = id_inner_map.find(dest);

		if(dest_part != src_part){ //This is an outer edge
			OuterEdgeInit<TypeHinter> oe_init(dest, ev);
			OuterEdge<TypeHinter>* outer_edge = outer_edge_factory.newObject(oe_init);
			size_t oe_id = outer_edges.size();
			outer_edges.push_back(outer_edge);

			info->outer_edges.push_back(oe_id);
			info->outer_subgraphs.insert(dest_part);
		}
		else{//this is an inner edge
			MappingMap::iterator dest_mi = id_inner_map.find(dest);
			if(dest_mi == id_inner_map.end()){
				throw BaseException("Edges added before dest vertex");
			}

			Vertex<TypeHinter>* dest_vertex = vertices[dest_mi->second];
			VertexInfo* dest_vertex_info = vertex_infos[dest_mi->second];

			InnerEdgeInit<TypeHinter> ie_init(src_vertex, dest_vertex, ev);
			InnerEdge<TypeHinter>* inner_edge = inner_edge_factory.newObject(ie_init);
			size_t ie_id = inner_edges.size();
			inner_edges.push_back(inner_edge);

			info->inner_edges.push_back(ie_id);
			dest_vertex_info->dest_inner_edges.push_back(ie_id);
		}
	}
	else{
		if(dest_part == order){ //record this input edge
			//add the border vertex if necessary
			MappingMap::iterator src_mi = border_id_inner_map.find(src);

			BorderVertex<TypeHinter>* border_vertex = NULL;
			BorderVertexInfo* border_vertex_info = NULL;
			if(src_mi == border_id_inner_map.end()){
				//add the vertex
				int inner_id = border_id_inner_map.size();

				//create the new vertex
				BorderVertexInit<TypeHinter> bvi(src, inner_id);
				border_vertex = border_vertex_factory.newObject(bvi);
				border_vertex->setPart(src_part);

				border_vertices.push_back(border_vertex);
				border_id_inner_map[src] = inner_id;

				DefaultInit<BorderVertexInfo> di;
				border_vertex_info = border_vertex_info_factory.newObject(di);
				border_vertex_infos.push_back(border_vertex_info);
			}
			else{
				int inner_id = src_mi->second;
				border_vertex = border_vertices[inner_id];
				border_vertex_info = border_vertex_infos[inner_id];
			}

			MappingMap::iterator dest_mi = id_inner_map.find(dest);
			if(dest_mi == id_inner_map.end()){
				throw BaseException("Edges added before dest vertex");
			}

			int inner_id = dest_mi->second;
			Vertex<TypeHinter>* dest_vertex = vertices[inner_id];
			//_TRACEE("add %ld to %d\n", dest, inner_id);
			VertexInfo* info = vertex_infos[inner_id];

			InEdgeInit<TypeHinter> in_init(*border_vertex, *dest_vertex, ev);
			InEdge<TypeHinter>* in_edge = in_edge_factory.newObject(in_init);
			//_TRACEE("%ld->%ld\n", in_edge->getSrc(), in_edge->getDest().getId());
			size_t in_id = in_edges.size();
			in_edges.push_back(in_edge);

			info->in_edges.push_back(in_id);
			border_vertex_info->in_edges.push_back(in_id);
		}
	}
}


template<class TypeHinter>
void bsp::runtime::Provider<TypeHinter>::dump(){
    _TRACEE("vertex count = %zd, inener edges = %zd, outer edges = %zd, in edges = %zd\n",
            vertices.size(), inner_edges.size(), outer_edges.size(), in_edges.size());

    typename vector<Vertex<TypeHinter>*>::iterator vi = vertices.begin();
    typename vector<VertexInfo*>::iterator info_vi = vertex_infos.begin();
    for(;vi != vertices.end(),info_vi != vertex_infos.end();++vi,++info_vi){
        Vertex<TypeHinter>* vertex = *vi;
        VertexInfo* info = *info_vi;

        _TRACEE("vertex %ld\n", vertex->getId());
        _TRACEE("inner edges: \n");
        vector<int>::iterator nvi = info->inner_edges.begin();
        for(;nvi != info->inner_edges.end();++nvi){
            InnerEdge<TypeHinter>* ie = inner_edges[*nvi];
            _TRACEE("\t-> %ld, value = %lf\n", ie->getDest()->getId(), ie->getValue());
        }

        _TRACEE("outer edges: \n");
        nvi = info->outer_edges.begin();
        for(;nvi != info->outer_edges.end();++nvi){
            OuterEdge<TypeHinter>* oe = outer_edges[*nvi];
            _TRACEE("\t-> %ld, value = %lf\n", oe->getDest(), oe->getValue());
        }

        _TRACEE("in edges: \n");
        nvi = info->in_edges.begin();
        for(;nvi != info->in_edges.end();++nvi){
            InEdge<TypeHinter>* ie = in_edges[*nvi];
            _TRACEE("\tfrom %ld to %ld, value = %lf, %p\n", ie->getSrc().getId(), ie->getDest().getId(), ie->getValue());
        }

        _TRACEE("outer workers: \n");
        unordered_set<int>::iterator si = info->outer_subgraphs.begin();
        for(;si != info->outer_subgraphs.end();++si){
            _TRACEE("\thas edges to %d\n", *si);
        }
    }
}

#endif   /* ----- #ifndef SYSTEM_PROVIDER_HPP_INC  ----- */
