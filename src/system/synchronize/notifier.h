/*
 * =====================================================================================
 *
 *       Filename:  notifier.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年11月26日 16时46分57秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef  SYSTEM_SYNCHRONIZER_NOTIFIER_INC
#define  SYSTEM_SYNCHRONIZER_NOTIFIER_INC

#include "common/thread_group.h"
#include "common/thread_local.hpp"
#include <unordered_map>
#include "common/atomic.h"
#include <boost/lockfree/spsc_queue.hpp>
#include "system/runtime_constants.h"
#include "system/runtime.h"

using bsp::multi_thread::IndexedRunnable;
using bsp::multi_thread::ThreadGroup;
using bsp::atomic::TurnedSynchronizer;
using std::unordered_map;
using bsp::atomic::ConcurrentBitmap;
using boost::lockfree::spsc_queue;
using bsp::atomic::atomic_incre;
using bsp::runtime::BorderVertex;

namespace bsp {
    namespace runtime {
        namespace synchronize {
            template<class TypeHinter>
            struct NotifyRemoteMessage {
                typedef typename TypeHinter::MessageValue MV;

                NotifyRemoteMessage() :
                        from(0), border_vertex(NULL) { }

                NotifyRemoteMessage(int _f, MV &_mv, VertexId _vid, BorderVertex<TypeHinter> *_bv) :
                        from(_f), mv(_mv), vid(_vid), border_vertex(_bv) {
                }

                int from;
                MV mv;
                VertexId vid;
                BorderVertex<TypeHinter> *border_vertex;
            };

            template<class TypeHinter>
            struct NotifyLocalMessage {
                typedef typename TypeHinter::MessageValue MV;

                NotifyLocalMessage() :
                        vertex(NULL) { }

                NotifyLocalMessage(MV &_mv, Vertex<TypeHinter> *_v) :
                        mv(_mv), vertex(_v) {
                }

                MV mv;
                Vertex<TypeHinter> *vertex;
            };

            template<class TypeHinter>
            struct NotifyMessage {
                NotifyMessage() : last_updated_turn(0){
                }

                typedef typename TypeHinter::NotifyValue NV;

                NV nv;
                int last_updated_turn;
            };

            template<class TypeHinter>
            struct NotifierThreadContainer {
                NotifierThreadContainer(size_t _size) {
                    in_messages.resize(_size);
                    inner_messages.resize(_size);
                }

//                unordered_map<VertexId, NotifyMessage<TypeHinter> > in_messages;
//                unordered_map<VertexId, NotifyMessage<TypeHinter> > inner_messages;

                vector<NotifyMessage<TypeHinter> > in_messages;
                vector<NotifyMessage<TypeHinter> > inner_messages;


//                vector<NotifyMessage<TypeHinter> > remote_messages;
//                vector<NotifyMessage<TypeHinter> > local_messages;

//                unordered_map<VertexId, vector<InEdge<TypeHinter> *> > in_messages;
//                unordered_map<VertexId, vector<InnerEdge<TypeHinter> *> > inner_messages;
            };

            struct NotiferGlobalContainer {
                NotiferGlobalContainer(size_t size) : bitmap(size) {

                }

                ConcurrentBitmap bitmap;
            };

            template<class TypeHinter>
            struct NotifierThreadContext {
                NotifierThreadContainer<TypeHinter> *old_container;
                NotifierThreadContainer<TypeHinter> *new_container;
            };

            template<class TypeHinter>
            class NewNotifier;

            template<class TypeHinter>
            class NewNotifierRunnable : public IndexedRunnable {
            public:
                typedef typename TypeHinter::Provider PROVIDER;
                typedef typename TypeHinter::Program PROGRAM;
            public:
                NewNotifierRunnable(int index) :
                        IndexedRunnable(index),
                        remote_queue(2000000),
                        local_queue(2000000),
                        turn_remote_remained(0),
                        turn_local_remained(0) {
                }

                void setNotifier(NewNotifier<TypeHinter> *_n) {
                    notifier = _n;
                    if (notifier->program.isNotify()) {
                        context.old_container = new NotifierThreadContainer<TypeHinter>(
                                notifier->provider.totalLocalVertices());
                        context.new_container = new NotifierThreadContainer<TypeHinter>(
                                notifier->provider.totalLocalVertices());
                    }
                }

                void swap() {
                    std::swap(context.old_container, context.new_container);
//                    context.new_container->in_messages.clear();
//                    context.new_container->inner_messages.clear();

                    _TRACEE("old container in size = %ld\n", context.old_container->in_messages.size());
                    _TRACEE("old container inner size = %ld\n", context.old_container->inner_messages.size());
                    turn_remote_remained = 0;
                    turn_local_remained = 0;
                }

                void run();

                TurnedSynchronizer &getSynchronizer() {
                    return synchronizer;
                }

                NotifierThreadContext<TypeHinter> &getContext() {
                    return context;
                }

                spsc_queue<NotifyRemoteMessage<TypeHinter> > &getRemoteQueue() {
                    return remote_queue;
                }

                spsc_queue<NotifyLocalMessage<TypeHinter> > &getLocalQueue() {
                    return local_queue;
                }

                NotifyMessage<TypeHinter>* getNewRemoteBuffer(int inner_id, int turn){
                    NotifyMessage<TypeHinter>& message = context.new_container->in_messages[inner_id];
                    if(message.last_updated_turn == turn){
                        return &message;
                    }

                    return NULL;
                }

                NotifyMessage<TypeHinter> *getNewLocalBuffer(int inner_id, int turn) {
                    NotifyMessage<TypeHinter>& message = context.new_container->inner_messages[inner_id];
                    if(message.last_updated_turn == turn){
                        return &message;
                    }

                    return NULL;
                }


                NotifyMessage<TypeHinter> *getRemoteBuffer(int inner_id, int turn) {
//                    typename unordered_map<VertexId, NotifyMessage<TypeHinter> >::iterator mi = context.old_container->in_messages.find(
//                            vid);
//                    if (mi != context.old_container->in_messages.end()) {
//                        return &mi->second;
//                    }
                    NotifyMessage<TypeHinter>& message = context.old_container->in_messages[inner_id];
                    if(message.last_updated_turn == turn){
                        return &message;
                    }

                    return NULL;
                };

                NotifyMessage<TypeHinter> *getLocalBuffer(int inner_id, int turn) {
                    NotifyMessage<TypeHinter>& message = context.old_container->inner_messages[inner_id];
                    if(message.last_updated_turn == turn){
                        return &message;
                    }

                    return NULL;
                }
//                    typename unordered_map<VertexId, NotifyMessage<TypeHinter> >::iterator mi = context.old_container->inner_messages.find(
//                            vid);
//                    if (mi != context.old_container->inner_messages.end()) {
//                        return &mi->second;
//                    }
//
//                    return NULL;
//                };


//                NotifyMessage<TypeHinter> *getLocalBuffer(const VertexId &vid, int turn) {
//                    typename unordered_map<VertexId, NotifyMessage<TypeHinter> >::iterator mi = context.old_container->inner_messages.find(
//                            vid);
//                    if (mi != context.old_container->inner_messages.end()) {
//                        return &mi->second;
//                    }
//
//                    return NULL;
//                };

                void increRemote() {
                    atomic_incre(&turn_remote_remained, 1);
                }

                void increLocal() {
                    atomic_incre(&turn_local_remained, 1);
                }

                void decreRemote() {
                    atomic_incre(&turn_remote_remained, -1);
                }

                void decreLocal() {
                    atomic_incre(&turn_local_remained, -1);
                }

            private:
                void _handle_remote_message(NotifyRemoteMessage<TypeHinter> &rm);

                void _handle_local_message(NotifyLocalMessage<TypeHinter> &lm);

            private:
                NewNotifier<TypeHinter> *notifier;
                TurnedSynchronizer synchronizer;

                NotifierThreadContext<TypeHinter> context;

                spsc_queue<NotifyRemoteMessage<TypeHinter> > remote_queue;
                spsc_queue<NotifyLocalMessage<TypeHinter> > local_queue;

                //other statistics
                int turn_remote_remained;
                int turn_local_remained;
            };

            template<class TypeHinter>
            class NotifierIterator {
            public:
                NotifierIterator(NewNotifier<TypeHinter> &nn, int _iid, int _t, bool use_new = false) :
                        notifier(nn), inner_id(_iid), turn(_t),
                        remote_index(0), local_index(0), use_new(use_new) {
                }

                NotifyMessage<TypeHinter> *remoteGetNext() {
                    while (remote_index < notifier.worker_size) {
                        NotifyMessage<TypeHinter> *message = NULL;
                        if(use_new){
                            message = notifier.workers.getRunnable(
                                    remote_index)->getNewRemoteBuffer(inner_id, turn);
                        }
                        else{
                            message = notifier.workers.getRunnable(
                                    remote_index)->getRemoteBuffer(inner_id, turn);
                        }

                        if (message != NULL) {
                            ++remote_index;
                            return message;
                        }
                        else{
                            ++remote_index;
                        }
                    }

                    return NULL;
//
//
//                        if (!remote_current) {//a queue has been finished
//                            while (remote_index < notifier.worker_size) {
//                                remote_current = notifier.workers.getRunnable(remote_index)->getRemoteBuffer(vid);
//                                if (remote_current) {
//                                    break;
//                                }
//
//                                ++remote_index;
//                            }
//
//                            if (remote_current == NULL) {
//                                assert(remote_index == notifier.worker_size);
//                                return NULL;
//                            }
//
//                            remote_vi = remote_current->begin();
//                        }
//
//                        //then we can iterator the remote_vi
//                        if (remote_vi != remote_current->end()) {
//                            InEdge<TypeHinter> *result = *remote_vi;
//                            ++remote_vi;
//                            return result;
//                        }
//                        else {
//                            remote_current = NULL;
//                            remote_index += 1;
//                        }
                }


                NotifyMessage<TypeHinter> *localGetNext() {
                    while (local_index < notifier.worker_size) {
                        NotifyMessage<TypeHinter> *message = NULL;
                        if(use_new){
                            message = notifier.workers.getRunnable(
                                    local_index)->getNewLocalBuffer(inner_id, turn);
                        }
                        else{
                            message = notifier.workers.getRunnable(
                                    local_index)->getLocalBuffer(inner_id, turn);
                        }


                        if (message != NULL) {
                            ++local_index;
                            return message;
                        }
                        else{
                            ++local_index;
                        }
                    }

                    return NULL;
//
//                    if (!local_current) {//a queue has been finished
//                        while (local_index < notifier.worker_size) {
//                            local_current = notifier.workers.getRunnable(local_index)->getLocalBuffer(vid);
//                            if (local_current) {
//                                break;
//                            }
//                            ++local_index;
//                        }
//
//                        if (local_current == NULL) {
//                            assert(local_index == notifier.worker_size);
//                            return NULL;
//                        }
//
//                        local_vi = local_current->begin();
//                    }
//
//                    //then we can iterator the remote_vi
//                    if (local_vi != local_current->end()) {
//                        InnerEdge<TypeHinter> *result = *local_vi;
//                        ++local_vi;
//                        return result;
//                    }
//                    else {
//                        local_current = NULL;
//                        local_index += 1;
//                    }1
                }

            private:
                NewNotifier<TypeHinter> &notifier;
//                VertexId vid;
                int inner_id;
                int turn;

                int remote_index;
                int local_index;
                bool use_new;
            };

            template<class TypeHinter>
            class NewNotifier {
            public:
                typedef typename TypeHinter::MessageValue MV;
                typedef typename TypeHinter::BufferValue BV;
                typedef typename TypeHinter::Program PROGRAM;
                typedef typename TypeHinter::Provider PROVIDER;

                friend class NewNotifierRunnable<TypeHinter>;

                friend class NotifierIterator<TypeHinter>;

            public:
                NewNotifier(int _ws, PROVIDER &_p, PROGRAM &_pro) :
                        worker_size(_ws),
                        workers(_ws),
                        turnStopped(false),
                        provider(_p),
                        program(_pro),
                        old_global_container(_p.totalLocalVertices()),
                        new_global_container(_p.totalLocalVertices()),
                        turn(0) {
                    for (int i = 0; i < worker_size; ++i) {
                        NewNotifierRunnable<TypeHinter> *nr = workers.getRunnable(i);
                        nr->setNotifier(this);
                    }
                }

                void start() {
                    if (program.isNotify()) {
                        workers.start();
                    }
                }

                void stop() {
                    if (program.isNotify()) {
                        for (int i = 0; i < worker_size; ++i) {
                            NewNotifierRunnable<TypeHinter> *nr = workers.getRunnable(i);
                            nr->markStop();

                            nr->getSynchronizer().notifyForTurnStarted();
                        }

                        workers.joinAll();
                    }
                }

                void turnInitialize() {
                    if (program.isNotify()) {
                        turnStopped = false;

                        for (int i = 0; i < worker_size; ++i) {
                            NewNotifierRunnable<TypeHinter> *nr = workers.getRunnable(i);
                            nr->swap();
                        }

                        std::swap(old_global_container, new_global_container);
                        new_global_container.bitmap.clearAll();

                        for (int i = 0; i < worker_size; ++i) {
                            NewNotifierRunnable<TypeHinter> *nr = workers.getRunnable(i);
                            nr->getSynchronizer().notifyForTurnStarted();
                        }
                    }
                }

                void turnFinalize() {
                    if (program.isNotify()) {
                        turnStopped = true;
                        for (int i = 0; i < worker_size; ++i) {
                            NewNotifierRunnable<TypeHinter> *nr = workers.getRunnable(i);
                            nr->getSynchronizer().waitForMyTask();
                        }
                    }

                    ++turn;
                }

                void onNewRemoteNotify(NotifyRemoteMessage<TypeHinter>  &nm) {
                    int toId = nm.from % worker_size;
                    spsc_queue<NotifyRemoteMessage<TypeHinter> > &remote_queue = workers.getRunnable(toId)->getRemoteQueue();

                    while (!remote_queue.push(nm));
                    workers.getRunnable(toId)->increRemote();
                }

                void onNewLocalNotifier(NotifyLocalMessage<TypeHinter> &lm, int queue_index) {
                    int toId = queue_index % worker_size;
                    spsc_queue<NotifyLocalMessage<TypeHinter> > &local_queue = workers.getRunnable(toId)->getLocalQueue();

                    while (!local_queue.push(lm));
                    workers.getRunnable(toId)->increLocal();
                }

                bool hasMessageReceived(const VertexId &vid) {
                    return old_global_container.bitmap.getBit(vid);
                }

                NotifierIterator<TypeHinter> *getNotifierIterator(int inner_id, int turn, bool use_new = false) {
                    return new NotifierIterator<TypeHinter>(*this, inner_id, turn, use_new);
                }

            private:
                int worker_size;
                ThreadGroup<NewNotifierRunnable<TypeHinter> > workers;

                //these variables are used to communicate with the worker threads
                bool turnStopped;

                //Global variables needed
                PROVIDER &provider;
                PROGRAM &program;

                NotiferGlobalContainer old_global_container;
                NotiferGlobalContainer new_global_container;

                int turn;
            };
        }
    }
}


#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZER_NOTIFIER_INC  ----- */
