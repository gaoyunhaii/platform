/*
 * =====================================================================================
 *
 *       Filename:  message_handler.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年12月29日 17时54分12秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYNCHRONIZE_MESSAGE_HANDLER_INC
#define  SYSTEM_SYNCHRONIZE_MESSAGE_HANDLER_INC

#include <boost/lockfree/spsc_queue.hpp>
#include "common/util.h"
#include "common/thread_group.h"
#include "common/thread_group.hpp"
#include "system/synchronize/message.h"
#include "system/synchronize/message_buffer.h"
#include "system/mpi_util.h"
using bsp::runtime::synchronize::MessageBuffer;
using boost::lockfree::spsc_queue;
using bsp::multi_thread::ThreadGroup;
using bsp::runtime::synchronize::CompositeMessage;
using bsp::runtime::get_comm_size;
using bsp::atomic::atomic_incre;
using bsp::atomic::atomic_incre_re_after;
using bsp::multi_thread::IndexedRunnable;
using bsp::atomic::TurnedSynchronizer;
using bsp::common::get_time_us;

namespace bsp{
    namespace runtime{
        namespace synchronize{
            struct RecvMessageWrapper{
                int from;
                void* buffer;
                size_t size;
            };

            class MRCoordinator {
            public:
                MRCoordinator(int _nt) :
                        nr_total(_nt), nr_remain(_nt) {
                    message_count = new int[nr_total];
                    memset(message_count, 0, sizeof(int) * nr_total);
                };

                ~MRCoordinator(){
                    delete message_count;
                }

                bool onDataMessage(int from) {
                    int current = atomic_incre_re_after<int>(message_count + from, -1);
                    //_TRACEE("current of %d is %d\n", from, current);
                    //print_count();
                    if (current == 0) {
                        return _on_finished_one(from);
                    }

                    return false;
                }


                bool onStatMessage(int from, int nr_message) {
                    int current = atomic_incre_re_after(message_count + from, nr_message);
                    //_TRACEE("on stat current of %d is %d, nr_message = %d\n", from, current, nr_message);
                    //print_count();
                    if (current == 0) {
                        return _on_finished_one(from);
                    }

                    return false;
                }

                void reset() {
                    memset(message_count, 0, sizeof(int) * nr_total);
                    nr_remain = nr_total;
                }

                void print_count(){
                    _TRACEE("mess_count: ");
                    for(int i = 0;i < nr_total;++i){
                        printf("%d ", message_count[i]);
                    }
                    printf("\n");
                }

            private:
                bool _on_finished_one(int which) {
                    assert(message_count[which] == 0);

                    int remain = atomic_incre_re_after(&nr_remain, -1);
                    //_TRACEE("finish one, remain = %d\n", remain);
                    if (remain == 0) {
                        return true;
                    }

                    return false;
                }

            private:
                int nr_total;
                int nr_remain;

                int *message_count;
            };


            template<class TypeHinter>
            class MessageHandler;

            template<class TypeHinter>
            class MessageHandlerRunnable : public IndexedRunnable{
            public:
                friend class MessageHandler<TypeHinter>;
            public:
                typedef typename TypeHinter::BufferValue BV;
                typedef typename TypeHinter::MessageValue MV;
                typedef typename TypeHinter::Program PROGRAM;
            public:
                MessageHandlerRunnable(int index) :
                        IndexedRunnable(index),
                        parent_handler(NULL){

                }

                void setMessageHandler(MessageHandler<TypeHinter>* parent){
                    this->parent_handler = parent;
                }

//                spsc_queue<RecvMessageWrapper>& getQueue(){
//                    return queue;
//                }

                TurnedSynchronizer& getSynchronizer(){
                    return synchronizer;
                }

                void run();
            private:
                bool _on_composite_message(BoundedBuffer& bb, size_t size, int from){
                    //_TRACEE("on composite message\n");

                    atomic_incre(&parent_handler->phy_mess_this_turn, 1);

                    size_t sub_size;
                    int sub_type;

                    size_t init_offset = bb.offset();

                    bool to_exit = false;
                    size_t mess_count;
                    bb >> mess_count;
//                    _TRACEE("composite: message_count = %d\n", mess_count);

                    for(int i = 0; i < mess_count; ++i){
//                        bb >> sub_type;
//                        bb >> sub_size;

                        uint16_t compressed;
                        bb >> compressed;

                        //_TRACEE("compressed = %d\n", compressed);
                        sub_type = (compressed & 0x0e000) >> 13;
                        sub_size = compressed & 0x01fff;

                        //_TRACEE("inner message, type = %d, size = %ld\n", sub_type, sub_size);
                        BoundedBuffer sub_buffer(bb.getCurrentP());

                        bool sub_to_exit;
                        switch(sub_type){
                            case NORMAL_MESSAGE:
                                sub_to_exit = _on_data_message(sub_buffer, sub_size, from);
                                break;
                            case CONTROL_MESSAGE:
                                sub_to_exit = _on_control_message(sub_buffer, sub_size, from);
                                break;
                            case AGGRE_MESSAGE:
                                sub_to_exit = _on_aggregable_message(sub_buffer, sub_size, from);
                                break;
                            default:
                                throw BaseException("Bad sub type");
                        }

                        to_exit |= sub_to_exit;
                        bb.advance(sub_size);
                    }

                    if(bb.offset() - init_offset != size){
                        _TRACEE("bad message, used = %ld, origin = %ld\n", bb.offset() - init_offset, size);
                    }
                    assert(bb.offset() - init_offset == size);
                    return to_exit;
                }

                bool _on_data_message(BoundedBuffer& bb, size_t size, int from){
                    NormalMessage<MV> normal_message;
                    size_t init_offset = bb.offset();

                    long __tmp_start = get_time_us();
                    normal_message.deserialize(bb, size);
                    assert((bb.offset() - init_offset) == size);
                    long __tmp_end = get_time_us();
                    //tm_deserial += (__tmp_end - __tmp_start);

                    __tmp_start = get_time_us();
                    parent_handler->buffer.onNewRemoteMessage(from, normal_message.src, normal_message.mv);
                    //parent_handler->buffer.onNewMessage(normal_message.src, normal_message.mv, parent_handler->program, rank == from);
                    __tmp_end = get_time_us();
                    //tm_notify += (__tmp_end - __tmp_start);

                    atomic_incre(&parent_handler->bus_mess_this_turn, 1);

                    bool fin = parent_handler->coordinator->onDataMessage(from);
                    return fin;
                }

                bool _on_control_message(BoundedBuffer& bb, size_t size, int from){
                    ControlMessage control_message;
                    size_t init_offset = bb.offset();

                    control_message.deserialize(bb, size);

                    if(bb.offset() - init_offset != size){
                        _TRACEE("bad message, used = %ld, origin = %ld\n", bb.offset() - init_offset, size);
                    }
                    assert((bb.offset() - init_offset) == size);

                    //_TRACEE("on control message, nr_message = %d\n", control_message.nr_messages);
                    bool fin = parent_handler->coordinator->onStatMessage(from, control_message.nr_messages);
                    return fin;
                }

                bool _on_aggregable_message(BoundedBuffer& bb, size_t size, int from){
                    AggregateMessage aggre_message;
                    aggre_message.deserialize(bb, size);

                    size_t remain_length = size - bb.offset();
                    parent_handler->aggregator_manager.onNewAggregateMessage(aggre_message.name.c_str(), bb, remain_length);

                    bool fin = parent_handler->coordinator->onDataMessage(from);
                    return fin;
                }
            private:
//                spsc_queue<RecvMessageWrapper> queue;
                TurnedSynchronizer synchronizer;

                MessageHandler<TypeHinter>* parent_handler;
                int rank;

                //statistics
//                long tm_receiver;
//                long tm_deserial;
//                long tm_notify;
            };

            template<class TypeHinter>
            class MessageHandler{
            public:
                friend class MessageHandlerRunnable<TypeHinter>;

                typedef typename TypeHinter::BufferValue BV;
                typedef typename TypeHinter::MessageValue MV;
                typedef typename TypeHinter::Program PROGRAM;
            public:
                MessageHandler(int queue_size, int worker_size, int _cs, PROGRAM& p, MessageBuffer<TypeHinter>& _buffer, AggregatorManager& am) :
                        handlers(worker_size), turn_stopped(false), stopped(false),
                        bus_mess_this_turn(0), phy_mess_this_turn(0),
                        comm_size(_cs), program(p), buffer(_buffer), aggregator_manager(am),
                        coordinator(NULL){
                    int size = handlers.getSize();
                    for(int i = 0;i < size;++i){
                        MessageHandlerRunnable<TypeHinter>* mhr = handlers.getRunnable(i);
                        mhr->setMessageHandler(this);
                    }

                    //coordinator
                    coordinator = new MRCoordinator(comm_size);

                    for(int i = 0;i < queue_size;++i){
                        queues.push_back(new spsc_queue<RecvMessageWrapper>(1000000));
                    }
                }

                ~MessageHandler(){
                    delete coordinator;
                    vector<spsc_queue<RecvMessageWrapper>* >::iterator vi = queues.begin();
                    for(;vi != queues.end();++vi){
                        delete (*vi);
                    }
                }

                void start() {
                    handlers.start();
                }

                void stop() {
                    int size = handlers.getSize();
                    for(int i = 0;i < size;++i){
                        MessageHandlerRunnable<TypeHinter>* mhr = handlers.getRunnable(i);
                        mhr->markStop();
                        mhr->getSynchronizer().notifyForTurnStarted();
                    }

                    handlers.joinAll();
                }

                void turnInitialize() {
                    turn_stopped = false;
                    bus_mess_this_turn = 0;
                    phy_mess_this_turn = 0;

                    coordinator->reset();
                    int size = handlers.getSize();
                    for(int i = 0;i < size;++i){
                        MessageHandlerRunnable<TypeHinter>* mhr = handlers.getRunnable(i);
                        mhr->getSynchronizer().notifyForTurnStarted();
                    }
                }

                void turnFinalize() {
                    int size = handlers.getSize();
                    for(int i = 0;i < size;++i) {
                        MessageHandlerRunnable<TypeHinter>* mhr = handlers.getRunnable(i);
                        mhr->getSynchronizer().waitForMyTask();
                    }
                }

                virtual void onNewMessage(int from, void* message, size_t size){
                    int max = queues.size();
                    int chosen = from % max;
                    spsc_queue<RecvMessageWrapper>& queue = getQueue(chosen);

                    RecvMessageWrapper wrapper;
                    wrapper.buffer = message;
                    wrapper.from = from;
                    wrapper.size = size;

                    //_TRACEE("send %p, from = %d, size = %ld\n", wrapper.buffer, wrapper.from, wrapper.size);

                    while(!queue.push(wrapper));
                }

                int getBusMessThisTurn(){
                    return bus_mess_this_turn;
                }

                int getPhyMessThisTurn(){
                    return phy_mess_this_turn;
                }
            private:
                spsc_queue<RecvMessageWrapper>& getQueue(int index){
                    return *(queues[index]);
                }
            private:
                //threads
                ThreadGroup<MessageHandlerRunnable<TypeHinter> > handlers;

                //for flow control
                bool turn_stopped;
                bool stopped;

                //the message statistics
                int bus_mess_this_turn;
                int phy_mess_this_turn;

                //context objects
                //MPI_Comm& comm;
                int comm_size;
                PROGRAM &program;
                MessageBuffer<TypeHinter> &buffer;
                AggregatorManager &aggregator_manager;

                MRCoordinator* coordinator;
                vector<spsc_queue<RecvMessageWrapper>* > queues;
            };
        }
    }
}


#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_MESSAGE_HANDLER_INC  ----- */
