/*
 * =====================================================================================
 *
 *       Filename:  syn_executor.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年10月13日 11时13分00秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef  SYSTEM_SYNCHRONIZE_SYN_EXECUTOR_HPP_INC
#define  SYSTEM_SYNCHRONIZE_SYN_EXECUTOR_HPP_INC

#include "system/synchronize/syn_executor.h"
#include "system/synchronize/message_buffer.h"
#include "system/synchronize/message_buffer.hpp"
#include "common/util.h"
using bsp::runtime::synchronize::MessageBuffer;
using bsp::runtime::synchronize::Receiver;
using bsp::atomic::atomic_incre_re;
using bsp::common::get_time_us;

template <class TypeHinter>
void bsp::runtime::synchronize::SynExecutor<TypeHinter>::run(){
    int comm_size = get_comm_size(comm);
    int rank = get_rank(comm);

    //initialize all the required component
    AggregatorManager aggregator_manager(comm_size);
    p_am = &aggregator_manager;
    aggregator_manager.start();

    //now we clone a comm for sender and receiver

    MPI_Comm* comms = new MPI_Comm[comm_size];
    for(int i = 0;i < comm_size;++i){
        MPI_Comm_dup(comm, comms + i);
    }

//    MPI_Comm message_comm;
//    MPI_Comm_dup(comm, &message_comm);
    Sender<TypeHinter> sender(comms, threads->getWorkerSize(), 4);
    p_sender = &sender;
    sender.start();
    _TRACEE("sender started\n");

    int receiver_worker_size = 2;
    int handler_size = 2;
    NewNotifier<TypeHinter> notifier(receiver_worker_size, provider, program);
    p_notifier = &notifier;
    notifier.start();
    _TRACEE("notifier started\n");

    //start the receiver
    MessageBuffer<TypeHinter> buffer(receiver_worker_size, rank, provider, program, notifier);
    p_message_buffer = &buffer;
    buffer.start();
    _TRACEE("buffer started\n");

    MessageHandler<TypeHinter> handler(receiver_worker_size, handler_size, comm_size, program, buffer, aggregator_manager);
    p_message_handler = &handler;
    handler.start();
    _TRACEE("handed started\n");

    //start the receive
    Receiver<TypeHinter> receiver(comms[rank], handler);
    p_receiver = &receiver;
    _TRACEE("now we start to receive messages\n");
    receiver.start();

    //wait till every one is prepared
    MPI_Barrier(comm);
    _TRACEE("now all has been started\n");

    threads->start();
    _TRACEE("worker threads all has been started\n");

    //now we do some execution
    int total_messages = 0;
    int total_local_messages = 0;
    int total_update = 0;

    while(true){
        _TRACEE("------start turn %d\n", turn);
        active_count = 0;

        //start a new turn
        sender.turnInitialize();
        long sender_start = get_time_us();
        _TRACEE("------turn %d, sender initialized\n",  turn);

        aggregator_manager.turnIntialize();
        long am_start = get_time_us();
        _TRACEE("------turn %d, aggregator manager initialized\n",  turn);

        notifier.turnInitialize();
        long not_start = get_time_us();
        _TRACEE("------turn %d, not initialized\n",  turn);

        buffer.turnInitialize();
        long buffer_start = get_time_us();
        _TRACEE("------turn %d, buffer initialized\n",  turn);

        handler.turnInitialize();
        long handler_start = get_time_us();
        _TRACEE("------turn %d, handler initialized\n",  turn);

        receiver.turnInitialize();
        _TRACEE("------turn %d, receiver initialized\n",  turn);
        long receiver_start = get_time_us();

        next_to_compute = 0;

        //do our computation
        long exec_start = get_time_us();
        _TRACEE("------turn %d, before computation\n",  turn);
        program.beforeTurn(*this);
        threads->execute(&SynExecutor<TypeHinter>::updateOneTurn);
        _TRACEE("------turn %d, after computation\n",  turn);
        long exec_end = get_time_us();

        _TRACEE("%s exec used %ld mill\n %s", GREEN, (exec_end - exec_start) / 1000, NONE);

        //here we stop the sender first
        sender.turnFinalize();
        _TRACEE("sender used %ld mill\n", (get_time_us() - sender_start) / 1000);
        _TRACEE("------turn %d, sender stopped\n",  turn);

        //we first make sure all messages are sent, then we send control messages
        int* nr_messages = sender.getBusMessageCount();
        //send control messages
        for(int i = 0;i < comm_size;++i){
            _TRACEE("------turn %d, send %d messages to %d\n",  turn, nr_messages[i], i);
            ControlMessage control_message(nr_messages[i]);
            sender.sendRawMessage(&control_message, i);
        }

        //then we can wait till all messages are received
//        receiver.turnFinalize();
//        _TRACEE("receiver used %ld mill\n", (get_time_us() - receiver_start) / 1000);
//        _TRACEE("receiver stopped\n");

        handler.turnFinalize();
        _TRACEE("handler used %ld mill\n", (get_time_us() - handler_start) / 1000);
        _TRACEE("handler stopped\n");

        buffer.turnFinalize();
        _TRACEE("buffer used %ld mill\n", (get_time_us() - buffer_start) / 1000);
        _TRACEE("buffer stopped\n");

        notifier.turnFinalize();
        _TRACEE("notifier used %ld mill\n", (get_time_us() - not_start) / 1000);
        _TRACEE("notifier stopped\n");

        aggregator_manager.waitTillAllReceived();
        _TRACEE("aggregator all received\n");

        //here we wait till all are added
        program.updateNextTurnAggregator(*this);
        aggregator_manager.turnFinalize();

        _TRACEE("am used %ld mill\n", (get_time_us() - am_start) / 1000);
        _TRACEE("aggregator stopped\n");

        //MPI_Barrier(comm);
        _TRACEE("------end turn %d\n\n\n\n",  turn);
        ++ turn;

        //we adjust whether we need to stop
        int my_value = 0;
        int data_messages = handler.getBusMessThisTurn();
        int local_messages = buffer.getLocalMessThisTurn();

        if(active_count != 0 || data_messages != 0 || local_messages != 0){
            my_value = 1;
        }

        int theysaycont = program.afterTurn(*this, comm);
        if(theysaycont == 0){
            my_value = 0;
        }

        int all_values = 0;
        MPI_Allreduce(&my_value, &all_values, 1, MPI_INT, MPI_SUM, comm);

        _TRACEE("active = %d, dm = %d, lm = %d, my_value = %d, all_values = %d, update = %d\n", active_count, data_messages, local_messages, my_value, all_values, full_update_this_turn);

        total_messages += data_messages;
        total_local_messages += local_messages;
        total_update += full_update_this_turn;

        full_update_this_turn = 0;

        _TRACEE("all used %ld mill\n", (get_time_us() - am_start) / 1000);
        _TRACEE("size_count: turn = %d, count = %d, total = %d, size =  %ld\n", turn, data_messages, data_messages + local_messages, sender.total_size);

        if(all_values == 0){
            //then we can exit
            break;
        }
    }

    _TRACEE("Now we do cleanup\n");

    int all_total = 0;
    int all_local = 0;
    int all_update = 0;
    MPI_Allreduce(&total_messages, &all_total, 1, MPI_INT, MPI_SUM, comm);
    MPI_Allreduce(&total_local_messages, &all_local, 1, MPI_INT, MPI_SUM, comm);
    MPI_Allreduce(&total_update, &all_update, 1, MPI_INT, MPI_SUM, comm);
    _TRACEE("local sum: total messages: %d, local_messages = %d, update = %d\n", total_messages, total_local_messages, total_update);
    _TRACEE("sum: total messages: %d, local_messages = %d, update = %d\n", all_total, all_local, all_update);
    //now we do clean up
    threads->stop();

    //send a message to myself so that receiver can exit
    LocalControlMessage lcm(0);
    sender.sendRawMessage(&lcm, rank);
    _TRACEE("exit message sent to %d\n", rank);

    receiver.stop();
    _TRACEE("receiver stopped\n");
    buffer.stop();
    _TRACEE("buffer stopped\n");
    sender.stop();
    _TRACEE("sender stopped\n");
    aggregator_manager.stop();
    _TRACEE("am stopped\n");
    handler.stop();
    _TRACEE("handler stopped\n");
    notifier.stop();
    _TRACEE("notifier stopped\n");
}


template <class TypeHinter>
void bsp::runtime::synchronize::SynExecutor<TypeHinter>::updateOneTurn(int worker_id){
    _TRACEE("--worker %d starts \n", worker_id);
    long worker_begin = get_time_us();

    int nr_updates = 0;

    int total = provider.totalLocalVertices();
    while(true){
        int current = atomic_incre_re<int>(&next_to_compute, 1);
        if(current >= total){
            break;
        }

//        _TRACEE("worker thread is going to compute %d\n", current);

        Vertex<TypeHinter>* vertex = provider.getVertexByInnerId(current);

        //adjust if this vertices need to be updated
        if(p_notifier->hasMessageReceived(current)){//even if it has been halted, we wake up it
//            _TRACEE("worker thread found %ld has messages\n", vertex->getId());
            if(vertex->isHalt()){
                vertex->setHalt(false);
            }
        }

        if(vertex->isHalt()){
//            _TRACEE("worker thread skip %ld \n", vertex->getId());
            continue;
        }

        //now update this vertex;
        program.updateVertex(vertex, *this, worker_id);
        ++nr_updates;

        if(!vertex->isHalt()){
            atomic_incre(&active_count, 1);
        }
    }

    long worker_end = get_time_us();
    _TRACEE("-- worker_id = %d, used = %lf ms, updated = %d\n", worker_id, ((worker_end - worker_begin) / 1e3), nr_updates);
    atomic_incre(&full_update_this_turn, nr_updates);
}


#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_SYN_EXECUTOR_INC  ----- */

