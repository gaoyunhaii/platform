/*
 * =====================================================================================
 *
 *       Filename:  receiver.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年10月13日 11时20分16秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYNCHRONIZE_RECEIVER_HPP_INC
#define  SYSTEM_SYNCHRONIZE_RECEIVER_HPP_INC

#include "common/serialize_util.h"
#include "system/synchronize/receiver.hpp"
#include <mpi.h>
#include <cstdlib>
#include "system/synchronize/message.h"
#include "system/runtime_constants.h"
#include "common/util.h"
using bsp::common::serialize::BoundedBuffer;
using bsp::runtime::synchronize::MESSAGE_TYPE;
using bsp::common::get_time_us;

template <class TypeHinter>
void bsp::runtime::synchronize::ReceiverRunnable<TypeHinter>::run() {
    int rank;
    MPI_Comm_rank(receiver.comm, &rank);

    while(true){
        MPI_Status status;
        MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, receiver.comm, &status);

        int from = status.MPI_SOURCE;
        int length = 0;
        MPI_Get_count(&status, MPI_BYTE, &length);

        if(length <= 0){
            _TRACEE("bad messages received, length = %d\n", length);
            continue;
        }

        void* raw_buffer = malloc(length);
        int re = MPI_Recv(raw_buffer, length, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, receiver.comm, &status);

       // _TRACEE("message received, from = %d\n", from);
//        for(int i = 0;i < length;++i){
//            _TRACEE("\t %d\n", (int)(((char*)raw_buffer)[i]));
//        }

        if(from == rank){
            BoundedBuffer bb(raw_buffer);
            int type;
            size_t size;
            bb >> type >> size;
            //_TRACEE("receiver type = %d, size = %ld\n", type, size);
            if(type == LOCAL_CONTROL_MESS){
                LocalControlMessage lcm;
                lcm.deserialize(bb, size);
                if(lcm.type == 0){
                    free(raw_buffer);
                    break;
                }
                else{
                    free(raw_buffer);
                }
            }
            else if (type == CONTROL_MESSAGE){
                _TRACEE("receive control message received from %d\n", from);
                receiver.handler.onNewMessage(from, raw_buffer, length);
            }
            else{
                //free(raw_buffer);
                receiver.handler.onNewMessage(from, raw_buffer, length);
            }
        }
        else{
            receiver.handler.onNewMessage(from, raw_buffer, length);
        }
    }
    _TRACEE("receiver exit\n");

//    while(true){
//        //clear data
//        synchronizer.waitForTurnStart();
//        _after_turn_start();
//        _TRACEE("receive start new turn \n");
//
//        if(synchronizer.isStopped()){
//            break;
//        }
//
//        long d_start_time_us = get_time_us();
//        long start_time_us = get_time_us();
//        long end_time_us = get_time_us();
//        long mess_received = 0;
//
//
//        //other statistics
//        tm_receiver = 0;
//        tm_deserial = 0;
//        tm_notify = 0;
//
//        while(true){
//            //statistics
//            end_time_us = get_time_us();
//            if((end_time_us - start_time_us) > 10e6){
//                long elapsed = end_time_us - d_start_time_us;
//                _TRACEE("%s %d mills, received %d mess \n %s", GREEN, (end_time_us - start_time_us)/1e6, mess_received, NONE);
//                start_time_us = get_time_us();
//            }
//
//            //receive messages for this
//            MPI_Status status;
//            MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, receiver.comm, &status);
//
//            //receive and decode the message
//            int from = status.MPI_SOURCE;
//            int length = 0;
//            MPI_Get_count(&status, MPI_BYTE, &length);
//            if(length <= 0){
//                _TRACEE("bad messages received, length = %d\n", length);
//                continue;
//            }
////            _TRACEE("receiver: one more message from %d, length = %d\n", from, length);
//
//
//            long __tmp_start = get_time_us();
//            void* raw_buffer = malloc(length);
//            MPI_Recv(raw_buffer, length, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, receiver.comm, &status);
//            ++mess_received;
//            long __tmp_end = get_time_us();
//            tm_receiver += (__tmp_end - __tmp_start);
//
//            BoundedBuffer bb(raw_buffer);
//
//            int type;
//            size_t size;
//            bb >> type;
//            bb >> size;
//
//            bool to_exit = false;
//
////            _TRACEE("receiver: one more message from %d, length = %d, type = %d, size = %ld\n", from, length, type, size);
//
//            //The possible type: composite, control message and aggregable message
//            switch(type){
//                case COMPOSITE_MESSAGE:
//                    to_exit = _on_composite_message(bb, size, from);
//                    break;
//                case NORMAL_MESSAGE:
//                    to_exit = _on_data_message(bb, size, from);
//                    break;
//                case CONTROL_MESSAGE:
//                    to_exit = _on_control_message(bb, size, from);
//                    break;
//                case AGGRE_MESSAGE:
//                    to_exit = _on_aggregable_message(bb, size, from);
//                    break;
//                default:
//                    throw new BaseException("bad type found, will exit");
//            }
//
//            if(to_exit){
//                long elapsed = end_time_us - d_start_time_us;
//                _TRACEE("%s receiver used %f mills, received %d mess, tm_rec = %f, tm_des = %f, tm_not = %f\n %s", RED,
//                        (end_time_us - start_time_us)/1e3, mess_received,
//                        tm_receiver / 1e3, tm_deserial / 1e3, tm_notify / 1e3,
//                        NONE);
//                start_time_us = get_time_us();
//                break;
//            }
//
////            long start = bsp::common::get_time_millis();
////
////            if(type == CONTROL_MESSAGE){
////                ControlMessage control_message;
////                control_message.deserializeNoType(bb, length - sizeof(int));
////                bool fin = coordinator.onStatMessage(from, control_message.nr_messages);
////                if(fin){
////                    break;
////                }
////            }
////            else if(type == NORMAL_MESSAGE){
////                NormalMessage<MV> normal_message;
////                normal_message.deserializeNoType(bb, length - sizeof(int));
////
////                buffer.onNewMessage(normal_message.src, normal_message.mv, program, rank == from);
////                bool fin = coordinator.onDataMessage(from);
////                if(fin){
////                    break;
////                }
////            }
////            else if(type == AGGRE_MESSAGE){
////                AggregateMessage aggre_message;
////                aggre_message.deserializeNoType(bb, length - sizeof(int));
////                int remain_length = length - bb.offset();
////
////                aggregator_manager.onNewAggregateMessage(aggre_message.name.c_str(), bb, remain_length);
////            }
////            else{
////                _TRACEE("bad messages received, type = %d\n", type);
////            }
////
////            long end = bsp::common::get_time_millis();
////            _TRACEE("handle messages used %ld mill\n", (end - start));
//        }
//
//        //here we start the next
//        synchronizer.notifyForMyTaskFinished();
//    }
    _TRACEE("receiver finished\n");
}



#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_RECEIVER_HPP_INC  ----- */
