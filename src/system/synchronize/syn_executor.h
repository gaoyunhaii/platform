/*
 * =====================================================================================
 *
 *       Filename:  syn_executor.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年10月13日 11时12分23秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYNCHRONIZE_SYN_EXECUTOR_INC
#define  SYSTEM_SYNCHRONIZE_SYN_EXECUTOR_INC

#include "common/atomic.h"
#include "common/dthread.h"
#include <vector>
#include "mpi.h"
#include "system/synchronize/receiver.h"
#include "system/synchronize/sender.h"
#include "system/synchronize/aggregator.h"
#include "system/synchronize/notifier.h"
using bsp::multi_thread::DaemonThread;
using bsp::multi_thread::Runnable;
using std::vector;
using bsp::runtime::synchronize::MessageBuffer;
using bsp::runtime::synchronize::Receiver;
using bsp::runtime::synchronize::Sender;
using bsp::runtime::synchronize::NewNotifier;
using bsp::atomic::TurnedSynchronizer;


namespace  bsp{
    namespace runtime{
        namespace synchronize{
            template <class TypeHinter>
            class SynExecutor;

            template <class TypeHinter>
            class WorkerThread;

            template <class TypeHinter>
            class WorkerThreadRunnable : public Runnable{
            public:
                typedef void (SynExecutor<TypeHinter>::*ExecuteFunc)();

                WorkerThreadRunnable(WorkerThread<TypeHinter> &_wt, int _o):
                        workerThread(_wt), order(_o){
                }

                void run(){
                    while(true){
                        synchronizer.waitForTurnStart();
                        //check for stop
                        if(workerThread.isStopped()){
                            break;
                        }

                        //execute current func
                        workerThread.executeFunc(order);

                        //notify
                        synchronizer.notifyForMyTaskFinished();
                    }
                }

                TurnedSynchronizer& getSynchronizer(){
                    return synchronizer;
                }
            private:
                WorkerThread<TypeHinter> &workerThread;
                TurnedSynchronizer synchronizer;
                int order;
            };


            template <class TypeHinter>
            class WorkerThread {
            public:
                typedef typename TypeHinter::Provider PROVIDER;
                typedef void (SynExecutor<TypeHinter>::*ExecuteFunc)(int worker_id);

                WorkerThread(SynExecutor<TypeHinter>& _se, int total):
                            syn_executor(_se), is_stopped(false){

                    for(int i = 0;i < total;++i){
                        WorkerThreadRunnable<TypeHinter>* runnable =
                                new WorkerThreadRunnable<TypeHinter>(*this, i);
                        DaemonThread* dt = new DaemonThread(runnable, false, false);


                        runnables.push_back(runnable);
                        threads.push_back(dt);
                    }
                }

                ~WorkerThread(){
                    for(int i = 0;i < runnables.size();++i){
                        delete threads[i];
                        delete runnables[i];
                    }
                }

                void start(){
                    vector<DaemonThread*>::iterator vi = threads.begin();
                    for(;vi != threads.end();++vi){
                        (*vi)->start();
                    }
                }

                void stop(){
                    this->is_stopped = true;
                    typename vector<WorkerThreadRunnable<TypeHinter>*>::iterator vi = runnables.begin();
                    for(;vi != runnables.end();++vi){
                        (*vi)->getSynchronizer().notifyForTurnStarted();
                    }

                    vector<DaemonThread*>::iterator tvi = threads.begin();
                    for(;tvi != threads.end();++tvi){
                        (*tvi)->join();
                    }
                }

                void execute(ExecuteFunc func){
                    this->func = func;

                    typename vector<WorkerThreadRunnable<TypeHinter>*>::iterator vi = runnables.begin();
                    for(;vi != runnables.end();++vi){
                        (*vi)->getSynchronizer().notifyForTurnStarted();
                    }

                    vi = runnables.begin();
                    for(;vi != runnables.end();++vi){
                        (*vi)->getSynchronizer().waitForMyTask();
                    }
                }

                //methods for thread
                bool isStopped(){
                    return is_stopped;
                }

                void executeFunc(int worker_id){
                    (syn_executor.*func)(worker_id);
                }

                int getWorkerSize() const{
                    return threads.size();
                }
            private:
                SynExecutor<TypeHinter>& syn_executor;
                bool is_stopped;

                ExecuteFunc func;

                vector<WorkerThreadRunnable<TypeHinter>* > runnables;
                vector<DaemonThread*> threads;
            };
            

            template <class TypeHinter>
            class SynExecutor{
            public:
                typedef typename TypeHinter::Provider PROVIDER;
                typedef typename TypeHinter::Program PROGRAM;

                SynExecutor(PROVIDER& _p, PROGRAM& _pro, MPI_Comm _c):
                                rank(0), turn(0),
                                provider(_p), program(_pro), comm(_c), next_to_compute(0), active_count(0),
                                full_update_this_turn(0){
                    threads = new WorkerThread<TypeHinter>(*this, 2);
                    rank = get_rank(comm);
                }

                ~SynExecutor(){
                    delete threads;
                }

                void run();

                void updateOneTurn(int worker_id);

                //other method for the users
                PROVIDER& getProvider(){
                    return provider;
                }

                PROGRAM& getProgram(){
                    return program;
                }

                MPI_Comm& getComm(){
                    return comm;
                }

                MessageBuffer<TypeHinter>& getMessageBuffer(){
                    return *p_message_buffer;
                }

                Receiver<TypeHinter>& getReceiver(){
                    return *p_receiver;
                }

                Sender<TypeHinter>& getSender(){
                    return *p_sender;
                }

                AggregatorManager& getAggregatorManager(){
                    return *p_am;
                }

                NewNotifier<TypeHinter>& getNewNotifier(){
                    return *p_notifier;
                }

                int getRank(){
                    return rank;
                }

                int getTurn(){
                    return turn;
                }


            private:
                WorkerThread<TypeHinter>* threads;
                int rank;
                int turn;

                PROVIDER &provider;
                PROGRAM &program;

                MPI_Comm comm;

                //tmp pointers
                NewNotifier<TypeHinter>* p_notifier;
                MessageBuffer<TypeHinter>* p_message_buffer;
                MessageHandler<TypeHinter>* p_message_handler;
                Receiver<TypeHinter>* p_receiver;
                Sender<TypeHinter>* p_sender;
                AggregatorManager* p_am;

                int next_to_compute;
                int active_count;

                //stat
                int full_update_this_turn;
            };
        }
    }
}





#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_SYN_EXECUTOR_INC  ----- */
