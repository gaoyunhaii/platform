/*
 * =====================================================================================
 *
 *       Filename:  message_handler.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年12月29日 17时54分17秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYNCHRONIZE_MESSAGE_HANDLER_HPP_INC
#define  SYSTEM_SYNCHRONIZE_MESSAGE_HANDLER_HPP_INC

#include <cstdlib>
using bsp::atomic::atomic_set;

template<class TypeHinter>
void bsp::runtime::synchronize::MessageHandlerRunnable<TypeHinter>::run() {
    vector<spsc_queue<RecvMessageWrapper>* > our_queues;
    int max = parent_handler->queues.size();
    int handler_size = parent_handler->handlers.getSize();

    for(int i = getIndex();i < max;i += handler_size){
        our_queues.push_back(parent_handler->queues[i]);
    }

    while (true) {
        synchronizer.waitForTurnStart();

        if(isStopped()){
            _TRACEE("handler %d exit\n", getIndex());
            break;
        }

        //for one turn
//        int total_empty = 0;

        int last_not = 0;

        int handled = 0;
        while (true) {
            RecvMessageWrapper wrapper;
            bool to_exit = false;

            int this_turn_handled = 0;

            vector<spsc_queue<RecvMessageWrapper>* >::iterator vi = our_queues.begin();
            for(;vi != our_queues.end();++vi){
                while (((*vi)->pop(wrapper))) {
                    ++this_turn_handled;

                    ++handled;
                    //handle it
                    BoundedBuffer bb(wrapper.buffer);
                    int type;
                    size_t size;
                    bb >> type;
                    bb >> size;

//                    uint16_t compressed;
//                    bb >> compressed;

//                    _TRACEE("compressed = %d\n", compressed);
//                    type = (compressed & 0x0e0) >> 13;
//                    size = compressed & 0x1fff;

//                _TRACEE("got %p, from = %d, size = %ld\n", wrapper.buffer, wrapper.from, wrapper.size);
//                _TRACEE("handle messge, type = %d, size = %ld\n", type, size);
//                for(int i = 0;i < wrapper.size;++i){
//                    _TRACEE("\t %d\n", (int)(((char*)wrapper.buffer)[i]));
//                }

                    switch (type) {
                        case COMPOSITE_MESSAGE:
                            to_exit = _on_composite_message(bb, size, wrapper.from);
                            break;
                        case NORMAL_MESSAGE:
                            to_exit = _on_data_message(bb, size, wrapper.from);
                            break;
                        case CONTROL_MESSAGE:
                            _TRACEE("handler control message received from %d\n", wrapper.from);
                            to_exit = _on_control_message(bb, size, wrapper.from);
                            break;
                        case AGGRE_MESSAGE:
                            to_exit = _on_aggregable_message(bb, size, wrapper.from);
                            break;
                        default:
                            throw new BaseException("bad type found, will exit");
                    }
                    free(wrapper.buffer);

                    _TRACEE("handled messge, type = %d, size = %ld, to_exit = %d\n", type, size, to_exit);

                    if(to_exit){ //then I notify all the others can exit
                        _TRACEE("handler will exit %d\n", wrapper.from);
                        atomic_set(&parent_handler->turn_stopped, true);
                        goto TURN_FINISHED;
                    }

                    if(parent_handler->turn_stopped){
                        goto TURN_FINISHED;
                    }


                    if(parent_handler->bus_mess_this_turn - last_not >= 1000000){
                        _TRACEE("total handled %d messages\n", parent_handler->bus_mess_this_turn);
                    }
                }
            }

            if(parent_handler->turn_stopped){
                goto TURN_FINISHED;
            }

            if(this_turn_handled == 0){
                usleep(10000); //sleep 10 ms
            }

//            ++empty_check;
//            ++total_empty;
//            if(empty_check > 10){
//                usleep(10000);
//                empty_check = 0;
//            }
        }

    TURN_FINISHED:
        _TRACEE("handled = %d, total_empty = %d\n", handled, 0);

        synchronizer.notifyForMyTaskFinished();
    }
}


#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_MESSAGE_HANDLER_HPP_INC  ----- */

