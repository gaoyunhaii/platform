/*
 * =====================================================================================
 *
 *       Filename:  message.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年10月19日 14时20分05秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYNCHRONIZE_MESSAGE_INC
#define  SYSTEM_SYNCHRONIZE_MESSAGE_INC

#include "system/runtime_constants.h"
#include "common/serialize_util.h"
#include "system/synchronize/aggregator.h"
using bsp::common::serialize::BoundedBuffer;

namespace  bsp{
    namespace runtime{
        namespace synchronize{

            //definitions for the message type
            enum MESSAGE_TYPE{
                CONTROL_MESSAGE, NORMAL_MESSAGE, AGGRE_MESSAGE, COMPOSITE_MESSAGE, LOCAL_CONTROL_MESS
            };

            class Message{
            public:
                virtual int getType() const = 0;
                virtual size_t getSize() const = 0;
                virtual void serialize(BoundedBuffer& bb, size_t size) const = 0;
                virtual void deserialize(BoundedBuffer& bb, size_t size) = 0;
            };

            template <class MV>
            class NormalMessage : public Message{
            public:
                NormalMessage(){}
                NormalMessage(const VertexId& _id, const MV& _mv) : src(_id), mv(_mv){}

                int getType() const {
                    return NORMAL_MESSAGE;
                }

                size_t getSize() const{
                    //return sizeof(VertexId) +  mv.getSize();

                    return sizeof(uint32_t) + mv.getSize();
                }

                void serialize(BoundedBuffer& bb, size_t size) const{
                    //bb << src;
                    uint32_t tmp = (uint32_t)src;
                    bb << tmp;
                    mv.serialize(bb, size - sizeof(int) - sizeof(VertexId));
                }

                void deserialize(BoundedBuffer& bb, size_t size){
                    //bb >> src;
                    uint32_t tmp;
                    bb >> tmp;
                    src = tmp;

                    mv.deserialize(bb, size);
                }

            public:
                VertexId src;
                MV mv;
            };

            class ControlMessage  : public Message{
            public:
                ControlMessage() : nr_messages(0){}
                ControlMessage(size_t _nrm) : nr_messages(_nrm){}
                size_t getSize() const{
                    return sizeof(size_t);
                }

                int getType() const{
                    return CONTROL_MESSAGE;
                }

                void serialize(BoundedBuffer& bb, size_t size) const{
                    bb << nr_messages;
                }

                void deserialize(BoundedBuffer& bb, size_t size){
                    bb >> nr_messages;
                }

            public:
                size_t nr_messages;
            };

            class LocalControlMessage : public Message{
            public:
                LocalControlMessage() : type(-1){}
                LocalControlMessage(int _t) : type(_t){}

                size_t getSize() const{
                    return sizeof(int);
                }

                int getType() const{
                    return LOCAL_CONTROL_MESS;
                }

                void serialize(BoundedBuffer& bb, size_t size) const{
                    bb << type;
                }

                void deserialize(BoundedBuffer& bb, size_t size){
                    bb >> type;
                }
            public:
                int type;
            };


            class AggregateMessage : public Message{
            public:
                AggregateMessage():value(NULL){}
                AggregateMessage(const char* _n, Aggregable* _v) :
                        name(_n), value(_v){}

                int getType() const{
                    return AGGRE_MESSAGE;
                }

                size_t getSize() const{
                    return sizeof(size_t) + name.size() + value->getSize();
                }

                void serialize(BoundedBuffer& bb, size_t size) const{
                    bb << name;
                    value->serialize(bb, value->getSize());
                }

                void deserialize(BoundedBuffer& bb, size_t size){
                    bb >> name;
                }
            public:
                string name;
                Aggregable* value;
            };


            class CompositeMessage : public Message{
            public:
                CompositeMessage() {}
                CompositeMessage(const VertexId& _id){}

                ~CompositeMessage(){
                    vector<Message*>::const_iterator vi = messages.begin();
                    for(;vi != messages.end();++vi){
                        delete (*vi);
                    }
                }

                int getType() const{
                    return COMPOSITE_MESSAGE;
                }

                size_t getSize() const{
                    size_t total = sizeof(size_t); //count
                    vector<Message*>::const_iterator vi = messages.begin();
                    for(;vi != messages.end();++vi){
                        //total = total + sizeof(int) + sizeof(size_t) + (*vi)->getSize(); //type size value
                        total = total + sizeof(uint16_t) + (*vi)->getSize(); //type size value
                    }

                    return total;
                }

                void addMessage(Message* message){
                    messages.push_back(message);
                }

                size_t getMessageCount(){
                    return messages.size();
                }

                void serialize(BoundedBuffer& bb, size_t size) const{
                    bb << messages.size();
                    vector<Message*>::const_iterator vi = messages.begin();
                    for(;vi != messages.end();++vi){
                        //encode the uint16_t
                        size_t size = (*vi)->getSize();
                        int type = (*vi)->getType();

                        uint16_t compressed = 0;
                        compressed = compressed | ((type & 0x07) << 13);
                        compressed = compressed | (size & 0x01fff);
                        //_TRACEE("size = %ld, type = %d, sender compressed = %d\n", size, type, compressed);

                        //bb << (*vi)->getType() << size;
                        bb << compressed;
                        (*vi)->serialize(bb, size);
                    }
                }

                void deserialize(BoundedBuffer& bb, size_t size){
                    throw new BaseException("Please deserize composite message manually!");
                }
            public:
                vector<Message*> messages;
            };
        }
    }
}

#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_MESSAGE_INC  ----- */
