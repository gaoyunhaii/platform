/*
 * =====================================================================================
 *
 *       Filename:  aggregator.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  11/04/2015 10:05:24 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYNCHRONIZE_AGGREGATOR_INC
#define  SYSTEM_SYNCHRONIZE_AGGREGATOR_INC

#include "common/serialize_util.h"
#include "common/sexception.h"
#include "common/atomic.h"
#include <string>
#include <unordered_map>
using bsp::common::serialize::BoundedBuffer;
using std::string;
using std::unordered_map;
using bsp::common::BaseException;
using bsp::atomic::CountDownLatch;

namespace bsp{
    namespace runtime{
        namespace synchronize{
            class Aggregable{
            public:
                virtual size_t getSize() = 0;
                virtual void aggregate(const Aggregable* other) = 0;
                virtual void aggregate(BoundedBuffer& bb, size_t size) = 0;

                virtual void serialize(BoundedBuffer& bb, size_t size) = 0;
                virtual void deserialize(BoundedBuffer& bb, size_t size) = 0;
            };

            class AggregatorManager{
            public:
                AggregatorManager(int _r):comm_size(_r){
                    this->last_turn = new unordered_map<string, Aggregable*>();
                    this->this_turn = new unordered_map<string, Aggregable*>();
                }

                void start(){

                }

                void stop(){

                }

                void turnIntialize(){
                    //initialize the nr_messages
                    _init_nr_message();
                }

                void turnFinalize(){
                    std::swap(last_turn, this_turn);
                    unordered_map<string, Aggregable*>::iterator mi = this_turn->begin();
                    for(;mi != this_turn->end();++mi){
                        delete mi->second;
                    }

                    this_turn->clear();
                }

                void addAggregable(const char* name, Aggregable* init){
                    (*this_turn)[string(name)] = init;
                }

                Aggregable* getAggregable(const char* name){
                    unordered_map<string, Aggregable*>::iterator mi = last_turn->find(string(name));
                    if(mi == last_turn->end()){
                        return NULL;
                    }

                    return mi->second;
                }

                void aggregate(const char* name, Aggregable* to_add){
                    unordered_map<string, Aggregable*>::iterator mi = this_turn->find(string(name));
                    if(mi == this_turn->end()){
                        throw new BaseException("aggregate before add");
                    }

                    mi->second->aggregate(to_add);
                }

                void onNewAggregateMessage(const char* name, BoundedBuffer& bb, size_t size){
                    unordered_map<string, Aggregable*>::iterator mi = this_turn->find(string(name));
                    if(mi == this_turn->end()){
                        throw new BaseException("aggregate before add");
                    }

                    mi->second->aggregate(bb, size);

                    latch.countDown();
                }

                void waitTillAllReceived(){
                    latch.wait();
                };
            private:
                void _init_nr_message(){
                    latch.setValue(this_turn->size() * comm_size);
                }
            private:
                unordered_map<string, Aggregable*>* last_turn;
                unordered_map<string, Aggregable*>* this_turn;
                int comm_size;

                //to make sure I have received all messages
                CountDownLatch latch;
            };
        }
    }
}



#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_AGGREGATOR_INC  ----- */
