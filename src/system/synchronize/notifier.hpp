/*
 * =====================================================================================
 *
 *       Filename:  notifier.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年11月26日 16时47分12秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYNCHRONIZER_NOTIFIER_HPP_INC
#define  SYSTEM_SYNCHRONIZER_NOTIFIER_HPP_INC

#include "system/runtime_constants.h"
#include "system/provider.h"
using bsp::runtime::BorderVertexInfo;

template <class TypeHinter>
void bsp::runtime::synchronize::NewNotifierRunnable<TypeHinter>::run() {
    while(true){
        synchronizer.waitForTurnStart();
        if(isStopped()){
            break;
        }

        int index = getIndex();
        int local_handled = 0;
        int remote_handled = 0;

        while(true){
            int this_turn_handled = 0;

            NotifyRemoteMessage<TypeHinter> rm;
            while(remote_queue.pop(rm)){
                _handle_remote_message(rm);
                ++remote_handled;

                ++this_turn_handled;

                decreRemote();
            }

            NotifyLocalMessage<TypeHinter> lm;
            int poped = 0;
            while(local_queue.pop(lm)){
                _handle_local_message(lm);
                ++poped;
                ++local_handled;

                ++this_turn_handled;

                decreLocal();

                if(poped > 100000){
                    break;
                }
            }

            if(notifier->turnStopped){
                _TRACEE("notifier %d found turn stopped\n", getIndex());
                assert(turn_remote_remained >= 0);
                while(turn_remote_remained != 0){
                    while(remote_queue.pop(rm)){
                        _handle_remote_message(rm);
                        ++remote_handled;

                        decreRemote();
                    }
                }
                _TRACEE("notifier %d found turn remote stopped\n", getIndex());

                assert(turn_local_remained >= 0);
                while(turn_local_remained != 0){
                    while(local_queue.pop(lm)){
                        _handle_local_message(lm);
                        ++local_handled;

                        decreLocal();
                    }
                }
                _TRACEE("notifier %d found turn local stopped\n", getIndex());

                synchronizer.notifyForMyTaskFinished();
                break;
            }

            if(this_turn_handled == 0){
                usleep(10000);
            }
        }

        _TRACEE("**notifier %d handled remote = %d, local = %d\n", index, remote_handled, local_handled);
    }
}

template <class TypeHinter>
void bsp::runtime::synchronize::NewNotifierRunnable<TypeHinter>::_handle_remote_message(NotifyRemoteMessage<TypeHinter> &rm) {
    bool need_detail = notifier->program.isNotify();
    if(!need_detail){
        return;
    }

    if(rm.border_vertex){
        //TODO in default to notify all the neighbors
        BorderVertexInfo* border_vertex_info = notifier->provider.getBorderVertexInfoByInnerId(rm.border_vertex->getInnerId());
        vector<int>& in_index = border_vertex_info->in_edges;
        vector<int>::iterator vi = in_index.begin();

        vector<InEdge<TypeHinter>*>& in_edges = notifier->provider.getInEdges();
        for(;vi != in_index.end();++vi){
            InEdge<TypeHinter>* edge = in_edges[*vi];
            const VertexId &dest_id = edge->getDest().getId();
            //_TRACEE("notifier for %ld, *vi = %d, dest_id = %ld\n", rm.vid, *vi, dest_id);
            int inner_id = edge->getDest().getInnerId();

            //set bit
            notifier->new_global_container.bitmap.setBit(inner_id);

            //here we merge the edge
            NotifyMessage<TypeHinter>& notify_message = context.new_container->in_messages[inner_id];
            notifier->program.onNewRemoteNotifyMessage(notify_message, rm.vid, rm.mv, edge);
            notify_message.last_updated_turn = notifier->turn;
            //context.new_container->in_messages[dest_id].push_back(edge);
        }
    }
    else{
        //TODO: directly update the notify buffer
    }

//    vector<InEdge<TypeHinter>*>& in_edges = notifier->provider.getInEdges();
//    BorderVertex<TypeHinter>* border_vertex = notifier->provider.getBorderVertexById(rm.vid);

    //vector<int>* in_index = notifier->provider.getInEdgeIndicesFromSrc(rm.vid);
    //_TRACEE("notifier for %ld, in_index.size = %d\n", rm.vid, in_index->size());

    //_TRACEE("notifier for %ld, need_detail = %d\n", rm.vid, need_detail);


}

template <class TypeHinter>
void bsp::runtime::synchronize::NewNotifierRunnable<TypeHinter>::_handle_local_message(NotifyLocalMessage<TypeHinter> &lm) {
    bool need_detail = notifier->program.isNotify();
    if(!need_detail){
        return;
    }

    vector<InnerEdge<TypeHinter>*>& inner_edges = notifier->provider.getInnerEdges();
    VertexInfo* info = notifier->provider.getVertexInfoByInnerId(lm.vertex->getInnerId());

    vector<int>& inner_index = info->inner_edges;

    //_TRACEE("notifier for %ld, in_index.size = %d\n", lm.vid, inner_index.size());
    //_TRACEE("notifier for %ld, need_detail = %d\n", lm.vid, need_detail);

    vector<int>::iterator vi = inner_index.begin();
    for(;vi != inner_index.end();++vi){
        InnerEdge<TypeHinter>* inner_edge = inner_edges[*vi];
        int inner_id = inner_edge->getDest()->getInnerId();
        notifier->new_global_container.bitmap.setBit(inner_id);

        NotifyMessage<TypeHinter>& notify_message = context.new_container->inner_messages[inner_edge->getDest()->getInnerId()];
        notifier->program.onLocalNotifyMessage(notify_message, lm.mv, lm.vertex, inner_edge);
        notify_message.last_updated_turn = notifier->turn;

        //context.new_container->inner_messages[dest_id].push_back(inner_edge);
    }
}


#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZER_NOTIFIER_HPP_INC  ----- */
