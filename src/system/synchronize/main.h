/*
 * =====================================================================================
 *
 *       Filename:  main.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/21/2015 11:37:04 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_MAIN_INC
#define  SYSTEM_MAIN_INC

#include "common/hdfs_client.h"
#include "common/configuration.h"
#include "system/runtime_constants.h"
#include "system/provider.h"
#include "system/provider.hpp"
#include "system/partition.h"
#include "system/input.h"
#include "system/input.hpp"
#include "system/input_parser.h"
#include "system/synchronize/notifier.h"

using bsp::common::HDFSClient;
using bsp::common::Configuration;
using bsp::runtime::Provider;
using bsp::runtime::PrePartitioner;

namespace bsp {
    namespace runtime {
        namespace synchronize {

            template<class TypeHinter>
            class DefaultProgram {
            public:
                typedef typename TypeHinter::MessageValue MV;
                typedef typename TypeHinter::BufferValue BV;
                typedef typename TypeHinter::Provider PROVIDER;
                typedef MessageBuffer<TypeHinter> MESSAGE_BUFFER;
                typedef Sender<TypeHinter> SENDER;
                typedef SynExecutor<TypeHinter> EXECUTOR;

                virtual bool isNotify() {
                    return false;
                }

                //notify
                virtual void onNewRemoteNotifyMessage(NotifyMessage<TypeHinter>& notify_message, const VertexId& vid, const MV& mv, InEdge<TypeHinter>* in_edge){ }
                virtual void onLocalNotifyMessage(NotifyMessage<TypeHinter>& notify_message, const MV& mv, const Vertex<TypeHinter>* vertex, InnerEdge<TypeHinter>* inner_edge){ }

//                virtual BV* initBufferMessage() {
//                    return new BV();
//                }
//
//                virtual void clearBufferMessage(const VertexId& vid, BV* pbv){
//                    delete pbv;
//                }

                virtual void onNewBufferMessage(const VertexId &vid, const MV &message, BV &value) { }

                virtual void updateNextTurnAggregator(EXECUTOR& executor){ }
                virtual void updateVertex(Vertex<TypeHinter>* next, EXECUTOR& executor, int channel_index){ }

                virtual bool beforeTurn(EXECUTOR& executor){
                    return true;
                }

                virtual bool afterTurn(EXECUTOR& executor, MPI_Comm& comm){
                    return true;
                }
            };

            template<class TypeHinter>
            class DefaultTypeHinter {
            public:
                typedef float VertexValue;
                typedef float EdgeValue;
                typedef float BufferValue;
                typedef float MessageValue;
                typedef float NotifyValue;
                typedef PrePartitioner Partitioner;
                typedef AdjLineParser<TypeHinter> InputParser;
                typedef FileInput<TypeHinter> InputClass;
                typedef DefaultProgram<TypeHinter> Program;
                typedef bsp::runtime::Provider<TypeHinter> Provider;
                typedef bsp::runtime::synchronize::SynExecutor<TypeHinter> Executor;
            };


            template<class TypeHinter>
            class Driver {
            public:
                typedef typename TypeHinter::VertexValue VV;
                typedef typename TypeHinter::EdgeValue EV;
                typedef typename TypeHinter::Partitioner PART;
                typedef typename TypeHinter::InputClass INPUT;
                typedef typename TypeHinter::Program PROGRAM;

                Driver(const char *_cf);

                ~Driver();

                void input_data(INPUT &input);

                Configuration &getConf() {
                    return conf;
                }

                Provider<TypeHinter> &getProvider() {
                    return *provider;
                }

                void run_program(PROGRAM &program);

            private:
                Configuration conf;
                HDFSClient *hdfs_client;
                PART *part;
                Provider<TypeHinter> *provider;
            };
        }
    }
}


#endif   /* ----- #ifndef SYSTEM_MAIN_INC  ----- */
