/*
 * =====================================================================================
 *
 *       Filename:  receiver.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年10月13日 11时15分18秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYNCHRONIZE_RECEIVER_INC
#define  SYSTEM_SYNCHRONIZE_RECEIVER_INC

#include "common/dthread.h"
#include "system/synchronize/message_buffer.h"
#include "system/synchronize/message_handler.h"
#include <mpi.h>
#include <pthread.h>
#include <cassert>
#include "common/util.h"
#include "common/atomic.h"
#include "common/dthread.h"
#include "message.h"
#include "system/synchronize/aggregator.h"
#include "system/mpi_util.h"
#include <cassert>

using bsp::multi_thread::Runnable;
using bsp::multi_thread::DaemonThread;
using bsp::runtime::synchronize::MessageBuffer;
using bsp::atomic::atomic_incre_re_after;
using bsp::atomic::MutexGetLock;
using bsp::multi_thread::DaemonThread;
using bsp::runtime::synchronize::Aggregable;
using bsp::runtime::synchronize::AggregatorManager;
using bsp::atomic::atomic_incre_re_after;
using bsp::common::get_time_us;

namespace bsp {
    namespace runtime {
        namespace synchronize {
            template<class TypeHinter>
            class Receiver;

            template<class TypeHinter>
            class ReceiverRunnable : public Runnable {
            public:
                ReceiverRunnable(Receiver<TypeHinter> &_r) : receiver(_r) {
                    int comm_size = get_comm_size(receiver.comm);
                    rank = get_rank(receiver.comm);
                }

                ~ReceiverRunnable() {

                }

                void run();

            private:
                Receiver<TypeHinter> &receiver;
                int rank;
            };


            template<class TypeHinter>
            class Receiver {
            public:
                friend class ReceiverRunnable<TypeHinter>;

                typedef typename TypeHinter::BufferValue BV;
                typedef typename TypeHinter::MessageValue MV;
                typedef typename TypeHinter::Program PROGRAM;


                Receiver(MPI_Comm& _c, MessageHandler<TypeHinter>& _h) :
                        comm(_c), handler(_h){
                    runnable = new ReceiverRunnable<TypeHinter>(*this);
                }

                ~Receiver() {
                    delete this->thread;
                    delete this->runnable;
                }

                void start() {
                    this->thread = new DaemonThread(runnable, false, false, NULL);
                    this->thread->start();
                }

                void stop() {
                    //this->thread->stop();
                    this->thread->join();
                }

                void turnInitialize() {

                }


                void turnFinalize() {

                }

            private:
                MPI_Comm& comm;
                MessageHandler<TypeHinter>& handler;

                //thread
                ReceiverRunnable<TypeHinter> *runnable;
                DaemonThread *thread;
            };
        }
    }
}


#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_RECEIVER_INC  ----- */
