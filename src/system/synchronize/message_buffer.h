/*
 * =====================================================================================
 *
 *       Filename:  message_buffer.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年12月31日 14时20分21秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef  SYSTEM_SYNCHRONIZE_MESSAGE_BUFFER_INC
#define  SYSTEM_SYNCHRONIZE_MESSAGE_BUFFER_INC

#include <pthread.h>
#include <unordered_map>
#include "common/obj_factory.h"
#include "common/thread_local.h"
#include "common/thread_local.hpp"
#include "system/runtime.h"
#include "system/runtime_bg.h"
#include "common/atomic.h"
#include <vector>
#include "common/dthread.h"
#include "system/runtime_constants.h"
#include <boost/lockfree/spsc_queue.hpp>
#include "system/synchronize/notifier.h"
#include "common/atomic.h"
#include "common/obj_factory.h"

using bsp::common::ObjFactory;
using bsp::common::DefaultInit;
using std::unordered_map;
using bsp::runtime::VertexId;
using bsp::atomic::ConcurrentBitmap;
using bsp::atomic::TurnedSynchronizer;
using std::vector;
using bsp::multi_thread::Runnable;
using bsp::multi_thread::DaemonThread;
using boost::lockfree::spsc_queue;
using bsp::runtime::synchronize::NotifyRemoteMessage;
using bsp::runtime::synchronize::NotifyLocalMessage;
using bsp::runtime::synchronize::NewNotifier;
using bsp::atomic::atomic_incre;
using bsp::common::ObjFactory;

namespace bsp {
    namespace runtime {
        namespace synchronize {
            template<class TypeHinter>
            class MessageBuffer;

            template<class TypeHinter>
            class ValueContainer {
            public:
                typedef typename TypeHinter::BufferValue BV;
                typedef typename TypeHinter::Provider PROVIDER;

//                static BV* NULL_BV_POINTER;
            public:
                friend class MessageBuffer<TypeHinter>;

                ValueContainer(PROVIDER &_p) :
                        provider(_p) {
                    //init the values
                    values.resize(provider.totalBorderVertices());
                }

                ~ValueContainer() {

                }

                void reset() {
//                    typename unordered_map<VertexId, BV*>::iterator mi = values_by_id.begin();
//                    for(;mi != values_by_id.end();++mi){
//                        delete mi->second;
//                    }
//                    values_by_id.clear();
                }

                BV &findValue(int inner_id) {
                    return values[inner_id];
//
//                    typename unordered_map<VertexId, BV*>::iterator mi = values_by_id.find(vid);
//                    if(mi == values_by_id.end()){
//                        return ValueContainer::NULL_BV_POINTER;
//                    }
//
//                    return mi->second;
                }

//                BV* allocAnObj(){
//                    return obj_factory->newObject(DefaultInit<BV>());
//                }

//                void insertValue(const VertexId& vid, BV* value){
//                    values_by_id[vid] = value;
//                }

//                unordered_map<VertexId, BV*>& getValuesById(){
//                    return values_by_id;
//                };
            private:
                PROVIDER &provider;
                vector<BV> values;
            };

//            template <class TypeHinter>
//            struct GlobalValueContainer{
//                typedef typename TypeHinter::Provider PROVIDER;
//
//                GlobalValueContainer(PROVIDER& _p):
//                        message_map(_p.totalLocalVertices()){
//                }
//
//                ConcurrentBitmap message_map;
//            };


            template<class TypeHinter>
            struct BufferContext {
                BufferContext() :
                        old_container(NULL),
                        new_container(NULL) {
                }

                void swap() {
                    std::swap(old_container, new_container);
                    new_container->reset();
                    //_TRACEE("buffer old remote container size = %ld\n", old_container->getValuesById().size());
                }

                ValueContainer<TypeHinter> *old_container;
                ValueContainer<TypeHinter> *new_container;
            };

            template<class TypeHinter>
            class MessageBuffer {
            public:
                typedef typename TypeHinter::MessageValue MV;
                typedef typename TypeHinter::BufferValue BV;
                typedef typename TypeHinter::Partitioner PARTITIONER;
                typedef typename TypeHinter::Program PROGRAM;
                typedef typename TypeHinter::Provider PROVIDER;

            public:
                MessageBuffer(int _ss, int _rank, PROVIDER &_p, PROGRAM &_pro, NewNotifier<TypeHinter> &_n) :
                        split_size(_ss),
                        rank(_rank),
                        provider(_p), program(_pro), notifier(_n),
                        local_mess_this_turn(0)
//                            old_global_container(NULL),
//                            new_global_container(NULL)
                {
                    contexts.resize(_ss);
                    for (int i = 0; i < _ss; ++i) {
                        contexts[i].old_container = new ValueContainer<TypeHinter>(_p);
                        contexts[i].new_container = new ValueContainer<TypeHinter>(_p);
                    }
//
//                    //the local context
//                    old_local_container = new ValueContainer<TypeHinter>(_p);
//                    new_local_container = new ValueContainer<TypeHinter>(_p);
//
//                    old_global_container = new GlobalValueContainer<TypeHinter>(_p);
//                    new_global_container = new GlobalValueContainer<TypeHinter>(_p);
                }

                ~MessageBuffer() {
//                    delete old_local_container;
//                    delete new_local_container;
//
//                    //the all the remote context
                    typename vector<BufferContext<TypeHinter> >::iterator vi = contexts.begin();
                    for (; vi != contexts.end(); ++vi) {
                        delete (*vi).old_container;
                        delete (*vi).new_container;
                    }
                }

                void start() {
                    vector<Vertex<TypeHinter> *> &vertices = provider.getVertices();
                    typename vector<Vertex<TypeHinter> *>::iterator vi = vertices.begin();

//                    for(;vi != vertices.end();++vi){
//                        old_local_container->insertValue((*vi)->getId(), new BV());
//                        new_local_container->insertValue((*vi)->getId(), new BV());
//
//                    }
                }

                void stop() {

                }

                void turnInitialize() {
                    local_mess_this_turn = 0;
                    typename vector<BufferContext<TypeHinter> >::iterator vi = contexts.begin();
                    for (; vi != contexts.end(); ++vi) {
                        vi->swap();
                    }

//                    std::swap(old_local_container, new_local_container);
//                    unordered_map<VertexId, BV*>& by_id = old_local_container->values_by_id;
//                    typename unordered_map<VertexId, BV*>::iterator mvi = by_id.begin();
//                    for(;mvi != by_id.end();++mvi){
//                        _TRACEE("id = %ld, value = %f, addr = %p\n", mvi->first, *mvi->second, mvi->second);
//                    }

                    //no need to reset

//                    std::swap(old_global_container, new_global_container);
                }

                void turnFinalize() {

                }

                void onNewRemoteMessage(int from, const VertexId &vid, MV &mv) {
                    int contextId = from % split_size;
                    //_insert_value_into_container(contexts[contextId].new_container, contexts[contextId].old_container, vid, mv);

                    //now do update
                    BufferContext<TypeHinter>& context = contexts[contextId];
                    BorderVertex<TypeHinter>* border_vertex = provider.getBorderVertexById(vid);
                    if(border_vertex){
                        program.onNewBufferMessage(vid, mv, context.new_container->values[border_vertex->getInnerId()]);
                    }
                    else{
                        //TODO: save the value if needed for non-border-vertex if needed
                    }

                    if (program.isNotify()) {
                        NotifyRemoteMessage<TypeHinter> nm(from, mv, vid, border_vertex);
                        notifier.onNewRemoteNotify(nm);
                    }
                }

                void onNewLocalMessage(Vertex<TypeHinter> &vertex, MV &mv, int queue_index) {
//                    _insert_value_into_container(new_local_container, old_local_container, vid, mv);
                    //now we can update the new value directly
                    program.onNewBufferMessage(vertex.getId(), mv, vertex.getMutableBufferValue());

                    if (program.isNotify()) {
                        NotifyLocalMessage<TypeHinter> lm(mv, &vertex);
                        notifier.onNewLocalNotifier(lm, queue_index);
                    }
                    atomic_incre(&local_mess_this_turn, 1);
                }

                int getLocalMessThisTurn() {
                    return local_mess_this_turn;
                }

                BV& findRemoteOldValue(BorderVertex<TypeHinter>& border_vertex) const {
//                    PARTITIONER &part = provider.getPart();
//                    int from = part.getPart(border_vertex.getId());

                    int from = border_vertex.getPart();

                    int contextId = from % split_size;
                    BV &ov = contexts[contextId].old_container->findValue(border_vertex.getInnerId());
                    return ov;
                }

                BV& findLocalOldValue(Vertex<TypeHinter>& vertex) const {
                    return vertex.getMutableBufferValue();
                }

            private:
//                void _insert_value_into_container(ValueContainer<TypeHinter>* container,
//                                                  ValueContainer<TypeHinter>* old_container, const VertexId& vid, MV& mv){
//                    //now we try to calculate some thing
////                    if(container == new_local_container){
////                        _TRACEE("_insert for %ld\n", vid);
////                    }
//
//                    BV* base_value = container->findValue(vid);
//                    if(base_value == ValueContainer<TypeHinter>::NULL_BV_POINTER) {
//                        base_value = container->allocAnObj();
//
//                        //base_value = program.initBufferMessage();
//
//                        container->insertValue(vid, base_value);
//                        base_value = container->findValue(vid);
//                    }
//
//                    BV* old_base_value = old_container->findValue(vid);
//                    if(old_base_value != NULL){
//                        *base_value = *old_base_value;
//                    }
//
//                    program.onNewBufferMessage(vid, mv, *base_value);
//                }
            private:
                //equal to the size of the handler threads. Each thread use split buffer, and only maintaince speicific vertices
                int split_size;
                int rank;

                //global values
                PROVIDER &provider;
                PROGRAM &program;
                NewNotifier<TypeHinter> &notifier;

                vector<BufferContext<TypeHinter> > contexts;

//                ValueContainer<TypeHinter>* old_local_container;
//                ValueContainer<TypeHinter>* new_local_container;

                int local_mess_this_turn;
//
//                GlobalValueContainer<TypeHinter>* old_global_container;
//                GlobalValueContainer<TypeHinter>* new_global_container;
            };

        }
    }
}


#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_MESSAGE_BUFFER_INC  ----- */
