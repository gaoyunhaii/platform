/*
 * =====================================================================================
 *
 *       Filename:  sender.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  10/22/2015 09:12:46 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYNCHRONIZE_SENDER_INC
#define  SYSTEM_SYNCHRONIZE_SENDER_INC

#include <mpi.h>
#include <cstdlib>
#include <boost/lockfree/spsc_queue.hpp>
#include "common/serialize_util.h"
#include "common/dthread.h"
#include "common/thread_local.h"
#include "common/atomic.h"
#include "system/mpi_util.h"
#include "common/util.h"

using bsp::common::serialize::BoundedBuffer;
using boost::lockfree::spsc_queue;
using bsp::multi_thread::DaemonThread;
using bsp::atomic::TurnedSynchronizer;
using bsp::multi_thread::ThreadLocal;
using bsp::common::get_time_us;

namespace bsp{
    namespace runtime{
        namespace synchronize{
            struct MessageWrapper{
                Message* message;
                int to;
            };

            struct SenderBuffer{
                SenderBuffer() : cm(NULL), size(0){
                    cm = new CompositeMessage();
                }

                SenderBuffer(const SenderBuffer& sb){
                    size = 0;
                    cm = new CompositeMessage();
                    _TRACEE("sb copied\n");
                }

                ~SenderBuffer(){
                    if(cm){
                        delete cm;
                    }
                }

                void reset(){
                    if(cm){
                        delete cm;
                    }
                    cm = new CompositeMessage();
                    size  = 0;
                }

                void addMessage(Message* message){
                    cm->addMessage(message);
                    //add size
                    size += message->getSize();
                }

                CompositeMessage* cm;
                size_t size;
            };


            class ThreadContext{
            public:
                ThreadContext():messages(5000000){}
                void addMessage(const MessageWrapper& mess){
                    while(!messages.push(mess));
                }
            public:
                spsc_queue<MessageWrapper> messages;
            };


            template <class TypeHinter>
            class Sender;

            template <class TypeHinter>
            class SenderRunnable : public bsp::multi_thread::Runnable{
            public:
                SenderRunnable(int _order, Sender<TypeHinter>& _s):
                            order(_order), sender(_s), needRefresh(false){
                    int comm_size;
                    MPI_Comm_size(sender.comms[0], &comm_size);
                    _TRACEE("comm_size = %d\n", comm_size);


//                    for(int i = 0;i < comm_size;++i){
//                        next_messages.push_back(SenderBuffer());
//                    }
                    next_messages.resize(comm_size);

                    nr_bus_messages = new int[comm_size]();
                }

                void run(){
                    int sender_size = sender.send_threads.size();

                    while(true){
                        synchronizer.waitForTurnStart();
                        _after_turn_start();
                        _TRACEE("sender thread start one turn\n");
                        start_time_us = get_time_us();
                        total_mess = 0;
                        total_inner_mess = 0;
                        total_size = 0;

                        if(synchronizer.isStopped()){
                            break;
                        }

                        //then we check till need notify
                        while(!needRefresh){
                            int this_turn_popped = 0;

                            int nrContexts = sender.all_channels.size();
                            for (int i = 0; i < nrContexts;++i){
                                if(i % sender_size == order){//then i will read it
                                    ThreadContext* context = sender.all_channels[i];

                                    MessageWrapper wrapper;
                                    while(context->messages.pop(wrapper)){
//                                        _TRACEE("send run: one message to %d\n", wrapper.to);
                                        ++this_turn_popped;

                                        //count this messages
                                        ++nr_bus_messages[wrapper.to];

                                        _add_to_cm_and_send(wrapper.to, wrapper.message);
                                    }
                                }
                            }

                            if(this_turn_popped == 0){
                                usleep(10000);
                            }
                        }

                        //then it means all messages are sent, we clear the buffers
                        int nrContexts = sender.all_channels.size();
                        for (int i = 0; i < nrContexts;++i){
                            if(i % sender_size == order){//then i will read it
                                ThreadContext* context = sender.all_channels[i];

                                MessageWrapper wrapper;
                                while(context->messages.pop(wrapper)){
//                                    _TRACEE("send run: one message to %d\n", wrapper.to);

                                    //count this messages
                                    ++nr_bus_messages[wrapper.to];

                                    _add_to_cm_and_send(wrapper.to, wrapper.message);
                                }
                            }
                        }

//                        _TRACEE("now do refresh\n");
                        vector<SenderBuffer>::iterator vi = next_messages.begin();
                        for(int i = 0;vi != next_messages.end();++vi, ++i){
                            //_TRACEE("i = %d\n", i);
                            if(vi->cm->getMessageCount() > 0){
//                                _TRACEE("i = %d, step 2\n", i);
                                sender.sendRawMessage(vi->cm, i);
                                vi->reset();
//                                _TRACEE("i = %d, step 3\n", i);
                            }
//                            _TRACEE("sent %d\n", i);
                        }


                        _before_turn_end();

                        end_time_us = get_time_us();
                        _TRACEE("%s time used = %d, total_mess = %d, total_inner_mess = %d, total_size = %d \n %s", GREEN, (int)((end_time_us-start_time_us)/1e6), total_mess, total_inner_mess, total_size, NONE);

                        synchronizer.notifyForMyTaskFinished();
//                        _TRACEE("sender thread end one turn\n");
                    }
                }

                TurnedSynchronizer& getSynchronizer(){
                    return synchronizer;
                }

                int* getNrBusMessages(){
                    return nr_bus_messages;
                }

                void refreshBuffer(){
                    _TRACEE("runnable set to true\n");
                    bsp::atomic::atomic_set<bool>(&needRefresh, true);
                }
            private:
                void _before_turn_end(){
                    needRefresh = false;
                }

                void _after_turn_start(){
                    int comm_size = get_comm_size(sender.comms[0]);
                    memset(nr_bus_messages, 0, sizeof(int) * comm_size);
                }

                void _add_to_cm_and_send(int to, Message* message){
//                    _TRACEE("send run: add to cm one message to %d\n", to);
                    SenderBuffer& mb = next_messages[to];
                    mb.addMessage(message);
//                    _TRACEE("send run: added one message to %d\n", to);

                    end_time_us = get_time_us();
                    if((end_time_us - new_start_time_us) > 10e6){
                        //alread 10 second
                        _TRACEE("%s sender time used = %d, total_mess = %d, total_size = %d \n %s", GREEN, (int)((end_time_us-start_time_us)/1e6), total_mess, total_size, NONE);
                        new_start_time_us = get_time_us();
                    }

                    //if available, we send it
                    if(mb.size > 8192){//send it out
                        //update statistics
                        ++total_mess;
                        total_size += mb.cm->getSize();
                        total_inner_mess += mb.cm->getMessageCount();

                        sender.total_size += (mb.cm->getSize() + 12);

                        //send
                        sender.sendRawMessage(mb.cm, to);
                        mb.reset();
                    }
                }
            private:
                int order; // cal by %
                vector<SenderBuffer> next_messages;
                TurnedSynchronizer synchronizer;

                bool needRefresh;
                Sender<TypeHinter>& sender;

                //statistics
                int* nr_bus_messages;
            private:
                //statistics
                int total_mess;
                int total_inner_mess;
                int total_size;
                long start_time_us;
                long new_start_time_us;
                long end_time_us;
            };



            template <class TypeHinter>
            class Sender{
            public:
                friend class SenderRunnable<TypeHinter>;
            public:
                Sender(MPI_Comm* _cs, int _worker_size, int _sender_size = 1): comms(_cs){
                    int comm_size = get_comm_size(comms[0]);
                    nr_messages = new int[comm_size]();
                    nr_bus_messages = new int[comm_size]();

                    for(int i = 0;i < _worker_size;++i){
                        ThreadContext* tc = new ThreadContext();
                        all_channels.push_back(tc);
                    }

                    for(int i = 0;i < _sender_size;++i){
                        SenderRunnable<TypeHinter>* sr = new SenderRunnable<TypeHinter>(i, *this);
                        runnables.push_back(sr);

                        DaemonThread* thread = new DaemonThread(sr, false, false);
                        send_threads.push_back(thread);
                    }
                }

                ~Sender(){
                    delete nr_messages;
                    delete nr_bus_messages;

                    vector<ThreadContext*>::iterator vi = all_channels.begin();
                    for(;vi != all_channels.end();++vi){
                        delete (*vi);
                    }

                    vector<DaemonThread*>::iterator dvi = send_threads.begin();
                    for(;dvi != send_threads.end();++dvi){
                        delete (*dvi);
                    }

                    typename vector<SenderRunnable<TypeHinter>*>::iterator svi = runnables.begin();
                    for(;svi != runnables.end();++svi){
                        delete (*svi);
                    }
                }

                void start(){
                    vector<DaemonThread*>::iterator dvi = send_threads.begin();
                    for(;dvi != send_threads.end();++dvi){
                        (*dvi)->start();
                    }
                }

                void turnInitialize(){
                    resetMessCount();
                    _async_notify_turn_start();
                }

                void turnFinalize(){
                    //compute all the mess count
                    asyncOnNeedRefresh();

                    //wait till all threads send up messages
                    _async_till_all_sent();

                    //then we count the message count
                    _async_reduce_messages();
                }

                void stop(){
                    typename vector<SenderRunnable<TypeHinter>*>::iterator svi = runnables.begin();
                    for(;svi != runnables.end();++svi){
                        (*svi)->getSynchronizer().markStop();
                        (*svi)->getSynchronizer().notifyForTurnStarted();
                    }

                    vector<DaemonThread*>::iterator vi = send_threads.begin();
                    for(;vi != send_threads.end();++vi){
                        (*vi)->join();
                    }
                }

                //sync methods
                void sendRawMessage(const Message* message, int to){
//                    _TRACEE("before sent one");
                    size_t size = message->getSize();
//                    _TRACEE("before sent one, size = %ld\n", size);
                    int type = message->getType();
//                    _TRACEE("send one raw message to %d, type = %d, size = %ld\n", to, type, size);

                    void* buffer = malloc(size + sizeof(int) + sizeof(size_t));
                    BoundedBuffer bb(buffer);
                    bb << type << size;

//                    _TRACEE("end send before serial %d\n", to);
                    message->serialize(bb, size);
//                    _TRACEE("end send after serial %d\n", to);
                    MPI_Send(buffer,size + sizeof(int) + sizeof(size_t), MPI_CHAR, to, 0, comms[to]);
                    free(buffer);

//                    _TRACEE("end send one raw message to %d\n", to);
                }

                //async methods
                void asyncSendMessage(Message* message, int to, int channel_index){
                    ThreadContext* channel = all_channels[channel_index];
                    MessageWrapper wrapper;
                    wrapper.message = message;
                    wrapper.to = to;

                    while(!channel->messages.push(wrapper));

//                    _TRACEE("push one async message to %d by %d\n", to, channel_index);
                }

                void asyncOnNeedRefresh(){
                    typename vector<SenderRunnable<TypeHinter>*>::iterator svi = runnables.begin();
                    for(;svi != runnables.end();++svi){
                        (*svi)->refreshBuffer();
                    }
                }

                int* getMessageCount(){
                    return nr_messages;
                }

                int* getBusMessageCount(){
                    return nr_bus_messages;
                }

                int getRank(){
                    return get_rank(comms[0]);
                }

            private:
                void resetMessCount(){
                    int comm_size = get_comm_size(comms[0]);
                    memset(nr_messages, 0, sizeof(int) * comm_size);
                    memset(nr_bus_messages, 0, sizeof(int) * comm_size);

                    total_size = 0;
                }

                void _async_till_all_sent(){
                    typename vector<SenderRunnable<TypeHinter>*>::iterator svi = runnables.begin();
                    for(;svi != runnables.end();++svi){
                        (*svi)->getSynchronizer().waitForMyTask();
                    }
                }

                void _async_notify_turn_start(){
                    typename vector<SenderRunnable<TypeHinter>*>::iterator svi = runnables.begin();
                    for(;svi != runnables.end();++svi){
                        (*svi)->getSynchronizer().notifyForTurnStarted();
                    }
                }

                void _async_reduce_messages(){
                    int comm_size = get_comm_size(comms[0]);
                    typename vector<SenderRunnable<TypeHinter>*>::iterator svi = runnables.begin();
                    for(;svi != runnables.end();++svi){
                        int* nr_local_messages = (*svi)->getNrBusMessages();
                        for(int j = 0;j < comm_size;++j){
                            nr_bus_messages[j] += nr_local_messages[j];
                        }
                    }
                }

            private:
                MPI_Comm* comms;

                //we'll need to count how much is sent
                int* nr_messages;
                int* nr_bus_messages;



                //now we set up the message buffer
                vector<ThreadContext*> all_channels;

                //all threads
                vector<SenderRunnable<TypeHinter>*> runnables;
                vector<DaemonThread*> send_threads;
            public:
                size_t total_size;
            };
        }
    }
}


#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_SENDER_INC  ----- */
