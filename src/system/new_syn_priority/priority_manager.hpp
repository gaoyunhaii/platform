/*
 * =====================================================================================
 *
 *       Filename:  priority_manager.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年02月24日 14时57分46秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYN_PRIORITY_PRIORITY_MANAGER_HPP_INC
#define  SYSTEM_SYN_PRIORITY_PRIORITY_MANAGER_HPP_INC


#include "system/new_syn_priority/syn_priority_executor.h"


#endif   /* ----- #ifndef SYSTEM_SYN_PRIORITY_PRIORITY_MANAGER_HPP_INC  ----- */
