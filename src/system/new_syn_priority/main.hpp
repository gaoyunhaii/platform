/*
 * =====================================================================================
 *
 *       Filename:  main.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年02月24日 14时23分53秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYN_PRIORITY_MAIN_HPP_INC
#define  SYSTEM_SYN_PRIORITY_MAIN_HPP_INC


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <mpi.h>
#include "system/new_syn_priority/main.h"
#include "system/runtime_constants.h"
#include "system/input.h"
#include "system/input.hpp"
#include "common/file_util.h"

template <class TypeHinter>
bsp::runtime::syn_priority::PriorityDriver<TypeHinter>::PriorityDriver(const char* cf){
    //read configure files
    conf.readFile(cf);

    //get the order
    //TODO check re
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    conf.setIntValue(bsp::runtime::CONF_KEY_ORDER, rank);

    //now reset the standard out to file
    string local_dir;
    //conf.getValue(local_dir, bsp::runtime::CONF_KEY_TASK_DIR_LOCAL, "/tmp/platform");
    local_dir = "/tmp/platform";

    string stdout_path;
    string rank_str = std::to_string((long long int)rank);
    rank_str += "_output";
    bsp::common::file_join_path(stdout_path, local_dir.c_str(), rank_str.c_str(), NULL);
    int fd = open(stdout_path.c_str(), O_RDWR | O_CREAT | O_TRUNC, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
    dup2(fd, STDOUT_FILENO);

    //init the hdfs client
    string hdfsAddr;
    conf.getValue(hdfsAddr, bsp::runtime::CONF_KEY_HDFS_ADDR, "localhost");
    int port = conf.getIntValue(bsp::runtime::CONF_KEY_HDFS_PORT, 9000);

    hdfs_client = new HDFSClient(hdfsAddr.c_str(), port);
    hdfs_client->connect();

    part = new PART(conf, *hdfs_client);
    provider = new Provider<TypeHinter>(conf, *part);
}

template <class TypeHinter>
bsp::runtime::syn_priority::PriorityDriver<TypeHinter>::~PriorityDriver() {
    delete provider;

    delete part;

    hdfs_client->close();
    delete hdfs_client;
}


template <class TypeHinter>
void bsp::runtime::syn_priority::PriorityDriver<TypeHinter>::input_data(INPUT& input) {
    part->init();
    input.input(this->conf, *hdfs_client, *provider);

    //then we summarize all the vertices
    int local_total_v = provider->totalLocalVertices();
    int local_total_e = provider->totalLocalEdge();

    int total_v, total_e;

    MPI_Allreduce(&local_total_v, &total_v, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(&local_total_e, &total_e, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

    provider->setTotalVertices(total_v);
    provider->setTotalEdges(total_e);

    _TRACEE("local v = %d, local e = %d, v = %d, e = %d\n", local_total_v, local_total_e, total_v, total_e);
    int total_in = provider->getInEdges().size();
    int total_inner = provider->getInnerEdges().size();
    int total_outer = provider->getOuterEdges().size();
    _TRACEE("total in = %d, total inner = %d, total outer = %d\n", total_in, total_inner, total_outer);
}

template <class TypeHinter>
void bsp::runtime::syn_priority::PriorityDriver<TypeHinter>::run_program(PROGRAM &program, PRIORITY_MANAGER& priority_manager) {
    EXECUTOR executor(*provider, program, priority_manager, MPI_COMM_WORLD);
    executor.run();
}

#endif   /* ----- #ifndef SYSTEM_SYN_PRIORITY_MAIN_HPP_INC  ----- */
