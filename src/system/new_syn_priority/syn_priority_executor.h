/*
 * =====================================================================================
 *
 *       Filename:  syn_priority_executor.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年02月24日 14时37分11秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef  SYSTEM_SYN_PRIORITY_SYN_PRIORITY_EXECUTOR_INC
#define  SYSTEM_SYN_PRIORITY_SYN_PRIORITY_EXECUTOR_INC

#include "common/atomic.h"
#include "common/dthread.h"
#include <vector>
#include "mpi.h"
#include "system/synchronize/receiver.h"
#include "system/synchronize/sender.h"
#include "system/synchronize/aggregator.h"
#include "system/synchronize/notifier.h"
#include "system/new_syn_priority/priority_manager.h"
using bsp::multi_thread::DaemonThread;
using bsp::multi_thread::Runnable;
using bsp::runtime::Vertex;
using std::vector;
using bsp::runtime::synchronize::MessageBuffer;
using bsp::runtime::synchronize::Receiver;
using bsp::runtime::synchronize::Sender;
using bsp::runtime::synchronize::NewNotifier;
using bsp::runtime::synchronize::MessageHandler;
using bsp::atomic::TurnedSynchronizer;

namespace bsp {
    namespace runtime {
        namespace syn_priority {

            template <class TypeHinter>
            class SynPriorityExecutor;


            template <class TypeHinter>
            class WorkerThread;

            template <class TypeHinter>
            struct WorkerThreadContext{
                vector<Vertex<TypeHinter>* > to_remove;
                vector<Vertex<TypeHinter>* > to_add;
            };

            template <class TypeHinter>
            class WorkerThreadRunnable : public Runnable{
            public:
                typedef void (SynPriorityExecutor<TypeHinter>::*ExecuteFunc)();

                WorkerThreadRunnable(WorkerThread<TypeHinter> &_wt, int _o):
                        workerThread(_wt), order(_o){
                }

                void run(){
                    while(true){
                        synchronizer.waitForTurnStart();
                        //check for stop
                        if(workerThread.isStopped()){
                            break;
                        }

                        //execute current func
                        workerThread.executeFunc(order, context);

                        //notify
                        synchronizer.notifyForMyTaskFinished();
                    }
                }

                TurnedSynchronizer& getSynchronizer(){
                    return synchronizer;
                }

                WorkerThreadContext<TypeHinter>& getContext(){
                    return context;
                }
            private:
                WorkerThread<TypeHinter> &workerThread;
                TurnedSynchronizer synchronizer;
                int order;

                //the context
                WorkerThreadContext<TypeHinter> context;
            };


            template <class TypeHinter>
            class WorkerThread {
            public:
                typedef typename TypeHinter::Provider PROVIDER;
                typedef void (SynPriorityExecutor<TypeHinter>::*ExecuteFunc)(int worker_id, WorkerThreadContext<TypeHinter>& context);

                WorkerThread(SynPriorityExecutor<TypeHinter>& _se, int total):
                        syn_executor(_se), is_stopped(false){

                    for(int i = 0;i < total;++i){
                        WorkerThreadRunnable<TypeHinter>* runnable =
                                new WorkerThreadRunnable<TypeHinter>(*this, i);
                        DaemonThread* dt = new DaemonThread(runnable, false, false);

                        runnables.push_back(runnable);
                        threads.push_back(dt);
                    }
                }

                ~WorkerThread(){
                    for(int i = 0;i < runnables.size();++i){
                        delete threads[i];
                        delete runnables[i];
                    }
                }

                void start(){
                    vector<DaemonThread*>::iterator vi = threads.begin();
                    for(;vi != threads.end();++vi){
                        (*vi)->start();
                    }
                }

                void stop(){
                    this->is_stopped = true;
                    typename vector<WorkerThreadRunnable<TypeHinter>*>::iterator vi = runnables.begin();
                    for(;vi != runnables.end();++vi){
                        (*vi)->getSynchronizer().notifyForTurnStarted();
                    }

                    vector<DaemonThread*>::iterator tvi = threads.begin();
                    for(;tvi != threads.end();++tvi){
                        (*tvi)->join();
                    }
                }

                vector<WorkerThreadRunnable<TypeHinter>* >& getRunnables(){
                    return runnables;
                }

                void execute(ExecuteFunc func){
                    this->func = func;

                    typename vector<WorkerThreadRunnable<TypeHinter>*>::iterator vi = runnables.begin();
                    for(;vi != runnables.end();++vi){
                        (*vi)->getSynchronizer().notifyForTurnStarted();
                    }

                    vi = runnables.begin();
                    for(;vi != runnables.end();++vi){
                        (*vi)->getSynchronizer().waitForMyTask();
                    }
                }

                //methods for thread
                bool isStopped(){
                    return is_stopped;
                }

                void executeFunc(int worker_id, WorkerThreadContext<TypeHinter>& context){
                    (syn_executor.*func)(worker_id, context);
                }

                int getWorkerSize() const{
                    return threads.size();
                }
            private:
                SynPriorityExecutor<TypeHinter>& syn_executor;
                bool is_stopped;

                ExecuteFunc func;

                vector<WorkerThreadRunnable<TypeHinter>* > runnables;
                vector<DaemonThread*> threads;
            };


            template <class TypeHinter>
            class SynPriorityExecutor{
            public:
                typedef typename TypeHinter::Provider PROVIDER;
                typedef typename TypeHinter::Program PROGRAM;

                SynPriorityExecutor(PROVIDER& _p, PROGRAM& _pro, PriorityManager<TypeHinter>& _p_pm, MPI_Comm _c):
                        rank(0), turn(0),
                        provider(_p), program(_pro), priority_manager(_p_pm),
                        comm(_c),
                        full_current(NULL),
                        partial_current(NULL),
                        active_count(0),
                        full_updates_this_turn(0), moved(0), partial_updates_this_turn(0){
                    threads = new WorkerThread<TypeHinter>(*this, 1);
                    rank = get_rank(comm);
                }

                ~SynPriorityExecutor(){
                    delete threads;
                }

                void turnInitialize(){
                    full_current = priority_manager.getCurrentList().getStart();
                    partial_current = priority_manager.getPartialList().getStart();
                }

                void turnFinalize(){

                }

                void run();

                void updateOneTurn(int worker_id, WorkerThreadContext<TypeHinter>& context);
                void updatePriorityOneTurn(int worker_id, WorkerThreadContext<TypeHinter>& context);

                //other method for the users
                PROVIDER& getProvider(){
                    return provider;
                }

                PROGRAM& getProgram(){
                    return program;
                }

                MPI_Comm& getComm(){
                    return comm;
                }

                MessageBuffer<TypeHinter>& getMessageBuffer(){
                    return *p_message_buffer;
                }

                Receiver<TypeHinter>& getReceiver(){
                    return *p_receiver;
                }

                Sender<TypeHinter>& getSender(){
                    return *p_sender;
                }

                AggregatorManager& getAggregatorManager(){
                    return *p_am;
                }

                NewNotifier<TypeHinter>& getNewNotifier(){
                    return *p_notifier;
                }

                PriorityManager<TypeHinter>& getPriorityManager(){
                    return priority_manager;
                }

                int getRank(){
                    return rank;
                }

                int getTurn(){
                    return turn;
                }

            private:
                WorkerThread<TypeHinter>* threads;
                int rank;
                int turn;

                PROVIDER &provider;
                PROGRAM &program;
                PriorityManager<TypeHinter>& priority_manager;

                MPI_Comm comm;

                //tmp pointers
                NewNotifier<TypeHinter>* p_notifier;
                MessageBuffer<TypeHinter>* p_message_buffer;
                MessageHandler<TypeHinter>* p_message_handler;
                Receiver<TypeHinter>* p_receiver;
                Sender<TypeHinter>* p_sender;
                AggregatorManager* p_am;

                //int next_to_compute;
                Vertex<TypeHinter>* full_current;
                Vertex<TypeHinter>* partial_current;

                int active_count;

                int full_updates_this_turn;
                int moved;
                int partial_updates_this_turn;
            };
        }
    }
}


#endif   /* ----- #ifndef SYSTEM_SYN_PRIORITY_SYN_PRIORITY_EXECUTOR_INC  ----- */
