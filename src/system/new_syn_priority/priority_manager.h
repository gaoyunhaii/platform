/*
 * =====================================================================================
 *
 *       Filename:  priority_manager.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年02月24日 14时56分34秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYN_PRIORITY_PRIORITY_MANAGER_INC
#define  SYSTEM_SYN_PRIORITY_PRIORITY_MANAGER_INC

#include "system/runtime.h"
#include <limits>
#include <map>

using bsp::runtime::Vertex;
using bsp::runtime::VertexList;
using std::map;

typedef long long int BucketId;
const BucketId END_BUCK = std::numeric_limits<BucketId>::max();

namespace bsp{
    namespace runtime{
        namespace syn_priority{
            template <class TypeHinter>
            class PriorityManager{
            public:
                typedef typename TypeHinter::Provider PROVIDER;
                typedef typename TypeHinter::Program PROGRAM;
                typedef typename TypeHinter::Executor EXECUTOR;
            public:
                PriorityManager(PROVIDER& _p, PROGRAM& _pro) :
                        provider(_p), program(_pro), current_bucket_id(0), current_max_bucket_id(0){
                    //initially all vertices should be in the partial list
                    partial_list.replace_another(provider.vertex_list);
                }

                /**
                 * Remove vertices before the program runs
                 */
                void addToInitBucket(Vertex<TypeHinter>* which){
                    partial_list.remove_vertex(which);
                    current_list.push_back(which);
                }

                void removeVerticesToPartial(vector<Vertex<TypeHinter>* >& to_remove){
                    typedef typename vector<Vertex<TypeHinter>* >::iterator VI;
                    for(VI vi = to_remove.begin();vi != to_remove.end();++vi){
                        //_TRACEE("%s remove %ld to partial\n%s", BLUE, (*vi)->getId(), NONE);
                        current_list.remove_vertex(*vi);
                        partial_list.push_back(*vi);
                    }
                }

                void addVerticesFromPartial(vector<Vertex<TypeHinter>* >& to_add){
                    typedef typename vector<Vertex<TypeHinter>* >::iterator VI;
                    for(VI vi = to_add.begin();vi != to_add.end();++vi){
                        //_TRACEE("%s add %ld to full\n%s", BLUE, (*vi)->getId(), NONE);
                        partial_list.remove_vertex(*vi);
                        current_list.push_back(*vi);
                        assert((*vi)->next != (*vi));
                        assert((*vi)->prev != (*vi));
                        assert((*vi)->fast_next != (*vi));
                        assert((*vi)->fast_prev != (*vi));
                    }
                }

                void finishCurrentBucket(){
                    finished_list.push_back_another(current_list);
                }

                void countBucketVertices(map<BucketId, int>&buck_count, EXECUTOR& executor){
                    for(Vertex<TypeHinter>* vi = partial_list.getStart();vi;vi = vi->next){
                        BucketId v_buck_id = program.getBucketId(vi, executor);
                        buck_count[v_buck_id]++;
                    }
                }

                BucketId getNextBucketId(EXECUTOR& executor){
                    BucketId min_buck_id = END_BUCK;
                    for(Vertex<TypeHinter>* vi = partial_list.getStart();vi;vi = vi->next){
                        BucketId v_buck_id = program.getBucketId(vi, executor);
                        if(v_buck_id < min_buck_id){
                            min_buck_id = v_buck_id;
                        }
                    }

                    return min_buck_id;
                }

                void getToNextBucket(BucketId next_id, BucketId next_max_id, EXECUTOR& executor){
                    Vertex<TypeHinter>* vi = partial_list.getStart();
                    while(vi){
                        BucketId v_buck_id = program.getBucketId(vi, executor);
                        if(v_buck_id >= next_id && v_buck_id <= next_max_id){
                            Vertex<TypeHinter>* nvi = vi->next;
                            vi->setHalt(false);
                            partial_list.remove_vertex(vi);
                            current_list.push_back(vi);
                            vi = nvi;
                        }
                        else{
                            vi = vi->next;
                        }
                    }

                    current_bucket_id = next_id;
                    current_max_bucket_id = next_max_id;
                }

                BucketId getCurrentBucket(){
                    return current_bucket_id;
                }

                BucketId getCurrentMaxBucket(){
                    return current_max_bucket_id;
                }

                VertexList<TypeHinter>& getFinishedList(){
                    return finished_list;
                }

                VertexList<TypeHinter>& getCurrentList(){
                    return current_list;
                }

                VertexList<TypeHinter>& getPartialList(){
                    return partial_list;
                }

            private:
                PROVIDER& provider;
                PROGRAM& program;

                VertexList<TypeHinter> finished_list;
                VertexList<TypeHinter> current_list;
                VertexList<TypeHinter> partial_list;

                //range [current bucket id, current max bucket id] will be computed
                BucketId current_bucket_id;
                BucketId current_max_bucket_id; //the current max is included
            };
        }
    }
}






#endif   /* ----- #ifndef SYSTEM_SYN_PRIORITY_PRIORITY_MANAGER_INC  ----- */
