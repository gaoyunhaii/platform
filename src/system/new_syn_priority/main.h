/*
 * =====================================================================================
 *
 *       Filename:  main.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年02月24日 14时23分38秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef  SYSTEM_SYN_PRIORITY_MAIN_INC
#define  SYSTEM_SYN_PRIORITY_MAIN_INC

#include "common/hdfs_client.h"
#include "common/configuration.h"
#include "system/runtime_constants.h"
#include "system/provider.h"
#include "system/provider.hpp"
#include "system/partition.h"
#include "system/input.h"
#include "system/input.hpp"
#include "system/input_parser.h"
#include "system/synchronize/notifier.h"
#include "system/new_syn_priority/priority_manager.h"
#include "system/new_syn_priority/priority_manager.hpp"
#include "system/new_syn_priority/syn_priority_executor.h"
#include "system/new_syn_priority/syn_priority_executor.hpp"

using bsp::common::HDFSClient;
using bsp::common::Configuration;
using bsp::runtime::Provider;
using bsp::runtime::PrePartitioner;
using bsp::runtime::synchronize::MessageBuffer;
using bsp::runtime::synchronize::Sender;
using bsp::runtime::synchronize::NotifyMessage;
using bsp::runtime::syn_priority::SynPriorityExecutor;
using bsp::runtime::syn_priority::PriorityManager;

namespace bsp{
    namespace runtime {
        namespace syn_priority{

            template<class TypeHinter>
            class DefaultPriorityProgram {
            public:
                typedef typename TypeHinter::MessageValue MV;
                typedef typename TypeHinter::BufferValue BV;
                typedef typename TypeHinter::Provider PROVIDER;
                typedef MessageBuffer<TypeHinter> MESSAGE_BUFFER;
                typedef Sender<TypeHinter> SENDER;
                typedef SynPriorityExecutor<TypeHinter> EXECUTOR;

                virtual bool isNotify() {
                    return false;
                }

                virtual bool isHeadFirst(){
                    return false;
                }

                //notify
                virtual void onNewRemoteNotifyMessage(NotifyMessage<TypeHinter>& notify_message, const VertexId& vid, const MV& mv, InEdge<TypeHinter>* in_edge){ }
                virtual void onLocalNotifyMessage(NotifyMessage<TypeHinter>& notify_message, const MV& mv, const Vertex<TypeHinter>* vertex, InnerEdge<TypeHinter>* inner_edge){ }

                virtual void onNewBufferMessage(const VertexId &vid, const MV &message, BV &value) { }

                virtual void updateNextTurnAggregator(EXECUTOR& executor){ }

                //distinguis full update and partial update
                virtual void partialUpdateVertex(Vertex<TypeHinter>* next, EXECUTOR& executor, int channel_index){}
                virtual void broadcastMessages(Vertex<TypeHinter>* next, EXECUTOR& executor, int channel_index){ }

                //virtual void fullUpdateVertex(Vertex<TypeHinter>* next, EXECUTOR& executor, int channel_index){ }

                //mapping vertex to the bucket
                virtual BucketId getBucketId(Vertex<TypeHinter>* next, EXECUTOR& executor) = 0;

                virtual bool beforeTurn(EXECUTOR& executor){
                    return true;
                }

                virtual bool afterTurn(EXECUTOR& executor, MPI_Comm& comm){
                    return true;
                }
            };


            template<class TypeHinter>
            class DefaultPriorityTypeHinter {
            public:
                typedef float VertexValue;
                typedef float EdgeValue;
                typedef float BufferValue;
                typedef float MessageValue;
                typedef float NotifyValue;
                typedef PrePartitioner Partitioner;
                typedef AdjLineParser<TypeHinter> InputParser;
                typedef FileInput<TypeHinter> InputClass;
                typedef DefaultPriorityProgram<TypeHinter> Program;
                typedef bsp::runtime::Provider<TypeHinter> Provider;

                typedef bsp::runtime::syn_priority::SynPriorityExecutor<TypeHinter> Executor;
                typedef bsp::runtime::syn_priority::PriorityManager<TypeHinter> PriorityManager;
            };


            template<class TypeHinter>
            class PriorityDriver {
            public:
                typedef typename TypeHinter::VertexValue VV;
                typedef typename TypeHinter::EdgeValue EV;
                typedef typename TypeHinter::Partitioner PART;
                typedef typename TypeHinter::InputClass INPUT;
                typedef typename TypeHinter::Program PROGRAM;
                typedef typename TypeHinter::PriorityManager PRIORITY_MANAGER;
                typedef typename TypeHinter::Executor EXECUTOR;

                PriorityDriver(const char *_cf);

                ~PriorityDriver();

                void input_data(INPUT &input);

                Configuration &getConf() {
                    return conf;
                }

                Provider<TypeHinter> &getProvider() {
                    return *provider;
                }

                void run_program(PROGRAM &program, PRIORITY_MANAGER& priority_manager);

            private:
                Configuration conf;
                HDFSClient *hdfs_client;
                PART *part;
                Provider<TypeHinter> *provider;
            };
        }
    }
}


#endif   /* ----- #ifndef SYSTEM_SYN_PRIORITY_MAIN_INC  ----- */
