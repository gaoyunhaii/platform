/*
 * =====================================================================================
 *
 *       Filename:  syn_priority_executor.hpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  2016年02月24日 14时37分14秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYN_PRIORITY_SYN_PRIORITY_EXECUTOR_HPP_INC
#define  SYSTEM_SYN_PRIORITY_SYN_PRIORITY_EXECUTOR_HPP_INC

#include "system/new_syn_priority/syn_priority_executor.h"
#include "system/synchronize/message_buffer.h"
#include "system/synchronize/message_buffer.hpp"
#include "common/util.h"
#include <limits>
#include <map>

using bsp::runtime::synchronize::MessageBuffer;
using bsp::runtime::synchronize::Receiver;
using bsp::runtime::synchronize::ControlMessage;
using bsp::runtime::synchronize::LocalControlMessage;
using bsp::atomic::atomic_incre_re;
using bsp::atomic::atomic_compare_and_swap;
using bsp::common::get_time_us;
using bsp::runtime::syn_priority::WorkerThreadRunnable;
using bsp::runtime::syn_priority::WorkerThreadContext;
using std::map;

template<class TypeHinter>
void bsp::runtime::syn_priority::SynPriorityExecutor<TypeHinter>::run() {
    int comm_size = get_comm_size(comm);
    int rank = get_rank(comm);

    //initialize all the required component
    AggregatorManager aggregator_manager(comm_size);
    p_am = &aggregator_manager;
    aggregator_manager.start();
    _TRACEE("am started\n");

    int total_vertices = provider.totalVertices();

    //now we clone a comm for sender and receiver
    MPI_Comm *comms = new MPI_Comm[comm_size];
    for (int i = 0; i < comm_size; ++i) {
        MPI_Comm_dup(comm, comms + i);
    }

//    MPI_Comm message_comm;
//    MPI_Comm_dup(comm, &message_comm);
    Sender<TypeHinter> sender(comms, threads->getWorkerSize(), 4);
    p_sender = &sender;
    sender.start();
    _TRACEE("sender started\n");

    int receiver_worker_size = 8;
    int handler_size = 8;
    NewNotifier<TypeHinter> notifier(receiver_worker_size, provider, program);
    p_notifier = &notifier;
    notifier.start();
    _TRACEE("notifier started\n");

    //start the receiver
    MessageBuffer<TypeHinter> buffer(receiver_worker_size, rank, provider, program, notifier);
    p_message_buffer = &buffer;
    buffer.start();
    _TRACEE("buffer started\n");

    MessageHandler<TypeHinter> handler(receiver_worker_size, handler_size, comm_size, program, buffer,
                                       aggregator_manager);
    p_message_handler = &handler;
    handler.start();
    _TRACEE("handler started\n");

    //start the receive
    Receiver<TypeHinter> receiver(comms[rank], handler);
    p_receiver = &receiver;
    _TRACEE("now we start to receive messages\n");
    receiver.start();
    _TRACEE("receiver started\n");

    //wait till every one is prepared
    MPI_Barrier(comm);
    _TRACEE("now all has been started\n");

    threads->start();
    _TRACEE("worker threads all has been started\n");

    //now we do some execution
    int total_messages = 0;
    int total_local_messages = 0;
    int total_full_updates = 0;
    int total_moved = 0;
    int total_partial_updates = 0;


    //statistics for the priority execution
    int stat_nr_buckets = 0;
    long long int stat_last_start = get_time_us();
    int stat_finished = 0;

    int stat_lastRemain = total_vertices;

    //all buffer
    bool use_auto = true;
    int buck_to_count = 20;
    long long int *pro_data = new long long int[64];
    long long int *pro_buffer = new long long int[comm_size * 64];
    int min = provider.totalVertices() / 20;
    if (min > 80000) {
        min = 80000;
    }

    while (true) {
        _TRACEE("------start turn %d\n", turn);
        active_count = 0;

        long long all_start = get_time_us();

        //start a new turn
        sender.turnInitialize();
        long sender_start = get_time_us();
        _TRACEE("------turn %d, sender initialized\n", turn);

        aggregator_manager.turnIntialize();
        long am_start = get_time_us();
        _TRACEE("------turn %d, aggregator manager initialized\n", turn);

        notifier.turnInitialize();
        long not_start = get_time_us();
        _TRACEE("------turn %d, not initialized\n", turn);

        buffer.turnInitialize();
        long buffer_start = get_time_us();
        _TRACEE("------turn %d, buffer initialized\n", turn);

        handler.turnInitialize();
        long handler_start = get_time_us();
        _TRACEE("------turn %d, handler initialized\n", turn);

        receiver.turnInitialize();
        _TRACEE("------turn %d, receiver initialized\n", turn);
        long receiver_start = get_time_us();

        //do our computation
        turnInitialize();
        long exec_start = get_time_us();
        _TRACEE("------turn %d, before computation\n", turn);
        program.beforeTurn(*this);
        threads->execute(&SynPriorityExecutor<TypeHinter>::updateOneTurn);
        _TRACEE("------turn %d, after computation\n", turn);
        long exec_end = get_time_us();
        turnFinalize();

        _TRACEE("%s exec used %ld mill\n %s", GREEN, (exec_end - exec_start) / 1000, NONE);

        //here we stop the send`er first
        sender.turnFinalize();
        _TRACEE("sender used %ld mill\n", (get_time_us() - sender_start) / 1000);
        _TRACEE("------turn %d, sender stopped\n", turn);

        //we first make sure all messages are sent, then we send control messages
        int *nr_messages = sender.getBusMessageCount();
        //send control messages
        for (int i = 0; i < comm_size; ++i) {
            _TRACEE("------turn %d, send %d messages to %d\n", turn, nr_messages[i], i);
            ControlMessage control_message(nr_messages[i]);
            sender.sendRawMessage(&control_message, i);
        }

        //then we can wait till all messages are received
//        receiver.turnFinalize();
//        _TRACEE("receiver used %ld mill\n", (get_time_us() - receiver_start) / 1000);
//        _TRACEE("receiver stopped\n");

        handler.turnFinalize();
        _TRACEE("handler used %ld mill\n", (get_time_us() - handler_start) / 1000);
        _TRACEE("handler stopped\n");

        buffer.turnFinalize();
        _TRACEE("buffer used %ld mill\n", (get_time_us() - buffer_start) / 1000);
        _TRACEE("buffer stopped\n");

        notifier.turnFinalize();
        _TRACEE("notifier used %ld mill\n", (get_time_us() - not_start) / 1000);
        _TRACEE("notifier stopped\n");

        aggregator_manager.waitTillAllReceived();
        _TRACEE("aggregator all received\n");

        //here we wait till all are added
        program.updateNextTurnAggregator(*this);
        aggregator_manager.turnFinalize();

        _TRACEE("am used %ld mill\n", (get_time_us() - am_start) / 1000);
        _TRACEE("aggregator stopped\n");

        //then here we first update the priority buckets
        turnInitialize();
        threads->execute(&SynPriorityExecutor<TypeHinter>::updatePriorityOneTurn);
        //here we collected all the to remove and to add, then we do the modification
        vector<WorkerThreadRunnable<TypeHinter> *> &runnables = threads->getRunnables();
        typedef typename vector<WorkerThreadRunnable<TypeHinter> *>::iterator WTRVI;
        for (WTRVI vi = runnables.begin(); vi != runnables.end(); ++vi) {
            WorkerThreadContext<TypeHinter> &context = (*vi)->getContext();
//            priority_manager.getCurrentList().print();
//            priority_manager.getPartialList().print();
            priority_manager.removeVerticesToPartial(context.to_remove);
//            _TRACEE("removed to partial\n");
//            priority_manager.getCurrentList().print();
//            priority_manager.getPartialList().print();

            priority_manager.addVerticesFromPartial(context.to_add);
//            _TRACEE("add from partial\n");
//            priority_manager.getCurrentList().print();
//            priority_manager.getPartialList().print();
            context.to_remove.clear();
            context.to_add.clear();
//            _TRACEE("cleared\n");
        }

        //then we define whether the current bucket is still active
        //we first try to know if current bucket has been finished
        //we adjust whether we need to stop
        int my_value = 0;
        int data_messages = handler.getBusMessThisTurn();
        int local_messages = buffer.getLocalMessThisTurn();

        if (active_count != 0 || data_messages != 0 || local_messages != 0) {
            my_value = 1;
        }

        int theysaycont = program.afterTurn(*this, comm);
        if (theysaycont == 0) {
            my_value = 0;
        }

        int all_values = 0;
        MPI_Allreduce(&my_value, &all_values, 1, MPI_INT, MPI_SUM, comm);

        _TRACEE("active = %d, dm = %d, lm = %d, my_value = %d, all_values = %d\n", active_count, data_messages,
                local_messages, my_value,
                all_values);
        _TRACEE("size_count: turn = %d, count = %d, all = %d, size =  %ld\n", turn, data_messages,
                data_messages + local_messages, sender.total_size);

        //here, if current bucket has been finished, we head to the next one
        if (all_values == 0) {
            priority_manager.finishCurrentBucket();

            //print count
            int finished = priority_manager.getFinishedList().size();
            int current = priority_manager.getCurrentList().size();
            int partial = priority_manager.getPartialList().size();

            long long int stat_current = get_time_us();

            //_TRACEE("tag_1, current buckets fin = %d, cur = %d, part = %d\n", finished, current, partial);
            //update and output statistics
            //_TRACEE("tag_1, nr_bucket = %d, time used = %f, finished = %d, avg = %f\n", stat_nr_buckets, (stat_current - stat_last_start) / 1.0e6, (finished - stat_finished),
            //        finished - stat_finished == 0 ? -1 : (stat_current - stat_last_start) / 1.0e6 / (finished - stat_finished) );

            BucketId result[2];
            result[0] = result[1] = END_BUCK;

            long g_start = get_time_us();
            if (!program.isHeadFirst()) {
                //count how many vertices in each bucket
                //update delta according to the next bucket id
                map<BucketId, int> buck_count;
                priority_manager.countBucketVertices(buck_count, *this);

                //print the buck_count;

                map<BucketId, int>::iterator mi = buck_count.begin();
                //for(;mi != buck_count.end();++mi){
                //    _TRACEE("tag_1, turn = %d, %ld=>%d\n", turn, mi->first, mi->second);
                //}

                pro_data[0] = buck_count.size();
                pro_data[1] = partial;
                pro_data[2] = current;

                int PRO_DATA_START = 3;

                mi = buck_count.begin();
                int added = 0;
                for (; mi != buck_count.end() && added < buck_to_count; ++mi) {
//                data[next++] = mi->first;
//                data[next++] = mi->second;
                    pro_data[2 * added + PRO_DATA_START] = mi->first;
                    pro_data[2 * added + PRO_DATA_START + 1] = mi->second;

                    ++added;
                }

                _TRACEE("start gather\n");
                //here we compute the next bucket. we do this with one special

                if (rank == 0) {
                    MPI_Gather(pro_data, 2 * buck_to_count + PRO_DATA_START, MPI_LONG_LONG_INT, pro_buffer,
                               2 * buck_to_count + PRO_DATA_START, MPI_LONG_LONG_INT, 0, comm);
                    for (int i = 0; i < (2 * buck_to_count + PRO_DATA_START) * comm_size; ++i) {
                        //_TRACEE("tag_1, %d = %ld\n",i, pro_buffer[i]);
                    }

                    //here we continue to compute
                    //start from the 21, we add all the data to the buck_count
                    map<BucketId, int> all_buck_count;
                    int each_buffer = 2 * buck_to_count + PRO_DATA_START;

                    int totalCurrent = 0;
                    int totalPartial = 0;


                    for (int i = 0; i < comm_size; ++i) {
                        int start = i * each_buffer;
                        for (int j = 0; j < pro_buffer[start] && j < buck_to_count; ++j) {
                            all_buck_count[pro_buffer[start + PRO_DATA_START + 2 * j]] = (int) pro_buffer[start +
                                                                                                          PRO_DATA_START +
                                                                                                          1 + 2 * j];
                        }

                        totalCurrent = totalCurrent + pro_buffer[start + 2];
                        totalPartial = totalPartial + pro_buffer[start + 1];
                    }

                    map<BucketId, int>::iterator mi = all_buck_count.begin();
                    for (; mi != all_buck_count.end(); ++mi) {
                        _TRACEE("tag_1, turn = %d, all %ld=>%d\n", turn, mi->first, mi->second);
                    }

                    //_TRACEE("tag_1, totalCurrent = %d, totalPartial = %d\n", totalCurrent, totalPartial);

                    int newFinished = stat_lastRemain - (totalPartial + totalCurrent);
                    _TRACEE("tag_1, turn = %d, total_cur = %d, total_par = %d, nr_bucket = %d, "
                                    "time used = %.9f, finished = %d, avg = %.9f\n",
                            turn,
                            totalCurrent, totalPartial,
                            stat_nr_buckets,
                            (stat_current - stat_last_start) / 1.0e6, newFinished,
                            newFinished == 0 ? -1 : (stat_current - stat_last_start) / 1.0e6 / newFinished);

                    stat_nr_buckets++;
                    stat_last_start = stat_current;
                    stat_finished = finished;
                    stat_lastRemain = totalPartial + totalCurrent;

                    if(use_auto){
                        if (all_buck_count.size() > 0) {
                            result[0] = all_buck_count.begin()->first;
                            map<BucketId, int>::iterator mi = all_buck_count.begin();
                            int total = 0;
                            int score = std::numeric_limits<int>::min();
                            BucketId best = END_BUCK;

                            for (; mi != all_buck_count.end(); ++mi) {
                                if (mi->first == END_BUCK) {
                                    continue;
                                }

                                //count the total size and the balance
                                total += mi->second;
                                int new_score = 200 - abs(min - total);
                                if (new_score >= score) {
                                    score = new_score;
                                    best = mi->first;
                                }
                            }

                            if (newFinished < min && newFinished >= 2) {
                                //map<BucketId , int>::reverse_iterator rmi = all_buck_count.rbegin();
                                //while(rmi != all_buck_count.rend() && rmi->first == END_BUCK){
                                //    ++rmi;
                                //}

                                //if(rmi != all_buck_count.rend()){
                                //	best = rmi->first + 200;
                                //}
                                //else{
                                best = END_BUCK - 1;
                                //}
                                //best = END_BUCK - 1;
                            }
                            else if (best != END_BUCK && total < min / 10) {
                                if (best > result[0]) {
                                    best = best + (best - result[0]) * 3;
                                }
                                else {
                                    best = best + 15;
                                }
                            }

                            result[1] = best;
                        }
                    }
                    else{
                        if (all_buck_count.size() > 0) { //choose the first
                            result[0] = all_buck_count.begin()->first;
                            result[1] = all_buck_count.begin()->first;
                        }
                    }

                    _TRACEE("tag_1, decided = %ld, %ld\n", result[0], result[1]);
                    MPI_Bcast(result, 2, MPI_LONG_LONG_INT, 0, comm);
                }
                else {
                    MPI_Gather(pro_data, 2 * buck_to_count + PRO_DATA_START, MPI_LONG_LONG_INT, NULL,
                               2 * buck_to_count + PRO_DATA_START, MPI_LONG_LONG_INT, 0, comm);
                    MPI_Bcast(result, 2, MPI_LONG_LONG_INT, 0, comm);
                }
            }
            else { //for the other, use the traditional method
                BucketId next_buck_id = priority_manager.getNextBucketId(*this);
                BucketId min_buck_id;
                MPI_Allreduce(&next_buck_id, &min_buck_id, 1, MPI_LONG_LONG_INT, MPI_MIN, comm);

                result[0] = result[1] = min_buck_id;
            }

            _TRACEE("tag_1, final get %ld, %ld\n", result[0], result[1]);

            long g_end = get_time_us();
            _TRACEE("tag_1, gather used %d\n", (int) ((g_end - g_start) / 1e6));

//            BucketId next_buck_id = priority_manager.getNextBucketId(*this);
//
//            //then every bucket go decide the minimal bucket id
//            BucketId min_buck_id;
//            MPI_Allreduce(&next_buck_id, &min_buck_id, 1, MPI_LONG_LONG_INT, MPI_MIN, comm);
//
//            _TRACEE("tag_1, turn = %d, next buck id = %d, min buck id = %d\n", turn, next_buck_id, min_buck_id);
//
//            if(min_buck_id == END_BUCK){ //no more buckets can be computed any more
//                break;
//            }
//
//            //or we head to the next bucket
//            priority_manager.getToNextBucket(min_buck_id, min_buck_id, *this);

            _TRACEE("tag_1, turn = %d, next buck id = %ld %ld\n", turn, result[0], result[1]);
            if (result[1] == END_BUCK) { //no more vertices
                assert(result[0] == END_BUCK);
                break;
            }
            priority_manager.getToNextBucket(result[0], result[1], *this);

            //print count
            //finished = priority_manager.getFinishedList().size();
            //current = priority_manager.getCurrentList().size();
            //partial = priority_manager.getPartialList().size();

            //_TRACEE("tag_1, current buckets fin = %d, cur = %d, part = %d\n", finished, current, partial);
        }

        _TRACEE("all used %ld mill\n", (get_time_us() - all_start) / 1000);
        _TRACEE("all stopped\n");



//
//        if (all_values == 0) {
//            //then we can exit
//            break;
//        }

        //MPI_Barrier(comm);
//        int finished = priority_manager.getFinishedList().count();
//        int current = priority_manager.getCurrentList().count();
//        int partial = priority_manager.getPartialList().count();
//
//        int to_sum[] = {data_messages, local_messages, full_updates_this_turn, partial_updates_this_turn, moved, finished, current, partial};
//        int size = sizeof(to_sum) / sizeof(int);
//        int sum_result[size];
//        MPI_Allreduce(to_sum, sum_result, size, MPI_INT, MPI_SUM, comm);
//        _TRACEE("sum : %d %d, %d %d, %d %d %d, %d %d %d\n", turn, priority_manager.getCurrentBucket(),
//                sum_result[0], sum_result[1], sum_result[2], sum_result[3], sum_result[4], sum_result[5], sum_result[6], sum_result[7]);
//        _TRACEE("turn = %d, used = %d ms, remote msg = %d, local msg = %d, full = %d, partial = %d, moved = %d, current_buck_id = %ld\n",
//                turn, all_used, data_messages, local_messages, full_updates_this_turn, partial_updates_this_turn, moved, priority_manager.getCurrentBucket());
//        _TRACEE("------end turn %d\n\n\n\n", turn);

        //update states
        total_messages += data_messages;
        total_local_messages += local_messages;

        total_full_updates += full_updates_this_turn;
        total_moved += moved;
        total_partial_updates += partial_updates_this_turn;

        full_updates_this_turn = 0;
        moved = 0;
        partial_updates_this_turn = 0;

        ++turn;
        _TRACEE("end turn %d\n\n\n\n\n", turn);
    }

    _TRACEE("Now we do cleanup\n");

    int all_total = 0;
    int all_local = 0;
    int all_full = 0;
    int all_partial = 0;
    int all_moved = 0;
    MPI_Allreduce(&total_messages, &all_total, 1, MPI_INT, MPI_SUM, comm);
    MPI_Allreduce(&total_local_messages, &all_local, 1, MPI_INT, MPI_SUM, comm);
    MPI_Allreduce(&total_full_updates, &all_full, 1, MPI_INT, MPI_SUM, comm);
    MPI_Allreduce(&total_moved, &all_moved, 1, MPI_INT, MPI_SUM, comm);
    MPI_Allreduce(&total_partial_updates, &all_partial, 1, MPI_INT, MPI_SUM, comm);

    _TRACEE("total messages: %d, local messages : %d, full = %d, partil = %d, turn = %d\n", all_total, all_local,
            all_full, all_partial, turn);
    _TRACEE("moved = my messages full partial moved = %d %d %d\n", total_full_updates, total_partial_updates,
            total_moved);
    _TRACEE("moved = %d\n", total_moved);

    //now we do clean up
    threads->stop();

    //send a message to myself so that receiver can exit
    LocalControlMessage lcm(0);
    sender.sendRawMessage(&lcm, rank);
    _TRACEE("exit message sent to %d\n", rank);

    receiver.stop();
    _TRACEE("receiver stopped\n");
    buffer.stop();
    _TRACEE("buffer stopped\n");
    sender.stop();
    _TRACEE("sender stopped\n");
    aggregator_manager.stop();
    _TRACEE("am stopped\n");
    handler.stop();
    _TRACEE("handler stopped\n");
    notifier.stop();
    _TRACEE("notifier stopped\n");
}


template<class TypeHinter>
void bsp::runtime::syn_priority::SynPriorityExecutor<TypeHinter>::updateOneTurn(int worker_id,
                                                                                WorkerThreadContext<TypeHinter> &context) {
    _TRACEE("--worker %d starts \n", worker_id);
    long worker_begin = get_time_us();

    int nr_full_updates = 0;
    int nr_moved = 0;
    int nr_partial_updates = 0;

    int has_to_reback = 0;

    //now we do for the full update
    while (true) {
        Vertex<TypeHinter> *cur = full_current;
        if (cur) {
            //compute where to jump
            Vertex<TypeHinter> *next = (cur->fast_next ? cur->fast_next : cur->next);
            if (!atomic_compare_and_swap<Vertex<TypeHinter> *>(&full_current, cur, next)) {
                ++has_to_reback;
                continue;
            }

            //then for every vertex in the list
            for (Vertex<TypeHinter> *iter = cur; iter != next; iter = iter->next) {
                //_TRACEE("iter = %d\n", iter->getInnerId());
                //TODO update for cur
                int inner_id = iter->getInnerId();
                if (p_notifier->hasMessageReceived(inner_id)) {//even if it has been halted, we wake up it
//            _TRACEE("worker thread found %ld has messages\n", vertex->getId());
                    if (iter->isHalt()) {
                        iter->setHalt(false);
                    }
                }

                if (!program.isHeadFirst()) {
                    BucketId bucket_id = program.getBucketId(iter, *this);
                    //_TRACEE("bucket id = %ld, %ld, %d\n", bucket_id, priority_manager.getCurrentBucket(), priority_manager.getCurrentMaxBucket());
                    assert(bucket_id >= priority_manager.getCurrentBucket() &&
                           bucket_id <= priority_manager.getCurrentMaxBucket());
                }


                if (iter->isHalt()) {
                    continue;
                }

                program.partialUpdateVertex(iter, *this, worker_id);
                program.broadcastMessages(iter, *this, worker_id);

                ++nr_full_updates;
            }
        }
        else {
            break;
        }
    }

    //now we do update for the parital vertex
    while (true) {
        Vertex<TypeHinter> *cur = partial_current;
        if (cur) {
            //compute where to jump
            Vertex<TypeHinter> *next = (cur->fast_next ? cur->fast_next : cur->next);
            if (!atomic_compare_and_swap<Vertex<TypeHinter> *>(&partial_current, cur, next)) {
                ++has_to_reback;
                continue;
            }

            //then for every vertex in the list
            for (Vertex<TypeHinter> *iter = cur; iter != next; iter = iter->next) {
                //TODO update for cur
                int inner_id = iter->getInnerId();
                if (p_notifier->hasMessageReceived(inner_id)) {//even if it has been halted, we wake up it
//            _TRACEE("worker thread found %ld has messages\n", vertex->getId());
                    if (iter->isHalt()) {
                        iter->setHalt(false);
                    }
                }

                if (iter->isHalt()) {
//            _TRACEE("worker thread skip %ld \n", vertex->getId());
                    continue;
                }

                program.partialUpdateVertex(iter, *this, worker_id);
                if (!program.isHeadFirst()) {
                    //here we check its new bucket id, if it's bucket can be decided to get into current bucket, then we also send messages
                    BucketId bucket_id = program.getBucketId(iter, *this);
                    if (bucket_id >= priority_manager.getCurrentBucket() &&
                        bucket_id <= priority_manager.getCurrentMaxBucket()) {
                        //then we also do notification
                        ++nr_full_updates;
                        ++nr_moved;
                        program.broadcastMessages(iter, *this, worker_id);
                    }
                    else {
                        ++nr_partial_updates;
                    }
                }
                else {
                    ++nr_partial_updates;
                }
            }
        }
        else {
            break;
        }
    }

    atomic_incre<int>(&full_updates_this_turn, nr_full_updates);
    atomic_incre<int>(&moved, nr_moved);
    atomic_incre<int>(&partial_updates_this_turn, nr_partial_updates);

    long worker_end = get_time_us();
    _TRACEE("-- worker_id = %d, used = %lf ms, full updated = %d, partial updated = %d, reback = %d\n", worker_id,
            ((worker_end - worker_begin) / 1e3), nr_full_updates, nr_partial_updates, has_to_reback);
}


template<class TypeHinter>
void bsp::runtime::syn_priority::SynPriorityExecutor<TypeHinter>::updatePriorityOneTurn(int worker_id,
                                                                                        WorkerThreadContext<TypeHinter> &context) {
    _TRACEE("--worker %d starts \n", worker_id);
    long worker_begin = get_time_us();

    BucketId current_buck_id = priority_manager.getCurrentBucket();
    BucketId current_max_buck_id = priority_manager.getCurrentMaxBucket();

    while (true) {
        Vertex<TypeHinter> *cur = full_current;
        if (cur) {
            //compute where to jump
            Vertex<TypeHinter> *next = (cur->fast_next ? cur->fast_next : cur->next);
            if (!atomic_compare_and_swap<Vertex<TypeHinter> *>(&full_current, cur, next)) {
                continue;
            }

            //then for every vertex in the list
            for (Vertex<TypeHinter> *iter = cur; iter != next; iter = iter->next) {
                BucketId buck_id = program.getBucketId(iter, *this);
                if (buck_id > current_max_buck_id) {
                    context.to_remove.push_back(iter);
                }
                else {
                    if (!iter->isHalt()) {
                        atomic_incre(&active_count, 1);
                    }
                }
            }
        }
        else {
            break;
        }
    }

    while (true) {
        Vertex<TypeHinter> *cur = partial_current;
        if (cur) {
            //compute where to jump
            Vertex<TypeHinter> *next = (cur->fast_next ? cur->fast_next : cur->next);
            if (!atomic_compare_and_swap<Vertex<TypeHinter> *>(&partial_current, cur, next)) {
                continue;
            }

            //then for every vertex in the list
            for (Vertex<TypeHinter> *iter = cur; iter != next; iter = iter->next) {
                BucketId buck_id = program.getBucketId(iter, *this);
                if (buck_id >= current_buck_id && buck_id <= current_max_buck_id) {
                    context.to_add.push_back(iter);
                    if (!iter->isHalt()) {
                        //iter->setHalt(false);
                        atomic_incre(&active_count, 1);
                    }
                }
            }
        }
        else {
            break;
        }
    }

    long worker_end = get_time_us();
    _TRACEE("--worker %d starts \n", worker_id);
}


#endif   /* ----- #ifndef SYSTEM_SYN_PRIORITY_SYN_PRIORITY_EXECUTOR_INC  ----- */
