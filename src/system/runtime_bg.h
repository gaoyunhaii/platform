/*
 * =====================================================================================
 *
 *       Filename:  runtime_bg.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/20/2015 02:14:00 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_RUNTIME_BG_INC
#define  SYSTEM_RUNTIME_BG_INC

#include "system/runtime.h"
#include "common/cache.h"
#include <vector>

using std::vector;
using bsp::runtime::Vertex;
using bsp::runtime::BorderVertex;
using bsp::runtime::InnerEdge;
using bsp::runtime::OuterEdge;
using bsp::runtime::InEdge;
using bsp::common::NeverFreeCache;
using bsp::runtime::VertexId;

template<class TypeHinter>
class VertexInit {
public:
    typedef typename TypeHinter::VertexValue VV;

    VertexInit(const VertexId &_vid, int _iid, const VV &_vv) :
            vid(_vid), inner_id(_iid), vv(_vv) { }

    Vertex<TypeHinter> *init(void *ptr) const {
        return new(ptr) Vertex<TypeHinter>(vid, inner_id, vv);
    }

private:
    const VertexId &vid;
    int inner_id;
    const VV &vv;
};


template<class TypeHinter>
class BorderVertexInit {
public:
    typedef typename TypeHinter::VertexValue VV;

    BorderVertexInit(const VertexId &_vid, int _iid) :
            vid(_vid), inner_id(_iid) { }

    BorderVertex<TypeHinter> *init(void *ptr) const {
        return new(ptr) BorderVertex<TypeHinter>(vid, inner_id);
    }

private:
    const VertexId &vid;
    int inner_id;
};


template<class TypeHinter>
class InnerEdgeInit {
public:
    typedef typename TypeHinter::VertexValue VV;
    typedef typename TypeHinter::EdgeValue EV;

    InnerEdgeInit(Vertex<TypeHinter> *_s, Vertex<TypeHinter> *_d, const EV &_v) :
            src(_s), dest(_d), value(_v) { }

    InnerEdge<TypeHinter> *init(void *ptr) const {
        return new(ptr) InnerEdge<TypeHinter>(src, dest, value);
    }

private:
    Vertex<TypeHinter> *src;
    Vertex<TypeHinter> *dest;
    const EV &value;
};

template<class TypeHinter>
class OuterEdgeInit {
public:
    typedef typename TypeHinter::EdgeValue EV;

    OuterEdgeInit(const VertexId &_vid, const EV &_value) :
            vid(_vid), value(_value) { }

    OuterEdge<TypeHinter> *init(void *ptr) const {
        return new(ptr) OuterEdge<TypeHinter>(vid, value);
    }

private:
    const VertexId &vid;
    const EV &value;
};

template<class TypeHinter>
class InEdgeInit {
public:
    typedef typename TypeHinter::EdgeValue EV;

    InEdgeInit(BorderVertex<TypeHinter>& _src, Vertex<TypeHinter> &_d, const EV &_value) :
            src(_src), dest(_d), value(_value) { }

    InEdge<TypeHinter> *init(void *ptr) const {
        return new(ptr) InEdge<TypeHinter>(src, dest, value);
    }

private:
    BorderVertex<TypeHinter>& src;
    Vertex<TypeHinter> &dest;
    const EV &value;
};


#endif   /* ----- #ifndef SYSTEM_RUNTIME_BG_INC  ----- */
