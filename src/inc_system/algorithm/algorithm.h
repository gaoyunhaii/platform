//
// Created by owner-pc on 11/14/16.
//

#ifndef BSP_INC_SYSTEM_ALGORITHM_ALGORITHM_HPP
#define BSP_INC_SYSTEM_ALGORITHM_ALGORITHM_HPP

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include "common/configuration.h"
#include "common/obj_factory.h"
#include "common/util.h"
#include "inc_system/graph.h"
#include "inc_system/communicate/message.h"
#include "inc_system/main/inc_executor.h"

using std::vector;
using std::unordered_map;
using bsp::common::ObjFactory;
using bsp::common::DefaultInit;
using bsp::common::Configuration;
using bsp::inc_system::ModificationRecvMessage;
using namespace bsp::inc_system;

//template <class VV, class EV>
//class AlgorithmDataPartition{
//public:
//    typedef unordered_map<int, VV> VertexValueMap;
//    typedef unordered_map<int, EV> EdgeValueMap;
//
//    PROPERTY_ACCESSOR(VertexValueMap, vertex_values)
//    PROPERTY_ACCESSOR(EdgeValueMap, edge_values)
//private:
//    VertexValueMap vertex_values;
//    EdgeValueMap edge_values;
//};
//
//template <class VV, class EV>
//class AlgorithmData{
//    typedef vector<AlgorithmDataPartition<VV, EV> > DataPartitionVector;
//
//    PROPERTY_ACCESSOR(DataPartitionVector, data_partitions)
//private:
//    DataPartitionVector data_partitions;
//};

namespace bsp {
namespace inc_system {

class Algorithm {
public:
    virtual void on_new_graph_modification(const ModificationRecvMessage &recv_message, IncExecutor &environment) = 0;
    virtual void on_vertex_update(const VertexId &vid, IncExecutor &environment) = 0;
};


class AlgorithmManager {
public:
    PROPERTY_ACCESSOR(vector<Algorithm *>, algorithms);

private:
    vector<Algorithm *> algorithms;
};

}
}


#endif //BSP_INC_SYSTEM_STORAGE_ALGORITHM_HPP
