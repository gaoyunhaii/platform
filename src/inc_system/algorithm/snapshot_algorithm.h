//
// Created by owner-pc on 17-1-4.
//

#ifndef BSP_INC_SYSTEM_ALGORITHM_SNAPSHOT_ALGORITHM_H
#define BSP_INC_SYSTEM_ALGORITHM_SNAPSHOT_ALGORITHM_H

#include <string>
#include "common/hdfs_client.h"
#include "inc_system/main/inc_executor.h"
#include <iostream>
#include <fstream>
#include <sstream>

using bsp::common::HDFSClient;
using std::string;
using std::ostringstream;
using bsp::inc_system::IncExecutor;

namespace bsp {
namespace inc_system {

class SnapshotAlgorithm : public bsp::inc_system::Algorithm {
public:
    SnapshotAlgorithm(HDFSClient &_client, const char *_sd, IncExecutor& _exe) :
            hdfs_client(_client), snapshot_dir(_sd), inc_executor(_exe),  outstream(NULL){
        //local tmp file
        int rank = inc_executor.inc_get_rank();
        ostringstream oss;
        oss << "/tmp/" << rank << ".file";
        local_path = oss.str();
        outstream = new ofstream(local_path.c_str());
    }

    virtual void on_new_graph_modification(const ModificationRecvMessage &recv_message, IncExecutor &environment) {
        _TRACEE("on recv message and snapshot for %d\n", recv_message.global_id);
        //modify the local file
        vector<GraphModification>& mods = *recv_message.modification;
        for(auto vi = mods.begin();vi != mods.end();++vi){
            if(vi->type == ADD_EDGE){
                (*outstream) << vi->from << "\t" << vi->to << "\t" << vi->weight << "\n";
            }
        }
        outstream->flush();

        ostringstream remote_path;
        remote_path << snapshot_dir << "/" << inc_executor.inc_get_rank() << "." << recv_message.global_id;
        string rp_str = remote_path.str();

        hdfs_client.localToRemote(local_path.c_str(), rp_str.c_str());
    }

    virtual void on_vertex_update(const VertexId &vid, IncExecutor &environment) {
        //do nothing
    }

private:
    HDFSClient &hdfs_client;
    string snapshot_dir;

    string local_path;
    ofstream* outstream;

    IncExecutor& inc_executor;
};

}
};


#endif //BSP_INC_SYSTEM_ALGORITHM_SNAPSHOT_ALGORITHM_H
