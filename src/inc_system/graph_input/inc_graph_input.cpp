//
// Created by owner-pc on 12/19/16.
//

#include "inc_system/graph_input/inc_graph_input.h"
#include "common/util.h"
#include "inc_system/util/mpi_util.h"
using bsp::common::str_tokenize;
using bsp::inc_system::graph_input::InputPartMap;


void bsp::inc_system::graph_input::IncGraphInput::open() {
    reader = new bsp::common::rw::LineReader(new HDFSFileInputStream(client, input_path.c_str()), false,  64 * 1024 * 1024);
}

void bsp::inc_system::graph_input::IncGraphInput::close(){
    delete reader;
    reader = NULL;
}

std::shared_ptr<InputPartMap>  bsp::inc_system::graph_input::IncGraphInput::nextBatchEdges(){
    InputPartMap* map = new InputPartMap();
    int nr_works = get_comm_size(comm);

    int rand_lines = ((double)random()) / RAND_MAX * 5 + 2;
    _TRACEE("rand lines = %d\n", rand_lines);

    string line;
    vector<string> parts;

    for(int i = 0;i < rand_lines;++i){
        ssize_t re = reader->readline(line);
        _TRACEE("reaed line: %s\n", line.c_str());
        if(re > 0){
            parts.clear();
            str_tokenize(line.c_str(), '\t', parts);

            VertexId from = atoll(parts[0].c_str());
            VertexId to = atoll(parts[1].c_str());
            double weight = atof(parts[2].c_str());

            int from_worker = part_manager.queryVertexWorker(from);
            int to_worker = part_manager.queryVertexWorker(to);

            map->insert(std::make_pair(part_manager.queryVertexWorker(from), GraphModification(ADD_EDGE, from, to, weight)));
            map->insert(std::make_pair(part_manager.queryVertexWorker(to), GraphModification(ADD_REVERSE_EDGE, from, to, weight)));
        }
        else{
            break;
        }
    }

    return std::shared_ptr<InputPartMap>(map);
}

void bsp::inc_system::graph_input::TransactionIniter::TransactionIniterRunnable::run() {
    _TRACEE("input start from %s \n", parent.input_path.c_str());
    IncGraphInput input(parent.comm, parent.input_path.c_str(), parent.client, parent.part_manager);
    input.open();
    while(true){
        std::shared_ptr<InputPartMap> lines = input.nextBatchEdges();
        if(lines->size() == 0){
            break;
        }

        //now start a transaction with this tuples
        TransactionStat* stat = NULL;
        int my_local_id = parent.local_id++;
        {
            bsp::atomic::RWLockGetWrite get_write(&parent.trans_map_lock);
            parent.trans_map[my_local_id] = TransactionStat();
            stat = &parent.trans_map[my_local_id];
        }

        //now set stat
        stat->state = INIT;
        stat->modification = lines;
        //compute the involved worker
        int last = -1;
        InputPartMap::iterator mi = lines->begin();
        for(;mi != lines->end();++mi){
            _TRACEE("input worker = %d(type: %d, %d %d %f)\n", mi->first, mi->second.type, mi->second.from, mi->second.to, mi->second.weight);
            if(mi->first != last){
                stat->involved_worker.push_back(mi->first);
                last = mi->first;
            }
        }

        NewTransactionMessage message;
        message.local_id = my_local_id;

        //_TRACEE("message.local_id = %d\n", message.local_id);
        parent.sender.sendRawMessage(&message, 0);
        sleep(30);
    }

    //now we have finished all the transactions
    //TODO: wait for the termination of the execution
    _TRACEE("end input, now go sleep\n");
    while(true){
        sleep(30);
    }

    input.close();
}

void bsp::inc_system::graph_input::TransactionIniter::onTransIdGot(const NewTransIdMessage& message){
    TransactionStat* stat = NULL;
    {
        bsp::atomic::RWLockGetRead get_read(&trans_map_lock);
        if(trans_map.find(message.local_id) == trans_map.end()){
            _TRACEE("Warning: not found trans %d", message.local_id);
            return;
        }

        stat = &trans_map[message.local_id];
    }

    stat->trans_id = message.global_id;
    _TRACEE("local_id %d has global id %d\n\n\n\n", message.local_id, message.global_id);

    //now send a message to execute it
    std::shared_ptr<InputPartMap> modification = stat->modification;
    InputPartMap::iterator mi = modification->begin();

    int comm_size = get_comm_size(comm);
    for(int i = 0;i < comm_size;++i){
        if(modification->count(i) > 0){
            ModificationSendMessage mod_message(modification.get(), i, message.global_id);
            sender.sendRawMessage(&mod_message, i);
        }
    }
}