//
// Created by owner-pc on 12/19/16.
//

#ifndef BSP_INC_SYSTEM_GRAPH_INPUT_INC_GRAPH_INPUT_HPP
#define BSP_INC_SYSTEM_GRAPH_INPUT_INC_GRAPH_INPUT_HPP

#include "common/atomic.h"
#include "common/hdfs_client.h"
#include "common/rw.h"
#include <string>
#include <vector>
#include <unordered_set>
#include <memory>
#include <tuple>
#include <map>
#include <mpi.h>
#include "inc_system/graph.h"
#include "inc_system/runtime_constants.h"
#include "common/dthread.h"
#include "inc_system/communicate/message.h"
#include "inc_system/communicate/sender.h"
#include "inc_system/storage/partition_manager.h"
using std::string;
using bsp::inc_system::GraphModification;
using bsp::inc_system::GraphModificationType;

namespace bsp{
namespace inc_system{
namespace graph_input{

typedef std::multimap<int, GraphModification> InputPartMap;

class IncGraphInput{
public:
    IncGraphInput(MPI_Comm& _comm, const char* _ip, HDFSClient& _client, PartitionManager& _pm) :
            comm(_comm), input_path(_ip), client(_client), part_manager(_pm),
            reader(NULL){

    }
    void open();
    void close();

    std::shared_ptr<InputPartMap>  nextBatchEdges();
private:
    MPI_Comm& comm;
    string input_path;
    HDFSClient& client;
    PartitionManager& part_manager;

    bsp::common::rw::LineReader* reader;
};


enum TRANS_STATE{
    INIT,
    STORAGE_HANDLING,
    STORAGE_HANDLED,
    COMPUTING,
    COMPUTED
};

struct TransactionStat{
    TransactionStat() : trans_id(-1), state(INIT){}

    int trans_id;
    TRANS_STATE state;
    std::vector<int> involved_worker;
    std::unordered_set<int> confirmed;
    std::shared_ptr<InputPartMap> modification;
};


class TransactionIniter{
public:
    class TransactionIniterRunnable : public bsp::multi_thread::Runnable{
    public:
        TransactionIniterRunnable(TransactionIniter& _p) : parent(_p){

        }
        void run();
    private:
        TransactionIniter& parent;
    };

public:
    friend class TransactionIniterRunnable;
    TransactionIniter(MPI_Comm& _comm, const char* _ip, HDFSClient& _client, Sender& _sender, PartitionManager& _pm) :
            comm(_comm),
            input_path(_ip), client(_client), sender(_sender), part_manager(_pm), runnable(NULL), thread(NULL),
                local_id(0){
        pthread_rwlock_init(&trans_map_lock, NULL);
    }

    void start(){
        runnable = new TransactionIniterRunnable(*this);
        thread = new bsp::multi_thread::DaemonThread(runnable);
        thread->start();
    }

    void stop(){
        thread->stop();
    }

    void onTransIdGot(const NewTransIdMessage& message);

private:
    MPI_Comm& comm;
    string input_path;
    HDFSClient& client;
    Sender& sender;
    PartitionManager& part_manager;

    TransactionIniterRunnable* runnable;
    bsp::multi_thread::DaemonThread* thread;
    int local_id;

    pthread_rwlock_t trans_map_lock;
    std::unordered_map<int, TransactionStat> trans_map;
};


}
}
}

#endif //BSP_INC_GRAPH_INPUT_HPP
