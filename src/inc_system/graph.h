//
// Created by owner-pc on 10/22/16.
//

#ifndef BSP_GRAPH_H
#define BSP_GRAPH_H
#include <limits>
#include "inc_system/runtime_constants.h"
#include "common/util.h"

namespace bsp {
namespace inc_system {

class Edge;
class Vertex {
public:
    Vertex(const VertexId& _vid, int _inner_id = -1) :
            vid(_vid), inner_id(_inner_id),
            is_deleted(false)
    {

    }
    PROPERTY_ACCESSOR(vector<Edge*>, out_edges);
    PROPERTY_ACCESSOR(vector<Edge*>, in_edges);
private:
	const VertexId& vid;
    int inner_id;

    //if it is deleted
    bool is_deleted;

    //then the vectors to hold different edges
    vector<Edge*> out_edges;
    vector<Edge*> in_edges;
};

class Edge {
public:
    Edge(const VertexId& _source, const VertexId& _dest) : source(_source), dest(_dest) {

    }
private:
    VertexId source;
    VertexId dest;

    bool is_deleted;
};

class VertexInit {
public:
    VertexInit(const VertexId &_vid, int _iid) :
            vid(_vid), inner_id(_iid) { }

    Vertex *init(void *ptr) const {
        return new(ptr) Vertex(vid, inner_id);
    }

private:
    const VertexId &vid;
    int inner_id;
};

class EdgeInit{
public:
	EdgeInit(const VertexId& _source, const VertexId& _dest) :
			source(_source), dest(_dest){}
	Edge *init(void* ptr) const {
		return new(ptr) Edge(source, dest);
	}
private:
    VertexId source;
    VertexId dest;
};


}
}


#endif //BSP_GRAPH_H
