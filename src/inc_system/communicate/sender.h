/*
 * =====================================================================================
 *
 *       Filename:  sender.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  10/22/2015 09:12:46 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYNCHRONIZE_SENDER_INC
#define  SYSTEM_SYNCHRONIZE_SENDER_INC

#include <mpi.h>
#include <cstdlib>
#include <boost/lockfree/spsc_queue.hpp>
#include "common/serialize_util.h"
#include "common/dthread.h"
#include "common/thread_local.h"
#include "common/atomic.h"
#include "inc_system/util/mpi_util.h"
#include "inc_system/communicate/message.h"
#include "common/util.h"

using bsp::common::serialize::BoundedBuffer;
using boost::lockfree::spsc_queue;
using bsp::multi_thread::DaemonThread;
using bsp::atomic::TurnedSynchronizer;
using bsp::multi_thread::ThreadLocal;
using bsp::common::get_time_us;
using bsp::common::set_pr_name;

namespace bsp {
namespace inc_system {

struct MessageWrapper {
    Message *message;
    int to;
};

struct SenderBuffer {
    SenderBuffer() : cm(NULL), size(0) {
        cm = new CompositeMessage();
    }

    SenderBuffer(const SenderBuffer &sb) {
        size = 0;
        cm = new CompositeMessage();
        _TRACEE("sb copied\n");
    }

    ~SenderBuffer() {
        if (cm) {
            delete cm;
        }
    }

    void reset() {
        if (cm) {
            delete cm;
        }
        cm = new CompositeMessage();
        size = 0;
    }

    void addMessage(Message *message) {
        cm->addMessage(message);
        //add size
        size += message->getSize();
    }

    CompositeMessage *cm;
    size_t size;
};


class ThreadContext {
public:
    ThreadContext() : messages(5000000) { }

    void addMessage(const MessageWrapper &mess) {
        while (!messages.push(mess));
    }

public:
    spsc_queue<MessageWrapper> messages;
};


//class Sender;


class Sender {
public:
    friend class SenderRunnable;
public:
    class SenderRunnable : public bsp::multi_thread::Runnable {
    public:
        SenderRunnable(int _order, Sender &_s) :
                order(_order), sender(_s), stopped(false) {
            int comm_size;
            MPI_Comm_size(sender.comms[0], &comm_size);
            _TRACEE("comm_size = %d\n", comm_size);

            next_messages.resize(comm_size);
        }

        void run() {
            set_pr_name("sender", order);
            size_t sender_size = sender.send_threads.size();
            while (!stopped) {
                //then we check till need notify
                int this_turn_popped = 0;

                size_t nrContexts = sender.all_channels.size();
                for (int i = 0; i < nrContexts; ++i) {
                    if (i % sender_size == order) {//then i will read it
                        ThreadContext *context = sender.all_channels[i];

                        MessageWrapper wrapper;
                        while (context->messages.pop(wrapper)) {
//                      _TRACEE("send run: one message to %d\n", wrapper.to);
                            ++this_turn_popped;
                            _add_to_cm_and_send(wrapper.to, wrapper.message);
                        }
                    }
                }

                if (this_turn_popped == 0) {
                    usleep(10000);
                }
            }
        }

        void notifyStop() {
            bsp::atomic::atomic_set<bool>(&stopped, true);
        }

    private:
        void _add_to_cm_and_send(int to, Message *message) {
            SenderBuffer &mb = next_messages[to];
            mb.addMessage(message);

            //if available, we send it
            if (mb.size > 8192) {//send it out
                //send
                sender.sendRawMessage(mb.cm, to);
                mb.reset();
            }
        }

    private:
        int order; // cal by %
        vector<SenderBuffer> next_messages;
        Sender &sender;

        bool stopped;
    };

public:
    Sender(MPI_Comm *_cs, int _worker_size, int _sender_size = 1) : comms(_cs) {
        int comm_size = get_comm_size(comms[0]);

        for (int i = 0; i < _worker_size; ++i) {
            ThreadContext *tc = new ThreadContext();
            all_channels.push_back(tc);
        }

        for (int i = 0; i < _sender_size; ++i) {
            SenderRunnable *sr = new SenderRunnable(i, *this);
            runnables.push_back(sr);

            DaemonThread *thread = new DaemonThread(sr, false, false);
            send_threads.push_back(thread);
        }
    }

    ~Sender() {
        vector<ThreadContext *>::iterator vi = all_channels.begin();
        for (; vi != all_channels.end(); ++vi) {
            delete (*vi);
        }

        vector<DaemonThread *>::iterator dvi = send_threads.begin();
        for (; dvi != send_threads.end(); ++dvi) {
            delete (*dvi);
        }

        typename vector<SenderRunnable *>::iterator svi = runnables.begin();
        for (; svi != runnables.end(); ++svi) {
            delete (*svi);
        }
    }

    void start() {
        vector<DaemonThread *>::iterator dvi = send_threads.begin();
        for (; dvi != send_threads.end(); ++dvi) {
            (*dvi)->start();
        }
    }

    void stop() {
        typename vector<SenderRunnable *>::iterator svi = runnables.begin();
        for (; svi != runnables.end(); ++svi) {
            (*svi)->notifyStop();
        }

        vector<DaemonThread *>::iterator vi = send_threads.begin();
        for (; vi != send_threads.end(); ++vi) {
            (*vi)->join();
        }
    }

    //sync methods
    void sendRawMessage(const Message *message, int to) {
        size_t size = message->getSize();
        int type = message->getType();

        void *buffer = malloc(size + sizeof(int) + sizeof(size_t));
        BoundedBuffer bb(buffer);
        bb << type << size;

        message->serialize(bb, size);
        _TRACEE("send %ld bytes to %d\n", size + sizeof(int) + sizeof(size_t), to);
        MPI_Send(buffer, size + sizeof(int) + sizeof(size_t), MPI_CHAR, to, 0, comms[to]);
        free(buffer);
    }

    //async methods
    void asyncSendMessage(Message *message, int to, int channel_index) {
        ThreadContext *channel = all_channels[channel_index];
        MessageWrapper wrapper;
        wrapper.message = message;
        wrapper.to = to;

        while (!channel->messages.push(wrapper));
    }

    int getRank() {
        return get_rank(comms[0]);
    }

private:
    MPI_Comm *comms;

    //now we set up the message buffer
    vector<ThreadContext *> all_channels;

    //all threads
    vector<SenderRunnable *> runnables;
    vector<DaemonThread *> send_threads;
};

}
}


#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_SENDER_INC  ----- */
