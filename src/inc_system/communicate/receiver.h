/*
 * =====================================================================================
 *
 *       Filename:  receiver.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年10月13日 11时15分18秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYNCHRONIZE_RECEIVER_INC
#define  SYSTEM_SYNCHRONIZE_RECEIVER_INC

#include "common/dthread.h"
#include <mpi.h>
#include <pthread.h>
#include <cassert>
#include "common/util.h"
#include "common/atomic.h"
#include "common/dthread.h"
#include "inc_system/communicate/message.h"
#include "inc_system/communicate/message_handler.h"
#include "inc_system/util/mpi_util.h"
#include <cassert>

using bsp::multi_thread::Runnable;
using bsp::multi_thread::DaemonThread;
using bsp::atomic::atomic_incre_re_after;
using bsp::atomic::MutexGetLock;
using bsp::multi_thread::DaemonThread;
using bsp::atomic::atomic_incre_re_after;
using bsp::common::get_time_us;

namespace bsp {
namespace inc_system {

//class Receiver;
class Receiver {
public:
    class ReceiverRunnable : public Runnable {
    public:
        ReceiverRunnable(Receiver &_r) : receiver(_r) {
            int comm_size = get_comm_size(receiver.comm);
            rank = get_rank(receiver.comm);
        }

        ~ReceiverRunnable() {

        }

        void run();

    private:
        Receiver &receiver;
        int rank;
    };
public:
    friend class ReceiverRunnable;

    Receiver(MPI_Comm &_c, MessageHandler &_h) :
            comm(_c), handler(_h) {
        runnable = new ReceiverRunnable(*this);
    }

    ~Receiver() {
        delete this->thread;
        delete this->runnable;
    }

    void start() {
        this->thread = new DaemonThread(runnable, false, false, NULL);
        this->thread->start();
    }

    void stop() {
        //this->thread->stop();
        this->thread->join();
    }

private:
    MPI_Comm &comm;
    MessageHandler &handler;

    //thread
    ReceiverRunnable *runnable;
    DaemonThread *thread;
};

}
}


#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_RECEIVER_INC  ----- */
