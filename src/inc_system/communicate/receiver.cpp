/*
 * =====================================================================================
 *
 *       Filename:  receiver.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年10月13日 11时20分16秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYNCHRONIZE_RECEIVER_HPP_INC
#define  SYSTEM_SYNCHRONIZE_RECEIVER_HPP_INC

#include "common/serialize_util.h"
#include "inc_system/communicate/receiver.h"
#include <mpi.h>
#include <cstdlib>
#include "inc_system/communicate/message.h"
#include "inc_system/runtime_constants.h"
#include "common/util.h"
#include "common/utils.hpp"

using bsp::common::serialize::BoundedBuffer;
using bsp::inc_system::MESSAGE_TYPE;
using bsp::common::get_time_us;

void bsp::inc_system::Receiver::ReceiverRunnable::run() {
    int rank;
    MPI_Comm_rank(receiver.comm, &rank);
    bsp::common::set_pr_name("receiver", 0);

    while (true) {
        MPI_Status status;
        _TRACEE("probe start");
        MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, receiver.comm, &status);
        _TRACEE("probe success: message from %d\n", status.MPI_SOURCE);

        int from = status.MPI_SOURCE;
        int length = 0;
        MPI_Get_count(&status, MPI_BYTE, &length);

        if (length <= 0) {
            _TRACEE("bad messages received, length = %d\n", length);
            continue;
        }

        void *raw_buffer = malloc(length);
        int re = MPI_Recv(raw_buffer, length, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, receiver.comm, &status);

        if (from == rank) {
            BoundedBuffer bb(raw_buffer);
            int type;
            size_t size;
            bb >> type >> size;
            //_TRACEE("receiver type = %d, size = %ld\n", type, size);
            if (type == LOCAL_CONTROL_MESS) {
                LocalControlMessage lcm;
                lcm.deserialize(bb, size);
                if (lcm.type == 0) {
                    free(raw_buffer);
                    break;
                }
                else {
                    free(raw_buffer);
                }
            }
            else if (type == CONTROL_MESSAGE) {
                _TRACEE("receive control message received from %d\n", from);
                receiver.handler.onNewMessage(from, raw_buffer, length);
            }
            else {
                //free(raw_buffer);
                receiver.handler.onNewMessage(from, raw_buffer, length);
            }
        }
        else {
            receiver.handler.onNewMessage(from, raw_buffer, length);
        }
    }
    _TRACEE("receiver exit\n");
    _TRACEE("receiver finished\n");
}


#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_RECEIVER_HPP_INC  ----- */
