/*
 * =====================================================================================
 *
 *       Filename:  message_handler.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年12月29日 17时54分17秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYNCHRONIZE_MESSAGE_HANDLER_HPP_INC
#define  SYSTEM_SYNCHRONIZE_MESSAGE_HANDLER_HPP_INC

#include "inc_system/communicate/message_handler.h"
#include <cstdlib>
#include "common/utils.hpp"

using bsp::common::set_pr_name;
using bsp::atomic::atomic_set;

void bsp::inc_system::MessageHandler::MessageHandlerRunnable::run() {
    set_pr_name("handler", getIndex());
    vector<spsc_queue<RecvMessageWrapper> *> our_queues;
    int max = parent_handler->queues.size();
    int handler_size = parent_handler->handlers.getSize();

    for (int i = getIndex(); i < max; i += handler_size) {
        our_queues.push_back(parent_handler->queues[i]);
    }

    while (!isStopped()) {
        RecvMessageWrapper wrapper;

        int this_turn_handled = 0;
        vector<spsc_queue<RecvMessageWrapper> *>::iterator
                vi = our_queues.begin();
        for (; vi != our_queues.end(); ++vi) {
            while (((*vi)->pop(wrapper))) {
                ++this_turn_handled;

                //handle it
                BoundedBuffer bb(wrapper.buffer);
                int type;
                size_t size;
                bb >> type;
                bb >> size;

                //now find the function to handle this message
                if(parent_handler->funcs.find(type) == parent_handler->funcs.end()){
                    //throw this message
                    _TRACEE("message deserted with type %d\n", type);
                    return;
                }

                bool to_exit = parent_handler->funcs[type](wrapper, bb);
                //free(wrapper.buffer);

                if(to_exit){
                    this->markStop();
                }
            }
        }

        if (this_turn_handled == 0) {
            usleep(10000); //sleep 10 ms
        }
    }
}


#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_MESSAGE_HANDLER_HPP_INC  ----- */

