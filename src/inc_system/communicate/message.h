/*
 * =====================================================================================
 *
 *       Filename:  message.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年10月19日 14时20分05秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  INC_SYSTEM_SYNCHRONIZE_MESSAGE_INC
#define  INC_SYSTEM_SYNCHRONIZE_MESSAGE_INC

#include "inc_system/runtime_constants.h"
#include "common/serialize_util.h"
#include "common/sexception.h"
#include <vector>
#include <tuple>
#include <map>
#include <exception>

using bsp::common::serialize::BoundedBuffer;
using bsp::common::BaseException;
using std::multimap;

namespace bsp {
namespace inc_system {

//definitions for the message type
enum MESSAGE_TYPE {
    CONTROL_MESSAGE, NORMAL_MESSAGE, COMPOSITE_MESSAGE, LOCAL_CONTROL_MESS,

    //added message
            NEW_TRANSACTION, NEW_TRANS_ID,
    GRAPH_MODIFICATION, MODIFICATION_END, START_NEW_VERSION, VERSION_COMPLETE
};

class Message {
public:
    virtual int getType() const = 0;

    virtual size_t getSize() const = 0;

    virtual void serialize(BoundedBuffer &bb, size_t size) const = 0;

    virtual void deserialize(BoundedBuffer &bb, size_t size) = 0;
};

class NewTransactionMessage : public Message {
public:
    NewTransactionMessage() : local_id(-1) { }

    virtual int getType() const {
        return NEW_TRANSACTION;
    }

    virtual size_t getSize() const {
        return sizeof(int);
    }

    virtual void serialize(BoundedBuffer &bb, size_t size) const {
        bb << local_id;
    }

    virtual void deserialize(BoundedBuffer &bb, size_t size) {
        bb >> local_id;
    }

public:
    int local_id;
};

class NewTransIdMessage : public Message {
public:
    NewTransIdMessage() : local_id(0), global_id(0) {

    }

    virtual int getType() const {
        return NEW_TRANS_ID;
    }

    virtual size_t getSize() const {
        return sizeof(int) + sizeof(int);
    }

    virtual void serialize(BoundedBuffer &bb, size_t size) const {
        bb << local_id;
        bb << global_id;
    }

    virtual void deserialize(BoundedBuffer &bb, size_t size) {
        bb >> local_id;
        bb >> global_id;
    }

public:
    int local_id;
    int global_id;
};


enum GraphModificationType {
    ADD_EDGE, REMOVE_EDGE, ADD_VERTEX, REMOVE_VERTEX, ADD_REVERSE_EDGE, REMOVE_REVERSE_EDGE
};

struct GraphModification {
public:
    GraphModification() : type(ADD_EDGE), from(0), to(0), weight(0) { }

    GraphModification(GraphModificationType _type, const VertexId &_f, const VertexId _t, double _w) :
            type(_type), from(_f), to(_t), weight(_w) { }

    GraphModificationType type;
    VertexId from;
    VertexId to;
    double weight;
};

class ModificationRecvMessage : public Message {
public:
    ModificationRecvMessage(std::vector<GraphModification> *mod = NULL) : modification(mod) {

    }

    virtual int getType() const {
        return GRAPH_MODIFICATION;
    }

    virtual size_t getSize() const {
//        if(modification){
//            size_t size = sizeof(int);
//            std::vector<GraphModification>::const_iterator vi = modification->begin();
//            for(;vi != modification->end();++vi){
//                size = size + sizeof(int) + 2 * sizeof(VertexId) + sizeof(double);
//            }
//
//            return size;
//        }
//        return 0;
        throw std::exception();
    }

    virtual void serialize(BoundedBuffer &bb, size_t size) const {
//        if(modification){
//            bb << (int)(modification->size());
//            std::vector<GraphModification>::const_iterator vi = modification->begin();
//            for(;vi != modification->end();++vi){
//                const GraphModification& mod = *vi;
//                bb << (int)mod.type << mod.from << mod.to << mod.weight;
//            }
//        }
        throw std::exception();
    }

    virtual void deserialize(BoundedBuffer &bb, size_t size) {
        if (size != 0) {
            bb >> global_id;

            int size;
            bb >> size;
            modification = new std::vector<GraphModification>(size);
            for (int i = 0; i < size; ++i) {
                GraphModification &mod = modification->operator[](i);
                int raw_type;
                bb >> raw_type;
                mod.type = GraphModificationType(raw_type);
                bb >> mod.from >> mod.to >> mod.weight;
            }
        }
        else {
            if (modification) {
                delete modification;
            }
        }
    }

    std::vector<GraphModification> *modification;
    int global_id;
};


class ModificationSendMessage : public Message {
public:
    ModificationSendMessage() : part_map(NULL), to(0), global_id(-1) { }

    ModificationSendMessage(std::multimap<int, GraphModification> *_m, int _t, int _gid) :
            part_map(_m), to(_t), global_id(_gid) { }


    virtual int getType() const {
        return GRAPH_MODIFICATION;
    }

    virtual size_t getSize() const {
        size_t single_size = sizeof(int) + 2 * sizeof(VertexId) + sizeof(double);
        int count = part_map->count(to);

        return sizeof(int) + single_size * count;
    }

    virtual void serialize(BoundedBuffer &bb, size_t size) const {
        bb << global_id << (int) (part_map->count(to));

        std::multimap<int, GraphModification>::const_iterator mi = part_map->find(to);
        std::multimap<int, GraphModification>::const_iterator end = part_map->upper_bound(to);

        for (; mi != end; ++mi) {
            const GraphModification &gm = mi->second;
            bb << (int) gm.type << gm.from << gm.to << gm.weight;
        }
    }

    virtual void deserialize(BoundedBuffer &bb, size_t size) {
        throw exception();
    }

public:
    std::multimap<int, GraphModification> *part_map;
    int to;
    int global_id;
};


class ModifictaionEndMessage : public Message {
public:
    ModifictaionEndMessage() : global_id(-1) { }
    ModifictaionEndMessage(int _gid) : global_id(_gid) { }

    virtual int getType() const {
        return MODIFICATION_END;
    }

    virtual size_t getSize() const {
        return sizeof(int);
    }

    virtual void serialize(BoundedBuffer &bb, size_t size) const {
        bb << global_id;
    }

    virtual void deserialize(BoundedBuffer &bb, size_t size) {
        bb >> global_id;
    }

public:
    int global_id;
};


//transaction control
class RawMessage : public Message {
public:
    RawMessage() : type(-1), buffer(NULL), buffer_size(0) { }

    RawMessage(int _type, void *_buffer, size_t _bsize) : type(_type), buffer(_buffer), buffer_size(_bsize) { }

    ~RawMessage() {
        if (buffer != NULL) {
            free(buffer);
        }

        buffer = NULL;
    }

    virtual int getType() const {
        return type;
    }

    virtual size_t getSize() const {
        return buffer_size;
    }

    virtual void serialize(BoundedBuffer &bb, size_t size) const {
        bb.writeRaw(buffer, buffer_size);
    }

    virtual void deserialize(BoundedBuffer &bb, size_t size) {
        buffer = (void *) malloc(buffer_size);
        bb.readRaw(buffer, buffer_size);
    }

public:
    int type;
    void *buffer;
    size_t buffer_size;
};

class LocalControlMessage : public Message {
public:
    LocalControlMessage() : type(-1) { }

    LocalControlMessage(int _t) : type(_t) { }

    size_t getSize() const {
        return sizeof(int);
    }

    int getType() const {
        return LOCAL_CONTROL_MESS;
    }

    void serialize(BoundedBuffer &bb, size_t size) const {
        bb << type;
    }

    void deserialize(BoundedBuffer &bb, size_t size) {
        bb >> type;
    }

public:
    int type;
};

class CompositeMessage : public Message {
public:
    CompositeMessage() { }

    CompositeMessage(const VertexId &_id) { }

    ~CompositeMessage() {
        vector<Message *>::const_iterator vi = messages.begin();
        for (; vi != messages.end(); ++vi) {
            delete (*vi);
        }
    }

    int getType() const {
        return COMPOSITE_MESSAGE;
    }

    size_t getSize() const {
        size_t total = sizeof(size_t); //count
        vector<Message *>::const_iterator vi = messages.begin();
        for (; vi != messages.end(); ++vi) {
            //total = total + sizeof(int) + sizeof(size_t) + (*vi)->getSize(); //type size value
            total = total + sizeof(uint16_t) + (*vi)->getSize(); //type size value
        }

        return total;
    }

    void addMessage(Message *message) {
        messages.push_back(message);
    }

    size_t getMessageCount() {
        return messages.size();
    }

    void serialize(BoundedBuffer &bb, size_t size) const {
        bb << messages.size();
        vector<Message *>::const_iterator vi = messages.begin();
        for (; vi != messages.end(); ++vi) {
            //encode the uint16_t
            size_t size = (*vi)->getSize();
            int type = (*vi)->getType();

            uint16_t compressed = 0;
            compressed = compressed | ((type & 0x07) << 13);
            compressed = compressed | (size & 0x01fff);
            //_TRACEE("size = %ld, type = %d, sender compressed = %d\n", size, type, compressed);

            //bb << (*vi)->getType() << size;
            bb << compressed;
            (*vi)->serialize(bb, size);
        }
    }

    void deserialize(BoundedBuffer &bb, size_t size) {
        throw new BaseException("Please deserize composite message manually!");
    }

public:
    vector<Message *> messages;
};

}
}

#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_MESSAGE_INC  ----- */
