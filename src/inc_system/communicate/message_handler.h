/*
 * =====================================================================================
 *
 *       Filename:  message_handler.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年12月29日 17时54分12秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_SYNCHRONIZE_MESSAGE_HANDLER_INC
#define  SYSTEM_SYNCHRONIZE_MESSAGE_HANDLER_INC

#include <boost/lockfree/spsc_queue.hpp>
#include "common/util.h"
#include "common/atomic.h"
#include "common/thread_group.h"
#include "common/thread_group.hpp"
#include "inc_system/communicate/message.h"
#include "inc_system/util/mpi_util.h"

using boost::lockfree::spsc_queue;
using bsp::multi_thread::ThreadGroup;
using bsp::inc_system::CompositeMessage;
using bsp::inc_system::get_comm_size;
using bsp::atomic::atomic_incre;
using bsp::atomic::atomic_incre_re_after;
using bsp::multi_thread::IndexedRunnable;
using bsp::atomic::TurnedSynchronizer;
using bsp::common::get_time_us;
using bsp::common::serialize::BoundedBuffer;

namespace bsp {
namespace inc_system {
struct RecvMessageWrapper {
    int from;
    void *buffer;
    size_t size;
};

typedef std::function<bool (RecvMessageWrapper wrapper, BoundedBuffer& bb)> MessageHandlerFunc;

class MessageHandler {
public:
    friend class MessageHandlerRunnable;
public:
    class MessageHandlerRunnable : public IndexedRunnable {
    public:
        friend class MessageHandler;

    public:
        MessageHandlerRunnable(int index) :
                IndexedRunnable(index),
                parent_handler(NULL) {
        }

        void setMessageHandler(MessageHandler *parent) {
            this->parent_handler = parent;
        }

        void run();
    private:
        MessageHandler *parent_handler;
        int rank;
    };
public:
    MessageHandler(int queue_size, int worker_size) :
            handlers(worker_size), turn_stopped(false), stopped(false)
    {

        int size = handlers.getSize();
        for (int i = 0; i < size; ++i) {
            MessageHandlerRunnable* mhr = handlers.getRunnable(i);
            mhr->setMessageHandler(this);
        }

        for (int i = 0; i < queue_size; ++i) {
            queues.push_back(new spsc_queue<RecvMessageWrapper>(1000000));
        }
    }

    ~MessageHandler() {
        vector<spsc_queue<RecvMessageWrapper> *>::iterator vi = queues.begin();
        for (; vi != queues.end(); ++vi) {
            delete (*vi);
        }
    }

    void start() {
        handlers.start();
    }

    void stop() {
        int size = handlers.getSize();
        for (int i = 0; i < size; ++i) {
            MessageHandlerRunnable *mhr = handlers.getRunnable(i);
            mhr->markStop();
        }

        handlers.joinAll();
    }

    virtual void onNewMessage(int from, void *message, size_t size) {
        int max = queues.size();
        int chosen = from % max;
        spsc_queue<RecvMessageWrapper> &queue = getQueue(chosen);

        RecvMessageWrapper wrapper;
        wrapper.buffer = message;
        wrapper.from = from;
        wrapper.size = size;

        while (!queue.push(wrapper));
    }

    void setHandlerFunc(int type, const MessageHandlerFunc& func){
        funcs[type] = func;
    }

private:
    spsc_queue<RecvMessageWrapper> &getQueue(int index) {
        return *(queues[index]);
    }

private:
    //threads
    ThreadGroup<MessageHandlerRunnable> handlers;

    //for flow control
    bool turn_stopped;
    bool stopped;

    vector<spsc_queue<RecvMessageWrapper> *> queues;
    unordered_map<int, MessageHandlerFunc> funcs;
};

}
}


#endif   /* ----- #ifndef SYSTEM_SYNCHRONIZE_MESSAGE_HANDLER_INC  ----- */
