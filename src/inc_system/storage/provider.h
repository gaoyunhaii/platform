/*
 * =====================================================================================
 *
 *       Filename:  provider.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年10月31日 15时03分20秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef  BSP_INC_SYSTEM_STORAGE_PROVIDER_INC
#define  BSP_INC_SYSTEM_STORAGE_PROVIDER_INC

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include "common/configuration.h"
#include "common/obj_factory.h"
#include "common/util.h"
#include "inc_system/graph.h"
#include "inc_system/storage/partition_manager.h"
#include "inc_system/communicate/message.h"

using std::vector;
using std::unordered_map;
using bsp::common::ObjFactory;
using bsp::common::DefaultInit;
using bsp::common::Configuration;
using bsp::inc_system::PartitionManager;
using bsp::inc_system::GraphModification;
using bsp::inc_system::GraphModificationType;

namespace bsp {
namespace inc_system {

class GraphPartition {
public:
    typedef unordered_map<VertexId, int> InnerIndicesMap;

    GraphPartition(int _id) : id(_id) {
        pthread_rwlock_init(&lock, NULL);
    }

    void addVertex(const VertexId &vid);
    void addEdge(const VertexId &source, const VertexId &dest);
    void addReverseEdge(const VertexId& source, const VertexId& dest);

    pthread_rwlock_t* get_lock(){
        return &lock;
    }

private:
    int id;
    vector<Vertex *> vertices;
    ObjFactory<Vertex, VertexInit> vertex_factory;

    vector<Edge *> edges;
    ObjFactory<Edge, EdgeInit> edge_factory;

    //indices for fast location of the vertice
    InnerIndicesMap vertex_indices;

    //just add a lock
    pthread_rwlock_t lock;
};

class Provider {
public:
    Provider(PartitionManager &_pm, int _cs, int _r) : part_manager(_pm), comm_size(_cs), rank(_r) {

    }

    void init();

    void onInsertVertex(const GraphModification& mod);

    void onInsertEdge(const GraphModification& mod);

    void onInsertReverseEdge(const GraphModification& mod);

private:
    PartitionManager part_manager;
    int comm_size;
    int rank;

    unordered_map<int, GraphPartition *> partitions;
};


}
}


#endif   /* ----- #ifndef BSP_INC_SYSTEM_STORAGE_PROVIDER_INC  ----- */
