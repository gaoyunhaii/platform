//
// Created by owner-pc on 12/29/16.
//

#ifndef BSP_INC_SYSTEM_STORAGE_PARTITION_MANAGER_H
#define BSP_INC_SYSTEM_STORAGE_PARTITION_MANAGER_H

#include <unordered_map>
#include "inc_system/runtime_constants.h"
using std::unordered_map;

namespace bsp{
namespace inc_system{

class PartitionManager{
public:
    PartitionManager(int _tp, int _nw) : total_partition(_tp), nr_worker(_nw){
        //now just round robin
        part_to_worker.reserve(total_partition);
        for(int i = 0;i < total_partition;++i){
            part_to_worker[i] = i % nr_worker;
        }
    }

    int queryPartitionWorker(int p){
        unordered_map<int, int>::iterator mi = part_to_worker.find(p);
        if(mi == part_to_worker.end()){
            return -1;
        }

        return mi->second;
    }

    int queryVertexWorker(const VertexId& vertex_id){
        int part_no = vertex_id % total_partition;
//        _TRACEE("part no = %d\n", part_no);
        int worker_no = queryPartitionWorker(part_no);
        return worker_no;
    }

    const unordered_map<int, int>& get_assign_map() const{
        return part_to_worker;
    };

    PROPERTY_ACCESSOR(int, total_partition);

private:
    int total_partition;
    int nr_worker;
    unordered_map<int, int> part_to_worker;
};
}
}


#endif //BSP_INC_SYSTEM_STORAGE_PARTITION_MANAGER_H
