//
// Created by owner-pc on 11/14/16.
//
#include <cassert>
#include <inc_system/communicate/message.h>
#include "inc_system/storage/provider.h"
#include "common/atomic.h"
#include "common/util.h"
#include "common/utils.hpp"

using bsp::inc_system::GraphPartition;
using namespace bsp::atomic;
using namespace bsp::common;

void bsp::inc_system::GraphPartition::addVertex(const VertexId &vid) {
    int next_inner_id = this->vertices.size();

    VertexInit vi(vid, next_inner_id);
    Vertex *vertex = vertex_factory.newObject(vi);
    vertices.push_back(vertex);

    vertex_indices[vid] = next_inner_id;
}

void bsp::inc_system::GraphPartition::addEdge(const VertexId &source, const VertexId &dest) {
    EdgeInit ei(source, dest);

    Edge *edge = edge_factory.newObject(ei);
    edges.push_back(edge);

    //now change vertices info
    InnerIndicesMap::iterator iii = vertex_indices.find(source);
    if (iii != vertex_indices.end()) {
        Vertex *source_vertex = vertices[iii->second];
        source_vertex->mutable_out_edges().push_back(edge);
    }
}

void bsp::inc_system::GraphPartition::addReverseEdge(const VertexId &source, const VertexId &dest) {
    EdgeInit ei(source, dest);

    Edge *edge = edge_factory.newObject(ei);
    edges.push_back(edge);

    InnerIndicesMap::iterator iii = vertex_indices.find(dest);
    if (iii != vertex_indices.end()) {
        Vertex *dest_vertex = vertices[iii->second];
        dest_vertex->mutable_in_edges().push_back(edge);
    }
}

void bsp::inc_system::Provider::init() {
    //now we init all paritions and open them for read & write
    unordered_map<int, int> map = part_manager.get_assign_map();
    unordered_map<int, int>::iterator mi = map.begin();
    for (; mi != map.end(); ++mi) {
        if (mi->second == rank) {
            GraphPartition *partition = new GraphPartition(mi->first);
            partitions[mi->first] = partition;
        }
    }
}

void bsp::inc_system::Provider::onInsertVertex(const GraphModification &mod) {
    if (mod.type != GraphModificationType::ADD_VERTEX) {
        _TRACEE("call insert vertex with non-add-vertex type\n");
        return;
    }

    int part_no = part_manager.queryPartitionWorker(mod.from);
    GraphPartition *part = find_in_map<int, GraphPartition *>(partitions, part_no, NULL);
    assert(part != NULL && "part == null when insert vertex");

    {
        RWLockGetWrite get_lock(part->get_lock());
        part->addVertex(mod.from);
    }
}

void bsp::inc_system::Provider::onInsertEdge(const GraphModification &mod) {
    assert(mod.type == GraphModificationType::ADD_EDGE);
    int from_part_no = part_manager.queryPartitionWorker(mod.from);

    GraphPartition *from_part = find_in_map<int, GraphPartition *>(partitions, from_part_no, NULL);
    if (from_part != NULL) {
        RWLockGetWrite get_lock(from_part->get_lock());
        from_part->addEdge(mod.from, mod.to);
    }
}


void bsp::inc_system::Provider::onInsertReverseEdge(const GraphModification &mod) {
    assert(mod.type == GraphModificationType::ADD_REVERSE_EDGE);
    int to_part_no = part_manager.queryPartitionWorker(mod.to);

    GraphPartition *to_part = find_in_map<int, GraphPartition *>(partitions, to_part_no, NULL);
    if (to_part != NULL) {
        RWLockGetWrite get_lock(to_part->get_lock());
        to_part->addReverseEdge(mod.from, mod.to);
    }
}