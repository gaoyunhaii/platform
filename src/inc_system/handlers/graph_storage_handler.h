//
// Created by owner-pc on 12/26/16.
//

#ifndef BSP_INC_SYSTEM_HANDLER_GRAPH_STORAGE_HANDLER_H
#define BSP_INC_SYSTEM_HANDLER_GRAPH_STORAGE_HANDLER_H

#include "inc_system/algorithm/algorithm.h"
#include <vector>
#include "common/atomic.h"
#include "inc_system/communicate/message.h"
#include "inc_system/communicate/message_handler.h"
#include "inc_system/main/inc_executor.h"
#include "inc_system/graph_input/inc_graph_input.h"

using std::vector;
using namespace bsp::inc_system;
using namespace bsp::inc_system::graph_input;
using bsp::atomic::atomic_incre_re;

namespace bsp {
namespace inc_system {
namespace handlers {

class GraphModificationHandler {
public:
    GraphModificationHandler(IncExecutor &_exe, AlgorithmManager &_am) :
            inc_executor(_exe), manager(_am) {
    }

    bool operator()(RecvMessageWrapper wrapper, BoundedBuffer &bb) {
        Sender &sender = inc_executor.get_sender();
        Provider &provider = inc_executor.get_provider();

        ModificationRecvMessage recv_message;
        recv_message.deserialize(bb, wrapper.size - sizeof(int) - sizeof(size_t));

        std::vector<GraphModification> &mods = *(recv_message.modification);
        vector<GraphModification>::iterator vi = mods.begin();
        for (; vi != mods.end(); ++vi) {
            _TRACEE("on new message, %d, %ld->%d(%lf)\n", vi->type, vi->from, vi->to, vi->weight);
            if (vi->type == GraphModificationType::ADD_VERTEX) {
                provider.onInsertVertex(*vi);
            }
            else if (vi->type == GraphModificationType::ADD_EDGE) {
                provider.onInsertEdge(*vi);
            }
            else if (vi->type == GraphModificationType::ADD_REVERSE_EDGE){
                provider.onInsertReverseEdge(*vi);
            }
            else{
                assert(false && "modification type not supported yet");
            }
        }

        ModifictaionEndMessage end_message;
        end_message.global_id = recv_message.global_id;
        sender.sendRawMessage(&end_message, wrapper.from);

        //then we still need to notify the algorithms to run, including snapshot
        vector<Algorithm *> algorithms = manager.get_algorithms();
        vector<Algorithm *>::iterator avi = algorithms.begin();
        for (; avi != algorithms.end(); ++avi) {
            (*avi)->on_new_graph_modification(recv_message, inc_executor);
        }

        free(wrapper.buffer);
        return false;
    }

private:
    IncExecutor &inc_executor;
    AlgorithmManager &manager;
};

}
}
}

#endif //BSP_INC_SYSTEM_HANDLER_GRAPH_STORAGE_HANDLER_H
