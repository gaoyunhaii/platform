//
// Created by owner-pc on 12/19/16.
//

#ifndef BSP_INC_SYSTEM_HANDLER_NEW_TRANSACTION_HANDLER_HPP
#define BSP_INC_SYSTEM_HANDLER_NEW_TRANSACTION_HANDLER_HPP

#include "common/atomic.h"
#include "inc_system/communicate/message.h"
#include "inc_system/communicate/message_handler.h"
#include "inc_system/main/inc_executor.h"
#include "inc_system/graph_input/inc_graph_input.h"
using namespace bsp::inc_system;
using namespace bsp::inc_system::graph_input;
using bsp::atomic::atomic_incre_re;

namespace bsp{
namespace inc_system{
namespace handlers{

class NewTransactionHandler{
public:
    NewTransactionHandler(IncExecutor& _exe):inc_executor(_exe), next_trans_id(0){

    }

    bool operator()(RecvMessageWrapper wrapper, BoundedBuffer& bb){
        Sender& sender = inc_executor.get_sender();
        NewTransactionMessage income_message;
        income_message.deserialize(bb, wrapper.size);

        NewTransIdMessage new_trans_message;
        new_trans_message.local_id = income_message.local_id;
        new_trans_message.global_id = atomic_incre_re(&next_trans_id, 1);

        _TRACEE("size = %ld, local id = %d-%d, global_id = %d\n", income_message.getSize(), wrapper.from, new_trans_message.local_id, new_trans_message.global_id);

        sender.sendRawMessage(&new_trans_message, wrapper.from);

        free(wrapper.buffer);
        return false;
    }
private:
    IncExecutor& inc_executor;
    int next_trans_id;
};

class NewTransIdHandler{
public:
    NewTransIdHandler(IncExecutor& _ie, TransactionIniter& _ti): inc_executor(_ie), transaction_initor(_ti){

    }

    bool operator()(RecvMessageWrapper wrapper, BoundedBuffer& bb){
        NewTransIdMessage income_message;
        income_message.deserialize(bb, wrapper.size);
        transaction_initor.onTransIdGot(income_message);

        free(wrapper.buffer);
        return false;
    }
private:
    IncExecutor& inc_executor;
    TransactionIniter& transaction_initor;
};



}
}
}


#endif //BSP_INC_SYSTEM_HANDLER_NEW_TRANSACTION_HANDLER_HPP
