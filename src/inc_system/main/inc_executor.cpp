//
// Created by owner-pc on 16-12-15.
//
#include "inc_executor.h"

void bsp::inc_system::IncExecutor::start_bg_services(){
    int comm_size = get_comm_size(comm);
    int rank = get_rank(comm);

    part_manager = new PartitionManager(comm_size, comm_size);

    provider = new Provider(*part_manager, comm_size, rank);

    //here we first init the partitions assigned to us
    provider->init();

    //now we clone a comm for sender and receiver
    MPI_Comm* comms = new MPI_Comm[comm_size];
    for(int i = 0;i < comm_size;++i){
        MPI_Comm_dup(comm, comms + i);
    }

    sender = new Sender(comms, 4, 4);
    sender->start();
    _TRACEE("sender started\n");

    int receiver_worker_size = 2;
    int handler_size = 2;
    handler = new MessageHandler(receiver_worker_size, handler_size);
    handler->start();
    _TRACEE("handler started\n");

    receiver = new Receiver(comms[rank], *handler);
    receiver->start();
    _TRACEE("receiver started\n");
}