/*
 * =====================================================================================
 *
 *       Filename:  main.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/21/2015 11:37:04 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_MAIN_INC
#define  SYSTEM_MAIN_INC

#include "common/hdfs_client.h"
#include "common/configuration.h"
#include "inc_system/runtime_constants.h"

using bsp::common::HDFSClient;
using bsp::common::Configuration;

namespace bsp {
namespace inc_system {

//template<class TypeHinter>
//class DefaultProgram {
//public:
//    typedef typename TypeHinter::MessageValue MV;
//    typedef typename TypeHinter::BufferValue BV;
//    //typedef typename TypeHinter::Provider PROVIDER;
//    typedef Sender <TypeHinter> SENDER;
//
//
//    virtual bool isNotify() {
//        return false;
//    }
//};

//template<class TypeHinter>
//class DefaultTypeHinter {
//public:
//    typedef float VertexValue;
//    typedef float EdgeValue;
//    typedef float BufferValue;
//    typedef float MessageValue;
//    typedef float NotifyValue;
//    typedef DefaultProgram<TypeHinter> Program;
//
//    //typedef bsp::runtime::Provider<TypeHinter> Provider;
//    //typedef bsp::runtime::synchronize::SynExecutor<TypeHinter> Executor;
//};

class Driver {
public:
//    typedef typename TypeHinter::VertexValue VV;
//    typedef typename TypeHinter::EdgeValue EV;
//    //typedef typename TypeHinter::Partitioner PART;
//    //typedef typename TypeHinter::InputClass INPUT;
//    typedef typename TypeHinter::Program PROGRAM;

    Driver(const char *_cf);

    ~Driver();

    //void input_data(INPUT &input);

    Configuration &getConf() {
        return conf;
    }

    HDFSClient& getHdfsClient(){
        return *hdfs_client;
    }

    //void run_program(PROGRAM &program);
private:
    Configuration conf;
    HDFSClient *hdfs_client;

    //PART *part;
};


}
}


#endif   /* ----- #ifndef SYSTEM_MAIN_INC  ----- */
