//
// Created by owner-pc on 16-12-15.
//

#ifndef BSP_INC_SYSTEM_MAIN_INC_EXECUTOR_H
#define BSP_INC_SYSTEM_MAIN_INC_EXECUTOR_H

#include "inc_system/storage/partition_manager.h"
#include "inc_system/storage/provider.h"
#include "inc_system/communicate/sender.h"
#include "inc_system/communicate/receiver.h"
#include "inc_system/communicate/message_handler.h"
#include "common/util.h"

using namespace bsp::inc_system;

namespace bsp{
namespace inc_system{

class IncExecutor{
public:
    IncExecutor(MPI_Comm _comm) : comm(_comm), provider(NULL), sender(NULL), handler(NULL), receiver(NULL){

    }

    PROPERTY_POINTER_READER(PartitionManager, part_manager);
    PROPERTY_POINTER_READER(Provider, provider);
    PROPERTY_POINTER_READER(Sender, sender);
    PROPERTY_POINTER_READER(MessageHandler, handler);
    PROPERTY_POINTER_READER(Receiver, receiver);

    void start_bg_services();

    int inc_get_rank(){
        return bsp::inc_system::get_rank(comm);
    }

    int inc_get_comm_size(){
        return bsp::inc_system::get_comm_size(comm);
    }

private:
    MPI_Comm comm;

    PartitionManager* part_manager;
    Provider* provider;
    Sender* sender;
    MessageHandler* handler;
    Receiver* receiver;
};

}
}



#endif //BSP_INC_SYSTEM_MAIN_INC_EXECUTOR_H
