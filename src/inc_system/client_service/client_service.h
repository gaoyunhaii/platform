//
// Created by owner-pc on 16-11-18.
//

#ifndef INC_SYSTEM_CLIENT_SERVICE_CLIENT_SERVICE_H
#define INC_SYSTEM_CLIENT_SERVICE_CLIENT_SERVICE_H


#include "client/gen-cpp/client_types.h"
#include "client/gen-cpp/ClientService.h"

namespace bsp {
namespace inc_system {

using bsp::inc_system::thrift::client::Transaction;
using bsp::inc_system::thrift::client::ClientServiceIf;

class ClassServiceImpl : public ClientServiceIf{
public:
    virtual ~ClassServiceImpl() { }
    virtual void startTransaction(Transaction& _return){
        //add a new communication pattern

        _return.tid = 0;
    }

    virtual void addVertices(const Transaction& transaction, const std::vector<Vertex> & vertices){

    }

    virtual void addEdges(const Transaction& transaction, const std::vector<Edge> & edges){

    }

    virtual void removeVertices(const Transaction& transaction, const std::vector<Vertex> & vertices){

    }


    virtual void removeEdges(const Transaction& transaction, const std::vector<Edge> & edges){

    }

    virtual void commit(const Transaction& transaction){

    }

    virtual void abort(const Transaction& transaction) {

    }
};


}
}


#endif //INC_SYSTEM_CLIENT_SERVICE__CLIENT_SERVICE_H
