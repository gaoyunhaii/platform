/*
 * =====================================================================================
 *
 *       Filename:  mpi_util.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年11月25日 16时56分00秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  SYSTEM_MPI_UTIL_INC
#define  SYSTEM_MPI_UTIL_INC

#include <mpi.h>

namespace bsp {
namespace inc_system {

inline int get_rank(MPI_Comm &comm) {
    int rank;
    MPI_Comm_rank(comm, &rank);
    return rank;
}

inline int get_comm_size(MPI_Comm &comm) {
    int comm_size;
    MPI_Comm_size(comm, &comm_size);
    return comm_size;
}


}
}


#endif   /* ----- #ifndef SYSTEM_MPI_UTIL_INC  ----- */
