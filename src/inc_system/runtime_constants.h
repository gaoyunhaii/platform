/*
 * =====================================================================================
 *
 *       Filename:  system_constants.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  09/12/2015 09:58:55 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  GaoYun (GY), gaoyunhenhao@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef  SYSTEM_RUNTIME_CONSTANTS_INC
#define  SYSTEM_RUNTIME_CONSTANTS_INC

#include <limits>
#include "common/util.h"

using bsp::common::get_time_us;

#define NONE         "\033[m"
#define RED          "\033[0;32;31m"
#define LIGHT_RED    "\033[1;31m"
#define GREEN        "\033[0;32;32m"
#define LIGHT_GREEN  "\033[1;32m"
#define BLUE         "\033[0;32;34m"
#define LIGHT_BLUE   "\033[1;34m"
#define DARY_GRAY    "\033[1;30m"
#define CYAN         "\033[0;36m"
#define LIGHT_CYAN   "\033[1;36m"
#define PURPLE       "\033[0;35m"
#define LIGHT_PURPLE "\033[1;35m"
#define BROWN        "\033[0;33m"
#define YELLOW       "\033[1;33m"
#define LIGHT_GRAY   "\033[0;37m"
#define WHITE        "\033[1;37m"
#ifdef DEBUG
#define _TRACE(unit, str) cout << __FILE__ << ":" << __LINE__ << "] " << unit << ">" << str << endl << std::flush;
#define _TRACEF(f, ...) printf("%s:%d] ", __FILE__, __LINE__);printf(f, ##__VA_ARGS__);fflush(stdout)
#define _TRACEO(f) printf("%s:%d] ", __FILE__, __LINE__);printf("iam %s from %p to %p\n", f, this, (const char*)this + sizeof(*this));fflush(stdout)
#else
#define _TRACE(unit, str)
#define _TRACEF(f, ...)
#define _TRACEO(f)
#endif

#define _TRACEE(f, ...) printf("%ld %s:%d] ", get_time_us(), __FILE__, __LINE__);printf(f, ##__VA_ARGS__);fflush(stdout)
#define _TRACETE(f, ...) printf("0x%lx-%s:%d] ",pthread_self(), __FILE__, __LINE__);printf(f, ##__VA_ARGS__);fflush(stdout)

typedef long long int VertexId;
const VertexId VERTEX_NOT_EXISTS = std::numeric_limits<VertexId>::max();

namespace bsp {
namespace runtime {

const char *const CONF_KEY_ORDER = "task.order";
const char *const CONF_KEY_HDFS_ADDR = "task.hdfs.addr";
const char *const CONF_KEY_HDFS_PORT = "task.hdfs.port";
const char *const CONF_KEY_TASK_DIR_LOCAL = "task.dir.local";

}
}


#endif   /* ----- #ifndef SYSTEM_RUNTIME_CONSTANTS_INC  ----- */
