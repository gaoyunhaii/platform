#!/usr/bin/env bash

FROM_DIR=$1
TO_DIR=$2

mkdir -p $TO_DIR

if [ -e $TO_DIR ];then
	#clear all files
	find $TO_DIR -maxdepth 1 -type f -name "*.h" -or -name "*.hpp" | 
		while read f;do
			rm $f
		done
fi

find $FROM_DIR -type f -name "*.h" -or -name "*.hpp" | 
	while read f;do
		cp -v $f $TO_DIR/${f##*/}
	done
