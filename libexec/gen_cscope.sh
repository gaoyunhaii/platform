#!/usr/bin/env bash
find . -type f -name "*.h" -o -name "*.hpp" -o -name "*.cpp" > cscope.files
cscope -bq
