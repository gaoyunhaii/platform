#!/usr/bin/env bash

TEMPLATE_DIR=$1

cat <<-EOF
#ifndef SLAVE_TEMPLATES_INC
#define SLAVE_TEMPLATES_INC
namespace bsp{ namespace slave{ namespace constants{

EOF

for f in `ls $TEMPLATE_DIR/*.temp`;do
	cat $f | python $TEMPLATE_DIR/tmp_to_str.py;
	printf "\n"
done

cat <<-EOF
}}}
#endif

EOF

