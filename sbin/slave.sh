#!/usr/bin/env bash

usage="$0 [start | stop]"

if [ $# -lt 1 ];then
	echo $usage;
	exit 1
fi

startstop=$1

. `dirname $0`/bsp-config.sh
PID_FILE=$BSP_TMP_DIR/slave_pid;

if [ "$startstop" = "start" ];then
	if [ -f $PID_FILE ];then
		cat $PID_FILE | xargs -I{} kill -2 {} > /dev/null 2>&1
		echo "BSP slave has started, stop it first";
	fi
	$BSP_HOME/bin/bsp_slave -s $BSP_HOME -d -p $PID_FILE;
	echo "slave has started";
elif [ "$startstop" = "stop" ];then
	if [ -f $PID_FILE ];then
		cat $PID_FILE | xargs -I{} kill -2 {} > /dev/null 2>&1
		echo "BSP slave has stopped";
		rm $PID_FILE
	else
		echo "BSP slave not found";
	fi
fi
