#!/usr/bin/env bash

. `dirname $0`/bsp-config.sh

hostname=`hostname`
printf $HOST_FORMAT $hostname
$BSP_HOME/sbin/master.sh start
printf "Wait 5 seconds to ensure master has start service\n"
printf "Will fixed in future versions\n"
sleep 5;

cat $BSP_HOME/conf/slaves | 
	while read s;do
		printf $HOST_FORMAT $s
		ssh -n $s "$BSP_HOME/sbin/slave.sh start"
	done
printf "BSP cluster has started\n"
