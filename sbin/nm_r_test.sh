#!/usr/bin/env bash

set -x;

[ -e result/result ] && rm result/result;


size=1;
while [ $size -le 70000 ];do 
	[ -e result/result_${size} ] && rm result/result_${size};

	i=0;
	while [ $i -le 50 ];do
		mpiexec -f ~/env/mpi/hostnames -n 2 ../bin/nm_test $size 10000;
		cp /tmp/nm_mpi_test result/result_${size};
		i=$((i + 1));
	done

	printf "%d\t" $size >> result/result;

	cat result/result_${size} | awk 'BEGIN{S1=0;S3=0;S5=0;NL=0;}{S1+=$1;S3+=$3;S5+=$5;NL+=1}END{print S1/NL,S3/NL,S5/NL}' >> result/result;

	size=$((size * 2));
done

set +x;
