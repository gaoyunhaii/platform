#!/usr/bin/env bash

. `dirname $0`/bsp-config.sh

cat $BSP_HOME/conf/slaves | 
	while read s;do
		printf $HOST_FORMAT $s
		ssh -n $s "$BSP_HOME/sbin/slave.sh stop"
	done
#sleep 10
printf $HOST_FORMAT `hostname`
$BSP_HOME/sbin/master.sh stop
