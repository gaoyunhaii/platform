#!/usr/bin/env bash

usage="$0 [start | stop]"

if [ $# -lt 1 ];then
	echo $usage;
	exit 1
fi

startstop=$1

. `dirname $0`/bsp-config.sh
PID_FILE=$BSP_TMP_DIR/master_pid;

if [ "$startstop" = "start" ];then
	if [ -f $PID_FILE ];then
		cat $PID_FILE | xargs -I{} kill -2 {} > /dev/null 2>&1
		echo "BSP master has already started, stop it first";
	fi

	$BSP_HOME/bin/bsp_master -s $BSP_HOME -d -p $PID_FILE;
	echo "master has started";

elif [ "$startstop" = "stop" ];then
	if [ -f $PID_FILE ];then
		cat $PID_FILE | xargs -I{} kill -2 {} > /dev/null 2>&1
		echo "BSP master has stopped";
		rm $PID_FILE
	else
		echo "BSP master not found";
	fi
fi
