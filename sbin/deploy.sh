#!/usr/bin/env bash


BASE_DIR=`dirname $0`/..
BASE_DIR=`cd $BASE_DIR;pwd`

for i in {2..6};do
	scp $BASE_DIR/bin/new_system/* bsp@node${i}:/home/bsp/env/platform/bin/new_system/
done
