#!/usr/bin/env bash

cd `dirname $0`/..

git add .
git commit -m "commit"
git push 52 master

ssh bsp@node2 "cd /home/bsp/env/platform;git pull origin master;cd build;cmake ..;make;cd ../bin/new_system;scpforall.sh pagerank;scpforall.sh sssp"
