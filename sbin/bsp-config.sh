#!/usr/bin/env bash
function join_env(){
	local ORI_ENV=$1
	local TO_ADD=$2

	local RESULT=$(echo "$ORI_ENV:$TO_ADD" |
		awk -F ':' '{
			n = split($0, parts);
			result = ""	
			for(k = 1;k <= n;++k){
				if(parts[k] != "" && to[parts[k]] == ""){
					to[parts[k]] = "1"	
					result = result ":" parts[k]
				}	
			}
			result = substr(result, 2)
			print result
		}')
	echo $RESULT
	return 0
}

#get BSP_HOME 
BSP_PREFIX=$(dirname $BASH_SOURCE)/.. #use $0 is wrong when running script with '. ./bsp-config.sh'
#echo "BSP_PREFIX is $BSP_PREFIX"
START_SLASH=$(echo $BSP_PREFIX | grep "^/")
START_HOME=$(echo $BSP_PREFIX | grep "^~")
if [ -z "$START_SLASH" ];then
	if [ -z "$START_HOME" ];then
		BSP_PREFIX=$PWD/$BSP_PREFIX
	else
		BSP_PREFIX=$HOME/${BSP_PREFIX#~}
	fi
fi
#echo "BSP_PREFIX is $BSP_PREFIX"

export BSP_HOME=$BSP_PREFIX

#add bsp-env to me
CONF_DIR=$BSP_PREFIX/conf
if [ -f $CONF_DIR/bsp-env.sh ];then
	. $CONF_DIR/bsp-env.sh
fi

if [ -f $CONF_DIR/bsp-gen-env.sh ];then
	. $CONF_DIR/bsp-gen-env.sh
fi

#echo "BSP_HOME: " $BSP_HOME

BSP_TMP_DIR=/tmp/bsp-${USER}
mkdir -p $BSP_TMP_DIR


#To locate dependency libraries
MY_LD_LIBRARY_PATH=$BSP_PREFIX/lib
LD_LIBRARY_PATH=$(join_env $LD_LIBRARY_PATH $MY_LD_LIBRARY_PATH)

#For hdfs client to locate dependency jars
MY_CLASSPATH=""
for dir in $HADOOP_JAVA_LIB_DIR;do
	for f in `ls $dir/*.jar 2> /dev/null`;do
		MY_CLASSPATH=$MY_CLASSPATH:$f
	done
	for f in `ls $dir/../*.jar 2> /dev/null`;do
		MY_CLASSPATH=$MY_CLASSPATH:$f
	done
	MY_CLASSPATH=$MY_CLASSPATH:$dir
done

CLASSPATH=$(join_env $CLASSPATH $MY_CLASSPATH)

export LD_LIBRARY_PATH
export CLASSPATH

HOST_FORMAT="=====%s=====\n"
