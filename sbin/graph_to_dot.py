#!/usr/bin/env python

import sys

if __name__ == "__main__":
    print "digraph G{"
    
    start = None
    for line in sys.stdin:
        parts = line.strip().split(":")
        start = parts[0]

        for i in xrange(1, len(parts)):
            if(parts[i].strip() == ""):
                continue

            sub_parts = parts[i].split(",")
            to = sub_parts[0]
            label = sub_parts[1]

            print "\t %s->%s [label=\"%s\"];" % (start, to, label)

    print "}"
