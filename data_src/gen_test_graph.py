#!/usr/bin/evn python
import sys

if __name__ == "__main__":
    nr_vertices = 1000
    if len(sys.argv) > 1:
        nr_vertices = int(sys.argv[1])

    for i in xrange(1, nr_vertices + 1):
        for j in xrange(0, 10): #ten edges
            to_id = (i - 1 + j) % nr_vertices + 1             
            print "%d\t%d\t%f" % (i, to_id, i + to_id)
    pass
