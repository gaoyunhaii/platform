/**
 * Autogenerated by Thrift Compiler (0.9.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#ifndef ClientService_H
#define ClientService_H

#include <thrift/TDispatchProcessor.h>
#include "client_types.h"

namespace bsp { namespace inc_system { namespace thrift { namespace client {

class ClientServiceIf {
 public:
  virtual ~ClientServiceIf() {}
  virtual void startTransaction(Transaction& _return) = 0;
  virtual void addVertices(const Transaction& transaction, const std::vector<Vertex> & vertices) = 0;
  virtual void addEdges(const Transaction& transaction, const std::vector<Edge> & edges) = 0;
  virtual void removeVertices(const Transaction& transaction, const std::vector<Vertex> & vertices) = 0;
  virtual void removeEdges(const Transaction& transaction, const std::vector<Edge> & edges) = 0;
  virtual void commit(const Transaction& transaction) = 0;
  virtual void abort(const Transaction& transaction) = 0;
};

class ClientServiceIfFactory {
 public:
  typedef ClientServiceIf Handler;

  virtual ~ClientServiceIfFactory() {}

  virtual ClientServiceIf* getHandler(const ::apache::thrift::TConnectionInfo& connInfo) = 0;
  virtual void releaseHandler(ClientServiceIf* /* handler */) = 0;
};

class ClientServiceIfSingletonFactory : virtual public ClientServiceIfFactory {
 public:
  ClientServiceIfSingletonFactory(const boost::shared_ptr<ClientServiceIf>& iface) : iface_(iface) {}
  virtual ~ClientServiceIfSingletonFactory() {}

  virtual ClientServiceIf* getHandler(const ::apache::thrift::TConnectionInfo&) {
    return iface_.get();
  }
  virtual void releaseHandler(ClientServiceIf* /* handler */) {}

 protected:
  boost::shared_ptr<ClientServiceIf> iface_;
};

class ClientServiceNull : virtual public ClientServiceIf {
 public:
  virtual ~ClientServiceNull() {}
  void startTransaction(Transaction& /* _return */) {
    return;
  }
  void addVertices(const Transaction& /* transaction */, const std::vector<Vertex> & /* vertices */) {
    return;
  }
  void addEdges(const Transaction& /* transaction */, const std::vector<Edge> & /* edges */) {
    return;
  }
  void removeVertices(const Transaction& /* transaction */, const std::vector<Vertex> & /* vertices */) {
    return;
  }
  void removeEdges(const Transaction& /* transaction */, const std::vector<Edge> & /* edges */) {
    return;
  }
  void commit(const Transaction& /* transaction */) {
    return;
  }
  void abort(const Transaction& /* transaction */) {
    return;
  }
};


class ClientService_startTransaction_args {
 public:

  ClientService_startTransaction_args() {
  }

  virtual ~ClientService_startTransaction_args() throw() {}


  bool operator == (const ClientService_startTransaction_args & /* rhs */) const
  {
    return true;
  }
  bool operator != (const ClientService_startTransaction_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const ClientService_startTransaction_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_startTransaction_pargs {
 public:


  virtual ~ClientService_startTransaction_pargs() throw() {}


  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _ClientService_startTransaction_result__isset {
  _ClientService_startTransaction_result__isset() : success(false) {}
  bool success;
} _ClientService_startTransaction_result__isset;

class ClientService_startTransaction_result {
 public:

  ClientService_startTransaction_result() {
  }

  virtual ~ClientService_startTransaction_result() throw() {}

  Transaction success;

  _ClientService_startTransaction_result__isset __isset;

  void __set_success(const Transaction& val) {
    success = val;
  }

  bool operator == (const ClientService_startTransaction_result & rhs) const
  {
    if (!(success == rhs.success))
      return false;
    return true;
  }
  bool operator != (const ClientService_startTransaction_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const ClientService_startTransaction_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _ClientService_startTransaction_presult__isset {
  _ClientService_startTransaction_presult__isset() : success(false) {}
  bool success;
} _ClientService_startTransaction_presult__isset;

class ClientService_startTransaction_presult {
 public:


  virtual ~ClientService_startTransaction_presult() throw() {}

  Transaction* success;

  _ClientService_startTransaction_presult__isset __isset;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};

typedef struct _ClientService_addVertices_args__isset {
  _ClientService_addVertices_args__isset() : transaction(false), vertices(false) {}
  bool transaction;
  bool vertices;
} _ClientService_addVertices_args__isset;

class ClientService_addVertices_args {
 public:

  ClientService_addVertices_args() {
  }

  virtual ~ClientService_addVertices_args() throw() {}

  Transaction transaction;
  std::vector<Vertex>  vertices;

  _ClientService_addVertices_args__isset __isset;

  void __set_transaction(const Transaction& val) {
    transaction = val;
  }

  void __set_vertices(const std::vector<Vertex> & val) {
    vertices = val;
  }

  bool operator == (const ClientService_addVertices_args & rhs) const
  {
    if (!(transaction == rhs.transaction))
      return false;
    if (!(vertices == rhs.vertices))
      return false;
    return true;
  }
  bool operator != (const ClientService_addVertices_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const ClientService_addVertices_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_addVertices_pargs {
 public:


  virtual ~ClientService_addVertices_pargs() throw() {}

  const Transaction* transaction;
  const std::vector<Vertex> * vertices;

  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_addVertices_result {
 public:

  ClientService_addVertices_result() {
  }

  virtual ~ClientService_addVertices_result() throw() {}


  bool operator == (const ClientService_addVertices_result & /* rhs */) const
  {
    return true;
  }
  bool operator != (const ClientService_addVertices_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const ClientService_addVertices_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_addVertices_presult {
 public:


  virtual ~ClientService_addVertices_presult() throw() {}


  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};

typedef struct _ClientService_addEdges_args__isset {
  _ClientService_addEdges_args__isset() : transaction(false), edges(false) {}
  bool transaction;
  bool edges;
} _ClientService_addEdges_args__isset;

class ClientService_addEdges_args {
 public:

  ClientService_addEdges_args() {
  }

  virtual ~ClientService_addEdges_args() throw() {}

  Transaction transaction;
  std::vector<Edge>  edges;

  _ClientService_addEdges_args__isset __isset;

  void __set_transaction(const Transaction& val) {
    transaction = val;
  }

  void __set_edges(const std::vector<Edge> & val) {
    edges = val;
  }

  bool operator == (const ClientService_addEdges_args & rhs) const
  {
    if (!(transaction == rhs.transaction))
      return false;
    if (!(edges == rhs.edges))
      return false;
    return true;
  }
  bool operator != (const ClientService_addEdges_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const ClientService_addEdges_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_addEdges_pargs {
 public:


  virtual ~ClientService_addEdges_pargs() throw() {}

  const Transaction* transaction;
  const std::vector<Edge> * edges;

  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_addEdges_result {
 public:

  ClientService_addEdges_result() {
  }

  virtual ~ClientService_addEdges_result() throw() {}


  bool operator == (const ClientService_addEdges_result & /* rhs */) const
  {
    return true;
  }
  bool operator != (const ClientService_addEdges_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const ClientService_addEdges_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_addEdges_presult {
 public:


  virtual ~ClientService_addEdges_presult() throw() {}


  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};

typedef struct _ClientService_removeVertices_args__isset {
  _ClientService_removeVertices_args__isset() : transaction(false), vertices(false) {}
  bool transaction;
  bool vertices;
} _ClientService_removeVertices_args__isset;

class ClientService_removeVertices_args {
 public:

  ClientService_removeVertices_args() {
  }

  virtual ~ClientService_removeVertices_args() throw() {}

  Transaction transaction;
  std::vector<Vertex>  vertices;

  _ClientService_removeVertices_args__isset __isset;

  void __set_transaction(const Transaction& val) {
    transaction = val;
  }

  void __set_vertices(const std::vector<Vertex> & val) {
    vertices = val;
  }

  bool operator == (const ClientService_removeVertices_args & rhs) const
  {
    if (!(transaction == rhs.transaction))
      return false;
    if (!(vertices == rhs.vertices))
      return false;
    return true;
  }
  bool operator != (const ClientService_removeVertices_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const ClientService_removeVertices_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_removeVertices_pargs {
 public:


  virtual ~ClientService_removeVertices_pargs() throw() {}

  const Transaction* transaction;
  const std::vector<Vertex> * vertices;

  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_removeVertices_result {
 public:

  ClientService_removeVertices_result() {
  }

  virtual ~ClientService_removeVertices_result() throw() {}


  bool operator == (const ClientService_removeVertices_result & /* rhs */) const
  {
    return true;
  }
  bool operator != (const ClientService_removeVertices_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const ClientService_removeVertices_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_removeVertices_presult {
 public:


  virtual ~ClientService_removeVertices_presult() throw() {}


  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};

typedef struct _ClientService_removeEdges_args__isset {
  _ClientService_removeEdges_args__isset() : transaction(false), edges(false) {}
  bool transaction;
  bool edges;
} _ClientService_removeEdges_args__isset;

class ClientService_removeEdges_args {
 public:

  ClientService_removeEdges_args() {
  }

  virtual ~ClientService_removeEdges_args() throw() {}

  Transaction transaction;
  std::vector<Edge>  edges;

  _ClientService_removeEdges_args__isset __isset;

  void __set_transaction(const Transaction& val) {
    transaction = val;
  }

  void __set_edges(const std::vector<Edge> & val) {
    edges = val;
  }

  bool operator == (const ClientService_removeEdges_args & rhs) const
  {
    if (!(transaction == rhs.transaction))
      return false;
    if (!(edges == rhs.edges))
      return false;
    return true;
  }
  bool operator != (const ClientService_removeEdges_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const ClientService_removeEdges_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_removeEdges_pargs {
 public:


  virtual ~ClientService_removeEdges_pargs() throw() {}

  const Transaction* transaction;
  const std::vector<Edge> * edges;

  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_removeEdges_result {
 public:

  ClientService_removeEdges_result() {
  }

  virtual ~ClientService_removeEdges_result() throw() {}


  bool operator == (const ClientService_removeEdges_result & /* rhs */) const
  {
    return true;
  }
  bool operator != (const ClientService_removeEdges_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const ClientService_removeEdges_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_removeEdges_presult {
 public:


  virtual ~ClientService_removeEdges_presult() throw() {}


  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};

typedef struct _ClientService_commit_args__isset {
  _ClientService_commit_args__isset() : transaction(false) {}
  bool transaction;
} _ClientService_commit_args__isset;

class ClientService_commit_args {
 public:

  ClientService_commit_args() {
  }

  virtual ~ClientService_commit_args() throw() {}

  Transaction transaction;

  _ClientService_commit_args__isset __isset;

  void __set_transaction(const Transaction& val) {
    transaction = val;
  }

  bool operator == (const ClientService_commit_args & rhs) const
  {
    if (!(transaction == rhs.transaction))
      return false;
    return true;
  }
  bool operator != (const ClientService_commit_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const ClientService_commit_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_commit_pargs {
 public:


  virtual ~ClientService_commit_pargs() throw() {}

  const Transaction* transaction;

  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_commit_result {
 public:

  ClientService_commit_result() {
  }

  virtual ~ClientService_commit_result() throw() {}


  bool operator == (const ClientService_commit_result & /* rhs */) const
  {
    return true;
  }
  bool operator != (const ClientService_commit_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const ClientService_commit_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_commit_presult {
 public:


  virtual ~ClientService_commit_presult() throw() {}


  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};

typedef struct _ClientService_abort_args__isset {
  _ClientService_abort_args__isset() : transaction(false) {}
  bool transaction;
} _ClientService_abort_args__isset;

class ClientService_abort_args {
 public:

  ClientService_abort_args() {
  }

  virtual ~ClientService_abort_args() throw() {}

  Transaction transaction;

  _ClientService_abort_args__isset __isset;

  void __set_transaction(const Transaction& val) {
    transaction = val;
  }

  bool operator == (const ClientService_abort_args & rhs) const
  {
    if (!(transaction == rhs.transaction))
      return false;
    return true;
  }
  bool operator != (const ClientService_abort_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const ClientService_abort_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_abort_pargs {
 public:


  virtual ~ClientService_abort_pargs() throw() {}

  const Transaction* transaction;

  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_abort_result {
 public:

  ClientService_abort_result() {
  }

  virtual ~ClientService_abort_result() throw() {}


  bool operator == (const ClientService_abort_result & /* rhs */) const
  {
    return true;
  }
  bool operator != (const ClientService_abort_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const ClientService_abort_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class ClientService_abort_presult {
 public:


  virtual ~ClientService_abort_presult() throw() {}


  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};

class ClientServiceClient : virtual public ClientServiceIf {
 public:
  ClientServiceClient(boost::shared_ptr< ::apache::thrift::protocol::TProtocol> prot) :
    piprot_(prot),
    poprot_(prot) {
    iprot_ = prot.get();
    oprot_ = prot.get();
  }
  ClientServiceClient(boost::shared_ptr< ::apache::thrift::protocol::TProtocol> iprot, boost::shared_ptr< ::apache::thrift::protocol::TProtocol> oprot) :
    piprot_(iprot),
    poprot_(oprot) {
    iprot_ = iprot.get();
    oprot_ = oprot.get();
  }
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> getInputProtocol() {
    return piprot_;
  }
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> getOutputProtocol() {
    return poprot_;
  }
  void startTransaction(Transaction& _return);
  void send_startTransaction();
  void recv_startTransaction(Transaction& _return);
  void addVertices(const Transaction& transaction, const std::vector<Vertex> & vertices);
  void send_addVertices(const Transaction& transaction, const std::vector<Vertex> & vertices);
  void recv_addVertices();
  void addEdges(const Transaction& transaction, const std::vector<Edge> & edges);
  void send_addEdges(const Transaction& transaction, const std::vector<Edge> & edges);
  void recv_addEdges();
  void removeVertices(const Transaction& transaction, const std::vector<Vertex> & vertices);
  void send_removeVertices(const Transaction& transaction, const std::vector<Vertex> & vertices);
  void recv_removeVertices();
  void removeEdges(const Transaction& transaction, const std::vector<Edge> & edges);
  void send_removeEdges(const Transaction& transaction, const std::vector<Edge> & edges);
  void recv_removeEdges();
  void commit(const Transaction& transaction);
  void send_commit(const Transaction& transaction);
  void recv_commit();
  void abort(const Transaction& transaction);
  void send_abort(const Transaction& transaction);
  void recv_abort();
 protected:
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> piprot_;
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> poprot_;
  ::apache::thrift::protocol::TProtocol* iprot_;
  ::apache::thrift::protocol::TProtocol* oprot_;
};

class ClientServiceProcessor : public ::apache::thrift::TDispatchProcessor {
 protected:
  boost::shared_ptr<ClientServiceIf> iface_;
  virtual bool dispatchCall(::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, const std::string& fname, int32_t seqid, void* callContext);
 private:
  typedef  void (ClientServiceProcessor::*ProcessFunction)(int32_t, ::apache::thrift::protocol::TProtocol*, ::apache::thrift::protocol::TProtocol*, void*);
  typedef std::map<std::string, ProcessFunction> ProcessMap;
  ProcessMap processMap_;
  void process_startTransaction(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_addVertices(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_addEdges(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_removeVertices(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_removeEdges(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_commit(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
  void process_abort(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
 public:
  ClientServiceProcessor(boost::shared_ptr<ClientServiceIf> iface) :
    iface_(iface) {
    processMap_["startTransaction"] = &ClientServiceProcessor::process_startTransaction;
    processMap_["addVertices"] = &ClientServiceProcessor::process_addVertices;
    processMap_["addEdges"] = &ClientServiceProcessor::process_addEdges;
    processMap_["removeVertices"] = &ClientServiceProcessor::process_removeVertices;
    processMap_["removeEdges"] = &ClientServiceProcessor::process_removeEdges;
    processMap_["commit"] = &ClientServiceProcessor::process_commit;
    processMap_["abort"] = &ClientServiceProcessor::process_abort;
  }

  virtual ~ClientServiceProcessor() {}
};

class ClientServiceProcessorFactory : public ::apache::thrift::TProcessorFactory {
 public:
  ClientServiceProcessorFactory(const ::boost::shared_ptr< ClientServiceIfFactory >& handlerFactory) :
      handlerFactory_(handlerFactory) {}

  ::boost::shared_ptr< ::apache::thrift::TProcessor > getProcessor(const ::apache::thrift::TConnectionInfo& connInfo);

 protected:
  ::boost::shared_ptr< ClientServiceIfFactory > handlerFactory_;
};

class ClientServiceMultiface : virtual public ClientServiceIf {
 public:
  ClientServiceMultiface(std::vector<boost::shared_ptr<ClientServiceIf> >& ifaces) : ifaces_(ifaces) {
  }
  virtual ~ClientServiceMultiface() {}
 protected:
  std::vector<boost::shared_ptr<ClientServiceIf> > ifaces_;
  ClientServiceMultiface() {}
  void add(boost::shared_ptr<ClientServiceIf> iface) {
    ifaces_.push_back(iface);
  }
 public:
  void startTransaction(Transaction& _return) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->startTransaction(_return);
    }
    ifaces_[i]->startTransaction(_return);
    return;
  }

  void addVertices(const Transaction& transaction, const std::vector<Vertex> & vertices) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->addVertices(transaction, vertices);
    }
    ifaces_[i]->addVertices(transaction, vertices);
  }

  void addEdges(const Transaction& transaction, const std::vector<Edge> & edges) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->addEdges(transaction, edges);
    }
    ifaces_[i]->addEdges(transaction, edges);
  }

  void removeVertices(const Transaction& transaction, const std::vector<Vertex> & vertices) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->removeVertices(transaction, vertices);
    }
    ifaces_[i]->removeVertices(transaction, vertices);
  }

  void removeEdges(const Transaction& transaction, const std::vector<Edge> & edges) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->removeEdges(transaction, edges);
    }
    ifaces_[i]->removeEdges(transaction, edges);
  }

  void commit(const Transaction& transaction) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->commit(transaction);
    }
    ifaces_[i]->commit(transaction);
  }

  void abort(const Transaction& transaction) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->abort(transaction);
    }
    ifaces_[i]->abort(transaction);
  }

};

}}}} // namespace

#endif
