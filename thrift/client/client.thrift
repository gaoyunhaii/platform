namespace cpp bsp.inc_system.thrift.client

struct Vertex{
	1: i64 vertexId,
	2: binary vertexValue
}

struct Edge{
	1: i64 srcVertexId,
	2: i64 destVertexid,
	3: binary edgeValue		
}

struct Transaction{
	1: i64 tid
}

service ClientService{
	Transaction startTransaction(),
	void addVertices(1: Transaction transaction,  2: list<Vertex> vertices),
	void addEdges(1: Transaction transaction, 2: list<Edge> edges),
	void removeVertices(1: Transaction transaction, 2: list<Vertex> vertices),
	void removeEdges(1: Transaction transaction, 2: list<Edge> edges),
	void commit(1: Transaction transaction),
	void abort(1: Transaction transaction)
}
