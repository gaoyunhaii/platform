#!/usr/bin/env bash

BASE=`dirname $0`;
dir=$1;
if [ "x$dir" != "x" ] && [ -d $BASE/$dir ];then
	cd $BASE/$dir;
	rm -rf ./gen-cpp;
	thrift -r --gen cpp *.thrift;
	cd gen-cpp;
	find . -name "*.skeleton.cpp" | xargs rm 
else
	printf "$BASE/$dir has nothing to do with thrift\n"
fi
