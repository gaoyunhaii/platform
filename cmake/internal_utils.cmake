#utils for cmake 
function(ADD_DIRECTORY_TO_TEST dir)
	AUX_SOURCE_DIRECTORY(${dir} UTEST)

	SET(UTEST_SRC
		${UTEST}
		${PROJECT_SOURCE_DIR}/test_src/gtest_main.cpp
	)

	ADD_EXECUTABLE(${dir}_utest ${UTEST_SRC})
	add_dependencies(${dir}_utest ${dir}_lib)
	TARGET_LINK_LIBRARIES(${dir}_utest libgtest.a bsp_${dir} pthread log4cplus xml2 rt hdfs thrift thriftnb event)
	SET_TARGET_PROPERTIES(${dir}_utest PROPERTIES COMPILE_FLAGS ${SYSTEM_COMPILE_FLAGS})
endfunction()

macro(THRIFT_SOURCE _name _Name _variable)
	set(${_variable})
	list(APPEND ${_variable} ${PROJECT_SOURCE_DIR}/thrift/${_name}/gen-cpp/${_name}_constants.h)
	list(APPEND ${_variable} ${PROJECT_SOURCE_DIR}/thrift/${_name}/gen-cpp/${_name}_constants.cpp)
	list(APPEND ${_variable} ${PROJECT_SOURCE_DIR}/thrift/${_name}/gen-cpp/${_name}_types.h)
	list(APPEND ${_variable} ${PROJECT_SOURCE_DIR}/thrift/${_name}/gen-cpp/${_name}_types.cpp)
	list(APPEND ${_variable} ${PROJECT_SOURCE_DIR}/thrift/${_name}/gen-cpp/${_Name}.h)
	list(APPEND ${_variable} ${PROJECT_SOURCE_DIR}/thrift/${_name}/gen-cpp/${_Name}.cpp)
	#message(${${_variable}})

	if(NOT EXISTS ${PROJECT_SOURCE_DIR}/thrift/${_name}/gen-cpp/${_Name}.h)
		message("create initial thrift file for ${_name}")
		EXECUTE_PROCESS(
			COMMAND bash ${PROJECT_SOURCE_DIR}/thrift/gen_thrift.sh ${_name}
		)
	endif()
endmacro()

macro (ADD_THRIFT_COMMAND _name _output)
	#message("output: " ${_output})
	message("source dir" ${PROJECT_SOURCE_DIR})
	add_custom_command(
		OUTPUT ${_output}
		COMMAND bash ${PROJECT_SOURCE_DIR}/thrift/gen_thrift.sh ${_name}
		DEPENDS ${PROJECT_SOURCE_DIR}/thrift/${_name}/${_name}.thrift)
	
	add_custom_target(${_name}_thrift_target 
		DEPENDS ${_output})

	add_custom_command(TARGET ${_name}_thrift_target 
		POST_BUILD
		COMMAND bash ${PROJECT_SOURCE_DIR}/libexec/gen_dist_headers.sh ${PROJECT_SOURCE_DIR}/thrift/${_name}/gen-cpp ${PROJECT_SOURCE_DIR}/include/${_name}/gen-cpp
	)	
endmacro()


macro (ADD_ALGORITHM _name _files)
	MESSAGE(${SYSTEM_COMPILE_FLAGS})
	ADD_EXECUTABLE(${_name} ${_files})
	SET_TARGET_PROPERTIES(${_name} PROPERTIES COMPILE_FLAGS "${SYSTEM_COMPILE_FLAGS}")
	target_link_libraries(${_name} hdfs xml2 log4cplus tcmalloc profiler unwind leveldb)
endmacro()
